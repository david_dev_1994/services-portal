<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$route['default_controller'] = 'general';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

// ----------------AUTH PART--------------------
//Auth UI

//$route['sign-in'] = 'Auth/signIn';
//$route['sign-up'] = 'Auth/signUp';

//Auth Action
//$route['login'] = "Auth/login";
//$route['register'] = "Auth/register";

//Florida Authentication Routes
$route['flo_signIn'] = 'Auth/flo_signIn';     //displays signin page
$route['flo_login'] = "Auth/flo_login";        //authenticates florida user
$route['flo_register'] = "Auth/flo_register"; //registers florida user
$route['flo_sign-up'] = 'Auth/flo_signUp'; //displays sign-up page
$route['logout'] = "Auth/logout";         //florida logout
$route['flfs_forgot_password']='Auth/flfs_forgot_password';
$route['flo_resetpass'] = 'Auth/flo_resetpass';
//$route['flo_resetpass/(:any)'] = 'Auth/flo_resetpass';
$route['flo_resetpass/(.+)'] = 'Auth/flo_resetpass/$1';


//Calfire Authentication Routes
$route['cal_signIn'] = 'Auth/cal_signIn'; //displays signin page
$route['cal_login'] = "Auth/cal_login";          //authenticates florida user
$route['cal_register'] = "Auth/cal_register"; //registers florida user
$route['cal_sign-up'] = 'Auth/cal_signUp'; //displays sign-up page
$route['cal_logout'] = "Auth/cal_logout";
$route['calfire_forgot_password']='Auth/cal_forgot_password';
$route['cal_resetpass'] = 'Auth/cal_resetpass';
//$route['flo_resetpass/(:any)'] = 'Auth/flo_resetpass';
$route['cal_resetpass/(.+)'] = 'Auth/cal_resetpass/$1';


//USFS Authentication Routes (19-11-18)
$route['usfs_signIn'] = 'Auth/usfs_signIn'; //displays signin page
$route['usfs_login'] = "Auth/usfs_login";          //authenticates florida user
$route['usfs_register'] = "Auth/usfs_register"; //registers florida user
$route['usfs_sign-up'] = 'Auth/usfs_signUp'; //displays sign-up page
$route['usfs_logout'] = "Auth/usfs_logout";
$route['usfs_forgot_password']='Auth/usfs_forgot_password';
$route['usfs_resetpass'] = 'Auth/usfs_resetpass';
//$route['usfs_resetpass/(:any)'] = 'Auth/usfs_resetpass';
$route['usfs_resetpass/(.+)'] = 'Auth/usfs_resetpass/$1';


//Alberta Authentication Routes
$route['alb_signIn'] = 'Auth/alb_signIn'; //displays signin page
$route['alb_login'] = "Auth/alb_login";          //authenticates alberta user
$route['alb_register'] = "Auth/alb_register"; //registers alberta user
$route['alb_sign-up'] = 'Auth/alb_signUp'; //displays sign-up page for alberta
$route['alb_logout'] = "Auth/alb_logout"; //logging out alberta
$route['alberta_forgot_password']='Auth/alb_forgot_password';
$route['alb_resetpass'] = 'Auth/alb_resetpass';
$route['alb_resetpass/(.+)'] = 'Auth/alb_resetpass/$1';

$route['Esn-spread-sheet']  = "Users/Esn_spread_sheet";
$route['download_codeplug'] = "Users/download_codeplug";
$route['codeplug'] = "Users/codeplug";

//Alberta home
$route['Alberta'] = 'Users/default_ahs';



// LOGGED USER PAGES
//$route['my-page'] = "Users/default_ahs";
//$route['default-ahs']="Users/default_ahs";
//$route['download-manuals']="Users/download_codeplug";
//$route['Esn-spread-sheet']="Users/Esn_spread_sheet";
//$route['download-codeplug']="Users/download_codeplug";
//$route['upload-codeplug']="Users/upload_codeplug";
//$route['User/CalFire'] = "Users/CalFire"; //download page for calfire after login
//COMMON PAGES

// factory service
$route['factory-service'] = "General/factory_service";
$route['factory-service/pricing'] = "General/pricing";
$route['factory-service/service-dealers'] = "General/commonPages";
$route['factory-service/warranty'] = "General/warranty";
$route['parts/Parts_Distributor'] = "General/commonPages";


// Agency Portals
$route['agency_portals'] = "General/agency_portals";

// Return Merchandise Authorization
$route['return-merchandise-authorization'] = "General/return_merchandise_authorization";

//parts
$route['parts'] = "General/parts";
$route['parts/type/(:any)'] = "General/view_parts_type";
$route['parts/list/(:any)'] = "General/view_parts_list";


//products
$route['products'] = "General/commonPages";
$route['products/list'] = "General/list1";
#$route['products/RELM-Bulletins'] = "General/RELM_Bulletins"; //change#3 remove link and page
$route['products/Archived_Bulletins'] = "General/Archived_Bulletins"; //change#3 remove link and page
$route['products/Bulletins-search'] = "General/Bulletins_search";
#$route['products/All-Bulletins'] = "General/All_Bulletins";
$route['products/discontinued'] = "General/discontinued";
$route['products/details'] = "General/details";
$route['products/BK_KAA0733'] = "General/BK_KAA0733";
$route['products/BK_KAA0732'] = "General/BK_KAA0732";
$route['products/BK_KAA0730'] = "General/BK_KAA0730";
$route['products/BK-analog-radios'] = "General/BK_analog_radios";
$route['products/RELM-analog-radios'] = "General/RELM_analog_radios";
$route['products/All-Radios'] = "General/All_Radios";
$route['products/Discontinued-Radios'] = "General/Discontinued_Radios";
$route['products/BK_Digital'] = "General/BK_KAA0732";

//manulas
$route['Manuals/RELM-Manuals'] = "General/RELM_Manuals";
$route['Manuals/BK-Manuals'] = "General/bk_Manuals";
$route['Manuals/All-Manuals'] = "General/all_Manuals";
$route['Manuals/Discontinued-Manuals'] = "General/discontinued_Manuals";
$route['Manuals/search-Manuals'] = "General/RELM_Manuals";

$route['Kaa0733registration'] = "General/Kaa0733registration";
$route['Kaa0730registration'] = "General/Kaa0730registration";

//bulletins 
$route['bulletins'] = "General/Bulletins_search";
$route['bulletins/bk-bulletins'] = "General/list2";

//contact
#$route['contact'] = "General/commonPages";
$route['contact'] = "General/contact";
$route['contact/email-contact'] = "General/commonPages";
$route['contact/List-Request'] = "General/commonPages";


//forums
$route['forums'] = "General/commonPages";
$route['forums/BK-Forum'] = "General/BK_Forum";
$route['forums/BK-Forum/(:any)'] = "General/BK_Forum";
$route['forums/RELM-Forum'] = "General/RELM_Forum";
$route['forums/RELM-Forum/(:any)'] = "General/RELM_Forum";
$route['forums/profile/(:any)'] = "General/profile_Forum";
$route['forums/profile-comment/(:any)'] = "General/profile_comment_Forum";
$route['forums/add-forum/(:any)'] = "General/add_forum";


//CalFire
$route['CalFire'] = "General/CalFire";
$route['CalFire/request'] = "General/CalFire_request";
$route['CalFire/showall'] = "General/CalFire_showall";
$route['CalFire/edit/(:any)'] = "General/CalFire_view";
$route['CalFire/delete/(:any)'] = "General/CalFire_delete";
$route['CalFire_update'] = "General/CalFire_update";

//common download page calfire and florida (access-able after login)
$route['restricted'] = 'General/CalFire_restricted';


//All Softwares at one place
$route['software/update'] = "General/update_new";
$route['software/update/(:any)'] = "General/update_new/$1";

//software for KAA0733
#$route['software/update'] = "General/update";
//Software for KAA0732
$route['software/update2'] = "General/update2";
//Software for KAA0730
$route['software/update3'] = "General/update3";


//admin
$route['admin'] = "admin/Admin";


//point 1 Support/Technical Documentation
$route['tech_view'] = "General/tech_view";

//register user
$route['register1'] = "`hboard/register_user";

//Florida Forestory pages
$route['flfs'] = 'General/Florida';
$route['f_restricted'] = 'General/Florida_restricted';
$route['f_showall'] = "General/Florida_showall";
$route['f_request'] = "General/Florida_request";
$route['f_edit/(:any)'] = "General/Florida_view";
$route['f_delete/(:any)'] = "General/Florida_delete";


//USFS pages
$route['usfs'] = 'General/Usfs';
$route['usfs_restricted'] = 'General/Usfs_restricted';
$route['usfs_showall'] = "General/Usfs_showall";
$route['usfs_request'] = "General/Usfs_request";
$route['usfs_edit/(:any)'] = "General/Usfs_view";
$route['usfs_delete/(:any)'] = "General/Usfs_delete";


