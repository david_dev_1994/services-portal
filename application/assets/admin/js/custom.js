
// if ($('#download').length) {
//     // alert();
//     $('#download').DataTable({
//         ordering:false,
//         columns: [
//             { "width": "3px"}
//         ]
//     });
// }

if ($('#datatableId').length) {
    $('#datatableId').DataTable({
        initComplete: function () {
            this.api().columns().every( function () {
                var column = this;
                if (column.index() == 4) {  //skip if column 0
                    var select = $('<select><option value=""></option></select>')
                        .appendTo($(column.footer()).empty())
                        .on('change', function () {
                            var val = $.fn.dataTable.util.escapeRegex(
                                $(this).val()
                            );

                            column
                                .search(val ? '^' + val + '$' : '', true, false)
                                .draw();
                        });

                    column.data().unique().sort().each(function (d, j) {
                        select.append('<option value="' + d + '">' + d + '</option>')
                    });
                }
            });
        }
    });
}

if ($('#download').length) {
    $('#download').DataTable({
        "aaSorting": [],
        initComplete: function () {
            this.api().columns().every( function () {
                var column = this;
                // if (column.index()) {  //skip if column 0
                var select = $('<select style="width: 100% !important;"><option value=""></option></select>')
                    .appendTo($(column.footer()).empty())
                    .on('change', function () {
                        var val = $.fn.dataTable.util.escapeRegex(
                            $(this).val()
                        );

                        column
                            .search(val ? '^' + val + '$' : '', true, false)
                            .draw();
                    });

                column.data().unique().sort().each(function (d, j) {
                    select.append('<option value="' + strip_tag(d) + '">' + d + '</option>')
                });
                // }
            });
        }
    });
}

function strip_tag(str) {
    str = str.replace(/<{1}[^<>]{1,}>{1}/g, "");
    return str;
}

