$(document).ready(function () {

    $('#serial_number').mask('000-000-000',{placeholder: "000-000-000"});

    $('#select-Product').on('show.bs.modal', function (e) {
        // $(this).find('.revisionNumberDiv').html($(e.relatedTarget).data('revision'));
        // $(this).find('.editorDiv').html($(e.relatedTarget).data('editor'));

        $(this).find('.revisionNumberInp').attr('value', $(e.relatedTarget).data('revision'));
        $(this).find('.editorInp').attr('value', $(e.relatedTarget).data('editor'));
        $(this).find('.fileName').attr('value', $(e.relatedTarget).data('file_name'));
    });


    $('#firmFormSubmit').on('submit', function (e) {
        // alert();
        e.preventDefault();
        var form_data = new FormData(this);
        $.ajax({
            url: base_url+'general/firmFormSubmit', // point to server-side controller method
            dataType: 'json', // what to expect back from the server
            cache: false,
            contentType: false,
            processData: false,
            data: form_data,
            type: 'post',
            success: function (response) {
                if (response.status == 200) {
                    // $("#btnCloseModal").trigger("click");

                    $("#select-Product").modal('hide');
                    $('body').removeClass('modal-open');
                    $(".modal-backdrop").remove();

                    var a = document.createElement('a');
                    a.href = base_url+'assets/files/firmware/'+response.file;
                    window.open(a.href);
                    // a.href = base_url+'assets/files/firmware/';
                    // a.download = response.file;
                    // a.click();
                } else {
                    $('.errorToModal').html(response.message)
                    $(".errorToModal").removeClass('hidden');
                    $(".errorToModal").addClass('show');
                    alert();
                    setTimeout(function(){
                        $(".errorToModal").html('');
                        $(".errorToModal").addClass('hidden');
                        $(".errorToModal").fadeOut(500);
                    }, 5000);
                    console.log(response.message);
                }
            }
        });
    });

    $('#softFormSubmit').on('submit', function (e) {
        // alert();
        e.preventDefault();
        var form_data = new FormData(this);
        $.ajax({
            url: base_url+'general/softFormSubmit', // point to server-side controller method
            dataType: 'json', // what to expect back from the server
            cache: false,
            contentType: false,
            processData: false,
            data: form_data,
            type: 'post',
            success: function (response) {
                if (response.status == 200) {
                    // $("#btnCloseModal").trigger("click");

                    $("#select-Product").modal('hide');
                    $('body').removeClass('modal-open');
                    $(".modal-backdrop").remove();

                    var a = document.createElement('a');
                    a.href = base_url+'assets/files/software/';
                    window.open(a+response.file);
                    // a.download = response.file;
                    // a.open();
                } else {
                    $('.errorToModal').html(response.message)
                    $(".errorToModal").removeClass('hidden');
                    $(".errorToModal").addClass('show');
                    setTimeout(function(){
                        $(".errorToModal").html('');
                        $(".errorToModal").addClass('hidden');
                        $(".errorToModal").fadeOut(500);
                    }, 5000);
                    console.log(response.message);
                }
            }
        });
    });

    $('#select-Product').on('hidden.bs.modal', function () {
        $(this).find('form').trigger('reset');
    })

});