<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Yield  ::  HOOKS
 *
 * Adds layout support :: Similar to RoR <%= yield =>
 * '{yield}' will be replaced with all output generated by the controller/view.
 */
class Yeild {

    function doYeild() {
        global $OUT;
		$view='';
        $CI = & get_instance();
        $output = $CI->output->get_output();
        //$in_canvas = $CI->config->item('in_canvas');
        $default = APPPATH . 'views/layouts/admin/default.php'; //florida and calfire
        $default2 = APPPATH . 'views/layouts/alberta/default.php'; //alberta

        if (isset($CI->layout) && $CI->layout=='admin/default') {
            if (!preg_match('/(.+).php$/', $CI->layout)) {
                $CI->layout .= '.php';

                $requested = APPPATH . 'views/layouts/' . $CI->layout;

                if (file_exists($requested)) {
                    $layout = $CI->load->file($requested, true);
//                $view = str_replace("{{SITE URL}}",		site_url(),str_replace("{yield}", $output, $layout)); //look for yield in layout and replace with output
                    $view = str_replace("{yield}", $output, $layout); //look for yield in layout and replace with output
                }
            } else if (file_exists($default)) {
                $layout = $CI->load->file($default, true);
//            $view = str_replace("{{SITE URL}}",		site_url(),str_replace("{yield}", $output, $layout));
                $view = str_replace("{yield}", $output, $layout);
            }
        }else if(isset($CI->layout) && $CI->layout=='alberta/default'){

            if (!preg_match('/(.+).php$/', $CI->layout)) {
                $CI->layout .= '.php';

                $requested = APPPATH . 'views/layouts/' . $CI->layout;

                if (file_exists($requested)) {
                    $layout = $CI->load->file($requested, true);
//                $view = str_replace("{{SITE URL}}",		site_url(),str_replace("{yield}", $output, $layout)); //look for yield in layout and replace with output
                    $view = str_replace("{yield}", $output, $layout); //look for yield in layout and replace with output
                }
            } else if (file_exists($default2)) {
                $layout = $CI->load->file($default2, true);
//            $view = str_replace("{{SITE URL}}",		site_url(),str_replace("{yield}", $output, $layout));
                $view = str_replace("{yield}", $output, $layout);
            }

        } else {
            $view = $output;
        }
        $OUT->_display($view); //explicit call to display method to show result

    }

//    function doYeild() {
//        global $OUT;
//		$view='';
//        $CI = & get_instance();
//        $output = $CI->output->get_output();
//        //$in_canvas = $CI->config->item('in_canvas');
//        $default = APPPATH . 'views/layouts/admin/default.php';
//
//        if (isset($CI->layout)) {
//            if (!preg_match('/(.+).php$/', $CI->layout)) {
//                $CI->layout .= '.php';
//            }
//            $requested = APPPATH . 'views/layouts/' . $CI->layout;
//
//            if (file_exists($requested)) {
//                $layout = $CI->load->file($requested, true);
////                $view = str_replace("{{SITE URL}}",		site_url(),str_replace("{yield}", $output, $layout)); //look for yield in layout and replace with output
//                $view = str_replace("{yield}", $output, $layout); //look for yield in layout and replace with output
//            }
//        } else if (file_exists($default)) {
//            $layout = $CI->load->file($default, true);
////            $view = str_replace("{{SITE URL}}",		site_url(),str_replace("{yield}", $output, $layout));
//            $view = str_replace("{yield}", $output, $layout);
//        } else {
//            $view = $output;
//        }
//        $OUT->_display($view); //explicit call to display method to show result
//    }

}

?>
