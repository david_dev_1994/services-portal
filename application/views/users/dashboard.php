<section id="my_page-content">
    <div class="my_page_container">
        <div class="container" >
           <div style="margin-top:50px">
            <div class='col-md-6'>
                <img src='<?php echo base_url(); ?>assets/bk-technologies-relm-logo.jpg' class='img img-resonsive'>
            </div>
            <div class='col-md-6'>
                <img src='<?php echo base_url(); ?>assets/logo.jpg' class='img img-resonsive'>
            </div>
          </div>
        </div>
<div class='container'>
   <div class='row'>
      <div class="col-md-12">
         <center><h1 class='text text-danger'>Welcome to the Alberta Health Services BK Radio Web Portal</h1>
          <p class='text '>Use the links at the header to navigate through this portal</p>
          <br>
           <h2>For all orders, <a href='mailto:progell@bktechnologies.com'>click here</a>,, or contact:</h2>
           <h4 class='text text-danger'>Pete Rogell</h4>
            <h4 class='text text-danger'>V.P. of International Sales</h4>
            <h4 class='text text-danger'>321-953-7809</h4>
            <h4 class='text text-danger'>321-431-6208 Mobile</h4>
            <a href='mailto:progell@bktechnologies.com'>progell@bktechnologies.com</a>
</center>
      </div>
   </div>
 </div>
 <div class='container'>
   <div class='row'>
      <div class="col-md-6">
         <center>
             <h2>BK Technology Wireless, BK Radio Manufacturer Contacts:</h2>
             <h4 class="text text-danger">Techical Support/Customer Service 1-800-422-6281</h4>
             <a href="mailto:mobrien@bktechnologies.com">mobrien@bktechnologies.com</a>
<br><br>
             <h4 class="text text-danger">Jim Holthaus CTO 402-990-1551</h4>
             <a href="mailto:jholthaus@bktechnologies.com">jholthaus@bktechnologies.com</a>
<br><br>
             <h4 class="text text-danger">Cindy Kippley Progam Manager 321-953-7989</h4>
             <a href="mailto:ckippley@bktechnologies.com">ckippley@bktechnologies.com</a>
         </center>
      </div>
      <div class="col-md-6">
         <center>
             
             <h2>Alberta Health Services Radio  Communications Contacts:</h2>
             <h4 class="text text-danger">Alfred Klein  Director, EMS Dispatch Southern Communication Centre Alberta Health Services 403.612.8715</h4>
             <a href="mailto:alfred.klein@ahs.ca">alfred.klein@ahs.ca</a>
<br><br>
             <h4 class="text text-danger">Dawson Elliott Dispatch Communications Support Specialist AHS EMS IT 403.860.3816</h4>
             <a href="mailto:Dawson.Elliott@albertahealthservices.ca">Dawson.Elliott@albertahealthservices.ca</a>
<br><br>
             <h4 class="text text-danger">Doug MacKintosh Dispatch Communications Support Specialist AHS EMS IT 780.407.4480</h4>
             <a href="mailto:Douglas.MacKintosh@albertahealthservices.ca">Douglas.MacKintosh@albertahealthservices.ca</a>
             <br><br>
             <a href="www.albertahealthservices.ca " target="_blank">www.albertahealthservices.ca </a>
             <h4 class="text text-danger">Alberta Health Services
100, 3705 35 Street N.E., Calgary, AB, T1Y 6C2</h4>
         </center>
      </div>
   </div>
 </div>
  <div class='container'>
   <div class='row'>
      <div class="col-md-6 col-md-offset-3">
        <img src="<?php echo base_url(); ?>assets/KNG-FAMILY-WEB-10-x-10.png">
      </div>
  </div>
</div>
    </div>
</section>
