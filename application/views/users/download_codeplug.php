<section id="my_page-content">
    <div class="my_page_container">
      <div class='container' style="margin-top: 35px">
       <div class='row'>
          <div class="col-md-12">
            <center>
              <h2 class="text text-success">Digital & APCO P25 Radios</h2>
              <p>Click on a product for details</p>
            </center>
          </div>
          <div class="col-md-10 col-md-offset-1">
            <table class="table table-responsive table-bordered table-striped">
              <thead>
                <tr>
                  <th>Portables</th>
                  <th>Mobiles</th>
                  <th>Base Stations</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td><a href="<?php echo base_url(); ?>/assets/pdf/EMSPort3.1.neo">EMSPort3.1</a></td>
                  <td><a href="<?php echo base_url(); ?>/assets/pdf/EMSMOB3.2MSAT.neo">EMSMOB3.2MSAT</a></td>
                  <td></td>
                </tr>
                <tr>
                  <td><a href="<?php echo base_url(); ?>/assets/pdf/PS_CP_V2.3.neo">PS_CP_V2.3</a></td>
                  <td></td>
                  <td></td>
                </tr>

              </tbody>
            </table>
          </div>
       </div>
     </div>
    </div>
</section>
