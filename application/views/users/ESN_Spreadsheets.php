<?php
/**
 * Created by PhpStorm.
 * User: Maddy
 * Date: 15-Oct-18
 * Time: 3:51 PM
 */
?>

<div class="page-main">
    <div class="container">
        <div class="file-upload-box">
            <?php if ($this->session->flashdata("success")){?>
                <div class="text-center alert alert-success alert-dismissable">
                    <?= $this->session->flashdata("success")?>
                </div>
            <?php }?>
            <?php if ($this->session->flashdata("error")){?>
                <div class="text-center alert alert-danger alert-dismissable">
                    <?= $this->session->flashdata("error")?>
                </div>
            <?php }?>
            <?php if (isset($error)){?>
                <div class="text-center alert alert-danger alert-dismissable">
                    <?= $error?>
                </div>
            <?php }?>
                <h2 class="section-heading">Upload Your ESN File</h2>
                <p><small>The file type that can be uploaded is .csv, .xlsx, .xls</small></p>
<!--            <div class="row"><em class="text-center">Supported formats are xls,csv,xlsx</em></div>-->
            <div class="file-upload">
                <form action="<?=base_url('Users/upload_esn')?>" method="post" enctype="multipart/form-data">
                    <label for="upload" id="label" class="file-upload__label bk_blue">Choose your file</label>
                    <div class="md-form">
                        <input required="required" name="notes" type="text" id="materialLoginFormPassword" class="form-control">
                        <label for="materialLoginFormPassword">File Notes</label>
                    </div>

                    <!--                    <input type="file" name="file1">-->
                    <input id="upload" class="file-upload__input" type="file" name="file1" onchange="change(this)">

                    <div class="file-btn-wrap">
                        <button type="submit" name="FileUpload" value="submit" class="btn btn-default bk_red"><i class="fas fa-upload mr-1"></i> Upload File</button>
                    </div>
                </form>
            </div>

            <div class="as-table">
                <table class="table-striped">
                    <thead>
                    <tr>
                        <th>Uploaded Date</th>
                        <th>Uploaded By</th>
                        <th>Notes</th>
                        <th>Download</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php if (!isset($list)){ ?>
                    <?php
                    // echo '<pre>';print_r(count($filelist));exit;
                    for($i=0;$i<sizeof($filelist)-1;$i++){
                            if (file_exists('assets/pdf/'.$filelist[$i]->file)){?>
                    <tr>
                        <td><?= $filelist[$i]->date;?></td>
                        <td><?= $filelist[$i]->username;?></td>
                        <td><?= $filelist[$i]->notes;?></td>
                        <td><a href="<?=base_url('assets/pdf/').$filelist[$i]->file?>"><i class="fas fa-file-alt mr-1"></i>Download</a></td>
                    </tr>
                    <?php } } ?>
                        <?php }else{ ?>
                    <tr>
                        <td class="text-center" colspan="4">No Files Uploaded yet.</a></td>
                    </tr>
                    <?php }?>

                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
