<section id="my_page-content">
    <div class="my_page_container">
      <div class='container' style="margin-top: 35px">
       <div class='row'>
          <div class="col-md-12">
            <center>
              <h2 class="text text-success">Upload Your Codeplug File</h2>
              
            </center>
          </div>
          <div class="col-md-10 col-md-offset-1">
            <form method="post" enctype="multipart/form-data">
              <div class="col-md-4 col-md-offset-2">
                <div class="form-group">
                  <label>File</label>
                  <input type="file" name="file" class="form-control"> 
                </div>
              </div>
              <div class="col-md-4">
                <div class="form-group" style="margin-top: 23px;">
                  <input type="submit" name="FileUpload"  value="Upload" class="btn btn-info pull-left"> 
                </div>
              </div>
               
            </form>
          </div>
          <div class="col-md-4 col-md-offset-3" style="margin-top: 10px">
            <table class="table table-responsive table-bordered table-striped">
              <thead>
                <tr>
                  <th>Recently Uploaded Files</th>
                </tr>
              </thead>
              <tbody>
                <?php if (sizeof($filelists)>=1) {
                  foreach ($filelists as $key ) {
                    ?>
                     <tr>
                        <td><a href="<?php echo base_url(); ?>/assets/pdf/<?php echo $key->file ?>"><?php echo $key->file ?></a></td>
                      </tr>
                    <?php
                  }
                  ?>

                  <?php
                }else{
                  echo "<tr><td>No data in your table</td></tr>";
                } ?>
                
                

              </tbody>
            </table>
          </div>
       </div>
     </div>
    </div>
</section>
