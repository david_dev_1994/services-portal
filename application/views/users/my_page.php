<section id="my_page-content">
    <div class="my_page_container">
        <div class="relm_admin_page">
            <h3>BK Technology Administrator Page (Not viewable to dealers)</h3>
            <div class="container">
                <div class="row">
                    <div class="col">
                        <h5>Server Pages Admin</h5>
                        <a href="">Click here to access the service administration page</a><br>
                        <a href="">Create Software Serial Number for Customer Download</a><br>
                    </div>
                    <div class="col">
                        <h5>PRODUCT DEMO DASHBOARD</h5>
                        <a href="">Request a Demo</a><br>
                        <a href="">Manage Demo Profiles</a><br>
                    </div>
                </div>
                <div class="row">
                    <div class="col">
                        <h5>BK Technology.COM LOGIN ADMIN</h5>
                        <a href="">Find Users</a><br>
                        <a href="">Edit Users</a><br>
                        <a href="">Add Users</a><br>
                    </div>
                    <div class="col">
                        <h5>PORTAL ADMIN</h5>
                        <a href="">AHS ESN Upload</a><br>
                    </div>
                </div>
                <div class="repair_form_links__container">
                    <h5>REPAIR FORM LINKS</h5>
                    <table class="repair_form_links">
                        <tr>
                            <td>Standard</td>
                            <td><a href="">http://www.relmservice.com/Factory_Service/Repair_Form.html</a></td>
                        </tr>
                        <tr>
                            <td>Lancaster</td>
                            <td><a href="">http://www.relmservice.com/Factory_Service/Lancaster_Repair_Form.html</a></td>
                        </tr>
                        <tr>
                            <td>Alberta Health Services</td>
                            <td><a href="">http://www.relmservice.com/restricted/ahs-repair_request.asp</a></td>
                        </tr>
                        <tr>
                            <td>Harris County - Stephen</td>
                            <td><a href="">http://www.relmservice.com/harris/Stephen/request.asp</a></td>
                        </tr>
                        <tr>
                            <td>ECOMM 911</td>
                            <td><a href="">	http://www.relmservice.com/restricted/ecomm-repair-request.asp</a></td>
                        </tr>
                        <tr>
                            <td>CALFIRE</td>
                            <td><a href="">http://www.relmservice.com/calfire/index.asp</a></td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
        <div class="government_login">
            <h3>Government Login</h3>
            <div>
                <h5>Government Resources</h5>
                <a href="">Click here to Update Firmware/Software</a><br>
            </div>
        </div>
    </div>
</section>
