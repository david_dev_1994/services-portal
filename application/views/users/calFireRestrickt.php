<section id="my_page-content">
    <div class="my_page_container">
      <div class='container' style="margin-top: 35px">
       <div class='row'>
         
          <div class="col-md-10 col-md-offset-1" style="margin-top: 50px">
            <h4>KNG-M150R Downloads</h4>
            <table class="table table-responsive table-bordered table-striped">
              <thead>
                <tr>
                
                  <th>Upload Date</th>
                  <th>Download</th>
                  
                </tr>
              </thead>
              <tbody>
                <?php if (sizeof($filelists)>=1) {
                  foreach ($filelists as $key ) {
                   if( $key->Type == '1') {
                    ?>
                     <tr>
                        <td><?php echo $key->Description ?></td>
                        <td><a href="<?php echo base_url(); ?>/assets/images/<?php echo $key->file ?>">Download</a></td>
                      </tr>
                    <?php
                  }
              }
                  ?>

                  <?php
                }else{
                  echo "<tr><td>No data in your table</td></tr>";
                } ?>
                
                

              </tbody>
            </table>
          </div>
          <div class="col-md-10 col-md-offset-1" style="margin-top: 50px">
            <h4>KNG-P150CMD Downloads</h4>
            <table class="table table-responsive table-bordered table-striped">
              <thead>
                <tr>
                
                  <th>Upload Date</th>
                  <th>Download</th>
                  
                </tr>
              </thead>
              <tbody>
                <?php if (sizeof($filelists)>=1) {
                  foreach ($filelists as $key ) {
                   if( $key->Type == '2') {
                    ?>
                     <tr>
                        <td><?php echo $key->Description ?></td>
                        <td><a href="<?php echo base_url(); ?>/assets/images/<?php echo $key->file ?>">Download</a></td>
                      </tr>
                    <?php
                  }
              }
                  ?>

                  <?php
                }else{
                  echo "<tr><td>No data in your table</td></tr>";
                } ?>
                
                

              </tbody>
            </table>
          </div>
          <div class="col-md-10 col-md-offset-1" style="margin-top: 50px">
            <h4>Documents</h4>
            <table class="table table-responsive table-bordered table-striped">
              <thead>
                <tr>
                
                  <th>Upload Date</th>
                  <th>Download</th>
                  
                </tr>
              </thead>
              <tbody>
                <?php if (sizeof($filelists)>=1) {
                  foreach ($filelists as $key ) {
                   if( $key->Type == '3') {
                    ?>
                     <tr>
                        <td><?php echo $key->Description ?></td>
                        <td><a href="<?php echo base_url(); ?>/assets/images/<?php echo $key->file ?>">Download</a></td>
                      </tr>
                    <?php
                    }
                  }
                  ?>

                  <?php
                }else{
                  echo "<tr><td>No data in your table</td></tr>";
                } ?>
                
                

              </tbody>
            </table>
          </div>
       </div>
     </div>
    </div>
</section>
