<section id="main-conent" style="padding:0">
    <div class="main-conent__container">

        <div class='container services'>
            <center><h1>Support/Technical Documentation</h1></center>


            <div class="col-md-6 servks">
                <div>
                    <center>
                        <a href="<?php echo base_url();?>/products/list">
                            <div class="icon-wrap">
                                <i class="fa fa-file-text" aria-hidden="true"></i>
                            </div>
                            <h2>Bulletins</h2>
                        </a>
                    </center>
                </div>
            </div>

            <div class="col-md-6  servks">
                <div>
                    <center>
                        <a href="<?=base_url("/Manuals/BK-Manuals")?>">
                            <div class="icon-wrap">
                                <i class="fa fa-book" aria-hidden="true"></i>
                            </div>
                            <h2>BK Radio Manuals</h2>
                        </a>
                    </center>
                </div>
            </div>

    </div>
</section>
<style type="text/css">
    .main-conent__hp-description.bottom a:hover {
        background: #3fb6e8 !important;
        border: 2px solid #3fb6e8 !important;
        color: #ffffff !important;
        border-radius: 0px;
    }
    .main-conent__hp-description.bottom a{
        border: 1px solid #3fb6e8 !important;
        border-radius: 0px !important;
    }
</style>