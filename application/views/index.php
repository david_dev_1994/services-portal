<section id="main-conent" style="padding:0">
    <div class="main-conent__container">
         <div class='container-fluid' style='margin-top: 10px;'>

<div id="myCarousel" class="carousel slide" data-ride="carousel">
    <!-- Indicators -->
    <!-- <ol class="carousel-indicators">
      <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
      <li data-target="#myCarousel" data-slide-to="1"></li>
      <li data-target="#myCarousel" data-slide-to="2"></li>
    </ol> -->

    <!-- Wrapper for slides -->
    <div class="carousel-inner">
      <div class="item active">
        <img class="img-responsive" src="<?php echo base_url(); ?>/assets/banner/Banner Photo Heroes_cropped.jpg" alt="BK Technologies" style="width:100%;">
<div class="carousel-caption">
          <h3>Service</h3>
          <a target="_blank" href="<?=base_url("/factory-service/service-dealers")?>">Read More</a>
        </div>
      </div>

      <!-- <div class="item">
        <img src="<?php echo base_url(); ?>/assets/banner/Parts-Banner-1800x700.jpg" alt="Chicago" style="width:100%;">
<div class="carousel-caption">
          <h3>Parts</h3>
          <a target="_blank" href="<?=base_url("/parts")?>">Read More</a>
        </div>
      </div> -->
    
      <!-- <div class="item">
        <img src="<?php echo base_url(); ?>/assets/banner/Semiconductor-Banner.jpg">
<div class="carousel-caption">
          <h3>Repair</h3>
          <a target="_blank" href="<?=base_url("/factory-service")?>">Read More</a>
        </div>
      </div> -->
    </div>

    <!-- Left and right controls -->
    <!-- <a class="left carousel-control" href="#myCarousel" data-slide="prev">
      <span class="glyphicon glyphicon-chevron-left"></span>
      <span class="sr-only">Previous</span>
    </a>
    <a class="right carousel-control" href="#myCarousel" data-slide="next">
      <span class="glyphicon glyphicon-chevron-right"></span>
      <span class="sr-only">Next</span>
    </a> -->
  </div>
</div>

<div class="main-conent__hp-description top cta-1 col-lg-12">
            <div class='homeheading'>
                <h1>Welcome to BK Technologies<span class="trademark big">®</span></h1>
                <h3>Customer Service and Technical Support Department</h3>
                <div class='link'><a href="<?=base_url("/assets/pdf/Serviceform2019.pdf")?>" target="_blank">Submit a Service Request</a></div>
            </div>
        </div>


<div class='container services' style='background:linear-gradient(0deg,#fff,#fff),url(<?=base_url()?>assets/banner/banner-1018818_1280.jpg);background-repeat: no-repeat; background-attachment: fixed;background-position: center; background-size: cover; padding:40px 0;'>
        <center><h1>BK Service Menu</h1></center>

        <div class="col-md-3 servks">
            <div><center>
                <a href="<?=base_url("/products")?>">
                  
<div class="icon-wrap">
<i class="fa fa fa-book" aria-hidden="true"></i>
</div>
                  <h2>Support Materials</h2>
                </a>
            </center>
            </div>
        </div>
        <div class="col-md-3 servks">
            <div>
            	<center>
                <a href="<?=base_url("/factory-service")?>">
                <div class="icon-wrap">
<i class="fa fa-gear" aria-hidden="true"></i>
</div>
                  <h2>Repair</h2>
</a>
</center>
            </div>
        </div>
        <div class="col-md-3 servks">
            <div>
            	<center>
                <a href="<?=base_url("/products/list")?>">
<div class="icon-wrap">
<i class="fa fa-file-text" aria-hidden="true"></i>
</div>
                  <h2>Bulletins</h2>
</a>
</center>
            </div>
        </div>
        <div class="col-md-3 servks">
            <div><center>
                <a href="<?=base_url("/contact")?>">
<div class="icon-wrap">
<i class="fa fa-phone" aria-hidden="true"></i>
</div>
<h2>Contact</h2>

</a>
</center>            </div>
        </div>
</div>


<div class="container-fluid before-footer"><div class="row">        
<div class="main-conent__hp-description bottom cta-2 col-lg-12">
            <div class='homeheading'>
               
                <h3 style="text-transform: capitalize !important;">For more information on available products, visit our website</h3>
<div class="link"> <a href="https://www.bktechnologies.com/" target="_blank" style="font-weight: 500;">Learn More</a></div>
<!-- <div class="link"> <a href="<?php echo base_url();?>assets/pdf/BK_Radio_Drivers10-2011.zip" style="font-weight: 500;">Download BK Radio USB Drivers</a></div> -->
            </div>
        </div>
</div>
</div>

    </div>
</section>

<style>
section#main-conent {
    padding: 0px 0 !important;
}

</style>