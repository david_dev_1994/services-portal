<section id="main-conent">
    <div class="container text-center" id="contact">
<div class="row">
<div class="col-lg-12">
        <h1>Service Department Contact Information</h1>
        <h3>BK Technologies<span class="trademark">®</span></h3>
<div class="padding-20"></div>
<div class="row contact">
<div class="col-lg-4">
<div class="icon">
<i class="fa fa-map-marker" aria-hidden="true"></i>
</div>
<h4>7100 Technology Drive<br> West Melbourne, FL 32904</h4>
</div>
<div class="col-lg-4">
<div class="icon">
<i class="fa fa-phone" aria-hidden="true"></i>
</div>
<h4><a href="tel:8004226281">800-422-6281</a></h4>
</div>
<div class="col-lg-4">
<div class="icon">
<i class="fa fa-envelope" aria-hidden="true"></i>
</div>
<h4><a href="mailto:service@bktechnologies.com">service@bktechnologies.com</a></h4>
</div>
</div>
<div class="padding-20"></div>
<div class="row">
<div class="col-lg-4">
</div>
<div class="col-lg-4">
<a class="button" href="http://www.relmservice.com/Forums/BKForum/forum.asp" target="_blank">BK Technologies Forum</a>
</div>
<div class="col-lg-4">
</div>
</div>        
        <ul class="contact_new_ul">
<!--                <li><a href="--><?//=base_url("contact/email-contact")?><!--">Submit questions or comments</a></li>-->

<!--                <li><a href="--><?//=base_url("forums")?><!--">BK Technologies Forum</a></li>-->
                    <!-- <li><a href="http://www.relmservice.com/Forums/BKForum/forum.asp" target="_blank">BK Technology Forum</a></li> -->
<!--                <li><a href="">Sign-Up for Email Notices</a></li>-->
            </ul>
        </p>
    </div>
</div>
</div>
<div class="padding-20"></div>
<div class="padding-10"></div>
<div class="row contact-form">
<div class="col-lg-1"></div>
<div class="col-lg-10">
<div class="container text-center" id="contact_email">
        <?php if(isset($message)){
            ?>
            <center>
                <p style='margin-top: 34px;' class='alert alert-success'>Your information has been sent <br>
                    to the Service Department.</p>
            </center>
            <?php
        } else{?>
        <h1 style="color: #ffffff !important;">Send An E-Mail To The Service Department</h1>
        <div class="padding-10"></div>
        <form class="contact-form" action="" method="post">
            <div class="form-group">
                <div class="col-lg-6">
                    <label for="name">Name</label>
                    <input id="name" name='name' type="text" required>
                </div>
                <div class="col-lg-6">
                    <label for="company">Company</label>
                    <input type="text" name="company" id="company" required>
                </div>
            </div>
            <div class="form-group">
                <div class="col-lg-6">
                    <label for="email">Email</label>
                    <input type="email" name='email' id="email" required>
                </div>
                <div class="col-lg-6">
                    <label for="phone">Phone</label>
                    <input type="tel" name='phone' id="phone" required>
                </div>
            </div>
            <div class="form-group">
                <div class="col-lg-6">
                    <label for="address">Address</label>
                    <input type="text" name="address" id="address" required>
                </div>
                <div class="col-lg-6">
                    <label for="city">City</label>
                    <input type="text" name='city' id="city" required>
                </div>
            </div>
            <div class="form-group">
                <div class="col-lg-6">
                    <label for="state">State</label>
                    <select name="state" id="state" required>
                        <option>Select a State</option>
                        <option>Alabama</option>
                        <option>Alaska</option>
                        <option>Arizona</option>
                        <option>Arkansas</option>
                        <option>California</option>
                        <option>Colorado</option>
                        <option>Connecticut</option>
                        <option>Delaware</option>
                        <option>District Of Columbia</option>
                        <option>Florida</option>
                        <option>Georgia</option>
                        <option>Hawaii</option>
                        <option>Idaho</option>
                        <option>Illinois</option>
                        <option>Indiana</option>
                        <option>Iowa</option>
                        <option>Kansas</option>
                        <option>Kentucky</option>
                        <option>Louisiana</option>
                        <option>Maine</option>
                        <option>Maryland</option>
                        <option>Massachusetts</option>
                        <option>Michigan</option>
                        <option>Minnesota</option>
                        <option>Mississippi</option>
                        <option>Missouri</option>
                        <option>Montana</option>
                        <option>Nebraska</option>
                        <option>Nevada</option>
                        <option>New Hampshire</option>
                        <option>New Jersey</option>
                        <option>New   Mexico</option>
                        <option>New York</option>
                        <option>North Carolina</option>
                        <option>North Dakota</option>
                        <option>Ohio</option>
                        <option>Oklahoma</option>
                        <option>Oregon</option>
                        <option>Pennsylvania</option>
                        <option>Rhode Island</option>
                        <option>South Carolina</option>
                        <option>South Dakota</option>
                        <option>Tennessee</option>
                        <option>Texas</option>
                        <option>Utah</option>
                        <option>Vermont</option>
                        <option>Virginia</option>
                        <option>Washington</option>
                        <option>West Virginia</option>
                        <option>Wisconsin</option>
                        <option>Wyoming</option>
                    </select>
                </div>
                <div class="col-lg-6">
                    <label for="country">Country</label>
                    <select name="country" id="country" required>
                        <option value="USA" selected="selected">USA</option>
                        <option value="Not Listed">Not Listed</option>
                        <option value="Afghanistan">Afghanistan</option>
                        <option value="Albania">Albania</option>
                        <option value="Algeria">Algeria</option>
                        <option value="American Samoa">American   Samoa</option>
                        <option value="Angola">Angola</option>
                        <option value="Antigua &amp; Barbuda">Antigua &amp; Barbuda</option>
                        <option value="Argentina">Argentina</option>
                        <option value="Armenia">Armenia</option>
                        <option value="Aruba">Aruba</option>
                        <option value="Ascension Islands">Ascension   Islands</option>
                        <option value="Australia">Australia</option>
                        <option value="Austria">Austria</option>
                        <option value="Azerbaijan">Azerbaijan</option>
                        <option value="Bahamas">Bahamas</option>
                        <option value="Bahrain">Bahrain</option>
                        <option value="Bangladesh">Bangladesh</option>
                        <option value="Barbados">Barbados</option>
                        <option value="Belgium">Belgium</option>
                        <option value="Belize">Belize</option>
                        <option value="Benin">Benin</option>
                        <option value="Bermuda">Bermuda</option>
                        <option value="Bhutan">Bhutan</option>
                        <option value="Bolivia">Bolivia</option>
                        <option value="Bosnia &amp; Herzgov">Bosnia &amp;   Herzgov</option>
                        <option value="Botswana">Botswana</option>
                        <option value="Brazil">Brazil</option>
                        <option value="Brunei">Brunei</option>
                        <option value="Bulgaria">Bulgaria</option>
                        <option value="Burkina Faso">Burkina   Faso</option>
                        <option value="Burundi">Burundi</option>
                        <option value="Cambodia">Cambodia</option>
                        <option value="Cameroon">Cameroon</option>
                        <option value="Canada">Canada</option>
                        <option value="Central Africa Rep">Central   Africa Rep</option>
                        <option value="Chad Rep">Chad Rep</option>
                        <option value="Chile">Chile</option>
                        <option value="China">China</option>
                        <option value="Colombia">Colombia</option>
                        <option value="Congo">Congo</option>
                        <option value="Cook Isl">Cook Isl</option>
                        <option value="Costa Rica">Costa   Rica</option>
                        <option value="Croatia">Croatia</option>
                        <option value="Cuba">Cuba</option>
                        <option value="Cyprus">Cyprus</option>
                        <option value="Czech Rep">Czech Rep</option>
                        <option value="Denmark">Denmark</option>
                        <option value="Diego Garcia">Diego Garcia</option>
                        <option value="Djibouti">Djibouti</option>
                        <option value="Egypt">Egypt</option>
                        <option value="El Salvador">El Salvador</option>
                        <option value="Equatorial Guinea">Equatorial Guinea</option>
                        <option value="Eritrea">Eritrea</option>
                        <option value="Estonia">Estonia</option>
                        <option value="Ethiopia">Ethiopia</option>
                        <option value="Fiji Islands">Fiji   Islands</option>
                        <option value="Finland">Finland</option>
                        <option value="France">France</option>
                        <option value="French Antilles">French   Antilles</option>
                        <option value="French Guiana">French Guiana</option>
                        <option value="French Polynesia">French Polynesia</option>
                        <option value="Gabon">Gabon</option>
                        <option value="Gambia">Gambia</option>
                        <option value="Georgia">Georgia</option>
                        <option value="Germany">Germany</option>
                        <option value="Ghana">Ghana</option>
                        <option value="Gibraltar">Gibraltar</option>
                        <option value="Greece">Greece</option>
                        <option value="Greenland">Greenland</option>
                        <option value="Grenada">Grenada</option>
                        <option value="Guadeloupe">Guadeloupe</option>
                        <option value="Guam">Guam</option>
                        <option value="Guatemala">Guatemala</option>
                        <option value="Guinea Bissau">Guinea Bissau</option>
                        <option value="Guinea">Guinea</option>
                        <option value="Guyana">Guyana</option>
                        <option value="Haiti">Haiti</option>
                        <option value="Honduras">Honduras</option>
                        <option value="Hong Kong">Hong Kong</option>
                        <option value="Hungary">Hungary</option>
                        <option value="Iceland">Iceland</option>
                        <option value="India">India</option>
                        <option value="Indonesia">Indonesia</option>
                        <option value="Iran">Iran</option>
                        <option value="Iraq">Iraq</option>
                        <option value="Ireland">Ireland</option>
                        <option value="Israel">Israel</option>
                        <option value="Italy">Italy</option>
                        <option value="Ivory Coast">Ivory Coast</option>
                        <option value="Jamaica">Jamaica</option>
                        <option value="Japan">Japan</option>
                        <option value="Jordan">Jordan</option>
                        <option value="Kazahstan">Kazahstan</option>
                        <option value="Kenya">Kenya</option>
                        <option value="Kirighzia">Kirighzia</option>
                        <option value="Kiribati">Kiribati</option>
                        <option value="North Korea">Korea (North)</option>
                        <option value="South Korea">Korea (South)</option>
                        <option value="Kuwait">Kuwait</option>
                        <option value="Kyrgyzstan">Kyrgyzstan</option>
                        <option value="Laos">Laos</option>
                        <option value="Latvia">Latvia</option>
                        <option value="Lebanon">Lebanon</option>
                        <option value="Lesotho">Lesotho</option>
                        <option value="Liberia">Liberia</option>
                        <option value="Libya">Libya</option>
                        <option value="Liechtenstein">Liechtenstein</option>
                        <option value="Lithuania">Lithuania</option>
                        <option value="Luxembourg">Luxembourg</option>
                        <option value="Macao">Macao</option>
                        <option value="Macadonia">Macadonia</option>
                        <option value="Madagascar">Madagascar</option>
                        <option value="Malawi">Malawi</option>
                        <option value="Malaysia">Malaysia</option>
                        <option value="Maldives">Maldives</option>
                        <option value="Mali">Mali</option>
                        <option value="Malta">Malta</option>
                        <option value="Marshall Islands">Marshall   Islands</option>
                        <option value="Mauritania">Mauritania</option>
                        <option value="Mauritius">Mauritius</option>
                        <option value="Mexico">Mexico</option>
                        <option value="Micronesia">Micronesia</option>
                        <option value="Moldova">Moldova</option>
                        <option value="Monaco">Monaco</option>
                        <option value="Mongolia">Mongolia</option>
                        <option value="Monserrat">Monserrat</option>
                        <option value="Morocco">Morocco</option>
                        <option value="Mozambique">Mozambique</option>
                        <option value="Myanmar/Burma">Myanmar/Burma</option>
                        <option value="Namibia">Namibia</option>
                        <option value="Nauru">Nauru</option>
                        <option value="Nepal">Nepal</option>
                        <option value="Netherlands Antilles">Netherlands   Antilles</option>
                        <option value="Netherlands">Netherlands</option>
                        <option value="Nevis Island">Nevis Island</option>
                        <option value="New Caledonia">New   Caledonia</option>
                        <option value="New Zealand">New Zealand</option>
                        <option value="Nicaragua">Nicaragua</option>
                        <option value="Niger">Niger</option>
                        <option value="Nigeria">Nigeria</option>
                        <option value="Niue Island">Niue Island</option>
                        <option value="Norway">Norway</option>
                        <option value="Oman">Oman</option>
                        <option value="Pakistan">Pakistan</option>
                        <option value="Palau">Palau</option>
                        <option value="Panama">Panama</option>
                        <option value="Papau New Guinea">Papau New   Guinea</option>
                        <option value="Paraguay">Paraguay</option>
                        <option value="Peru">Peru</option>
                        <option value="Puerto Rico">Puerto Rico</option>
                        <option value="Philippines">Philippines</option>
                        <option value="Poland">Poland</option>
                        <option value="Portugal">Portugal</option>
                        <option value="Qatar">Qatar</option>
                        <option value="Reunion Island">Reunion   Island</option>
                        <option value="Romania">Romania</option>
                        <option value="Russia">Russia</option>
                        <option value="Rwanda">Rwanda</option>
                        <option value="Saipan">Saipan</option>
                        <option value="Sakhalin">Sakhalin</option>
                        <option value="San Marino">San Marino</option>
                        <option value="Sao Tome">Sao   Tome</option>
                        <option value="Saudi Arabia">Saudi Arabia</option>
                        <option value="Senegal Republic">Senegal Republic</option>
                        <option value="Saychelles">Saychelles</option>
                        <option value="Sierra Leone">Sierra   Leone</option>
                        <option value="Singapore">Singapore</option>
                        <option value="Slovakia">Slovakia</option>
                        <option value="Slovenia">Slovenia</option>
                        <option value="Solomon Islands">Solomon Islands</option>
                        <option value="Somalia">Somalia</option>
                        <option value="South Africa">South   Africa</option>
                        <option value="Spain">Spain</option>
                        <option value="Sri Lanka">Sri   Lanka</option>
                        <option value="St Helena">St Helena</option>
                        <option value="St Pierre">St Pierre</option>
                        <option value="Sudan">Sudan</option>
                        <option value="Suriname">Suriname</option>
                        <option value="Swaziland">Swaziland</option>
                        <option value="Sweden">Sweden</option>
                        <option value="Switzerland">Switzerland</option>
                        <option value="Syria">Syria</option>
                        <option value="Taiwan">Taiwan</option>
                        <option value="Tajikistan">Tajikistan</option>
                        <option value="Tanzania">Tanzania</option>
                        <option value="Thailand">Thailand</option>
                        <option value="Togolese Rep">Togolese   Rep</option>
                        <option value="Tonga">Tonga</option>
                        <option value="Trinidad/Tobago">Trinidad/Tobago</option>
                        <option value="Tunisia">Tunisia</option>
                        <option value="Turkey">Turkey</option>
                        <option value="Turkmenistan">Turkmenistan</option>
                        <option value="Turks/Caicos">Turks/Caicos</option>
                        <option value="Tuvalu Islands">Tuvalu   Islands</option>
                        <option value="Uganda">Uganda</option>
                        <option value="Ukraine">Ukraine</option>
                        <option value="United Arab Emirates">United Arab   Emirates</option>
                        <option value="United Kingdom">United Kingdom</option>
                        <option value="Uruguay">Uruguay</option>
                        <option value="Uzbekistan">Uzbekistan</option>
                        <option value="Vanuatu">Vanuatu</option>
                        <option value="Vatican City">Vatican   City</option>
                        <option value="Venezuela">Venezuela</option>
                        <option value="Vietnam">Vietnam</option>
                        <option value="Wallis/Futuna">Wallis/Futuna</option>
                        <option value="Western Samoa">Western   Samoa</option>
                        <option value="Yemen Arab Republic">Yemen Arab Republic</option>
                        <option value="Yugoslavia">Yugoslavia</option>
                        <option value="Zaire">Zaire</option>
                        <option value="Zambia">Zambia</option>
                        <option value="Zimbabwe">Zimbabwe</option>
                        <option value="Zanzibar">Zanzibar</option>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <div class="col-lg-6">
                    <label for="zip">Zip Code</label>
                    <input type="text" name='zip' id="zip" required>
                </div>
                <div class="col-lg-6">
                    <label for="fax">Fax</label>
                    <input type="text" name='fax' id="fax" required>
                </div>
                <br>
            </div>
            <div class="form-group">
                <div class="col-lg-12">
                <label for="comments">Comments or Questions</label>
                <textarea name="comment" id="comments" cols="30" rows="10" required></textarea>
                </div>
           </div>
<div class="form-group">
                <div class="col-lg-12">
                <p style="text-align: left !important;color: #ffffff !important;margin-bottom:20px !important;">As a provider of communications for many public safety organizations, the Service Department may prioritize responses according to incident requirements.</p>
                </div>
           </div>
            <div class="form-group">
<div class="col-lg-12">
                <input type="submit" name='contactsubmit' value='Send Message' class='btn btn-info'>
                <!-- <button class='btn btn-warning' type="reset">Reset Form</button> -->
</div>            
</div>
        </form>
        <?php }?>
</div>
</div>
<div class="col-lg-1"></div>
</div>
    </div>
</section>

<style>

section#main-conent {
padding-bottom: 0px !important;
}

</style>