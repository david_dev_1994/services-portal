<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<section id="main-conent">
    <div class="container" >
      <div class="col-md-12" style="margin-top: 25px">
          <?php if (sizeof($partstype) > 0) {
              foreach ($partstype as $key ) {
                  ?>
                  <a href="<?php echo base_url();?>/parts/type/<?php echo $key->id; ?>"><?php echo $key->typetitle; ?></a>| 
                  <?php
              }
          } ?>
      </div>
      <div class="col-md-12">
          <h5><?php echo $parts[0]->title ?></h5>
      </div>
      <div class="col-md-6">
         <?php if (sizeof($partsList) > 0) {
            ?>
            <table class="table table-responsive table-borderd table-striped">
                <thead>
                  <tr>
                    <th>Ref # </th>
                    <th>Part #</th>
                    <th>Description</th>
                  </tr>
                </thead>
                <tbody>
                    <?php foreach ($partsList as $key) {
                      ?>
                    <tr>
                        <td><?php echo $key->RefNo ?></td>
                        <td><?php echo $key->PartNo ?></td>
                        <td><?php echo $key->Descriptions ?></td>
                    </tr>
                    <?php } ?>
                </tbody>
            </table>
            <?php
         } ?>
      </div>
       <div class="col-md-6">
         <img src="<?php echo base_url();?>/assets/images/<?php echo $parts[0]->image; ?>" class="img img-responsive">
       </div>
    </div>
</section>