<section id="main-conent">
    <div class="container" id="parts">
        <div class="parts__common-replacement">
            <div class="parts_cr-header">
                <h3 class="font-bold">Common Replacement Parts</h3>
            </div>
            <div class="parts_cr-content">
                <div class="parts_cr-menu">
                    <ul>
                        <?php if(sizeof($parts) >0 ){ 
                          foreach ($parts as $key) {
                            
                            ?>
                        <li><a href="<?php echo base_url();?>/parts/type/<?php echo $key->id; ?>"><?php echo $key->typetitle; ?></a></li>
                        <?php }
                         } ?>
                        
                    </ul>
                </div>
                <div class="parts_cr-text">
                    <p>BK Technology Online parts lists and diagrams are designed to assist customers in
                    identifying some of our most requested parts and associated part numbers.</p>

                    <p>Part numbers and availability are subject to change.</p>

                    <p>Complete service manuals are available in printed or electronic form.
                    Manuals include circuit and component level product  information as well
                    as a detailed theories of electronic operation and schematic diagrams.</p>

                    <p>For information on complete service manual costs and availability
                    contact the customer service or order entry departments.</p>


                    <a href="<?php echo base_url();?>contact">Contact BK Technology</a>



                    <span class="font-bold">NOTE: PCB Modules are not available for sale.</span>

                    <span class="font-bold">Part Numbers and availability are subject to change.</span>

                    <span>Available parts can be purchased through BK Technology Wireless</span>

                    <span>or BK Technology Wireless Parts Distributor.</span>

                    <a href="<?php echo base_url();?>parts/Parts_Distributor">Parts Distributors</a>
                </div>
            </div>
        </div>
    </div>
</section>