<section id="main-conent">
    <div class="container" >
      <div class="col-md-12" style="margin-top: 25px">
          <?php if (sizeof($partstype) > 0) {
              foreach ($partstype as $key ) {
                  ?>
                  <a href="<?php echo base_url();?>/parts/type/<?php echo $key->id; ?>"><?php echo $key->typetitle; ?></a>| 
                  <?php
              }
          } ?>
      </div>
      <div class="col-md-12">
          <h5>Click the image below to show the indicated section.</h5>
      </div>
      <div class="col-md-12">
          <?php if (sizeof($parts) > 0) {
              foreach ($parts as $key ) {
                  ?>
                <div class="col-md-3">
                  <a href="<?php echo base_url();?>/parts/list/<?php echo $key->id; ?>">
                      <img src="<?php echo base_url();?>/assets/images/<?php echo $key->image; ?>" class='img img-responsive img-thumbnail'>
                      <p style="text-align:center;"><?php echo $key->title; ?></p>
                  </a>
                </div>
                  <?php
              }
          } ?>
      </div>
    </div>
</section>