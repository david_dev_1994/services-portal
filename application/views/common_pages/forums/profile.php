<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js">
        </script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/1000hz-bootstrap-validator/0.11.5/validator.min.js">
        </script>
<style type="text/css">
	.formsection2 {
    /* border: 1px solid red; */
    margin: 10px;
    padding: 5px;
   
}
</style>

<section id="main-conent">
    <div class="container padding-tb-100 text-center" id="forums">
      <div class="row">
      	<div class="col-md-12">
      		<center>
      			<?php if($forum[0]->type==1){
                    ?>
                    <img src="<?=base_url()?>assets/RELM_sm.jpg" class="img img-responsive">

                    <?php
      			}else{
                    ?>
                    <img src="<?=base_url()?>assets/BK_sm.jpg" class="img img-responsive">
                    <?php
      			} ?>
      			
      			<h2>Technical Forum</h2>
      			<p>Original Topic Posted on <?php echo $forum[0]->date; ?> by <?php echo $forum[0]->by; ?></p>
      			<a class="btn btn-link" data-toggle="modal" data-target="#myModal">Post a Reply</a>
      			
      		</center>
      	</div>
      </div>
      <div class="row">
      	<div class="col-md-10 col-md-offset-1 alert alert-info">
          <p> Topic : <?php echo $forum[0]->tile; ?></p>
          <p><?php echo $forum[0]->details; ?></p>
      	</div>
      	
      </div>
      <center><h4>Replies posted to this message</h4></center>
      <div class="row">
      	<div class="col-md-10 col-md-offset-1">
      		<?php foreach ($ans as $data) { ?>
      	  <div class="formsection2 well">
      		<center><p>  * * Reply from Service - <?php echo $data->date ;?> * *<br>
------------------------------------------------------------------- </p></center>
      		<div class="">
      			<p><?php echo $data->ans; ?></p>
      			<p><?php echo $data->details; ?></p>
             
            
      		</div>
      	  </div>
      	<?php } ?>
      
      	</div>
          
      </div>


<div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
         
        </div>
        <div class="modal-body" style="margin-top: 50px">
           <h4 class="modal-title">Add Comment</h4>
           <div class="row">
           	<div class="col-md-12">
           	<form data-toggle="validator" role="form" method="post">


                  <div class="form-group">
                      <label class="control-label" for="inputName">Name</label>
                      <input class="form-control" name="by"  data-error="Please enter  name field." id="inputName" placeholder="Name"  type="text" required />
                      <div class="help-block with-errors"></div>
                  </div>
                  
                  <div class="form-group">
                    <label for="inputEmail" class="control-label">Email</label>
                    <input type="email" name="email" class="form-control" id="inputEmail" placeholder="Email" required>
                    <div class="help-block with-errors"></div>
                  </div>
                  <div class="form-group">
                      <label class="control-label" for="inputName">Topic</label>
                      <input class="form-control" name="ans"  data-error="Please enter  topic field." id="inputName" placeholder="ans"  type="text" required />
                      <div class="help-block with-errors"></div>
                  </div>
                  <div class="form-group">
                    <label for="inputEmail" class="control-label">Details</label>
                    <textarea name="details" class="form-control"></textarea>
                    <div class="help-block with-errors"></div>
                  </div>
                     <div class="form-group">
                     	<input type="hidden" name="forumid" class="form-control" value="<?php echo $forum[0]->id;?>">
                      <button class="btn btn-primary" type="submit">
                          Submit
                      </button>
                  </div>
                </form>
            </div>
           </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>


    </div>
</section>

