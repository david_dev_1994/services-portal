<?php
/**
 * Created by PhpStorm.
 * User: Maddy
 * Date: 03-Oct-18
 * Time: 10:12 PM
 */
?>

<div class="header" xmlns="http://www.w3.org/1999/html">
    <div class="header-top">
        <div class="container-fluid">
            <div class="row align-items-center justify-content-between">
                <div class="col-md-auto">
                    <div class="logo-wrap">
                        <a href="<?=base_url('flfs')?>">
                            <img class="pad " src="<?=base_url('assets/styles/images/florida.png')?>" alt="BK Logo">
                        </a>
                    </div>
                </div>
                <div class="col-md-auto">
                    <div class="header-top-content">
                        <h3 class="phone">
                            <i class="fas fa-phone"></i>
                            <a href="tel: 1-800-422-6281">1-800-422-6281</a>
                        </h3>
                        <h3 class="email">
                            <i class="fas fa-envelope"></i>
                            <a href="mailto: service@bktechnologies.com">service@bktechnologies.com</a>
                        </h3>
                    </div>
                </div>
                <div class="col-md-auto">
                    <div class="logo-wrap bk-logo-portal">
                        <a href="https://www.bktechnologies.com" target="_blank">
                            <img src="<?=base_url('assets/styles/images/bk-logo-portal.png')?>" alt="BK Logo">
                        </a>
                    </div>
                </div>
<!--                <div class="col-md-auto">-->
<!--                    <div class="logo-wrap">-->
<!--                        <a href="--><?//=base_url()?><!--">-->
<!--                            <img src="--><?//= base_url('assets/styles/images/bk-logo.jpg')?><!--" alt="BK Logo">-->
<!--                        </a>-->
<!--                    </div>-->
<!--                </div>-->

            </div>
        </div>
    </div>
    <nav class="mb-1 navbar navbar-expand-lg navbar-dark info-color">
        <a class="navbar-brand" href="<?=base_url();?>">BK Technologies</a>
        <button class="navbar-toggler collapsed" type="button" data-toggle="collapse" data-target="#navbarSupportedContent-4" aria-controls="navbarSupportedContent-4" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="navbar-collapse collapse" id="navbarSupportedContent-4" style="">
            <ul class="navbar-nav ml-auto">

                <li class="nav-item">
                    <a class="nav-link waves-effect waves-light" href="<?=base_url('flfs')?>">
                        <i class="fas fa-home"></i> Return To Dashboard
                    </a>
                </li>

                <li class="nav-item active">
                        <a class="nav-link waves-effect waves-light" href="<?=base_url('logout')?>">
                            <i class="fas fa-power-off"></i> Logout
                        </a>
                </li>
            </ul>
        </div>
    </nav>
</div>

<!--<div class="page-main">-->
<!--    <div class="container">-->
<!--        <div class="as-table calfire-tables mt-40">-->
<!--            <table class="table table-striped">-->
<!--                <thead>-->
<!--                <tr>-->
<!--                    <th scope="col" colspan="2">KNG-M150R Downloads</th>-->
<!--                </tr>-->
<!--                </thead>-->
<!--                <tbody>-->
<!--                <tr>-->
<!--                    <td class="table-body-heading">Description</td>-->
<!--                    <td class="table-body-heading width-25">Download</td>-->
<!--                </tr>-->
<!--                --><?php //if (sizeof($filelists)>=1) {
//                    foreach ($filelists as $key ) {
//                        if( $key->Type == '1') {
//                            ?>
<!--                            <tr>-->
<!--                                <td>--><?php //echo $key->Description ?><!--</td>-->
<!--                                <td><a href="--><?php //echo base_url(); ?><!--/assets/images/--><?php //echo $key->file ?><!--" class="table-icon-link"><i class="fas fa-download mr-1">Download</a></td>-->
<!--                            </tr>-->
<!--                            --><?php
//                        }
//                    }
//                    ?>
<!--                    --><?php
//                }else{
//                    echo "<tr><td>No data in your table</td></tr>";
//                } ?>
<!--                </tbody>-->
<!--            </table>-->
<!--        </div>-->
<!---->
<!---->
<!--        <div class="as-table calfire-tables">-->
<!--            <table class="table table-striped">-->
<!--                <thead>-->
<!--                <tr>-->
<!--                    <th scope="col" colspan="2">KNG-P150CMD Downloads</th>-->
<!--                </tr>-->
<!--                </thead>-->
<!--                <tbody>-->
<!--                <tr>-->
<!--                    <td class="table-body-heading">Description</td>-->
<!--                    <td class="table-body-heading width-25">Download</td>-->
<!--                </tr>-->
<!--                --><?php //if (sizeof($filelists)>=1) {
//                    foreach ($filelists as $key ) {
//                        if( $key->Type == '2') {
//                            ?>
<!--                            <tr>-->
<!--                                <td>--><?php //echo $key->Description ?><!--</td>-->
<!--                                <td><a href="--><?php //echo base_url(); ?><!--/assets/images/--><?php //echo $key->file ?><!--" class="table-icon-link"><i class="fas fa-download mr-1">Download</a></td>-->
<!--                            </tr>-->
<!--                            --><?php
//                        }
//                    }
//                    ?>
<!--                    --><?php
//                }else{
//                    echo "<tr><td>No data in your table</td></tr>";
//                } ?>
<!--                </tbody>-->
<!--            </table>-->
<!--        </div>-->
<!---->
<!---->
<!--        <div class="as-table calfire-tables">-->
<!--            <table class="table table-striped">-->
<!--                <thead>-->
<!--                <tr>-->
<!--                    <th scope="col" colspan="2">Documents</th>-->
<!--                </tr>-->
<!--                </thead>-->
<!--                <tbody>-->
<!--                <tr>-->
<!--                    <td class="table-body-heading">Description</td>-->
<!--                    <td class="table-body-heading width-25">Download</td>-->
<!--                </tr>-->
<!--                --><?php //if (sizeof($filelists)>=1) {
//                    foreach ($filelists as $key ) {
//                        if( $key->Type == '3') {
//                            ?>
<!--                            <tr>-->
<!--                                <td>--><?php //echo $key->Description ?><!--</td>-->
<!--                                <td><a href="--><?php //echo base_url(); ?><!--/assets/images/--><?php //echo $key->file ?><!--" class="table-icon-link"><i class="fas fa-download mr-1">Download</a></td>-->
<!--                            </tr>-->
<!--                            --><?php
//                        }
//                    }
//                    ?>
<!---->
<!--                    --><?php
//                }else{
//                    echo "<tr><td>No data in your table</td></tr>";
//                } ?>
<!--                </tbody>-->
<!--            </table>-->
<!--        </div>-->
<!---->
<!---->
<!--    </div>-->
<!--</div>-->

<div class="page-main">
    <div class="container">
        <div class="as-table calfire-tables mt-40">
            <table class="table table-striped">
                <thead>
                <tr>
                    <th scope="col" colspan="2">KNG-M150R Downloads</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td class="table-body-heading">Description</td>
                    <td class="table-body-heading width-25">Download</td>
                </tr>
                <tr>
                    <td>Firmware 5.5.0f</td>
                    <td><a  href="<?php echo base_url(); ?>assets/images/KNG_5.5.0f_All_in_One.zip" class="table-icon-link"><i class="fas fa-download mr-1"></i>Download</a></td>
                </tr>
                <tr>
                    <td>Software: RES 5.5.3</td>
                    <td><a  href="<?php echo base_url(); ?>assets/images/RES_windows-x64_5_5_5.zip" class="table-icon-link"><i class="fas fa-download mr-1"></i>Download</a></td>
                </tr>
                </tbody>
            </table>
        </div>


        <div class="as-table calfire-tables">
            <table class="table table-striped">
                <thead>
                <tr>
                    <th scope="col" colspan="2">KNG-P150CMD Downloads</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td class="table-body-heading">Description</td>
                    <td class="table-body-heading width-25">Download</td>
                </tr>
                <tr>
                    <td>Firmware 5.5.0f</td>
                    <td><a href="<?php echo base_url(); ?>assets/images/KNG_5.5.0f_All_in_One.zip" class="table-icon-link"><i class="fas fa-download mr-1"></i>Download</a></td>
                </tr>
                <tr>
                    <td>Software: RES 5.5.3</td>
                    <td><a href="<?php echo base_url(); ?>assets/images/RES_windows-x64_5_5_5.zip" class="table-icon-link"><i class="fas fa-download mr-1"></i>Download</a></td>
                </tr>
                </tbody>
            </table>
        </div>


        <div class="as-table calfire-tables">
            <table class="table table-striped">
                <thead>
                <tr>
                    <th scope="col" colspan="2">Documents</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td class="table-body-heading">Description</td>
                    <td class="table-body-heading width-25">Open</td>
                </tr>
                <tr>
                    <td>BK KNG Radio Drivers</td>
                    <td><a target="_blank" href="<?php echo base_url(); ?>assets/images/BK_KNG_radio_Drivers_FW_RES.pdf" class="table-icon-link"><i class="fas fa-download mr-1"></i>Open</a></td>
                </tr>
                <tr>
                    <td>KNG-P150CMD Firmware Update Instructions</td>
                    <td><a target="_blank" href="<?php echo base_url(); ?>assets/images/KNG_P150CMD_Firmware_update_instructions_PDF.pdf" class="table-icon-link"><i class="fas fa-download mr-1"></i>Open</a></td>
                </tr>
                <tr>
                    <td>BK KNG-M150 TX Timeout</td>
                    <td><a target="_blank" href="<?php echo base_url(); ?>assets/images/BK_KNG_M150_TX_timeout_PDF.pdf" class="table-icon-link"><i class="fas fa-download mr-1"></i>Open</a></td>
                </tr>
                <tr>
                    <td>KNG Mobile Radio User's Manual</td>
                    <td><a target="_blank" href="<?php echo base_url(); ?>assets/images/KNG-M_User's_Manual_03-17.pdf" class="table-icon-link"><i class="fas fa-download mr-1"></i>Open</a></td>                                </tr>

                <tr>
                    <td>KAA0701 KNG/Legacy Radio Cloning Cable User's Guide</td>
                    <td><a target="_blank" href="<?php echo base_url(); ?>assets/images/KAA0701_User's_Manual.pdf" class="table-icon-link"><i class="fas fa-download mr-1"></i>Open</a></td>
                </tr>
                <tr>
                    <td>Installation instructions for KNG Mobiles</td>
                    <td><a target="_blank" href="<?php echo base_url(); ?>assets/images/KNG-M_Installation_Instructions_G.pdf" class="table-icon-link"><i class="fas fa-download mr-1"></i>Open</a></td>
                </tr>
                <tr>
                    <td>Instructions for installing KNG firmware updates</td>
                    <td><a target="_blank" href="<?php echo base_url(); ?>assets/images/Firmware_Instructions_I.pdf" class="table-icon-link"><i class="fas fa-download mr-1"></i>Open</a></td>
                </tr>
                <tr>
                    <td>KNG-P Portable Radio User's Manual</td>
                    <td><a target="_blank" href="<?php echo base_url(); ?>assets/images/KNG-P_User's_Manual_03-17.pdf" class="table-icon-link"><i class="fas fa-download mr-1"></i>Open</a></td>
                </tr>
                <tr>
                    <td>KAA0303-1 User's Manual</td>
                    <td><a target="_blank" href="<?php echo base_url(); ?>assets/images/KAA0303_User_Manual.pdf" class="table-icon-link"><i class="fas fa-download mr-1"></i>Open</a></td>
                </tr>


                </tbody>
            </table>
        </div>
    </div>
</div>