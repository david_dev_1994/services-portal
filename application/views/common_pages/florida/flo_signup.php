<?php
/**
 * Created by PhpStorm.
 * User: Maddy
 * Date: 08-Oct-18
 * Time: 4:54 PM
 */?>

<div class="header" xmlns="http://www.w3.org/1999/html">
    <div class="header-top">
        <div class="container-fluid">
            <div class="row align-items-center justify-content-between">
                <div class="col-md-auto">
                    <div class="logo-wrap">
                        <a href="<?=base_url('flfs')?>">
                            <img class="pad " src="<?=base_url('assets/styles/images/florida.png')?>" alt="BK Logo">
                        </a>
                    </div>
                </div>
                <div class="col-md-auto">
                    <div class="header-top-content">
                        <h3 class="phone">
                            <i class="fas fa-phone"></i>
                            <a href="tel: 1-800-422-6281">1-800-422-6281</a>
                        </h3>
                        <h3 class="email">
                            <i class="fas fa-envelope"></i>
                            <a href="mailto: service@bktechnologies.com">service@bktechnologies.com</a>
                        </h3>
                    </div>
                </div>
                <div class="col-md-auto">
                    <div class="logo-wrap bk-logo-portal">
                        <a href="https://www.bktechnologies.com" target="_blank">
                            <img src="<?=base_url('assets/styles/images/bk-logo-portal.png')?>" alt="BK Logo">
                        </a>
                    </div>
                </div>
<!--                <div class="col-md-auto">-->
<!--                    <div class="logo-wrap">-->
<!--                        <a href="--><?//=base_url()?><!--">-->
<!--                            <img src="--><?//= base_url('assets/styles/images/bk-logo.jpg')?><!--" alt="BK Logo">-->
<!--                        </a>-->
<!--                    </div>-->
<!--                </div>-->

            </div>
        </div>
    </div>
    <nav class="mb-1 navbar navbar-expand-lg navbar-dark info-color">
        <a class="navbar-brand" href="<?=base_url()?>">BK Technologies</a>
        <button class="navbar-toggler collapsed" type="button" data-toggle="collapse" data-target="#navbarSupportedContent-4" aria-controls="navbarSupportedContent-4" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="navbar-collapse collapse" id="navbarSupportedContent-4" style="">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item active">
                            <a class="nav-link waves-effect waves-light" href="<?=base_url('flo_login')?>">
                                <i class="fas fa-power-off"></i> Login
                            </a>
                </li>
            </ul>
        </div>
    </nav>
</div>

<div class="page-main">
    <div class="container">

        <div class="card login-card">

            <?php if (isset($msg)){?>
                <div class="errors_container alert alert-danger alert-dismissable">
                    <?= $msg?>
                </div>
            <?php }?>
            <?php if ($this->session->flashdata('message')){?>
                <div class="alert alert-success alert-dismissable">
                    <?= $this->session->flashdata('message')?>
                </div>
            <?php }?>

            <h5 class="card-header info-color white-text text-center py-4">
                <strong>Request Florida Forest Service Account</strong>
            </h5>
            <!--Card content-->
            <div class="card-body px-lg-5 pt-0">
                <!-- Form -->
                <form class="as-login-form" action="<?=base_url('/flo_register')?>" method="post">
                    <!-- Email -->
                    <div class="md-form">
                        <label for="username">Username</label>
                        <input required id="username" class='form-control' type="text" name="username" value="<?php echo set_value('username'); ?>" >
                    </div>
                    <!-- Password -->
                    <div class="md-form">
                        <label for="email">Email</label>
                        <input name="email" required type="email" id="email" class='form-control' value="<?php echo set_value('email'); ?>" >
                    </div>

                    <div class="md-form">
                        <label for="password">Password</label>
                        <input  name="password" required id="password" type="password" class='form-control' value="<?php echo set_value('password'); ?>">
                    </div>

                    <div class="md-form">
                        <label for="password_rpt">Password Repeat</label>
                        <input name="password_rpt" required id="password_rpt" type="password" class='form-control' value="<?php echo set_value('password_rpt'); ?>">
                    </div>

                    <div class="md-form">
                        <label for="number">Phone</label>
                        <input name="number" type="number" id="number" class='form-control' value="<?php echo set_value('number'); ?>" >
                        <input type="hidden" name="access" value="2">
                    </div>

                    <div class="md-form">
                        <select required name="acc_type" id="acc_type" class='form-control'>
                            <option disabled selected >Select Account Type</option>
                            <option value="1">Dealer</option>
                            <option value="2">Staff</option>
                        </select>
<!--                        <label for="account_type">Account Type</label>-->
                    </div>

                    <!-- Sign in button -->
                    <button class="btn btn-info btn-block my-4 waves-effect z-depth-0" type="submit">Submit</button>
                    <!-- Register -->
                    <p>Already have a registered account?
                        <a href="<?=base_url('flo_signIn')?>">Log-in here</a>
                    </p>
                </form>
                <!-- Form -->
            </div>
        </div>

    </div>
</div>
