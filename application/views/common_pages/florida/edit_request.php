<?php
/**
 * Created by PhpStorm.
 * User: Maddy
 * Date: 03-Oct-18
 * Time: 5:40 PM
 */
?>

<div class="header" xmlns="http://www.w3.org/1999/html">
    <div class="header-top">
        <div class="container-fluid">
            <div class="row align-items-center justify-content-between">
                <div class="col-md-auto">
                    <div class="logo-wrap">
                        <a href="<?=base_url('flfs')?>">
                            <img class="pad " src="<?=base_url('assets/styles/images/florida.png')?>" alt="BK Logo">
                        </a>
                    </div>
                </div>
                <div class="col-md-auto">
                    <div class="header-top-content">
                        <h3 class="phone">
                            <i class="fas fa-phone"></i>
                            <a href="tel: 1-800-422-6281">1-800-422-6281</a>
                        </h3>
                        <h3 class="email">
                            <i class="fas fa-envelope"></i>
                            <a href="mailto: service@bktechnologies.com">service@bktechnologies.com</a>
                        </h3>
                    </div>
                </div>
                <div class="col-md-auto">
                    <div class="logo-wrap bk-logo-portal">
                        <a href="https://www.bktechnologies.com" target="_blank">
                            <img src="<?=base_url('assets/styles/images/bk-logo-portal.png')?>" alt="BK Logo">
                        </a>
                    </div>
                </div>
<!--                <div class="col-md-auto">-->
<!--                    <div class="logo-wrap">-->
<!--                        <a href="--><?//=base_url()?><!--">-->
<!--                            <img src="--><?//= base_url('assets/styles/images/bk-logo.jpg')?><!--" alt="BK Logo">-->
<!--                        </a>-->
<!--                    </div>-->
<!--                </div>-->

            </div>
        </div>
    </div>
    <nav class="mb-1 navbar navbar-expand-lg navbar-dark info-color">
        <a class="navbar-brand" href="#">BK Technologies</a>
        <button class="navbar-toggler collapsed" type="button" data-toggle="collapse" data-target="#navbarSupportedContent-4" aria-controls="navbarSupportedContent-4" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
<!--        <div class="navbar-collapse collapse" id="navbarSupportedContent-4" style="">-->
<!--            <ul class="navbar-nav ml-auto">-->
<!--                -->
<!--                <li class="nav-item active">-->
<!--                    <a class="nav-link waves-effect waves-light" href="login.html">-->
<!--                        --><?php //if (!$this->session->access_level){?>
<!--                            <a class="nav-link waves-effect waves-light" href="--><?//=base_url('login')?><!--">-->
<!--                                <i class="fas fa-power-off"></i> Login-->
<!--                            </a>-->
<!--                        --><?php //}else{?>
<!--                            <a class="nav-link waves-effect waves-light" href="--><?//=base_url('logout')?><!--">-->
<!--                                <i class="fas fa-power-off"></i> Logout-->
<!--                            </a>-->
<!--                        --><?php //}?>
<!---->
<!--                    </a>-->
<!--                </li>-->
<!--            </ul>-->
<!--        </div>-->
        <div class="navbar-collapse collapse" id="navbarSupportedContent-4" style="">
            <ul class="navbar-nav ml-auto">

<!--                <li class="nav-item">-->
<!--                    <a class="nav-link waves-effect waves-light" href="--><?//=base_url('f_restricted')?><!--">-->
<!--                        <i class="fas fa-download"></i> Downloads-->
<!--                    </a>-->
<!--                </li>-->

                <li class="nav-item">
                    <a class="nav-link waves-effect waves-light" href="<?=base_url('flfs')?>">
                        <i class="fas fa-home"></i> Return To Dashboard
                    </a>
                </li>

                <li class="nav-item active">
                    <?php if (!$this->session->access_level){?>
                        <a class="nav-link waves-effect waves-light" href="<?=base_url('login')?>">
                            <i class="fas fa-power-off"></i> Login
                        </a>
                    <?php }else{?>
                        <a class="nav-link waves-effect waves-light" href="<?=base_url('logout')?>">
                            <i class="fas fa-power-off"></i> Logout
                        </a>
                    <?php }?>
                </li>
            </ul>
        </div>
    </nav>
</div>


<?php if(!isset($list1)) {?>

<div class="page-main">
    <div class="container">
        <div class="form-page">
            <div class="row justify-content-center">
                <div class="col-md-3">
                    <div class="btn-wrap">
                        <a href="<?=base_url('f_restricted')?>" class="btn btn-info btn-block"><i class="fas fa-download"></i>Downloads</a>
                    </div>
                </div>
            </div>
            <br>

            <h2 class="form-heading">Florida Forest Service Repair Form</h2>

            <?php if (isset($err_msg)){?>
                <div class=" alert alert-danger alert-dismissable">
                    <?= $err_msg?>
                </div>
            <?php }?>
            <?php if (isset($succ_msg)){?>
                <div class="text-center alert alert-success alert-dismissable">
                    <?= $succ_msg?>
                </div>
            <?php }?>
            <?php if ($this->session->flashdata("validation_errors")){?>
                <div class="text-center alert alert-danger alert-dismissable">
                    <?= $this->session->flashdata("validation_errors")?>
                </div>
            <?php }?>




            <form id="cal_fire" name="cal_fire" action="<?=base_url('General/Florida_update')?>" method="post">
                <div class="row">
                    <div class="col-lg-4">
                        <div class="form-group row">
                            <label for="inputEmail3MD" class="col-auto col-form-label">PO#</label>
                            <div class="col">
                                <div class="md-form mt-0">
                                    <input name="pu" type="text" required="required"  class="form-control" value="<?php echo $list[0]->pu;?>" id="inputEmail3MD">
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-4">
                        <div class="form-group row">
                            <label for="inputEmail3MD" class="col-auto col-form-label">Requested:</label>
                            <div class="col">
                                <div class="md-form mt-0">
                                    <input name="requested" type="text" value="<?php echo $list[0]->requested; ?>" required="required" autocomplete="off" class="form-control" id="inputEmail3MD">
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-4">
                        <div class="form-group row">
                            <label for="inputEmail3MD" class="col-auto col-form-label">PO Status</label>
                            <div class="col">
                                <div class="md-form mt-0">
                                    <select required="required" name="PO_Status" class="form-control">
                                        <option value="<?php echo $list[0]->PO_Status;?> " selected="selected"><?php echo $list[0]->PO_Status;?></option>
                                        <option value="Submitted">Submitted</option>
                                        <option value="In_Process">In_Process</option>
                                        <option value="Shipped">Shipped</option>
                                        <option value="Approved">Approved</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <!--                new insertions-->
                <div class="row">
                    <div class="col-lg-4">
                        <div class="form-group row">
                            <label for="inputEmail3MD" class="col-auto col-form-label">BK Tracking #</label>
                            <div class="col">
                                <div class="md-form mt-0">
                                    <input name="BK_TN" type="text" value="<?=$list[0]->BK_TN?>" required="required"  class="form-control" id="inputEmail3MD">
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-4">
                        <div class="form-group row">
                            <label for="inputEmail3MD" class="col-auto col-form-label">Ship Date:</label>
                            <div class="col">
                                <div class="md-form mt-0">
                                    <input name="Ship_Date" value="<?=$list[0]->Ship_Date?>" type="date" required="required" autocomplete="off" class="form-control" id="inputEmail3MD">
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-4">
                        <div class="form-group row">
                            <label for="inputEmail3MD" class="col-auto col-form-label">Repair Status</label>
                            <div class="col">
                                <div class="md-form mt-0">
                                    <select required="required" name="Repair_Status" class="form-control">
                                        <option value="<?php echo $list[0]->Repair_Status;?> " selected="selected"><?php echo $list[0]->Repair_Status;?></option>
                                        <option value="Review">Review</option>
                                        <option value="Repairable">Repairable</option>
                                        <option value="Un-Repairable">Un-Repairable</option>
                                        <option value="Not Supported">Not Supported</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--                new insertions ends-->


                <div class="row warranty-checkbox">
                    <div class="col-lg-6">
                        <div class="custom-control custom-checkbox">
                            <input type="hidden" name="warranyreplace" value="0">
                            <input type="checkbox" value="1" name="warranyreplace" <?php if($list[0]->warranyreplace==1){ echo "checked";} ?> class="custom-control-input" id="checkbox_1">
                            <label class="custom-control-label" for="checkbox_1">If this box is checked, this is warranty replacement part request.</label>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-6">
                        <div class="form-group row">
                            <label for="inputEmail3MD" class="col-auto col-form-label">Company</label>
                            <div class="col">
                                <div class="md-form mt-0">
                                    <select required="required" name="Company" class="form-control">
                                        <option value="<?php echo $list[0]->Company;?>" selected="selected"><?php echo $list[0]->Company;?></option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group row">
                            <label for="inputEmail3MD" class="col-auto col-form-label">Address</label>
                            <div class="col">
                                <div class="md-form mt-0">
                                    <input name="address" type="text" required="required" value="<?php echo $list[0]->address;?>" autocomplete="off" class="form-control" id="inputEmail3MD">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-6">
                        <div class="form-group row">
                            <label for="inputEmail3MD" class="col-auto col-form-label">City</label>
                            <div class="col">
                                <div class="md-form mt-0">
                                    <input name="city" type="text" value="<?php echo $list[0]->city;?>" required="required" autocomplete="off" class="form-control" id="inputEmail3MD">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group row">
                            <label for="inputEmail3MD" class="col-auto col-form-label">Zip Code</label>
                            <div class="col">
                                <div class="md-form mt-0">
                                    <input name="zip" type="text" value="<?php echo $list[0]->zip;?>" required="required" autocomplete="off" class="form-control" id="inputEmail3MD">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-6">
                        <div class="form-group row">
                            <label for="inputEmail3MD" class="col-auto col-form-label">Phone Number</label>
                            <div class="col">
                                <div class="md-form mt-0">
                                    <input name="pnum" type="text" value="<?php echo $list[0]->pnum;?>" required="required" autocomplete="off" class="form-control" id="inputEmail3MD">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group row">
                            <label for="inputEmail3MD" class="col-auto col-form-label">Email</label>
                            <div class="col">
                                <div class="md-form mt-0">
                                    <input name="email" type="email" value="<?php echo $list[0]->email;?>" required="required" autocomplete="off" class="form-control" id="inputEmail3MD">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-6">
                        <div class="form-group row">
                            <label for="inputEmail3MD" class="col-auto col-form-label">Contact Person Name</label>
                            <div class="col">
                                <div class="md-form mt-0">
                                    <input name="person_name" type="text" value="<?php echo $list[0]->person_name;?>" required="required" autocomplete="off" class="form-control" id="inputEmail3MD">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


                <div class="row warranty-checkbox">
                    <div class="col">
                        <div class="custom-control custom-checkbox">
                            <input type="hidden" name="credit_card_info" value="0">
                            <input type="checkbox" value="1" <?php if($list[0]->credit_card_info==1){ echo "checked";} ?> name="credit_card_info" selected="selected" class="custom-control-input" id="checkbox_2">
                            <label class="custom-control-label" for="checkbox_2">Call for credit card information</label>
                        </div>
                    </div>
                </div>


                <div class="row">
                    <div class="col-lg-12">
                        <div class="form-group row">
                            <label for="inputEmail3MD" class="col-12 col-form-label">Customer Tracking Number:</label>
                            <div class="col-sm-12">
                                <div class="md-form mt-0">
                                    <textarea name="CustomerTrackingNumber" class="form-control rounded-0" rows="5"><?php echo $list[0]->CustomerTrackingNumber;?></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-6">
                        <div class="form-group row">
                            <label for="inputEmail3MD" class="col-auto col-form-label">Model</label>
                            <div class="col">
                                <div class="md-form mt-0">
                                    <input required="required" name="Model" value="<?php echo $list[0]->Model;?>" type="text" class="form-control" id="inputEmail3MD">
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-6">
                        <div class="form-group row">
                            <label for="inputEmail3MD" class="col-auto col-form-label">	Warranty Status</label>
                            <div class="col">
                                <div class="md-form mt-0">
                                    <select required="required" name="WarrantyStatus" class="form-control">
                                        <option value="<?php echo $list[0]->WarrantyStatus;?>" selected="selected"><?php echo $list[0]->WarrantyStatus;?></option>
                                        <option value="Expired">&nbsp;Expired</option>
                                        <option value="Warranty">&nbsp;Warranty</option>
                                        <option value="Warranty_Recall">&nbsp;Warranty_Recall</option>
                                        <option value="Non_Warranty">&nbsp;Non_Warranty</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-6">
                        <div class="form-group row">
                            <label for="inputEmail3MD" class="col-auto col-form-label">Qty</label>
                            <div class="col">
                                <div class="md-form mt-0">
                                    <input required="required" name="Qty" type="text" size="3" value="<?php echo $list[0]->Qty;?>" class="form-control" id="inputEmail3MD">
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-6">
                        <div class="form-group row">
                            <label for="inputEmail3MD" class="col-auto col-form-label">Serial #</label>
                            <div class="col">
                                <div class="md-form mt-0">
                                    <input required="required" name="Serial" type="text" size="25" value="<?php echo $list[0]->Serial;?>" class="form-control" id="inputEmail3MD">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-4">
                        <h3 class="checkbox-heading">Audio</h3>
                        <div class="custom-control custom-checkbox">
                            <input type="hidden" name="NoAudio" value="0">
                            <input type="checkbox" name="NoAudio" value="1" <?php if($list[0]->NoAudio==1){ echo "checked"; } ?> selected="selected"  class="custom-control-input" id="audio_1">
                            <label class="custom-control-label" for="audio_1">No Audio</label>
                        </div>
                        <div class="custom-control custom-checkbox">
                            <input type="hidden" name="LowAudio" value="0">
                            <input type="checkbox" name="LowAudio" value="1" <?php if($list[0]->LowAudio==1){ echo "checked"; } ?> selected="selected" class="custom-control-input" id="audio_2">
                            <label class="custom-control-label" for="audio_2">Low Audio</label>
                        </div>
                        <div class="custom-control custom-checkbox">
                            <input type="hidden" name="IntermittentAudio" value="0">
                            <input type="checkbox" name="IntermittentAudio" value="1" <?php if($list[0]->IntermittentAudio==1){ echo "checked"; } ?> selected="selected" class="custom-control-input" id="audio_3">
                            <label class="custom-control-label" for="audio_3">Intermittent Audio</label>
                        </div>
                        <div class="custom-control custom-checkbox">
                            <input type="hidden" name="NoTXAudio" value="0">
                            <input type="checkbox" name="NoTXAudio" value="1" <?php if($list[0]->NoTXAudio==1){ echo "checked"; } ?> selected="selected" class="custom-control-input" id="audio_4">
                            <label class="custom-control-label" for="audio_4">No TX Audio</label>
                        </div>
                        <div class="custom-control custom-checkbox">
                            <input type="hidden" name="NoPLDPL" value="0">
                            <input type="checkbox" name="NoPLDPL" value="1" <?php if($list[0]->NoPLDPL==1){ echo "checked"; } ?> selected="selected" class="custom-control-input" id="audio_5">
                            <label class="custom-control-label" for="audio_5">No PL/DPL</label>
                        </div>
                        <div class="custom-control custom-checkbox">
                            <input type="hidden" name="ConstantAudion" value="0">
                            <input type="checkbox" name="ConstantAudion" value="1" <?php if($list[0]->ConstantAudion==1){ echo "checked"; } ?> selected="selected" class="custom-control-input" id="audio_6">
                            <label class="custom-control-label" for="audio_6">Constant Audion</label>
                        </div>
                        <div class="custom-control custom-checkbox">
                            <input type="hidden" name="WillnotSquelch" value="0">
                            <input type="checkbox" name="WillnotSquelch" <?php if($list[0]->WillnotSquelch==1){ echo "checked"; } ?> value="1" selected="selected" class="custom-control-input" id="audio_7">
                            <label class="custom-control-label" for="audio_7">Will not Squelch</label>
                        </div>
                    </div>

                    <div class="col-lg-4">
                        <h3 class="checkbox-heading">Display/Keypad</h3>
                        <div class="custom-control custom-checkbox">
                            <input type="hidden" name="LCD" value="0">
                            <input type="checkbox" name="LCD" value="1" <?php if($list[0]->LCD==1){ echo "checked"; } ?> selected="selected" class="custom-control-input" id="display_1">
                            <label class="custom-control-label" for="display_1">LCD</label>
                        </div>
                        <div class="custom-control custom-checkbox">
                            <input type="hidden" name="Keypad" value="0">
                            <input type="checkbox" name="Keypad" value="1" <?php if($list[0]->Keypad==1){ echo "checked"; } ?> selected="selected" class="custom-control-input" id="display_2">
                            <label class="custom-control-label" for="display_2">Keypad</label>
                        </div>
                        <div class="custom-control custom-checkbox">
                            <input type="hidden" name="NoDisplay" value="0">
                            <input type="checkbox" name="NoDisplay" value="1" <?php if($list[0]->NoDisplay==1){ echo "checked"; } ?> selected="selected" class="custom-control-input" id="display_3">
                            <label class="custom-control-label" for="display_3">No Display</label>
                        </div>
                        <div class="custom-control custom-checkbox">
                            <input type="hidden" name="LEDFaulty" value="0">
                            <input type="checkbox" name="LEDFaulty" value="1" <?php if($list[0]->LEDFaulty==1){ echo "checked"; } ?> selected="selected" class="custom-control-input" id="display_4">
                            <label class="custom-control-label" for="display_4">LED Faulty</label>
                        </div>
                    </div>

                    <div class="col-lg-4">
                        <h3 class="checkbox-heading">Damage</h3>
                        <div class="custom-control custom-checkbox">
                            <input type="hidden" name="Housing" value="0">
                            <input type="checkbox" name="Housing" value="1" <?php if($list[0]->Housing==1){ echo "checked"; } ?> selected="selected" class="custom-control-input" id="damage_1">
                            <label class="custom-control-label" for="damage_1">Housing</label>
                        </div>
                        <div class="custom-control custom-checkbox">
                            <input type="hidden" name="KnobsButtonsSwitches" value="0">
                            <input type="checkbox" name="KnobsButtonsSwitches"  <?php if($list[0]->KnobsButtonsSwitches==1){ echo "checked"; } ?> value="1" selected="selected" class="custom-control-input" id="damage_2">
                            <label class="custom-control-label" for="damage_2">Knobs/Buttons/Switches</label>
                        </div>
                        <div class="custom-control custom-checkbox">
                            <input type="hidden" name="Antenna" value="0">
                            <input type="checkbox" name="Antenna" value="1" <?php if($list[0]->Antenna==1){ echo "checked"; } ?> selected="selected" class="custom-control-input" id="damage_3">
                            <label class="custom-control-label" for="damage_3">Antenna</label>
                        </div>
                        <div class="custom-control custom-checkbox">
                            <input type="hidden" name="Label" value="0">
                            <input type="checkbox" name="Label" value="1" <?php if($list[0]->Label==1){ echo "checked"; } ?> selected="selected" class="custom-control-input" id="damage_4">
                            <label class="custom-control-label" for="damage_4">Label</label>
                        </div>
                        <div class="custom-control custom-checkbox">
                            <input type="hidden" name="BatteryContacts" value="0">
                            <input type="checkbox" name="BatteryContacts" value="1" <?php if($list[0]->BatteryContacts==1){ echo "checked"; } ?> selected="selected" class="custom-control-input" id="damage_5">
                            <label class="custom-control-label" for="damage_5">Battery Contacts</label>
                        </div>
                        <div class="custom-control custom-checkbox">
                            <input type="hidden" name="PhysicalDamage" value="0">
                            <input type="checkbox" name="PhysicalDamage" value="1" <?php if($list[0]->PhysicalDamage==1){ echo "checked"; } ?> selected="selected" class="custom-control-input" id="damage_6">
                            <label class="custom-control-label" for="damage_6">Physical Damage</label>
                        </div>
                        <div class="custom-control custom-checkbox">
                            <input type="hidden" name="ChemicalDamage" value="0">
                            <input type="checkbox" name="ChemicalDamage" value="1" <?php if($list[0]->ChemicalDamage==1){ echo "checked"; } ?> selected="selected" class="custom-control-input" id="damage_7">
                            <label class="custom-control-label" for="damage_7">Chemical Damage</label>
                        </div>
                        <div class="custom-control custom-checkbox">
                            <input type="hidden" name="LiquidDamage" value="0">
                            <input type="checkbox" name="LiquidDamage" value="1" <?php if($list[0]->LiquidDamage==1){ echo "checked"; } ?> selected="selected" class="custom-control-input" id="damage_8">
                            <label class="custom-control-label" for="damage_8">Liquid Damage</label>
                        </div>
                        <div class="custom-control custom-checkbox">
                            <input type="hidden" name="Dead" value="0">
                            <input type="checkbox" name="Dead" value="1"  <?php if($list[0]->Dead==1){ echo "checked"; } ?> selected="selected" class="custom-control-input" id="damage_9">
                            <label class="custom-control-label" for="damage_9">Dead</label>
                        </div>
                        <div class="custom-control custom-checkbox">
                            <input type="hidden" name="Fuse" value="0">
                            <input type="checkbox" name="Fuse" value="1" <?php if($list[0]->Fuse==1){ echo "checked"; } ?> selected="selected" class="custom-control-input" id="damage_10">
                            <label class="custom-control-label" for="damage_10">Fuse</label>
                        </div>
                    </div>
                </div>


                <div class="bottom-checkboxes">
                    <h3 class="checkbox-heading">Radio Functions</h3>
                    <div class="row">
                        <div class="col-lg-4">
                            <div class="custom-control custom-checkbox">
                                <input type="hidden" name="PoorRXSens" value="0">
                                <input type="checkbox" name="PoorRXSens" value="1" <?php if($list[0]->PoorRXSens==1){ echo "checked"; } ?> selected="selected" class="custom-control-input" id="radio_1">
                                <label class="custom-control-label" for="radio_1">Poor RX Sens</label>
                            </div>
                        </div>

                        <div class="col-lg-4">
                            <div class="custom-control custom-checkbox">
                                <input type="hidden" name="UnabletoReadProgram" value="0">
                                <input type="checkbox" name="UnabletoReadProgram" <?php if($list[0]->UnabletoReadProgram==1){ echo "checked"; } ?> value="1" selected="selected" class="custom-control-input" id="radio_2">
                                <label class="custom-control-label" for="radio_2">Unable to Read/Program</label>
                            </div>
                        </div>

                        <div class="col-lg-4">
                            <div class="custom-control custom-checkbox">
                                <input type="hidden" name="IntermittentRX" value="0">
                                <input type="checkbox" name="IntermittentRX" <?php if($list[0]->IntermittentRX==1){ echo "checked"; } ?> value="1" selected="selected" class="custom-control-input" id="radio_3">
                                <label class="custom-control-label" for="radio_3">Intermittent RX</label>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-4">
                            <div class="custom-control custom-checkbox">
                                <input type="hidden" name="NoScan" value="0">
                                <input type="checkbox" name="NoScan" value="1"  <?php if($list[0]->NoScan==1){ echo "checked"; } ?> selected="selected" class="custom-control-input" id="radio_4">
                                <label class="custom-control-label" for="radio_4">No Scan</label>
                            </div>
                        </div>

                        <div class="col-lg-4">
                            <div class="custom-control custom-checkbox">
                                <input type="hidden" name="NoTXr" value="0">
                                <input type="checkbox" name="NoTXr" value="1" <?php if($list[0]->NoTXr==1){ echo "checked"; } ?> selected="selected" class="custom-control-input" id="radio_5">
                                <label class="custom-control-label" for="radio_5">No TXr</label>
                            </div>
                        </div>

                        <div class="col-lg-4">
                            <div class="custom-control custom-checkbox">
                                <input type="hidden" name="IntermittentPower" value="0">
                                <input type="checkbox" name="IntermittentPower" value="1" <?php if($list[0]->IntermittentPower==1){ echo "checked"; } ?> selected="selected" class="custom-control-input" id="radio_6">
                                <label class="custom-control-label" for="radio_6">Intermittent Power</label>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-4">
                            <div class="custom-control custom-checkbox">
                                <input type="hidden" name="NoTrunking" value="0">
                                <input type="checkbox" name="NoTrunking" <?php if($list[0]->NoTrunking==1){ echo "checked"; } ?> value="1" selected="selected" class="custom-control-input" id="radio_7">
                                <label class="custom-control-label" for="radio_7">No Trunking</label>
                            </div>
                        </div>

                        <div class="col-lg-4">
                            <div class="custom-control custom-checkbox">
                                <input type="hidden" name="NoPower" value="0">
                                <input type="checkbox" name="NoPower" <?php if($list[0]->NoPower==1){ echo "checked"; } ?> value="1" selected="selected" class="custom-control-input" id="radio_8">
                                <label class="custom-control-label" for="radio_8">No Power</label>
                            </div>
                        </div>

                        <div class="col-lg-4">
                            <div class="custom-control custom-checkbox">
                                <input type="hidden" name="FailCode" value="0">
                                <input type="checkbox" name="FailCode" <?php if($list[0]->FailCode==1){ echo "checked"; } ?> value="1" selected="selected" class="custom-control-input" id="radio_9">
                                <label class="custom-control-label" for="radio_9">Fail Code (list code below)</label>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-lg-4">
                            <div class="custom-control custom-checkbox">
                                <input type="hidden" name="NoRX" value="0">
                                <input type="checkbox" name="NoRX" value="1" <?php if($list[0]->NoRX==1) echo 'checked="checked"'; ?> selected="selected" class="custom-control-input" id="radio_10">
                                <label class="custom-control-label" for="radio_10">No RX</label>
                            </div>
                        </div>

                        <div class="col-lg-4">
                            <div class="custom-control custom-checkbox">
                                <input type="hidden" name="LowPower" value="0">
                                <input type="checkbox" name="LowPower" <?php if($list[0]->LowPower==1){ echo "checked"; } ?> value="1" selected="selected" class="custom-control-input" id="radio_11">
                                <label class="custom-control-label" for="radio_11">Low Power</label>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="complaint-box">
                    <div class="custom-control custom-checkbox">
                        <input type="hidden" name="otherlistcomplaintbelow" value="0">
                        <input type="checkbox" name="otherlistcomplaintbelow" <?php if($list[0]->otherlistcomplaintbelow==1){ echo "checked"; }?> value="1" selected="selected" class="custom-control-input" id="complaint_1">
                        <label class="custom-control-label" for="complaint_1">Other (list complaint below)</label>
                    </div>
                    <div class="form-group row">
                        <div class="col-12">
                            <div class="md-form mt-0">
                                <textarea class="form-control rounded-0" name="otherlistcomplaintbelowtext" rows="5"><?php echo $list[0]->otherlistcomplaintbelowtext; ?></textarea>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-12">
                        <div class="form-group row">
                            <label for="inputEmail3MD" class="col-12 col-form-label">Comments</label>
                            <div class="col-sm-12">
                                <div class="md-form mt-0">
                                    <textarea name="Complaints" class="form-control rounded-0" rows="5"><?=$list[0]->Complaints?></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


                <div class="form-submit-wrap">
<!--                    <input type="hidden" id="Repair_Status" name="Repair_Status" value="Review">-->
<!--                    <input type="hidden" id="PO_Status" name="PO_Status" value="Submitted">-->
                    <input type="hidden" name="id" value="<?=$list[0]->id?>">
                    <button type="button" onclick="window.print();" class="btn btn-primary"><i class="fas fa-print mr-1"></i> Print</button>
                <?php if($this->session->is_admin or $this->session->access_level==2){?>
                    <button name="updateflorida" type="submit" id="Submit" form="cal_fire" value="Update" class="btn btn-success"><i class="fas fa-check mr-1"></i> Update</button>
                <?php } ?>
                </div>


            </form>


        </div>
    </div>
</div>

<?php }else{?>
    <div class="row form-page">
        <h2 class="form-handling">
            Oooops!!! No Data Found
        </h2>
    </div>
<?php }?>
