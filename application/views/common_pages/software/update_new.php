<?php
//     if(isset($_SESSION["ProductNumber"]))
// 		$ProductNumber = $_SESSION["ProductNumber"];
//     else
// 		$ProductNumber = '';
?>
<section id="main-conent">

    <div class="container text-center" id="software_update">

        <?php

        if ($_POST){

        if (isset($softwares)){

        ?>

        <p>Available Revisions for <?php echo $partname; ?></p>

        <div class="col-md-8 col-md-offset-2">

            <table class="table table-responsive table-striped table-hover table-bordered">

                <thead>

                <tr>

                    <th>Release Date</th>

                    <th>Revision Number</th>

                    <th>Part Number</th>

                    <th colspan="2">Release Notes</th>



                </tr>

                </thead>

                <tbody>

                <?php

                if (count($softwares) > 0) {

                    foreach ($softwares as $software) {

                        if($software->Version != '5.6.0a'){

                        echo "<tr><td>" . $software->Date . "</td><td>" . $software->Version . "</td><td>" . $software->PartNumber . "</td><td>" . $software->Notes . "</td><td><a href=".'#'." data-toggle=".'modal'." data-revision=" . $software->Version . " data-editor=". $software->PartNumber. " data-file_name=". $software->File_Name. " data-target=".'#select-Product'.">Download</a></td></tr>";

                        // echo "<tr><td>" . $software->Date . "</td><td>" . $software->Version . "</td><td>" . $software->PartNumber . "</td><td>" . $software->Notes . "</td><td><a href=".'/assets/files/software/'.$software->File_Name.">Download</a></td></tr>";

                    } }

                } else {

                    echo '<tr><td colspan="4">There are no editors avialable for this product.</td></tr>';

                }

                ?>

                </tbody>

            </table>

            <?php

            } else if (isset($message) && $message == "show") {

            ?>

                <p>You have entered an invalid ID number</p> <br>

                <a href="<?= base_url('software/update/'.$ProductNumber); ?>">Try Again</a>

            <?php                          

            }

            } else {

                ?>

                <h3 class="su__header">

                    BK Technologies<span style="vertical-align: super !important;font-size: 12px !important;">&reg;</span>

                    Radio Software Downloader

                </h3>

                <form method="post">

                    <div class="su__content">

                        <p>

                            To proceed to the software update page please enter the serial number from your software

                            disk.

                        </p>



                        <div>



                            <span style="text-align: left">Serial Number:</span>

                            <div>

                                <input maxlength="3" id="T1" name="T1"

                                       onkeyup="if (this.value.length == 3) document.getElementById('T2').focus();"

                                       type="text"> - <input id="T2" name="T2" maxlength="3"

                                                             onkeyup="if (this.value.length == 3) document.getElementById('T3').focus();"

                                                             type="text"> - <input maxlength="3" id="T3" name="T3"

                                                                                   type="text">

                            </div>

                            <button class="continue" type="submit">Continue</button>

                        </div>

                        <div style="justify-content: unset; margin-top: 10px; display: none">



                            <span style="padding-left: 5px; width: 169px; text-align: left">Product Number:</span>

                        </div>

                        <p>

                            For assistance, please contact tech support at <a href="tel:8004226281">(800) 422 6281.</a>

                        </p>

                    </div>



                </form>

                <?php

            }

            ?>

        </div>

</section>



<!-- Product POPUP -->

<div class="modal fade" id="select-Product" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

    <div class="modal-dialog">

        <div class="modal-content">



            <div class="modal-header">

                <h4 class="modal-title" id="myModalLabel">Please fill out the following information to enable download</h4>

                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>

            </div>



            <div class="modal-body">

                <div class="container-fluid">

                    <div class="row">

                        <form id="softFormSubmit">

                            <div class="col-md-12 form-group row-group">

                                <div id="errorToModal" class="errorToModal alert alert-danger alert-dismissible text-center row hidden"></div>

                                <div class="col-md-6 form-group">

                                    <label for="validationCustom01">Name: </label>

                                    <input id="input-field" name="name" class="personName form-control" required type="text">

                                </div>

                                <div class="col-md-6 form-group">

                                    <label for="validationCustom01">Company Name: </label>

                                    <input id="input-field" name="companyName" class="companyName form-control" required type="text">

                                </div>

                                <div class="col-md-6 form-group">

                                    <label for="validationCustom01">Number of Radios: </label>

                                    <input id="input-field" name="radioNumber" class="radioNumber form-control" value="1" required type="number" min="1">

                                </div>

                                <div class="col-md-6 form-group">

                                    <label for="validationCustom01">Email Address: </label>

                                    <input id="input-field" name="email" class="email form-control" type="email" required>

                                </div>

                                <div class="col-md-6 form-group">

                                    <label for="validationCustom01">Phone Number: </label>

                                    <input id="input-field" name="pnum" class="pnum form-control" type="text" required>

                                </div>

                                <div class="col-md-6 form-group">

                                    <label for="validationCustom01">Serial Number: </label>

                                    <input id="input-field" name="serial_number" class="serial_number form-control" type="text" required>

                                </div>

                                <div class="col-md-12 form-group">

                                    <label for="validationCustom01">Current Software version: </label>

                                    <input id="input-field" name="soft_version" class="soft_version form-control" type="text" required>

                                </div>

                                <div class="col-md-6 form-group" hidden>

                                    <label for="validationCustom01">Revision Number: </label>

                                    <!--                                    <div class="revisionNumberDiv"></div>-->

                                    <input id="input-field" name="revisionNumber" class="revisionNumberInp form-control" readonly type="text">

                                </div>

                                <div class="col-md-6 form-group" hidden>

                                    <label for="validationCustom01">Part Number: </label>

                                    <!--                                    <div class="editorDiv"></div>-->

                                    <input id="input-field" name="editor" class="editorInp form-control" readonly type="text">

                                </div>

                                <input id="input-field" name="fileName" class="fileName hidden">

                                <input id="input-field" name="software" value="software" class="hidden">

                            </div>

                            <div class="col-md-12 form-group">

                                <input value="SUBMIT" name="submit" class="btn btn-success form-control" type="submit">

                            </div>

                        </form>

                    </div>

                </div>

            </div>



            <div class="modal-footer">

                <!--                <button type="button" data-dismiss="modal" class="btn btn-default saveAndMore">Submit</button>-->

                <!--                <a  data-dismiss="modal" class="btn btn-primary generateQuote" onclick="return profileValidation();">Generate Quote</a>-->

                <button data-dismiss="modal" class="btn btn-primary">Close</button>

            </div>

        </div>

    </div>

</div>

<!-- END HELP POPUP -->





<style>



    button.continue {

        border-radius: 0px !important;

        background: #00a6d5 !important;

        border-color: #00a6d5 !important;

        color: #ffffff !important;

        font-family: Lato !important;

        text-transform: uppercase !important;

        font-size: 14px !important;

        display: inline-block !important;

        font-weight: 600 !important;

        text-align: center !important;

        padding: 5px 18px !important;

        transition: padding 0.5s ease, background 0.5s ease, border-color 0.5s ease, color 0.5s ease !important;

        border: none !important;

    }



    button.continue:hover {

        background: #b90000 !important;

    }



    input {

        padding: 3.5px 5px !important;

    }



</style>

<script>



    $(function () {

        $('td>a').each(function () {

            var href = $(this).attr('href');

            $(this).attr('href', '/service-portal' + href);

        });

        $('td>a>img').each(function () {

            var src = $(this).attr('src');

            $(this).attr('src', '/service-portal' + src);

        });





    });





</script>