<section id="main-conent">
<div class='container-fluid' id="f-service__pricing" style="background-image: black">
  <div class="row">
    <div class="col-md-10 col-md-offset-1" style="margin-top: 50px;" >

      <div style="background-color: #fff">
        <div class="ksnav">
          <center>
            <p>BK Technologies | 1-800-422-6281 |<a href="mailto:amunch@bktechnologies.com">amunch@bktechnologies.com</a> <a href="mailto:cwilliams@bktechnologies.com">cwilliams@bktechnologies.com</a></p>
            <h1>CAL FIRE Repair Profiles - </h1>
            <a href="">Leave Archive</a>
            <h2 style="margin-top: 50px" class="text text-danger">Repair Profile Archives</h2>
            <a class="btn btn-default" href="<?php echo base_url(); ?>CalFire/showall">Refresh Repair Archives</a>
            <a href="<?php echo base_url(); ?>CalFire/request">Request a Repair</a>
          </center>
        </div>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-md-12">
      <table class="table table-bordered table-responsive table-striped table-hover">
        <thead>
          <tr>
            <th>Edit</th>
            <th>Company </th>
            <th>Serial_Number</th>
            <th>Requested </th>
            <th>PO Status</th>
            <th>Customer Tracking</th>
            <th>Received Date</th>
            <?php if (true) {?>
            <th>Delete</th>
            <?php }?>
          </tr>
        </thead>
        <tbody>
          <?php if (sizeof($list)>=1) {
            foreach ($list as $key ) {
             
           ?>
          <tr>
            <td><a href="<?php echo base_url(); ?>CalFire/edit/<?php echo $key->id ?>">View</a></td>
            <td><?php echo $key->Company ?></td>
            <td><?php echo $key->Serial ?></td>
            <td><?php echo $key->requested ?></td>
            <td><?php echo $key->PO_Status ?></td>
            <td><?php echo $key->CustomerTrackingNumber ?></td>
            <td><?php echo $key->date ?></td>
            <?php if (true){?>
            <td><a href="<?php echo base_url(); ?>CalFire/delete/<?php echo $key->id ?>">Delete</a></td>
            <?php }?>
          </tr>

          <?php } } ?>
        </tbody>
      </table>
    </div>
  </div>
</div>
</section>