<?php
/**
 * Created by PhpStorm.
 * User: Maddy
 * Date: 03-Oct-18
 * Time: 5:40 PM
 */
?>

<div class="header" xmlns="http://www.w3.org/1999/html">
    <div class="header-top">
        <div class="container-fluid">
            <div class="row align-items-center justify-content-between">
                <div class="col-md-auto">
                    <div class="logo-wrap">
                        <a href="<?=base_url('CalFire')?>">
                            <img style="margin-left: -10px;" src="<?=base_url('assets/styles/images/calfire-logo-portal.png')?>" alt="BK Logo">
                        </a>
                    </div>
                </div>
                <div class="col-md-auto">
                    <div class="header-top-content">
                        <h3 class="phone">
                            <i class="fas fa-phone"></i>
                            <a href="tel: 1-800-422-6281">1-800-422-6281</a>
                        </h3>
                        <h3 class="email">
                            <i class="fas fa-envelope"></i>
                            <a href="mailto: service@bktechnologies.com">service@bktechnologies.com</a>
                        </h3>
                    </div>
                </div>
                <div class="col-md-auto">
                    <div class="logo-wrap bk-logo-portal">
                        <a href="https://www.bktechnologies.com" target="_blank">
                            <img src="<?=base_url('assets/styles/images/bk-logo-portal.png')?>" alt="BK Logo">
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <nav class="mb-1 navbar navbar-expand-lg navbar-dark info-color">
        <a class="navbar-brand" href="#">BK Technologies</a>
        <button class="navbar-toggler collapsed" type="button" data-toggle="collapse" data-target="#navbarSupportedContent-4" aria-controls="navbarSupportedContent-4" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
<!--        <div class="navbar-collapse collapse" id="navbarSupportedContent-4" style="">-->
<!--            <ul class="navbar-nav ml-auto">-->
<!---->
<!--                <li class="nav-item">-->
<!--                    <a class="nav-link waves-effect waves-light" href="--><?//=base_url('CalFire')?><!--">-->
<!--                        <i class="fas fa-book"></i> CalFire Home-->
<!--                    </a>-->
<!--                </li>-->
<!---->
<!--                <li class="nav-item active">-->
<!--                    <a class="nav-link waves-effect waves-light" href="login.html">-->
<!--                        --><?php //if (!$this->session->cal_access_level){?>
<!--                            <a class="nav-link waves-effect waves-light" href="--><?//=base_url('login')?><!--">-->
<!--                                <i class="fas fa-power-off"></i> Login-->
<!--                            </a>-->
<!--                        --><?php //}else{?>
<!--                            <a class="nav-link waves-effect waves-light" href="--><?//=base_url('cal_logout')?><!--">-->
<!--                                <i class="fas fa-power-off"></i> Logout-->
<!--                            </a>-->
<!--                        --><?php //}?>
<!---->
<!--                    </a>-->
<!--                </li>-->
<!--            </ul>-->
<!--        </div>-->
        <div class="navbar-collapse collapse" id="navbarSupportedContent-4" style="">
            <ul class="navbar-nav ml-auto">

                <li class="nav-item">
                    <a class="nav-link waves-effect waves-light" href="<?=base_url('CalFire')?>">
                        <i class="fas fa-home"></i> Return To Dashboard
                    </a>
                </li>
                <li class="nav-item active">
                    <?php if (!$this->session->cal_access_level){?>
                        <a class="nav-link waves-effect waves-light" href="<?=base_url('login')?>">
                            <i class="fas fa-power-off"></i> Login
                        </a>
                    <?php }else{?>
                        <a class="nav-link waves-effect waves-light" href="<?=base_url('cal_logout')?>">
                            <i class="fas fa-power-off"></i> Logout
                        </a>
                    <?php }?>
                </li>
            </ul>
        </div>
    </nav>
</div>

<div class="page-main">
    <div class="container">
        <div class="form-page">
            <div class="row justify-content-center">
                <div class="col-md-3">
                    <div class="btn-wrap">
                        <a href="<?php echo base_url(); ?>restricted" class="btn btn-info btn-block"><i class="fas fa-download mr-1"></i>Downloads</a>
                    </div>
                </div>
            </div>
            <br>

            <h2 class="form-heading">Cal Fire Repair Form</h2>

            <?php if (isset($err_msg)){?>
                <div class=" alert alert-danger alert-dismissable">
                    <?= $err_msg?>
                </div>
            <?php }?>
            <?php if (isset($succ_msg)){?>
                <div class="text-center alert alert-success alert-dismissable">
                    <?= $succ_msg?>
                </div>
            <?php }?>
            <?php if ($this->session->flashdata("validation_errors")){?>
                <div class="text-center alert alert-danger alert-dismissable">
                    <?= $this->session->flashdata("validation_errors")?>
                </div>
            <?php }?>

            <form id="cal_fire" name="cal_fire" method="post" >
                <div class="row">
                    <div class="col-lg-4">
                        <div class="form-group row">
                            <label for="inputEmail3MD" class="col-auto col-form-label">PO#</label>
                            <div class="col">
                                <div class="md-form mt-0">
                                    <input name="pu" type="text" required="required"  class="form-control" id="inputEmail3MD">
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-4">
                        <div class="form-group row">
                            <label for="inputEmail3MD" class="col-auto col-form-label">Requested:</label>
                            <div class="col">
                                <div class="md-form mt-0">
                                    <input name="requested" type="text" required="required" autocomplete="off" class="form-control" id="inputEmail3MD">
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-4">
                        <div class="form-group row">
                            <label for="inputEmail3MD" class="col-auto col-form-label">Status</label>
                            <div class="col">
                                <div class="md-form mt-0">
                                    <select required="required" name="statuss" class="form-control">
                                        <option value="Submitted">Submitted</option>
<!--                                        <option>Repaired</option>-->
<!--                                        <option>Cancel</option>-->
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row warranty-checkbox">
                    <div class="col">
                        <div class="custom-control custom-checkbox">
                            <input value="1"  type="checkbox" name="warranyreplace" class="custom-control-input" id="checkbox_1">
                            <label class="custom-control-label" for="checkbox_1">If this box is checked, this is warranty replacement part request.</label>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-6">
                        <div class="form-group row">
                            <label for="inputEmail3MD" class="col-auto col-form-label">Company</label>
                            <div class="col">
                                <div class="md-form mt-0">
                                    <select required="required" name="Company" class="form-control">
                                        <option value="" selected="selected">Select</option>
                                        <option value="Sacramento Command Center   /   3650 Schriever Ave Mather, CA 95655">
                                            &nbsp;Sacramento Command Center</option>

                                        <option value="Academy   /   CAL FIRE Academy, 4501 State Highway 104, Ione, CA 95640">
                                            &nbsp;Academy</option>

                                        <option value="Aviation Group   /   CAL FIRE Aviation Management, 5500 Price Ave. McClellan, CA 95652">
                                            &nbsp;Aviation Group</option>

                                        <option value="Mobile Equipment   /   CAL FIRE Mobile Equipment, 5950 Chiles Road Davis, CA 95618">
                                            &nbsp;Mobile Equpment</option>

                                        <option value="Office of the State Fire Marshal   /   1131 S Street Sacramento, CA 95814">
                                            &nbsp;Office of the State Fire Marshal</option>

                                        <option value="Amador-Eldorado Unit (2700)   /   2840 Mt. Danaher Road Camino, CA 95709">
                                            &nbsp;Amador-Eldorado Unit (2700)</option>

                                        <option value="Butte Unit (2100)   /   176 Nelson Avenue Oroville, CA 95965">
                                            &nbsp;Butte Unit (2100)</option>

                                        <option value="Fresno-Kings Unit (4300)   /   210 So. Academy Avenue Sanger, CA 93657">
                                            &nbsp;Fresno-Kings (4300)</option>

                                        <option value="Humboldt-Del Norte Unit (1200)   /   118 S. Fortuna Blvd. Fortuna, CA 95540-2796">
                                            &nbsp;Humboldt-Del Norte Unit (1200)</option>

                                        <option value="Lassen-Modoc Unit (2200)   /   697-345 Highway 36 Susanville, CA 96130">
                                            &nbsp;Lassen-Modoc Unit (2200)</option>

                                        <option value="Madera-Mariposa-Merced (4200)   /   5366 Hwy 49 North Mariposa, CA 95338">
                                            &nbsp;Madera-Mariposa-Merced (4200)</option>

                                        <option value="Marin County Fire (1500)   /   33 Castle Rock Ave. Woodacre, CA 94973">
                                            &nbsp;Marin County Fire (1500)</option>

                                        <option value="Mendocino Unit (1100)   /   17501 N. Hwy 101 Willits, CA 95490">
                                            &nbsp;Mendocino Unit (1100)</option>

                                        <option value="Nevada-Yuba-Placer Unit (2300)   /   13760 Lincoln Way Auburn, CA 95603 ">
                                            &nbsp;Nevada-Yuba-Placer Unit (2300)</option>

                                        <option value="Riverside Unit (3100)   /   210 W. San Jacinto Perris, CA 92570">
                                            &nbsp;Riverside Unit (3100)</option>

                                        <option value="San Benito-Monterey Unit (4600)   /   2221 Garden Road Monterey, CA 93940-5385">
                                            &nbsp;San Benito-Monterey Unit (4600)</option>

                                        <option value="San Bernardino Unit (3500)   /   3800 N. Sierra Way San Bernardino, CA 92405">
                                            &nbsp;San Bernardino Unit (3500)</option>

                                        <option value="San Diego Unit (3300)   /   2249 Jamacha Road El Cajon, CA 92019">
                                            &nbsp;San Diego Unit (3300)</option>

                                        <option value="San Luis Obispo Unit (3400)   /   635 N. Santa Rosa San Luis Obispo, CA 93405">
                                            &nbsp;San Luis Obispo Unit (3400)</option>

                                        <option value="San Mateo-Santa Cruz Unit (1700)   /   6059 Highway 9 Felton, CA 95018">
                                            &nbsp;San Mateo-Santa Cruz Unit (1700)</option>

                                        <option value="Santa Clara Unit (1600)   /   15670 S. Monterey St. Morgan Hill, CA 95037">
                                            &nbsp;Santa Clara Unit (1600)</option>

                                        <option value="Shasta-Trinity Unit (2400)   /   875 Cypress Ave Redding, CA 96001">
                                            &nbsp;Shasta-Trinity Unit (2400)</option>

                                        <option value="Siskiyou Unit (2600)   /   1809 Fairlane Rd. Yreka, CA 96097">
                                            &nbsp;Siskiyou Unit (2600)</option>

                                        <option value="Sonoma-Lake-Napa Unit (1400)   /   1199 Big Tree Lane St. Helena, CA 94574">
                                            &nbsp;Sonoma-Lake-Napa Unit (1400)</option>

                                        <option value="Tehama-Glenn Unit (2500)   /   604 Antelope Blvd. Red Bluff, CA 96080">
                                            &nbsp;Tehama-Glenn Unit (2500)</option>

                                        <option value="Telecom Unit /   1450 Expo Parkway, Suite C Sacramento, CA 95815">
                                            &nbsp;Telecom Unit)</option>

                                        <option value="Tulare Unit (4100)   /   1968 S. Lovers Lane Visalia, CA 93292">
                                            &nbsp;Tulare Unit (4100)</option>

                                        <option value="Tuolumne-Calaveras Unit (4400)   /   785 Mountain Ranch Rd. SanAndreas, CA 95249">
                                            &nbsp;Tuolumne-Calaveras Unit (4400)</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg">
                        <div class="form-group row">
                            <label for="inputEmail3MD" class="col-12 col-form-label">Customer Tracking Number:</label>
                            <div class="col-12">
                                <div class="md-form mt-0">
                                    <textarea name="CustomerTrackingNumber" class="form-control rounded-0" rows="5"></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-6">
                        <div class="form-group row">
                            <label for="inputEmail3MD" class="col-auto col-form-label">Model</label>
                            <div class="col">
                                <div class="md-form mt-0">
                                    <input required="required" name="Model" value="" type="text" class="form-control" id="inputEmail3MD">
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-6">
                        <div class="form-group row">
                            <label for="inputEmail3MD" class="col-auto col-form-label">	Warranty Status</label>
                            <div class="col">
                                <div class="md-form mt-0">
                                    <select required="required" name="WarrantyStatus" class="form-control">
                                        <option value="" selected="selected">Select</option>
                                        <option value="Expired">&nbsp;Expired</option>
                                        <option value="Warranty">&nbsp;Warranty</option>
                                        <option value="Warranty_Recall">&nbsp;Warranty_Recall</option>
                                        <option value="Non_Warranty">&nbsp;Non_Warranty</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-6">
                        <div class="form-group row">
                            <label for="inputEmail3MD" class="col-auto col-form-label">Qty</label>
                            <div class="col">
                                <div class="md-form mt-0">
                                    <input required="required" name="Qty" type="text" size="3" class="form-control" id="inputEmail3MD">
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-6">
                        <div class="form-group row">
                            <label for="inputEmail3MD" class="col-auto col-form-label">Serial #</label>
                            <div class="col">
                                <div class="md-form mt-0">
                                    <input required="required" name="Serial" type="text" size="25" value=" " class="form-control" id="inputEmail3MD">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-4">
                        <h3 class="checkbox-heading">Audio</h3>
                        <div class="custom-control custom-checkbox">
                            <input type="checkbox" name="NoAudio" value="1" selected="selected"  class="custom-control-input" id="audio_1">
                            <label class="custom-control-label" for="audio_1">No Audio</label>
                        </div>
                        <div class="custom-control custom-checkbox">
                            <input type="checkbox" name="LowAudio" value="1" selected="selected" class="custom-control-input" id="audio_2">
                            <label class="custom-control-label" for="audio_2">Low Audio</label>
                        </div>
                        <div class="custom-control custom-checkbox">
                            <input type="checkbox" name="IntermittentAudio" value="1" selected="selected" class="custom-control-input" id="audio_3">
                            <label class="custom-control-label" for="audio_3">Intermittent Audio</label>
                        </div>
                        <div class="custom-control custom-checkbox">
                            <input type="checkbox" name="NoTXAudio" value="1" selected="selected" class="custom-control-input" id="audio_4">
                            <label class="custom-control-label" for="audio_4">No TX Audio</label>
                        </div>
                        <div class="custom-control custom-checkbox">
                            <input type="checkbox" name="NoPLDPL" value="1" selected="selected" class="custom-control-input" id="audio_5">
                            <label class="custom-control-label" for="audio_5">No PL/DPL</label>
                        </div>
                        <div class="custom-control custom-checkbox">
                            <input type="checkbox" name="ConstantAudion" value="1" selected="selected" class="custom-control-input" id="audio_6">
                            <label class="custom-control-label" for="audio_6">Constant Audion</label>
                        </div>
                        <div class="custom-control custom-checkbox">
                            <input type="checkbox" name="WillnotSquelch" value="1" selected="selected" class="custom-control-input" id="audio_7">
                            <label class="custom-control-label" for="audio_7">Will not Squelch</label>
                        </div>
                    </div>

                    <div class="col-lg-4">
                        <h3 class="checkbox-heading">Display/Keypad</h3>
                        <div class="custom-control custom-checkbox">
                            <input type="checkbox" name="LCD" value="1" selected="selected" class="custom-control-input" id="display_1">
                            <label class="custom-control-label" for="display_1">LCD</label>
                        </div>
                        <div class="custom-control custom-checkbox">
                            <input type="checkbox" name="Keypad" value="1" selected="selected" class="custom-control-input" id="display_2">
                            <label class="custom-control-label" for="display_2">Keypad</label>
                        </div>
                        <div class="custom-control custom-checkbox">
                            <input type="checkbox" name="NoDisplay" value="1" selected="selected" class="custom-control-input" id="display_3">
                            <label class="custom-control-label" for="display_3">No Display</label>
                        </div>
                        <div class="custom-control custom-checkbox">
                            <input type="checkbox" name="LEDFaulty" value="1" selected="selected" class="custom-control-input" id="display_4">
                            <label class="custom-control-label" for="display_4">LED Faulty</label>
                        </div>
                    </div>

                    <div class="col-lg-4">
                        <h3 class="checkbox-heading">Damage</h3>
                        <div class="custom-control custom-checkbox">
                            <input type="checkbox" name="Housing" value="1" selected="selected" class="custom-control-input" id="damage_1">
                            <label class="custom-control-label" for="damage_1">Housing</label>
                        </div>
                        <div class="custom-control custom-checkbox">
                            <input type="checkbox" name="KnobsButtonsSwitches" value="1" selected="selected" class="custom-control-input" id="damage_2">
                            <label class="custom-control-label" for="damage_2">Knobs/Buttons/Switches</label>
                        </div>
                        <div class="custom-control custom-checkbox">
                            <input type="checkbox" name="Antenna" value="1" selected="selected" class="custom-control-input" id="damage_3">
                            <label class="custom-control-label" for="damage_3">Antenna</label>
                        </div>
                        <div class="custom-control custom-checkbox">
                            <input type="checkbox" name="Label" value="1" selected="selected" class="custom-control-input" id="damage_4">
                            <label class="custom-control-label" for="damage_4">Label</label>
                        </div>
                        <div class="custom-control custom-checkbox">
                            <input type="checkbox" name="BatteryContacts" value="1" selected="selected" class="custom-control-input" id="damage_5">
                            <label class="custom-control-label" for="damage_5">Battery Contacts</label>
                        </div>
                        <div class="custom-control custom-checkbox">
                            <input type="checkbox" name="PhysicalDamage" value="1" selected="selected" class="custom-control-input" id="damage_6">
                            <label class="custom-control-label" for="damage_6">Physical Damage</label>
                        </div>
                        <div class="custom-control custom-checkbox">
                            <input type="checkbox" name="ChemicalDamage" value="1" selected="selected" class="custom-control-input" id="damage_7">
                            <label class="custom-control-label" for="damage_7">Chemical Damage</label>
                        </div>
                        <div class="custom-control custom-checkbox">
                            <input type="checkbox" name="LiquidDamage" value="1" selected="selected" class="custom-control-input" id="damage_8">
                            <label class="custom-control-label" for="damage_8">Liquid Damage</label>
                        </div>
                        <div class="custom-control custom-checkbox">
                            <input type="checkbox" name="Dead" value="1" selected="selected" class="custom-control-input" id="damage_9">
                            <label class="custom-control-label" for="damage_9">Dead</label>
                        </div>
                        <div class="custom-control custom-checkbox">
                            <input type="checkbox" name="Fuse" value="1" selected="selected" class="custom-control-input" id="damage_10">
                            <label class="custom-control-label" for="damage_10">Fuse</label>
                        </div>
                    </div>
                </div>


                <div class="bottom-checkboxes">
                    <h3 class="checkbox-heading">Radio Functions</h3>
                    <div class="row">
                        <div class="col-lg-4">
                            <div class="custom-control custom-checkbox">
                                <input type="checkbox" name="PoorRXSens" value="1" selected="selected" class="custom-control-input" id="radio_1">
                                <label class="custom-control-label" for="radio_1">Poor RX Sens</label>
                            </div>
                        </div>

                        <div class="col-lg-4">
                            <div class="custom-control custom-checkbox">
                                <input type="checkbox" name="UnabletoReadProgram" value="1" selected="selected" class="custom-control-input" id="radio_2">
                                <label class="custom-control-label" for="radio_2">Unable to Read/Program</label>
                            </div>
                        </div>

                        <div class="col-lg-4">
                            <div class="custom-control custom-checkbox">
                                <input type="checkbox" name="IntermittentRX" value="1" selected="selected" class="custom-control-input" id="radio_3">
                                <label class="custom-control-label" for="radio_3">Intermittent RX</label>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-4">
                            <div class="custom-control custom-checkbox">
                                <input type="checkbox" name="NoScan" value="1" selected="selected" class="custom-control-input" id="radio_4">
                                <label class="custom-control-label" for="radio_4">No Scan</label>
                            </div>
                        </div>

                        <div class="col-lg-4">
                            <div class="custom-control custom-checkbox">
                                <input type="checkbox" name="NoTxr" value="1" selected="selected" class="custom-control-input" id="radio_5">
                                <label class="custom-control-label" for="radio_5">No TXr</label>
                            </div>
                        </div>

                        <div class="col-lg-4">
                            <div class="custom-control custom-checkbox">
                                <input type="checkbox" name="IntermittentPower" value="1" selected="selected" class="custom-control-input" id="radio_6">
                                <label class="custom-control-label" for="radio_6">Intermittent Power</label>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-4">
                            <div class="custom-control custom-checkbox">
                                <input type="checkbox" name="NoTrunking" value="1" selected="selected" class="custom-control-input" id="radio_7">
                                <label class="custom-control-label" for="radio_7">No Trunking</label>
                            </div>
                        </div>

                        <div class="col-lg-4">
                            <div class="custom-control custom-checkbox">
                                <input type="checkbox" name="NoPower" value="1" selected="selected" class="custom-control-input" id="radio_8">
                                <label class="custom-control-label" for="radio_8">No Power</label>
                            </div>
                        </div>

                        <div class="col-lg-4">
                            <div class="custom-control custom-checkbox">
                                <input type="checkbox" name="FailCode" value="1" selected="selected" class="custom-control-input" id="radio_9">
                                <label class="custom-control-label" for="radio_9">Fail Code (list code below)</label>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-4">
                            <div class="custom-control custom-checkbox">
                                <input type="checkbox" name="NoRX" value="1" selected="selected" class="custom-control-input" id="radio_10">
                                <label class="custom-control-label" for="radio_10">No RX</label>
                            </div>
                        </div>

                        <div class="col-lg-4">
                            <div class="custom-control custom-checkbox">
                                <input type="checkbox" name="LowPower" value="1" selected="selected" class="custom-control-input" id="radio_11">
                                <label class="custom-control-label" for="radio_11">Low Power</label>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="complaint-box">
                    <div class="custom-control custom-checkbox">
                        <input type="checkbox" name="otherlistcomplaintbelow" value="1" selected="selected" class="custom-control-input" id="complaint_1">
                        <label class="custom-control-label" for="complaint_1">Other (list complaint below)</label>
                    </div>
                    <div class="form-group row">
                        <div class="col-12">
                            <div class="md-form mt-0">
                                <textarea class="form-control rounded-0" name="otherlistcomplaintbelowtext" rows="5"></textarea>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="form-submit-wrap">
                    <input type="hidden" id="Repair_Status" name="Repair_Status" value="Review">
                    <input type="hidden" id="PO_Status" name="PO_Status" value="Submitted">

                    <button type="button" onclick="window.print();" class="btn btn-primary"><i class="fas fa-print mr-1"></i> Print</button>
                    <button name="Submitcalfire" type="submit" id="Submit" form="cal_fire" value="Submit" class="btn btn-success"><i class="fas fa-check mr-1"></i> Submit</button>
                </div>


            </form>
        </div>
    </div>
</div>

