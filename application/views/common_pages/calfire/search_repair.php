<?php
/**
 * Created by PhpStorm.
 * User: Maddy
 * Date: 03-Oct-18
 * Time: 3:52 PM
 */
//echo $this->session->cal_access_level;
//echo "<pre>";
//print_r($_SESSION);
//exit;
?>

<div class="header" xmlns="http://www.w3.org/1999/html">
    <div class="header-top">
        <div class="container-fluid">
            <div class="row align-items-center justify-content-between">
                <div class="col-md-auto">
                    <div class="logo-wrap">
                        <a href="<?=base_url('CalFire')?>">
                            <img style="margin-left: -10px;" src="<?=base_url('assets/styles/images/calfire-logo-portal.png')?>" alt="BK Logo">
                        </a>
                    </div>
                </div>
                <div class="col-md-auto">
                    <div class="header-top-content">
                        <h3 class="phone">
                            <i class="fas fa-phone"></i>
                            <a href="tel: 1-800-422-6281">1-800-422-6281</a>
                        </h3>
                        <h3 class="email">
                            <i class="fas fa-envelope"></i>
                            <a href="mailto: service@bktechnologies.com">service@bktechnologies.com</a>
                        </h3>
                    </div>
                </div>
                <div class="col-md-auto">
                    <div class="logo-wrap bk-logo-portal">
                        <a href="https://www.bktechnologies.com" target="_blank">
                            <img src="<?=base_url('assets/styles/images/bk-logo-portal.png')?>" alt="BK Logo">
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <nav class="mb-1 navbar navbar-expand-lg navbar-dark info-color">
        <a class="navbar-brand" href="<?=base_url()?>">BK Technologies</a>
        <button class="navbar-toggler collapsed" type="button" data-toggle="collapse" data-target="#navbarSupportedContent-4" aria-controls="navbarSupportedContent-4" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
<!--        <div class="navbar-collapse collapse" id="navbarSupportedContent-4" style="">-->
<!--            <ul class="navbar-nav ml-auto">-->
<!--                <li class="nav-item active">-->
<!--                    --><?php //if (!$this->session->cal_access_level){?>
<!--                        <a class="nav-link waves-effect waves-light" href="--><?//=base_url('login')?><!--">-->
<!--                            <i class="fas fa-power-off"></i> Login-->
<!--                        </a>-->
<!--                    --><?php //}else{?>
<!--                        <a class="nav-link waves-effect waves-light" href="--><?//=base_url('cal_logout')?><!--">-->
<!--                            <i class="fas fa-power-off"></i> Logout-->
<!--                        </a>-->
<!--                    --><?php //}?>
<!--                </li>-->
<!--            </ul>-->
<!--        </div>-->
        <div class="navbar-collapse collapse" id="navbarSupportedContent-4" style="">
            <ul class="navbar-nav ml-auto">

                <li class="nav-item">
                    <a class="nav-link waves-effect waves-light" href="<?=base_url('CalFire')?>">
                        <i class="fas fa-home"></i> Return To Dashboard
                    </a>
                </li>
                <li class="nav-item active">
                    <?php if (!$this->session->cal_access_level){?>
                        <a class="nav-link waves-effect waves-light" href="<?=base_url('login')?>">
                            <i class="fas fa-power-off"></i> Login
                        </a>
                    <?php }else{?>
                        <a class="nav-link waves-effect waves-light" href="<?=base_url('cal_logout')?>">
                            <i class="fas fa-power-off"></i> Logout
                        </a>
                    <?php }?>
                </li>
            </ul>
        </div>
    </nav>
</div>

<div class="page-main">
    <div class="container">
        <div class="table-top">
            <h2 class="form-heading">Cal Fire Search Records</h2>
            <div class="row ">
                <div class="offset-sm-2 col-md-3">
                    <div class="btn-wrap">
                        <a href="<?php echo base_url(); ?>CalFire/showall" class="btn btn-primary btn-block"><i class="fas fa-sync-alt"></i> Refresh Profiles</a>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="btn-wrap">
                        <a href="<?php echo base_url(); ?>CalFire/request" class="btn btn-default btn-block"><i class="fab fa-creative-commons-share mr-1"></i>Request a Repair</a>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="btn-wrap">
                        <a href="<?php echo base_url(); ?>restricted" class="btn btn-info btn-block"><i class="fas fa-download mr-1"></i>Downloads</a>
                    </div>
                </div>
            </div>
        </div>

        <?php if ($this->session->flashdata("success")){?>
            <div class="text-center alert alert-success alert-dismissable">
                <?= $this->session->flashdata("success")?>
            </div>
        <?php }?>
        <?php if ($this->session->flashdata("error")){?>
            <div class="text-center alert alert-danger alert-dismissable">
                <?= $this->session->flashdata("error")?>
            </div>
        <?php }?>


        <div class="as-table">
            <table class="table table-striped">
                <thead>
                <tr>
                    <?php if ($this->session->is_admin==true || $this->session->cal_access_level==3) {?>
                    <th scope="col">Edit</th>
                    <?php } else{?>
                        <th scope="col">View</th>
                    <?php }?>

                    <th scope="col">Company</th>
                    <th scope="col">Serial Number</th>
                    <th scope="col">Requested</th>
                    <th scope="col">PO Status</th>
                    <th scope="col">Customer Tracking</th>
                    <th scope="col">Recieved Date</th>
                    <?php if ($this->session->is_admin == true || $this->session->cal_access_level==3) {?>
                        <th>Delete</th>
                    <?php }?>

                </tr>
                </thead>
                <tbody class="bordertop">

                <?php if (isset($list) && $list!=400) {
                    foreach ($list as $key ) {
                        ?>
                <tr>
                    <?php if ($this->session->is_admin==true || $this->session->cal_access_level==3) {?>
                        <td><a href="<?php echo base_url(); ?>CalFire/edit/<?php echo $key->id ?>">Edit</a></td>
                    <?php } else{?>
                        <td><a href="<?php echo base_url(); ?>CalFire/edit/<?php echo $key->id ?>">View</a></td>
                    <?php }?>
                    <td><?php echo $key->Company ?></td>
                    <td><?php echo $key->Serial ?></td>
                    <td><?php echo $key->requested ?></td>
                    <td><?php echo $key->PO_Status ?></td>
                    <td><?php echo $key->CustomerTrackingNumber ?></td>
                    <td><?php echo $key->date ?></td>
                    <?php if ($this->session->is_admin == true || $this->session->cal_access_level==3) {?>
                        <td><a href="<?php echo base_url(); ?>CalFire/delete/<?php echo $key->id ?>">Delete</a></td>
                    <?php }?>
                </tr>
                    <?php } }else{ ?>
                    <tr><td colspan="8"><h3 class="text-center">No Data Found</h3></td></tr>
                <?php } ?>
                </tbody>
            </table>
        </div>
    </div>
</div>
