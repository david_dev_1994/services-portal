<?php
/**
 * Created by PhpStorm.
 * User: Maddy
 * Date: 02-Oct-18
 * Time: 8:41 PM
 */
?>
<div class="header" xmlns="http://www.w3.org/1999/html">
    <div class="header-top">
        <div class="container-fluid">
            <div class="row align-items-center justify-content-between">
                <div class="col-md-auto">
                    <div class="logo-wrap">
                        <a href="<?=base_url('CalFire')?>">
                            <img style="margin-left: -10px;" src="<?=base_url('assets/styles/images/calfire-logo-portal.png')?>" alt="BK Logo">
                        </a>
                    </div>
                </div>
                <div class="col-md-auto">
                    <div class="header-top-content">
                        <h3 class="phone">
                            <i class="fas fa-phone"></i>
                            <a href="tel: 1-800-422-6281">1-800-422-6281</a>
                        </h3>
                        <h3 class="email">
                            <i class="fas fa-envelope"></i>
                            <a href="mailto: service@bktechnologies.com">service@bktechnologies.com</a>
                        </h3>
                    </div>
                </div>
                <div class="col-md-auto">
                    <div class="logo-wrap bk-logo-portal">
                        <a href="https://www.bktechnologies.com" target="_blank">
                            <img src="<?=base_url('assets/styles/images/bk-logo-portal.png')?>" alt="BK Logo">
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <nav class="mb-1 navbar navbar-expand-lg navbar-dark info-color">
        <a class="navbar-brand" href="<?=base_url()?>">BK Technologies</a>
        <button class="navbar-toggler collapsed" type="button" data-toggle="collapse" data-target="#navbarSupportedContent-4" aria-controls="navbarSupportedContent-4" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="navbar-collapse collapse" id="navbarSupportedContent-4" style="">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item active">
                    <a class="nav-link waves-effect waves-light" href="#">
                        <a class="nav-link waves-effect waves-light" href="<?=base_url('cal_sign-up')?>">
                            <i class="fas fa-user"></i> Sign-up
                        </a>
                    </a>
                </li>
            </ul>
        </div>
    </nav>
</div>

<div class="page-main">
    <div class="container">

        <div class="card login-card">

            <?php if ($this->session->flashdata('error')){?>
                <div class="errors_container alert alert-danger alert-dismissable">
                    <?= $this->session->flashdata('error');?>
                </div>
            <?php }?>
            <?php if ($this->session->flashdata('success')){?>
                <div class="text-center errors_container alert alert-success alert-dismissable">
                    <?= $this->session->flashdata('success');?>
                </div>
            <?php }?>

            <?php if (isset($msg)){?>
                <div class="errors_container alert alert-danger alert-dismissable">
                    <?= $msg?>
                </div>
            <?php }?>

            <h5 class="card-header info-color white-text text-center py-4">
                <strong>CalFire Password Reset</strong>
            </h5>
            <!--Card content-->
            <div class="card-body px-lg-5 pt-0">
                <!-- Form -->
                <form class="as-login-form" action="<?=base_url('/cal_resetpass')?>" method="post">
                    <!-- Password -->
                    <div class="md-form">
                        <input name="c_password" required="required" type="password" id="materialLoginFormPassword" class="form-control">
                        <label for="materialLoginFormPassword">Password</label>
                        <input type="hidden" name="id" value="<?=$res;?>">
                    </div>
                    <!-- Password -->
                    <div class="md-form">
                        <input name="password" required="required" type="password" id="materialLoginFormPassword" class="form-control">
                        <label for="materialLoginFormPassword">Confirm Password</label>
                    </div>
                    <input class="btn btn-info btn-block my-4 waves-effect z-depth-0" name="reset" value="Reset" type="submit">
                    <!-- Register -->
                    <p>Request Access to
                        <a href="<?=base_url('cal_sign-up')?>" >bktechnologies.com</a>
                    </p>
                </form>
                <!-- Form -->

                <!-- Modal HTML -->


                <div id="myModal" class="modal modal-md fade">
                    <div class="modal-dialog modal-confirm">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h3 class="modal-title">CalFire Password Reset Form?</h3>
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            </div>

                            <div class="modal-body">
                                <form class="as-login-form" action="<?=base_url('calfire_forgot_password')?>" method="post">
                                    <!-- Email -->
                                    <div class="md-form">
                                        <input name="email" required="required" type="email" id="materialLoginFormEmail" class="form-control">
                                        <label for="materialLoginFormEmail">E-mail</label>
                                    </div>
                                    <!-- Sign in button -->
                                    <button class="btn btn-info btn-block form-control" name="reset" value="Reset" type="submit"> Reset</button>                                </form>
                            </div>
                            <div class="modal-footer">
                                <a type="button"  class="btn btn-info" data-dismiss="modal">Cancel</a>
                            </div>
                        </div>
                    </div>
                </div>


            </div>
        </div>

    </div>
</div>
