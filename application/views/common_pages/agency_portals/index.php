<section id="main-conent">
    <div class="container text-center" id="products">
<div class="row">
<div class="col-lg-12">
<h1>Agency Portals</h1>
            
<div class="padding-20"></div>
            <div class="col-md-3 servks">
                <div>
                    <center>
                        <a href="<?php echo base_url();?>/Alberta">
                            <div class="img-wrap">
                                <img src="/service-portal/assets/img/alberta_portal_logo.png">
                            </div>
                            <h2>Alberta Health</h2>
                        </a>
                    </center>
                </div>
            </div>

            <div class="col-md-3  servks">
                <div>
                    <center>
                        <a href="<?=base_url("/CalFire")?>">
                            <div class="img-wrap">
                                <img src="/service-portal/assets/img/calfire_portal_logo.png">
                            </div>
                            <h2>CalFire</h2>
                        </a>
                    </center>
                </div>
            </div>

            <div class="col-md-3  servks">
                <div>
                    <center>
                        <a href="<?=base_url("/flfs")?>">
                            <div class="img-wrap">
                                <img src="/service-portal/assets/img/florida_portal_logo.png">
                            </div>
                            <h2>Florida Forestry</h2>
                        </a>
                    </center>
                </div>
            </div>

            <div class="col-md-3  servks">
                <div>
                    <center>
                        <a href="<?=base_url("/usfs")?>">
                            <div class="img-wrap">
                                <img src="/service-portal/assets/img/usfs_portal_logo.png">
                            </div>
                            <h2>US Forestry</h2>
                        </a>
                    </center>
                </div>
            </div>
</div>
</div>

</div>
</div>

<div style="height:75px;"></div>
<div class="container-fluid before-footer"><div class="row">        
<div class="main-conent__hp-description bottom cta-2 col-lg-12">
            <div class='homeheading'>
               
                <h3 style="text-transform: capitalize !important;">For more information on available products, visit our website</h3>
<div class="link"> <a href="https://www.bktechnologies.com/" target="_blank" style="font-weight: 500;">Learn More</a></div>
<!-- <div class="link"> <a href="<?php echo base_url();?>assets/pdf/BK_Radio_Drivers10-2011.zip" style="font-weight: 500;">Download BK Radio USB Drivers</a></div> -->
            </div>
        </div>

    </div>
</section>

<style>

section#main-conent {
    padding: 150px 0 !important;
    padding-bottom: 0px !important;
}
body>section>div {
    padding-top: 0px !important;
}

</style>