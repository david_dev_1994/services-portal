<?php
/**
 * Created by PhpStorm.
 * User: Maddy
 * Date: 02-Oct-18
 * Time: 8:41 PM
 */
?>
<div class="header">
    <div class="header-top">
        <div class="container-fluid">
            <div class="row align-items-center justify-content-between">
                <div class="col-md-auto">
                    <div class="logo-wrap">
                        <a href="<?=base_url('Alberta')?>">
                            <img width="auto" height="85px" src="<?=base_url('assets/styles/images/alberta-logo.jpg')?>" alt="BK Logo">
                        </a>
                    </div>
                </div>
                <div class="col-md-auto">
                    <div class="header-top-content">
                        <h3 class="phone">
                            <i class="fas fa-phone"></i>
                            <a href="tel: 1-800-422-6281">1-800-422-6281</a>
                        </h3>
                        <h3 class="email">
                            <i class="fas fa-envelope"></i>
                            <a href="mailto:service@bktechnologies.com">service@bktechnologies.com</a>
                        </h3>
                    </div>
                </div>
                <div class="col-md-auto">
                    <div class="logo-wrap bk-logo-portal">
                        <a target="_blank" href="https://www.bktechnologies.com/">
                            <img src=<?=base_url("assets/styles/images/bk-logo-portal.png")?> alt="BK Logo">
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <nav class="mb-1 navbar navbar-expand-lg navbar-dark info-color">
        <a class="navbar-brand" href="<?=base_url()?>">BK Technologies</a>
        <button class="navbar-toggler collapsed" type="button" data-toggle="collapse" data-target="#navbarSupportedContent-4" aria-controls="navbarSupportedContent-4" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="navbar-collapse collapse" id="navbarSupportedContent-4" style="">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item active">
                            <a class="nav-link waves-effect waves-light" href="<?=base_url('alb_sign-up')?>">
                                <i class="fas fa-user"></i> Sign Up
                            </a>
                </li>
            </ul>
        </div>
    </nav>
</div>

<div class="page-main">
    <div class="container">

        <div class="card login-card">

            <?php if ($this->session->flashdata('error')){?>
                <div class=" text-center errors_container alert alert-danger alert-dismissable">
                    <?= $this->session->flashdata('error');?>
                </div>
            <?php }?>
            <?php if ($this->session->flashdata('success')){?>
                <div class="text-center errors_container alert alert-success alert-dismissable">
                    <?= $this->session->flashdata('success');?>
                </div>
            <?php }?>

            <?php if (isset($msg)){?>
                <div class="errors_container alert alert-danger alert-dismissable">
                    <?= $msg?>
                </div>
            <?php }?>

            <h5 class="card-header info-color white-text text-center py-4">
                <strong>Alberta Health Services - Sign In</strong>
            </h5>
            <!--Card content-->
            <div class="card-body px-lg-5 pt-0">
                <!-- Form -->
                <form class="as-login-form" action="<?=base_url('alb_login')?>" method="post">
                    <!-- Email -->
                    <div class="md-form">
                        <input required="required" name="email" type="email" id="materialLoginFormEmail" class="form-control">
                        <label for="materialLoginFormEmail">E-mail</label>
                    </div>
                    <!-- Password -->
                    <div class="md-form">
                        <input required="required" name="password" type="password" id="materialLoginFormPassword" class="form-control">
                        <label for="materialLoginFormPassword">Password</label>
                    </div>
                    <div class="d-flex justify-content-around">
                        <div>
                             Forgot password
                            <a href="#myModal" data-toggle="modal">Forgot password?</a>
                        </div>
                    </div>
                    <!-- Sign in button -->
                    <button class="btn btn-info btn-block my-4 waves-effect z-depth-0" type="submit">Sign in</button>
                    <!-- Register -->
                    <p>Request Access to
                        <a href="<?=base_url('alb_sign-up')?>" >bktechnologies.com</a>
                    </p>
                </form>
                <!-- Form -->

                <!-- Modal HTML -->


                <div id="myModal" class="modal modal-md fade">
                    <div class="modal-dialog modal-confirm">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h3 class="modal-title">Alberta Password Reset Form?</h3>
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            </div>

                            <div class="modal-body">
                                <form class="as-login-form" action="<?=base_url('alberta_forgot_password')?>" method="post">
                                    <!-- Email -->
                                    <div class="md-form">
                                        <input name="email" required="required" type="email" id="materialLoginFormEmail" class="form-control">
                                        <label for="materialLoginFormEmail">E-mail</label>
                                    </div>
                                    <!-- Sign in button -->
                                    <button class="btn btn-info btn-block my-4 waves-effect z-depth-0" type="submit">Submit</button>
                                </form>
                            </div>
                            <div class="modal-footer">
                                <a type="button"  class="btn btn-info" data-dismiss="modal">Cancel</a>
                            </div>
                        </div>
                    </div>
                </div>


            </div>
        </div>

    </div>
</div>
