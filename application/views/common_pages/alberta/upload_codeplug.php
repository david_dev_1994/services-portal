<?php
//print_r($this->session->userdata('alb_loggedUserId'));exit;
//print_r($this->session->userdata('alb_logged_email'));exit;
/**
 * Created by PhpStorm.
 * User: Maddy
 * Date: 15-Oct-18
 * Time: 3:51 PM
 */
?>

<div class="page-main">
    <div class="container">
        <div class="file-upload-box">
            <?php if ($this->session->flashdata("success")){?>
                <div class="text-center alert alert-success alert-dismissable">
                    <?= $this->session->flashdata("success")?>
                </div>
            <?php }?>
            <?php if ($this->session->flashdata("error")){?>
                <div class="text-center alert alert-danger alert-dismissable">
                    <?= $this->session->flashdata("error")?>
                </div>
            <?php }?>
            <?php if (isset($error)){?>
                <div class="text-center alert alert-danger alert-dismissable">
                    <?= $error?>
                </div>
            <?php }?>

            <?php if($this->session->userdata('alb_loggedUserId') == 48 || $this->session->userdata('alb_logged_acc_type') == 2){ ?>

            <h2 class="section-heading">Upload Your Codeplug File</h2>
            <div class="file-upload">
                <form action="<?=base_url('Users/upload_codeplug')?>" method="post" enctype="multipart/form-data">
                    <label for="upload" id="label" class="file-upload__label bk_blue">Choose your file</label>
                    <!--                    <input type="file" name="file1">-->
                    <select name="device">
                        <option value="portables">Portables</option>
                        <option value="mobiles">Mobiles</option>
                        <option value="base_stations">Base Stations</option>
                    </select>
                    <input id="upload" class="file-upload__input" type="file" name="file1" onchange="change(this);">

                    <div class="file-btn-wrap">
                        <button type="submit" name="FileUpload" value="submit" class="btn btn-default bk_red"><i class="fas fa-upload mr-1"></i> Upload File</button>
                    </div>
                </form>
            </div>

            <?php }else{ ?>
            <h2 class="section-heading">Codeplug List</h2>
            <?php } ?>
            <div class="as-table">
                <table class="table-striped">
                    <thead>
                    <tr>
                        <th>Uploaded Date</th>
                        <th>Uploaded By</th>
                        <th>File</th>
                        <th>Device</th>
                        <th>Download</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php if (!isset($list)){ ?>
                        <?php for($i=0;$i<sizeof($filelist)-1;$i++){
                            if (file_exists('assets/codeplug/'.$filelist[$i]->file)){?>
                                <tr>
                                    <td><?= date('m-d-Y',strtotime($filelist[$i]->date))?></td>
                                    <td><?= $filelist[$i]->name?:"Anonymous"; ?></td>
<!--                                    <td>--><?//= $filelist['username']?><!--</td>-->
                                    <td><?= $filelist[$i]->file?></td>
                                    <td><?= $filelist[$i]->device?></td>
                                    <td><a href="<?=base_url('assets/codeplug/').$filelist[$i]->file?>"><i class="fas fa-file-alt mr-1"></i>Download</a></td>
                                </tr>
                            <?php } } ?>
                    <?php }else{ ?>
                        <tr>
                            <td class="text-center" colspan="4">No Files Uploaded yet.</a></td>
                        </tr>
                    <?php }?>

                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
