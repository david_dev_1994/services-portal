<!--    <div class="header">-->
<!--        <div class="header-top">-->
<!--            <div class="container-fluid">-->
<!--                <div class="row align-items-center justify-content-between">-->
<!--                    <div class="col-md-auto">-->
<!--                        <div class="logo-wrap">-->
<!--                            <a href="--><?//=base_url();?><!--">-->
<!--                                <img src="--><?//=base_url('assets/styles/images/bk-logo-portal.png')?><!--" alt="BK Logo">-->
<!--                            </a>-->
<!--                        </div>-->
<!--                    </div>-->
<!--                    <div class="col-md-auto">-->
<!--                        <div class="header-top-content">-->
<!--                            <h3 class="phone">-->
<!--                                <i class="fas fa-phone"></i>-->
<!--                                <a href="tel: 1-800-422-6281">1-800-422-6281</a>-->
<!--                            </h3>-->
<!---->
<!--                            <h3 class="email">-->
<!--                                    <i class="fas fa-envelope"></i>-->
<!--                                    <a href="mailto:rleeney@bktechnologies.com">rleeney@bktechnologies.com </a>-->
<!--                                </h3>-->
<!--                        </div>-->
<!--                    </div>-->
<!--                </div>-->
<!--            </div>-->
<!--        </div>-->
<!--        <nav class="mb-1 navbar navbar-expand-lg navbar-dark info-color">-->
<!--            <a class="navbar-brand" href="--><?//=base_url();?><!--">BK Technologies</a>-->
<!--            <button class="navbar-toggler collapsed" type="button" data-toggle="collapse" data-target="#navbarSupportedContent-4" aria-controls="navbarSupportedContent-4" aria-expanded="false" aria-label="Toggle navigation">-->
<!--                <span class="navbar-toggler-icon"></span>-->
<!--            </button>-->
<!--            <div class="navbar-collapse collapse" id="navbarSupportedContent-4" style="">-->
<!--                <ul class="navbar-nav mr-auto">-->
<!---->
<!--                    <li class="nav-item dropdown">-->
<!--                        <a class="nav-link dropdown-toggle" href="#Products" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">-->
<!--                                  Products-->
<!--                                </a>-->
<!--                        <ul class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">-->
<!--                            <li><a target="_blank" class="dropdown-item" href="https://www.bktechnologies.com/product-category/portables/">Portable</a></li>-->
<!--                            <li><a target="_blank" class="dropdown-item" href="https://www.bktechnologies.com/product-category/mobiles/">Mobiles</a></li>-->
<!--                            <li><a target="_blank" class="dropdown-item" href="https://www.bktechnologies.com/product-category/base-stations/">Base Stations</a></li>-->
<!--                            -->
<!--                        </ul>-->
<!--                    </li>-->
<!--                    <li class="nav-item dropdown">-->
<!--                        <a class="nav-link dropdown-toggle" href="#Support" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">-->
<!--                                  Support-->
<!--                                </a>-->
<!--                        <ul class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">-->
<!--                            <li><a class="dropdown-item" href="#">Download Firmware/Software</a></li>-->
<!--                            <li><a class="dropdown-item" href="#">Download Codeplug</a></li>-->
<!--                            <li><a class="dropdown-item" href="#">Upload Codeplug</a></li>-->
<!--                        </ul>-->
<!--                    </li>-->
<!--                    <li class="nav-item dropdown">-->
<!--                        <a class="nav-link dropdown-toggle" href="#Documentation" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">-->
<!--                                  Documentation-->
<!--                                </a>-->
<!--                        <ul class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">-->
<!--                            <li><a class="dropdown-item" href="#">Download Manuals</a></li>-->
<!--                            <li><a target="_blank" class="dropdown-item" href="http://www.relmservice.com/AHS/Relm_amb_radio_install_guide.pdf">Service Installations</a></li>-->
<!--                            <li><a target="_blank" class="dropdown-item" href="http://www.relmservice.com/AHS/Trunked_Radio_Programming_Guide_Rev-7.pdf">Trunking Guide</a></li>-->
<!--                            <li><a target="_blank" class="dropdown-item" href="https://bktechnologies.com/wp-content/uploads/KAA0101%20Battery%20Field%20Capacity%20Testing.pdf">CADEX-Battery Field Capacity Testing</a></li>-->
<!--                            <li><a class="dropdown-item" href="--><?//=base_url('Esn-spread-sheet')?><!--">ESN Spread Sheet</a></li>-->
<!--                        </ul>-->
<!--                    </li>-->
<!--                </ul>-->
<!--                <ul class="navbar-nav ml-auto">-->
<!--                    <li class="nav-item active">-->
<!--                        <a class="nav-link waves-effect waves-light" href="--><?//=base_url('alb_logout')?><!--">-->
<!--                            <i class="fas fa-power-off"></i> Logout-->
<!--                        </a>-->
<!--                    </li>-->
<!--                </ul>-->
<!--            </div>-->
<!--        </nav>-->
<!--    </div>-->

<div class="page-main">
    <div class="container">
        <div class="top-logo">
            <img src="<?=base_url('assets/styles/images/alberta-logo.jpg')?>" alt="Alberta Logo" class="img-fluid">
        </div>
        <?php if ($this->session->flashdata('error')){?>
            <div class="offset-sm-3 col-md-6 text-center errors_container alert alert-danger alert-dismissable">
                <?= $this->session->flashdata('error');?>
            </div>
        <?php }?>
        <?php if ($this->session->flashdata('success')){?>
            <div class="offset-sm-3 col-md-6 text-center errors_container alert alert-success alert-dismissable">
                <?= $this->session->flashdata('success');?>
            </div>
        <?php }?>

        <div class="page-top-section">
            <div class="row">
                <div class="col-lg-12">
                    <div class="left-side">
                        <h2 style="text-align:center;">Welcome to the Alberta Health Services - BK Radio Web Portal</h2>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-3"></div>
                <div class="col-lg-6">
                    <div class="right-side">
                        <div class="c-box">
                            <h4 class="name">Pete Rogell</h4>
                            <h5 class="position">V.P. of International Sales</h5>
                            <span class="phone"><i class="fas fa-phone mr-1"></i>321-953-7809</a></span>
                            <span class="phone"><i class="fas fa-phone mr-1"></i> 321-431-6208</span>
                            <a href="mailto:progell@bktechnologies.com">
                                <i class="fas fa-envelope mr-1"></i>progell@bktechnologies.com
                            </a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3"></div>
            </div>
        </div>

        <div class="second-section">
            <h2 class="section-heading text-center">BK Technologies - BK Radio Manufacturer Contacts</h2>
            <div class="row">
                <div class="col-md-6 c-box-2">
                    <div class="box-content">
                        <h4 class="name">Technical Support</h4>
                        <span class="phone"><i class="fas fa-phone mr-1"></i> 800-422-6281</span>
                    </div>
                </div>
                <div class="col-md-6 c-box-2">
                    <div class="box-content">
                        <h4 class="name">Sharon Loper</h4>
                        <h4 class="position">Customer Service</h4>
                        <h5 class="position">Customer Service Admin Supervisor</h5>
                        <span class="phone"><i class="fas fa-phone mr-1"></i> 321-237-0689</span>
                        <a href="mailto:sloper@bktechnologies.com">
                            <i class="fas fa-envelope mr-1"></i> sloper@bktechnologies.com
                        </a>
                    </div>
                </div>
                <div class="col-md-6 c-box-2">
                    <div class="box-content">
                        <h4 class="name">Leroy Loper</h4>
                        <h4 class="position">Technical Support</h4>
                        <h5 class="position">Customer Service Manager</h5>
                        <span class="phone"><i class="fas fa-phone mr-1"></i> 321-953-7962</span><br>
                        <span class="phone"><i class="fas fa-mobile mr-1"></i> 321-616-3756</span>
                        <a href="mailto:lloper@bktechnologies.com">
                            <i class="fas fa-envelope mr-1"></i> lloper@bktechnologies.com
                        </a>
                    </div>
                </div>
                <div class="col-md-6 c-box-2">
                    <div class="box-content">
                        <h4 class="name">Steve Appling</h4>
                        <h4 class="position">Director</h4>
                        <h5 class="position">Maintenance, Service and  Integration</h5>
                        <span class="phone"><i class="fas fa-phone mr-1"></i> 321-237-1439</span><br>
                        <span class="phone"><i class="fas fa-mobile mr-1"></i> 321-750-7732</span>
                        <a href="mailto:sappling@bktechnologies.com">
                            <i class="fas fa-envelope mr-1"></i> sappling@bktechnologies.com
                        </a>
                    </div>
                </div>
            </div>
        </div>

        <div class="third-section">
            <h2 class="section-heading text-center">Alberta Health Services - Radio Communications Contacts</h2>
            <div class="row">
                <div class="col-md-6 c-box-2">
                    <div class="box-content">
                        <h4 class="name">Alfred Klein</h4>
                        <h5 class="position">Director, EMS Dispatch Southern Communication Centre Alberta Health Services</h5>
                        <span class="phone"><i class="fas fa-phone mr-1"></i> 403.612.8715</span>
                        <a href="mailto:alfred.klein@ahs.ca">
                            <i class="fas fa-envelope mr-1"></i> alfred.klein@ahs.ca
                        </a>
                    </div>
                </div>
                <div class="col-md-6 c-box-2">
                    <div class="box-content">
                        <h4 class="name">Dawson Elliott </h4>
                        <h5 class="position">Dispatch Communications Support Specialist AHS EMS IT</h5>
                        <span class="phone"><i class="fas fa-phone mr-1"></i> 403.860.3816</span>
                        <a href="mailto:Dawson.Elliott@ahs.ca">
                            <i class="fas fa-envelope mr-1"></i> Dawson.Elliott@ahs.ca
                        </a>
                    </div>
                </div>
                <div class="col-md-6 c-box-2">
                    <div class="box-content">
                        <h4 class="name">Doug MacKintosh</h4>
                        <h5 class="position">Dispatch Communications Support Specialist  AHS EMS IT</h5>
                        <span class="phone"><i class="fas fa-phone mr-1"></i> 780.407.4480</span>
                        <a href="mailto:Douglas.MacKintosh@ahs.ca">
                            <i class="fas fa-envelope mr-1"></i> Douglas.MacKintosh@ahs.ca
                        </a>
                    </div>
                </div>
                <div class="col-md-6 c-box-2">
                    <div class="box-content">
                        <h4 class="name">Alberta Health Services</h4>
                        <h5 class="position">100, 3705 35 Street N.E., Calgary, AB, T1Y 6C2</h5>
                        <span class="phone"><i class="fas fa-phone mr-1"></i> 780-342-2000</span>
                        <a target="_blank" href="https://www.albertahealthservices.ca">
                            <i class="fas fa-envelope mr-1"></i> www.albertahealthservices.ca
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="fourth-section">
        <div class="container">
            <div class="row">

                <div class="col-lg-6">
                    <div class="map-area">
                        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2506.223529108148!2d-113.98492028470892!3d51.08588214925639!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x537164eaccb08a55%3A0xf65b98d626a79e13!2sWhitehorn+Multi-Services+Centre!5e0!3m2!1sen!2s!4v1538759001062" width="100%" height="360" frameborder="0" style="border:0" allowfullscreen></iframe>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="img-wrap">
                        <img src="<?=base_url('assets/styles/images/KNG-FAMILY.png')?>" alt="Main Image" class="img-fluid">
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>