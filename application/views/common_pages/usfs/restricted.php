<!--<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">-->
<div class="header">
    <div class="header-top">
        <div class="container-fluid">
            <div class="row align-items-center justify-content-between">

                <div class="col-md-auto">
                    <div class="logo-wrap">
                        <a href="<?=base_url('usfs')?>">
                            <img class="usfs_pad" src="<?=base_url('assets/styles/images/usfs.png')?>" alt="USFS Logo">
                        </a>
                    </div>
                </div>

                <div class="col-md-auto">
                    <div class="header-top-content">
                        <h3 class="phone">
                            <i class="fas fa-phone"></i>
                            <a href="tel: 1-800-422-6281">1-800-422-6281</a>
                        </h3>
                        <h3 class="email">
                            <i class="fas fa-envelope"></i>
                            <a href="mailto:service@bktechnologies.com">service@bktechnologies.com</a>
                        </h3>
                    </div>
                </div>

                <div class="col-md-auto">
                    <div class="logo-wrap bk-logo-portal">
                        <a target="_blank" href="https://bktechnologies.com/">
                            <img src=<?=base_url("assets/styles/images/bk-logo.jpg")?> alt="BK Logo">
                        </a>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <nav class="mb-1 navbar navbar-expand-lg navbar-dark info-color">
        <a class="navbar-brand" href="<?=base_url();?>">BK Technologies</a>
        <button class="navbar-toggler collapsed" type="button" data-toggle="collapse" data-target="#navbarSupportedContent-4" aria-controls="navbarSupportedContent-4" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="navbar-collapse collapse" id="navbarSupportedContent-4" style="">
            <ul class="navbar-nav ml-auto">

                <li class="nav-item">
                    <a class="nav-link waves-effect waves-light" href="<?=base_url('usfs')?>">
                        <i class="fas fa-home"></i> Return To Dashboard
                    </a>
                </li>

                <li class="nav-item active">
                        <a class="nav-link waves-effect waves-light" href="<?=base_url('usfs_logout')?>">
                            <i class="fas fa-power-off"></i> Logout
                        </a>
                </li>
            </ul>
        </div>
    </nav>
</div>

<div class="page-main">
    <div class="container">
        <div class="row">
            <div class="col-lg-1" style="margin: 20px;"></div>
            <div class='col-lg-12'>
                <table class="table table-striped table-bordered">
                    <thead>
                    <tr>
                        <th>Model</th>
                        <th>Firmware</th>
                        <th>Software</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td>KNG Series (5000 Channel Radios)</td>
                        <td ><a href="<?php echo base_url('assets/files/firmware/KNG_5.6.0a_All_in_One.zip');?>">5.6.0A</a></td>
                        <td ><a href="<?php echo base_url('assets/files/software/KAA0733_5_6_0a.zip');?>">5.6.0</a></td>
                    </tr>
                    <tr>
                        <td>KNG-P150S</td>
                        <td ><a href="<?php echo base_url('assets/files/firmware/KNGs_Field_Update_1.6.1.zip');?>">1.6.1</a></td>
                        <td ><a href="<?php echo base_url('assets/files/software/KAA0730_1.2.0.zip');?>">1.2.0</a></td>
                    </tr>
                    <tr>
                        <td>KNG Series (5000 Channel Radios)</td>
                        <td ><a href="<?php echo base_url('assets/files/firmware/Lightning Light All-in-One 2.1.11-5.6.1.zip');?>">5.6.1</a></td>
                        <td >
                            <a href="<?php echo base_url('assets/files/software/RES_windows_XP_5_6_4.zip');?>">5.6.4 - Windows XP (32b)</a><br>
                            <a href="<?php echo base_url('assets/files/software/RES_windows_5_6_4.zip');?>">5.6.4 - Windows 7 and Windows 10 (32b)</a><br>
                            <a href="<?php echo base_url('assets/files/software/RES_windows-x64_XP_5_6_4.zip');?>">5.6.4 - Windows XP (64b)</a><br>
                            <a href="<?php echo base_url('assets/files/software/RES_windows-x64_5_6_4.zip');?>">5.6.4 - Windows 7 and Windows 10 (64b)</a></a>
                        </td>
                    </tr>
                    <!--
                    <tr>
                        <td>KAA0660</td>
                        <td><a href="<?php echo base_url('assets/files/firmware/5.6.1-20200310T155604Z-001.zip');?>">5.6.1</a></td>
                        <td>N/A</td>
                    </tr> -->
                    <tr>
                        <td>Group99 Patch</td>
                        <td ></td>
                        <td ></td>
                    </tr>
                    <tr>
                        <td>DPHX5102X</td>
                        <td ><a href="<?php echo base_url('assets/files/firmware/DPHx_Field_Update_103.zip');?>">3.5.5</a></td>
                        <td ><a href="<?php echo base_url('assets/files/software/LAA0744X_3.5.5.zip');?>">3.5.5</a></td>
                    </tr>
                    <tr>
                        <td>DMH5992X</td>
                        <td ><a href="<?php echo base_url('assets/files/firmware/DMH_Update_102.zip');?>">Update #102</a></td>
                        <td ><a href="<?php echo base_url('assets/files/software/LAA0745_1.1.3.zip');?>">1.1.3</a></td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<style>

    th {
        background: #294E6E;
        color: #FFF;
        /*font-weight:bold !important;*/
        font: 18px Lato !important;
    }

    td a {
        color: #337ab7!important;
    }

</style>