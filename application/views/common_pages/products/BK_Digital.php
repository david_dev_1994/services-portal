<section id="main-conent">
   
 <div class='container'>
  <div class='row'>
     <div class='col-md-4 col-md-offset-4'></div>
     <center>
      <h3 class='text text-success align-center'>Digital & APCO P25 Radios</h3>
      <p>Click on a product for details</p>
     </center>
    <table class='table  table-striped  table-bordered'>
        <thead>
              <tr>
                 <th>Portables</th>
                 <th>Mobiles</th>
                 <th>Base Stations</th>
                 <th>Repeaters</th>

              </tr>
        </thead>
        <tbody>
                <tr>
                    <td><a href='#'>KNG-P150 & CMD (5000 Ch.)</a></td> 
                    <td><a href=''>KNG-M150</a></td>
                    <td><a href=''>KNG-B150</a></td>
                    <td><a href=''></a></td>

                </tr>
                <tr>
                    <td><a href='#'>KNG-P400 (5000 Ch.)</a></td> 
                    <td><a href=''>KNG-M400</a></td>
                    <td><a href=''>KNG-B400</a></td>
                    <td><a href=''></a></td>

                </tr>
                <tr>
                    <td><a href='#'>KNG-P500 (5000 Ch.)</a></td> 
                    <td><a href=''>KNG-M500</a></td>
                    <td><a href=''>KNG-B500</a></td>
                    <td><a href=''></a></td>

                </tr>
                <tr>
                    <td><a href=''>KNG-P800 (5000 Ch.)</a></td> 
                    <td><a href=''>KNG-M800</a></td>
                    <td><a href=''>KNG-B800</a></td>
                    <td><a href=''></a></td>

                </tr>
                <tr>
                    <td><a href=''>KNG-P150S (512 Ch.)</a></td> 
                    <td><a href=''>DMH5992X
</a></td>
                    <td><a href=''>DBH-01</a></td>
                    <td><a href=''></a></td>

                </tr>
                 <tr>
                    <td><a href=''>KNG-P400S (512 Ch.)</a></td> 
                    <td><a href=''>DMH5992R</a></td>
                    <td><a href=''>DBH-100</a></td>
                    <td><a href=''></a></td>

                </tr>
<tr>
                    <td><a href=''>DPHX5102X</a></td> 
                    <td><a href=''></a></td>
                    <td><a href=''></a></td>
                    <td><a href=''></a></td>

                </tr>
<tr>
                    <td><a href=''>DPH5102-CMD</a></td> 
                    <td><a href=''></a></td>
                    <td><a href=''></a></td>
                    <td><a href=''></a></td>

                </tr>
<tr>
                    <td><a href=''>DPH5102X</a></td> 
                    <td><a href=''></a></td>
                    <td><a href=''></a></td>
                    <td><a href=''></a></td>

                </tr>
<tr>
                    <td><a href=''>KNG2-P150</a></td> 
                    <td><a href=''></a></td>
                    <td><a href=''></a></td>
                    <td><a href=''></a></td>

                </tr>
<tr>
                    <td><a href=''>KNG2-P400</a></td> 
                    <td><a href=''></a></td>
                    <td><a href=''></a></td>
                    <td><a href=''></a></td>

                </tr>
        </tbody>
    </table>
   <div class='col-md-12'>
      <center> <a class='align-center' href='#' >BK Radio Brand Analog Radios</a>|<a class='align-center' href='#' >BK Technology Brand Analog Radios</a>  </center>
   </div>

  </div> 
</div>
</section>
