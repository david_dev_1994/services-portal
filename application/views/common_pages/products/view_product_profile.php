<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<section id="main-conent">
    <div class="container">
  <div class="col-md-12" style="margin-top: 25px">
      <ul class="nav nav-tabs">
          <li class="active"><a data-toggle="tab" href="#home">Firmware & Patches</a></li>
          <li><a data-toggle="tab" href="#menu1">Software</a></li>
          <li><a data-toggle="tab" href="#menu2">Manuals Etc.</a></li>
          <li><a data-toggle="tab" href="#menu3">Drivers Etc..</a></li>
        </ul>

        <div class="tab-content">
          <div id="home" class="tab-pane fade in active">
            <table class="table table-responsive table-striped table-hover table-bordered">
          <thead>
              <tr>
                  <th>Release Date</th>
                  <th>Revision Number</th>
                  <th>Required Editor</th>
                  <th>Release Notes</th>
                  <th>Firmware</th>
              </tr>
          </thead>
          <tbody>
            <?php if (sizeof($productvariation)>0) {
                foreach ($productvariation as $key ) {

              ?>
                <tr>
                  <td><?php echo $key->reDate ?></td>
                  <td><?php echo $key->RevisionNumber ?></td>
                  <td><?php echo $key->RequiredEditor ?></td>
                  <td><a href="<?php echo base_url();?>/assets/images/<?php echo $key->ReleaseNotes; ?>"><img src='<?php echo base_url();?>/assets/img/ReleaseNotesIcon.jpg'></a></td>
                  <td><a href="<?php echo base_url();?>/assets/images/<?php echo $key->File; ?> target="_blank"">Download</td>
                </tr>
                <?php } }else{
                    ?>
                    <tr>
                        <th colspan="4">No data !</th>
                    </tr>
                    <?php
                } ?>
            </tbody>
            </table>
          </div>
          <div id="menu1" class="tab-pane fade">
            <table class="table table-responsive table-striped table-hover table-bordered">
              <thead>
                  <tr>
                      <th>Release Date</th>
                      <th>Revision Number</th>
                      <th>Part Number</th>
                      <th>Release Notes</th>
                      <th>Software</th>
                  </tr>
              </thead>
              <tbody>
                <?php if (sizeof($productSW)>0) {
                    foreach ($productSW as $key ) {
                       
                  ?>
                    <tr>
                      <td><?php echo $key->reDate ?></td>
                      <td><?php echo $key->RevisionNumber ?></td>
                      <td><?php echo $key->PartNumber?></td>
                      <td><a href="<?php echo base_url();?>/assets/images/<?php echo $key->ReleaseNotes ?>"><img src='<?php echo base_url();?>/assets/img/ReleaseNotesIcon.jpg'></a></td>
                      <td><a href="#">Upload</td>
                    </tr>
                    <?php } }else{
                        ?>
                        <tr>
                            <th colspan="4">No data !</th>
                        </tr>
                        <?php
                    } ?>
                </tbody>
                </table>
          </div>
          <div id="menu2" class="tab-pane fade">
            <table class="table table-responsive table-striped table-hover table-bordered">
              <thead>
                  <tr>
                      <th>Type</th>
                      <th>Type</th>
                      <th></th>
                  </tr>
              </thead>
              <tbody>
                <?php if (sizeof($productManual)>0) {
                    foreach ($productManual as $key ) {
                       
                  ?>
                    <tr>
                      <td><?php echo $key->Typemanual ?></td>
                      <td><?php echo $key->Description ?></td>
                      <td><a href="<?php echo base_url();?>/assets/images/<?php echo $key->File ?>" target="_blank">Download</a></td>
                     
                    </tr>
                    <?php } }else{
                        ?>
                        <tr>
                            <th colspan="4">No data !</th>
                        </tr>
                        <?php
                    } ?>
                </tbody>
                </table>
          </div>
          <div id="menu3" class="tab-pane fade">
            <table class="table table-responsive table-striped table-hover table-bordered">
              <thead>
                  <tr>
                      <th>Type</th>
                      <th>Type</th>
                      <th></th>
                  </tr>
              </thead>
              <tbody>
                <?php if (sizeof($ProductDriver)>0) {
                    foreach ($ProductDriver as $key ) {
                       
                  ?>
                    <tr>
                      <td><?php echo $key->OperatingSystem ?></td>
                      <td><?php echo $key->Notes ?></td>
                      <td><a href="<?php echo base_url();?>/assets/images/<?php echo $key->file ?>" target="_blank">Download</a></td>
                     
                    </tr>
                    <?php } }else{
                        ?>
                        <tr>
                            <th colspan="4">No data !</th>
                        </tr>
                        <?php
                    } ?>
                </tbody>
                </table>
          </div>
        </div>
      
  </div>
 
  
  </div>
</section>

<style>
th {
    width: 22% !important;
}

.nav-tabs>li.active>a {
    background: #00a6d5 !important;
    color: white !important;
    border-color: #00a6d5 !important;
    font-size: 16px !important;
    font-family: Lato !important;
    font-weight: 600 !important;
}

.nav-tabs>li>a {
    font-size: 16px !important;
    font-family: Lato !important;
}

table {
    margin-top: 20px !important;
}
</style>