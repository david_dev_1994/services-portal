<section id="main-conent">
    <div class="container text-center" id="products">
<div class="row">
<div class="col-lg-12">
<h1>Product Support</h1>
            <h4 style='margin-top: 10px;'>Software, Firmware and Publications</h4>
<div class="padding-10"></div>
        <p>For the KNG Series and KNG2 Portable Radios , starting with Firmware Release 5.5.0, NeoVision will no longer be
        supported. Instead the new editor, RES will take its place. The compatible RES (Radio Editing Software) version
        is 5.5.0. Please check with your Radio Administrator to see if this software is right for the specifications
        that fit your radio network configuration. If you own a valid copy of the KAA0732 Editor, your serial number
        will work for this updated software.</p>
<div class="padding-20"></div>
            <div class="col-md-12 servks">
                <div>
                    <center>
                        <a href="<?php echo base_url('products/BK_KAA0733');?>">
                            <div class="icon-wrap">
                                <i class="fa fa-user-o" aria-hidden="true"></i>
                            </div>
                            <h2>BK KNG Digital Radios</h2>
                        </a>
                    </center>
                </div>
            </div>

            <!--<div class="col-md-6  servks">
                <div>
                    <center>
                        <a href="<?//=base_url("/products/BK-analog-radios")?>">
                            <div class="icon-wrap">
                                <i class="fa fa-suitcase" aria-hidden="true"></i>
                            </div>
                            <h2>BK Technology Brand Analog Radios</h2>
                        </a>
                    </center>
                </div>
            </div>-->

       <!-- <div class="products__links">

            <a class="font-bold" href="<?php// echo base_url('products/BK_KAA0733');  ?>">KAA0732 > KAA0733 (UPGRADE ONLY)<br>
                If you have a valid copy of KAA0732 and would like to upgrade to KAA0733, click here</a>

            <a class="font-bold" href="<?php// echo base_url('products/BK_Digital');  ?>">BK KNG Digital Radios, Including KAA0733</a>

            <a class="font-bold" href="<?php// echo base_url('/products/BK-analog-radios');  ?>">BK Radio Analog Radios</a>
            <a class="font-bold" href="<?php// echo base_url('products/RELM-analog-radios');  ?>">BK Technology Brand Analog Radios</a>


            <ul>
                <li><a href="<?php// echo base_url('/products/All-Radios');  ?>" class="font-bold">Show all available</a></li>

                <li><a href="<?php// echo base_url('/products/Discontinued-Radios');  ?>" class="font-bold">Show discontinued items</a></li>

                <li><a href="<?php //echo base_url('Manuals/RELM-Manuals');  ?>" class="font-bold">Search all manuals</a></li>
            </ul>
        </div> -->
</div>
</div>
    </div>
</section>