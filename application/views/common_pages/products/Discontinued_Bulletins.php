<section id="main-conent">
   <div class='container'>
     <div class='row'>
     	 <div class="col-md-12">
<h1>Discontinued Bulletins</h1>
     	 	<div class='col-md-3'></div>
     	 	<div class='col-md-6'>
     	 		<center>
     	 			<a href="<?php echo base_url();?>/products/list">BK Bulletins  </a>
     	 			<br>
     	 			
     	 		</center>
     	 	</div>
     	 	<div class='col-md-3'></div>
     	 </div>
     	 
         <div class='col-md-12'>
         	<?php if (sizeof($bulletins)>0){ ?>
            <table class="table table-responsive table-bordered table-striped" style='width:100%'>
             <thead>
               <tr>
               	 <th class="model">Radio Model</th>
               	 <th class="bulletin">Bulletin #</th>
               	 <th class="release">Release Date</th>
               	 <th class="description">Description</th>
               	 <th class="download">Download</th>
               </tr>
             </thead>
             <tbody>
             	
             		
             	<?php foreach ($bulletins as $key ) {
             		?>
             	<tr>
             		<td><?php echo $key->Radio_Model; ?></td>
             		<td><?php echo $key->Bulletin; ?></td>
             		<td><?php echo $key->Release_Date; ?></td>
             		<td><?php echo $key->Description; ?></td>
             		<td><a href='<?php echo base_url();?>/assets/images/<?php echo $key->File; ?>' target="_blank">Download</a></td>

             	</tr>
             	<?php } ?>
             </tbody>
            </table>
            <?php }else{echo "<h2 style='text-align:center;'>Opps! No Data Found!</h2>"; }  ?>
         </div>
     </div>
   </div>
</section>

<style>

table td > table {
    margin-top: 0px !important;
    float: left !important;
    margin-left: -8px !important;
}

table td > table>tbody>tr>td {
    border: 0px solid #ddd !important;
}

td table tr {
    background: transparent !important;
}

</style>
