<section id="main-conent">
   <div class='container'>
     <div class='row'>
     	 <div class="col-md-12">
<h1>BK Radio<span class="trademark big">®</span> Manuals</h1>

     	 </div>
         <div class='col-md-12'>
         	<?php if (sizeof($Manuals)>0){ ?>
            <table class="table table-responsive table-bordered table-striped" id="bkmanualstable" style='width:100%'>
             <thead>
                <tr>
                <th class="model">Model</th>
                <th class="type">Type</th>
                <th class="document">Document</th>
                <th class="manual-download">Download</th>
              </tr>
            </thead>
             <tbody>
             	<?php foreach($Manuals as $row){?>
              <tr id="row_<?php echo $row->id;?>">
                <td><?php echo $row->Model;?></td>
                <td><?php echo $row->Type;?></td>
                <td><?php echo $row->Document;?></td>
                
                <td><a href='<?php echo base_url();?>/assets/images/<?php echo $row->file; ?>' target="_blank">Download</a></td>
              </tr>
              <?php } ?>
            </tbody>
            </table>
            <?php }else{echo "<h2 style='text-align:center;'>Opps! No Data Found!</h2>"; }  ?>
         </div>
     </div>
   </div>
</section>

<style>
th.model {
    width: 10% !important;
}
th.type {
    width: 10% !important;
}
th.manual-download {
    width: 5% !important;
}
</style>