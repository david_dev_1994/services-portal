<section id="main-conent">
    <div class="container text-center" id="software_update">
        <h3 class="su__header">
            BK Technologies<span style="vertical-align: super !important;font-size: 12px !important;">®</span> Serial Number Registration
        </h3>

        <div class="page-main">
            <div class="container">

                <div class="card login-card">

                    <?php if (isset($msg)){?>
                        <div class="errors_container alert alert-danger alert-dismissable">
                            <?= $msg?>
                        </div>
                    <?php }?>
                    <?php if ($this->session->flashdata('success')){?>
                        <div class="alert alert-success alert-dismissable">
                            <?= $this->session->flashdata('success')?>
                        </div>
                    <?php }?>
                    <?php if ($this->session->flashdata("validation_errors")){?>
                        <div class="text-center alert alert-danger alert-dismissable">
                            <?= $this->session->flashdata("validation_errors")?>
                        </div>
                    <?php }?>

                    <h5 class="card-header info-color white-text text-center py-4" style="margin-top: unset !important;">
                        <strong>Register Serial Number</strong>
                    </h5>
                    <!--Card content-->
                    <div class="card-body px-lg-5 pt-0" style="margin: 10px">
                        <!-- Form -->
                        <form class="as-login-form" action="" method="post">
<!--                            <div class="col-md-12 form-group">-->
<!--                                <label for="username">Serial Number</label>-->
<!--                                                            <input class='form-group' maxlength="3" id="T1" name="T1"-->
<!--                                                                   onkeyup="if (this.value.length == 3) document.getElementById('T2').focus();"-->
<!--                                                                   type="text"> - <input class='form-group' id="T2" name="T2" maxlength="3"-->
<!--                                                                                         onkeyup="if (this.value.length == 3) document.getElementById('T3').focus();"-->
<!--                                                                                         type="text"> - <input class='form-group' maxlength="3" id="T3" name="T3"-->
<!--                                                                                                               type="text">-->

<!--                                <input required  class='form-control' type="text" name="first_name" value="--><?php //echo set_value('first_name'); ?><!--" >-->
<!--                            </div>-->
                            <div class="col-md-6 form-group">
                                <label for="username">Serial Number</label>
                                <input required  class='form-control' type="text" name="serial_number" id="serial_number" value="<?php echo set_value('serial_number'); ?>" >
                            </div>
                            <div class="col-md-6 form-group">
                                <label for="username">Company or Organization</label>
                                <input required class='form-control' type="text" name="company" value="<?php echo set_value('company'); ?>" >
                            </div>
                            <div class="col-md-6 form-group">
                                <label for="username">First Name</label>
                                <input required  class='form-control' type="text" name="first_name" value="<?php echo set_value('first_name'); ?>" >
                            </div>
                            <div class="col-md-6 form-group">
                                <label for="username">Last Name</label>
                                <input required class='form-control' type="text" name="last_name" value="<?php echo set_value('last_name'); ?>" >
                            </div>
                            <div class="col-md-6 form-group">
                                <label for="email">Email</label>
                                <input name="email" required type="email" id="email" class='form-control' value="<?php echo set_value('email'); ?>" >
                            </div>

                            <!-- Sign in button -->
                            <button class="btn btn-info btn-block my-4 waves-effect z-depth-0 col-md-12 form-group" type="submit">Submit</button>
                            <!-- Register -->
                        </form>
                        <!-- Form -->
                    </div>
                </div>

            </div>
        </div>

<!--        <form method="post">-->
<!--            <div class="su__content">-->
<!---->
<!---->
<!---->
<!---->
<!--                <div class="container">-->
<!--                    <div class="row">-->
<!--                        <span>Serial Number:</span>-->
<!---->
<!--                        <div class="col-md-6">-->
<!--                            <input maxlength="3" id="T1" name="T1"-->
<!--                                   onkeyup="if (this.value.length == 3) document.getElementById('T2').focus();"-->
<!--                                   type="text"> - <input id="T2" name="T2" maxlength="3"-->
<!--                                                         onkeyup="if (this.value.length == 3) document.getElementById('T3').focus();"-->
<!--                                                         type="text"> - <input maxlength="3" id="T3" name="T3"-->
<!--                                                                               type="text">-->
<!--                        </div>-->
<!---->
<!--                        <div class="col-md-6">-->
<!--                            <button class="continue" type="submit">Continue</button>-->
<!--                        </div>-->
<!--                    </div>-->
<!--                </div>-->
<!--                <p>-->
<!--                    For assistance, please contact tech support at <a href="tel:8004226281">(800) 422 6281.</a>-->
<!--                </p>-->
<!--            </div>-->
<!---->
<!--        </form>-->
    </div>
</section>


<style>

    button.continue {
        border-radius: 0px !important;
        background: #00a6d5 !important;
        border-color: #00a6d5 !important;
        color: #ffffff !important;
        font-family: Lato !important;
        text-transform: uppercase !important;
        font-size: 14px !important;
        display: inline-block !important;
        font-weight: 600 !important;
        text-align: center !important;
        padding: 5px 18px !important;
        transition: padding 0.5s ease, background 0.5s ease, border-color 0.5s ease, color 0.5s ease !important;
        border: none !important;
    }

    button.continue:hover {
        background: #b90000 !important;
    }

    input {
        padding: 3.5px 5px !important;
    }

</style>
<!--<script>-->
<!---->
<!--    $(function () {-->
<!--        $('td>a').each(function () {-->
<!--            var href = $(this).attr('href');-->
<!--            $(this).attr('href', '/service-portal' + href);-->
<!--        });-->
<!--        $('td>a>img').each(function () {-->
<!--            var src = $(this).attr('src');-->
<!--            $(this).attr('src', '/service-portal' + src);-->
<!--        });-->
<!---->
<!---->
<!--    });-->
<!--</script>-->