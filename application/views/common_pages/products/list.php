<section id="main-conent">
   <div class='container'>
     <div class='row'>
     	 <div class="col-md-12">
<center>
     	 			<h1>BK Radio<span class="trademark big">®</span> Service Bulletins</h1>
     	 			<a href="<?php echo base_url();?>/products/Archived_Bulletins">Show Archived Bulletins </a>|
					<a href="<?=base_url("/Manuals/BK-Manuals")?>">Manuals</a>
     	 			<br>
     	 			<a href="<?php echo base_url();?>/products/discontinued">Click Here for discontinued items</a>
</center>
     	 </div>
         <div class='col-lg-12'>
         	<?php if (sizeof($bulletins)>0){ ?>
            <table class="table table-responsive table-bordered table-striped" style='width:100%'>
             <thead>
               <tr>
               	 <th class="model">Radio Model</th>
               	 <th class="bulletin">Bulletin #</th>
               	 <th class="release">Release Date</th>
               	 <th class="description">Description</th>
               	 <th class="download">Download</th>
               </tr>
             </thead>
             <tbody>
            <tr>
             		<td>All Radios</td>
             		<td>BKSB-1058</td>
             		<td>2020-03-19</td>
             		<td><p>Cleaning and disinfecting guidelines for BK Technologies radios.</p></td>
             		<td><a href="/service-portal//assets/images/BKSB-1058.pdf" target="_blank">Download</a></td>
             </tr>
             		
             	<?php foreach ($bulletins as $key ) {
             		?>
             	<tr>
             		<td><?php echo $key->Radio_Model; ?></td>
             		<td><?php echo $key->Bulletin; ?></td>
             		<td><?php echo $key->Release_Date; ?></td>
             		<td><?php echo $key->Description; ?></td>
             		<td><a href='<?php echo base_url();?>assets/images/<?php echo $key->File; ?>' target="_blank">Download</a></td>

             	</tr>
             	<?php } ?>
             </tbody>
            </table>
            <?php }else{echo "<h2 style='text-align:center;'>Opps! No Data Found!</h2>"; }  ?>
         </div>
     </div>
   </div>
</section>

<script>
$("td:contains('2016-10-06')").html("2015-10-06");
$("td:contains('2016-11-06')").html("2014-11-06");
</script>
