<?php
if(isset($_SESSION["ProductNumber"]))
	$ProductNumber = $_SESSION["ProductNumber"];
else
	$ProductNumber = '';
?>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<section id="main-conent">
    <div class="container">
        <div class="col-md-12" style="margin-top: 25px">
            <ul class="nav nav-tabs">
                <li class="active"><a data-toggle="tab" href="#home">Firmware & Patches</a></li>
                <li><a data-toggle="tab" href="#menu1">Software</a></li>
                <!-- <li><a data-toggle="tab" href="#menu2">Manuals Etc.</a></li> -->
                <li><a data-toggle="tab" href="#menu3">Drivers Etc..</a></li>
            </ul>

            <div class="tab-content">
                <div id="home" class="tab-pane fade in active">
                    <table class="table table-responsive table-striped table-hover table-bordered">
                        <thead>
                        <tr>
                            <th>Release Date</th>
                            <th>Revision Number</th>
                            <th>Required Editor</th>
                            <th>Release Notes</th>
                            <th>Firmware</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php if (sizeof($firmwares)>0) {
                            foreach ($firmwares as $key ) {
                                if($key->version != '5.6.0a'){
                                ?>
                                <tr>
                                    <td><?php echo $key->date ?></td>
                                    <td><?php echo $key->version ?></td>
                                    <td><?php echo $key->softwareversion ?></td>
                                    <td><?php echo str_replace("/assets","../assets",$key->notes); ?></td>
<!--                                    <td>--><?php //echo $key->notes; ?><!--</td>-->
<!--                                    <td><a href="--><?php //echo base_url();?><!--/assets/images/--><?php //echo $key->notes; ?><!--"><img src='--><?php //echo base_url();?><!--/assets/img/ReleaseNotesIcon.jpg'></a></td>-->
<!--                                    <td><a href="--><?php //echo base_url('assets/files/firmware/');?><!----><?php //echo $key->file_name; ?><!--">Download</td>-->

                                    <td><a href="#" data-toggle="modal" data-revision="<?php echo $key->version ?>" data-editor="<?php echo $key->softwareversion ?>" data-file_name="<?php echo $key->file_name;?>" data-target="#select-Product" >Download</td>
                                </tr>
                            <?php }
                            } }else{
                            ?>
                            <tr>
                                <th colspan="5">No data !</th>
                            </tr>
                            <?php
                        } ?>
                        </tbody>
                    </table>
                </div>
                <div id="menu1" class="tab-pane fade">
                    <table class="table table-responsive table-striped table-hover table-bordered">
                        <thead>
                        <tr>
                            <th>Release Date</th>
                            <th>Revision Number</th>
                            <th>Part Number</th>
                            <th>Release Notes</th>
                            <th>Software</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php if (sizeof($softwares)>0) {
                            foreach ($softwares as $key ) {
                                if($key->version != '5.6.0a'){
                                ?>
                                <tr>
                                    <td><?php echo $key->date ?></td>
                                    <td><?php echo $key->version ?></td>
                                    <td><?php echo $key->partnumber?></td>
                                    <td><?php echo str_replace("/assets","../assets",$key->notes); ?></td>
<!--                                    <td>--><?php //echo $key->notes; ?><!--</td>-->
<!--                                    <td><a href="--><?php //echo base_url();?><!--/assets/images/--><?php //echo $key->notes ?><!--"><img src='--><?php //echo base_url();?><!--/assets/img/ReleaseNotesIcon.jpg'></a></td>-->
                                    <td><a href="<?php echo base_url('software/update/'.$ProductNumber); ?>">Download</td>
                                </tr>
                            <?php
                            }
                        } }else{
                            ?>
                            <tr>
                                <th colspan="5">No data !</th>
                            </tr>
                            <?php
                        } ?>
                        </tbody>
                    </table>
                </div>
                <div id="menu2" class="tab-pane fade">
                    <table class="table table-responsive table-striped table-hover table-bordered">
                        <thead>
                        <tr>
                            <th>Type</th>
                            <th colspan="4"  style="min-width: 400px">Description</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php if (sizeof($manuals)>0) {
                            foreach ($manuals as $key ) {

                                ?>
                                <tr>
                                    <td><?php echo $key->type ?></td>
                                    <td><?php echo $key->description ?></td>
                                    <td><a href="<?php echo base_url('assets/files/maunal/');?><?php echo $key->file_name ?>" target="_blank">Download</a></td>

                                </tr>
                            <?php } }else{
                            ?>
                            <tr>
                                <th colspan="5">No data !</th>
                            </tr>
                            <?php
                        } ?>
                        </tbody>
                    </table>
                </div>
                <div id="menu3" class="tab-pane fade">
                    <table class="table table-responsive table-striped table-hover table-bordered">
                        <thead>
                        <tr>
                            <th>OperatingSystem</th>
                            <th colspan="4" style="min-width: 400px">Notes</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php if (sizeof($drivers)>0) {
                            foreach ($drivers as $key ) {

                                ?>
                                <tr>
                                    <td><?php echo $key->opsystem ?></td>
                                    <td><?php echo $key->notes ?></td>
                                    <td><a href="<?php echo base_url('assets/files/driver/');?><?php echo $key->driver ?>" target="_blank">Download</a></td>

                                </tr>
                            <?php } }else{
                            ?>
                            <tr>
                                <th colspan="5">No data !</th>
                            </tr>
                            <?php
                        } ?>
                        </tbody>
                    </table>
                </div>
            </div>

        </div>


    </div>
</section>

<!-- Product POPUP -->
<div class="modal fade" id="select-Product" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">

            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">Please fill out the following information to enable download</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            </div>

            <div class="modal-body">
                <div class="container-fluid">
                    <div class="row">
                        <form id="firmFormSubmit">
                            <div class="col-md-12 form-group row-group">
                                <div id="errorToModal" class="errorToModal alert alert-danger alert-dismissible text-center row hidden"></div>
                                <div class="col-md-6 form-group">
                                    <label for="validationCustom01">Name: </label>
                                    <input id="input-field" name="name" class="personName form-control" required type="text">
                                </div>
                                <div class="col-md-6 form-group">
                                    <label for="validationCustom01">Company Name: </label>
                                    <input id="input-field" name="companyName" class="companyName form-control" type="text">
                                </div>
                                <div class="col-md-6 form-group">
                                    <label for="validationCustom01">Number of Radios: </label>
                                    <input id="input-field" name="radioNumber" class="radioNumber form-control" value="1" required type="number" min="1">
                                </div>
                                <div class="col-md-6 form-group">
                                    <label for="validationCustom01">Email Address: </label>
                                    <input id="input-field" name="email" class="email form-control" type="email" required>
                                </div>
                                <div class="col-md-6 form-group">
                                    <label for="validationCustom01">Phone Number: </label>
                                    <input id="input-field" name="pnum" class="pnum form-control" type="text" required>
                                </div>
                                <div class="col-md-6 form-group">
                                    <label for="validationCustom01">Serial Number: </label>
                                    <input id="input-field" name="serial_number" class="serial_number form-control" type="text" required>
                                </div>
                                <div class="col-md-12 form-group">
                                    <label for="validationCustom01">Current Firmware version: </label>
                                    <input id="input-field" name="firm_version" class="firm_version form-control" type="text" required>
                                </div>
                                <div class="col-md-6 form-group" hidden>
                                    <label for="validationCustom01">Revision Number: </label>
<!--                                    <div class="revisionNumberDiv"></div>-->
                                    <input id="input-field" name="revisionNumber" class="revisionNumberInp form-control" readonly type="text">
                                </div>
                                <div class="col-md-6 form-group" hidden>
                                    <label for="validationCustom01">Required Editor: </label>
<!--                                    <div class="editorDiv"></div>-->
                                    <input id="input-field" name="editor" class="editorInp form-control" readonly type="text">
                                </div>
                                <input id="input-field" name="fileName" class="fileName hidden">
                                <input id="input-field" name="firmware" value="firmware" class="hidden">
                            </div>
                            <div class="col-md-12 form-group">
                                <input value="SUBMIT" name="submit" class="btn btn-success form-control" type="submit">
                            </div>
                        </form>
                    </div>
                </div>
            </div>

            <div class="modal-footer">
<!--                <button type="button" data-dismiss="modal" class="btn btn-default saveAndMore">Submit</button>-->
                <!--                <a  data-dismiss="modal" class="btn btn-primary generateQuote" onclick="return profileValidation();">Generate Quote</a>-->
                <button data-dismiss="modal" class="btn btn-primary">Close</button>
            </div>
        </div>
    </div>
</div>
<!-- END HELP POPUP -->

<style>
.table-responsive {
    display: table !important;
}
th {
    width: 22% !important;
}
.nav-tabs>li.active>a {
    background: #00a6d5 !important;
    color: white !important;
    border-color: #00a6d5 !important;
    font-size: 16px !important;
    font-family: Lato !important;
    font-weight: 600 !important;
}

.nav-tabs>li>a {
    font-size: 16px !important;
    font-family: Lato !important;
}

table {
    margin-top: 20px !important;
}
</style>