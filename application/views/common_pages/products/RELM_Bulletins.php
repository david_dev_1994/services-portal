<section id="main-conent">
   <div class='container'>
     <div class='row'>
     	 <div class="col-md-12">
     	 	<div class='col-md-3'></div>
     	 	<div class='col-md-6'>
     	 		<center>
     	 			<h3 style='font-weight: bold; margin-top:50px'>BK Technology Radio Service Bulletins</h3>
     	 			<a href="<?php echo base_url();?>/products/list">BK Bulletins  </a> | 
     	 			<a href="<?php echo base_url();?>/products/Bulletins-search">Search Bulletins</a>
     	 			<br>
     	 			<a href="<?php echo base_url();?>/products/discontinued">Click Here for discontinued items</a>
     	 		</center>
     	 	</div>
     	 	<div class='col-md-3'></div>
     	 </div>
         <div class='col-md-12'>
         	<?php if (sizeof($bulletins)>0){ ?>
            <table class="table table-responsive table-bordered table-striped" style='width:100%'>
             <thead>
               <tr>
               	 <th>Radio Model</th>
               	 <th>Bulletin #</th>
               	 <th>Release Date</th>
               	 <th>Description</th>
               	 <th></th>
               </tr>
             </thead>
             <tbody>
             	
             		
             	<?php foreach ($bulletins as $key ) {
             		?>
             	<tr>
             		<td><?php echo $key->Radio_Model; ?></td>
             		<td><?php echo $key->Bulletin; ?></td>
             		<td><?php echo $key->Release_Date; ?></td>
             		<td><?php echo $key->Description; ?></td>
             		<td><a href='<?php echo base_url();?>/assets/images/<?php echo $key->File; ?>' target="_blank">Download</a></td>

             	</tr>
             	<?php } ?>
             </tbody>
            </table>
            <?php }else{echo "<p style='text-align:center;'>Opps! No Data Found!</p>"; }  ?>
         </div>
     </div>
   </div>
</section>
