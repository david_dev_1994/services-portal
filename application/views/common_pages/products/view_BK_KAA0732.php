<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<section id="main-conent">
    <div class="container">
<div class="row">
  <div class="col-md-12">
      <div class="text-center">
         <h1>Digital & APCO P25 Radios</h1>
          <h4>Click on a product for details</h4>
      </div>
  </div>
</div>

<div class="row">
<div class="col-lg-1"></div>
  <div class="col-lg-10">
      <table class="table table-responsive table-striped table-hover table-bordered">
          <thead>
              <tr>
                  <th>Portables</th>
                  <th>Mobiles</th>
                  <th>Base Stations</th>
                  <th>Repeaters</th>
              </tr>
          </thead>
          <tbody>

          <tr>

              <?php
              echo '<td style="width: 25%">';
              foreach ($productlist['Portable'] as $val){
                  ?>

                  <form method="post">
                      <input type="hidden" name="productvariation" value="<?php echo $val->name ?>">
                      <input type="submit" class="btn btn-link" name="productlistsubmit" value="<?php echo $val->name ?>">
                  </form>

                  <?php
              }
              echo '</td><td style="width: 25%">';
              foreach ($productlist['Mobile'] as $val){
                  ?>
                  <form method="post">
                      <input type="hidden" name="productvariation" value="<?php echo $val->name ?>">
                      <input type="submit" class="btn btn-link" name="productlistsubmit" value="<?php echo $val->name ?>">
                  </form>
                  <?php
              }
              echo '</td><td style="width: 25%">';
              foreach ($productlist['Base'] as $val){
                  ?>
                  <form method="post">
                      <input type="hidden" name="productvariation" value="<?php echo $val->name ?>">
                      <input type="submit" class="btn btn-link" name="productlistsubmit" value="<?php echo $val->name ?>">
                  </form>
                  <?php
              }
              echo '</td><td style="width: 25%">';
              ?>
              <form method="post">
                  <input type="hidden" name="productvariation" value="RDPR">
                  <input type="submit" class="btn btn-link" name="productlistsubmit" value="RDPR">
              </form>

              <?php
              echo '</td>';
              ?>
          </tr>
          </tbody>
      </table>
  </div>
<div class="col-lg-1"></div>
</div>

<div class="row">
  <div class="col-md-12">
     <center>
        <!-- <a class='align-center' href='<?php// echo base_url();?>/products/BK-analog-radios' >BK Radio Brand Analog Radios</a>|
        <a class='align-center' href='<?php// echo base_url();?>/products/RELM-analog-radios' >BK Technology Brand Analog Radios</a> --> </center>
  </div>
  </div>
</div>
</section>

<style>

th {
width: 25% !important;
}
</style>