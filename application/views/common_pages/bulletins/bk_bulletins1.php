<section id="main-conent">
    <div class="container text-center" id="bk_bulletins">
        <p class="bk__header">
            BK Technology Radio Software Updater
        </p>
        <p>
            <a href="">BK Technology Bulletins</a> | <a href="">Search Bulletins</a>
        </p>
        <a href="">Click Here for discontinued items</a>
        <div class="bk__table">
            <table id='example'>
                <tr>
                    <td><b>Radio Model</b></td>
                    <td><b>Bulletin #</b></td>
                    <td><b>Release Date</b></td>
                    <td><b>Description</b></td>
                    <td></td>
                </tr>
                <tr>
                    <td>KNG &amp; KNG2 Portable</td>
                    <td>BKSB-1051</td>
                    <td>6/7/2017</td>
                    <td>Firmware Loading Compatibility</td>
                    <td><a class="LINK" target="_blank" href="../Bulletins/BK/BKSB-1051.pdf">Download</a>
                    </td>
                </tr>
                <tr>
                    <td>KNG, KNG2 Portable &amp; KNG Mobile</td>
                    <td>BKSB-1050</td>
                    <td>6/7/2017</td>
                    <td>Destination Clone Errors</td>
                    <td><a class="LINK" target="_blank" href="../Bulletins/BK/BKSB-1050.pdf">Download</a>
                    </td>
                </tr>
                <tr>
                    <td>KNG, KNG2 Portable &amp; KNG Mobile</td>
                    <td>BKSB-1049</td>
                    <td>6/7/2017</td>
                    <td>Update - Firmware (Ver. 5.5.0c)</td>
                    <td><a class="LINK" target="_blank" href="../Bulletins/BK/BKSB-1049.pdf">Download</a>
                    </td>
                </tr>
                <tr>
                    <td>KNG, KNG2 Portable &amp; KNG Mobile</td>
                    <td>BKSB-1048</td>
                    <td>1/30/2017</td>
                    <td>Software Update - KAA0733 RES (Ver. 5.5.0)
                        <p>
                            Replaces NeoVision KAA0732/735
                        </p>
                    </td>
                    <td><a class="LINK" target="_blank" href="../Bulletins/BK/BKSB-1048.pdf">Download</a>
                    </td>
                </tr>
                <tr>
                    <td>KNG, KNG2 Portable &amp; KNG Mobile</td>
                    <td>BKSB-1047</td>
                    <td>1/29/2017</td>
                    <td>Update - Firmware (Ver. 5.5.0a)</td>
                    <td><a class="LINK" target="_blank" href="../Bulletins/BK/BKSB-1047.pdf">Download</a>
                    </td>
                </tr>
                <tr>
                    <td>KNG Portable &amp; Mobile</td>
                    <td>BKSB-1043</td>
                    <td>7/29/2016</td>
                    <td>KNG to KNG cloning incompatibility</td>
                    <td><a class="LINK" target="_blank" href="../Bulletins/BK/BKSB-1043.pdf">Download</a>
                    </td>
                </tr>
                <tr>
                    <td>KNG Portable &amp; Mobile</td>
                    <td>BKSB-1042</td>
                    <td>6/3/2016</td>
                    <td>Firmware/Radio Editor release information</td>
                    <td><a class="LINK" target="_blank" href="../Bulletins/BK/BKSB-1042.pdf">Download</a>
                    </td>
                </tr>
                <tr>
                    <td>KNG-P150S, KNG-P400S</td>
                    <td>BKSB-1041</td>
                    <td>6/10/2015</td>
                    <td>2 Watt Alignment Utility</td>
                    <td><a class="LINK" target="_blank" href="../Bulletins/BK/BKSB-1041.pdf">Download</a>
                    </td>
                </tr>
                <tr>
                    <td>KNG-M, KAA0660</td>
                    <td>BKSB-1040</td>
                    <td>2/3/2015</td>
                    <td>Key ring on Hirose connector breaking.</td>
                    <td><a class="LINK" target="_blank" href="../Bulletins/BK/BKSB-1040.pdf">Download</a>
                    </td>
                </tr>
                <tr>
                    <td>KNG Portable &amp; Mobile</td>
                    <td>BKSB-1039</td>
                    <td>6/16/2014</td>
                    <td>Selectable Power Setting Changes</td>
                    <td><a class="LINK" target="_blank" href="../Bulletins/BK/BKSB-1039.pdf">Download</a>
                    </td>
                </tr>
                <tr>
                    <td>KNG-PxxxS</td>
                    <td>BKSB-1038</td>
                    <td>6/11/2014</td>
                    <td>Firmware/Radio Editor release information</td>
                    <td><a class="LINK" target="_blank" href="../Bulletins/BK/BKSB1038.pdf">Download</a>
                    </td>
                </tr>
                <tr>
                    <td>KNG Portable &amp; Mobile</td>
                    <td>BKSB-1034</td>
                    <td>9/14/2012</td>
                    <td>Threshold squelch default settting too tight</td>
                    <td><a class="LINK" target="_blank" href="../Bulletins/BK/BKSB1034.pdf">Download</a>
                    </td>
                </tr>
                <tr>
                    <td>KNG Portables, Mobiles and Control Heads</td>
                    <td>BKSB-1033</td>
                    <td>8/9/2012</td>
                    <td>Firmware/Radio Editor release information</td>
                    <td><a class="LINK" target="_blank" href="../Bulletins/BK/BKSB1033.pdf">Download</a>
                    </td>
                </tr>
                <tr>
                    <td>KNG Portables, Mobiles, Control Heads</td>
                    <td>BKSB-1032</td>
                    <td>3/21/2012</td>
                    <td>Add Features and general maintenance update.</td>
                    <td><a class="LINK" target="_blank" href="../Bulletins/BK/BKSB1032.pdf">Download</a>
                    </td>
                </tr>
                <tr>
                    <td>KNG Portables</td>
                    <td>BKSB-1030</td>
                    <td>4/15/2010</td>
                    <td>Firmware upgrade for added features.
                        <p>
                            PC Software update to 1.0.6.0
                        </p></td>
                    <td><a class="LINK" target="_blank" href="../Bulletins/BK/BKSB1030.pdf">Download</a>
                    </td>
                </tr>
                <tr>
                    <td>GMH, GMH+, DMH</td>
                    <td>BKSB-1029</td>
                    <td>11/17/2009</td>
                    <td>Blown display when jump starting vehicle</td>
                    <td><a class="LINK" target="_blank" href="../Bulletins/BK/BKSB-1029.pdf">Download</a>
                    </td>
                </tr>
                <tr>
                    <td>KNG Portable</td>
                    <td>BKSB-1028</td>
                    <td>8/5/2009</td>
                    <td>Firmware update information</td>
                    <td><a class="LINK" target="_blank" href="../Bulletins/BK/BKSB-1028.pdf">Download</a>
                    </td>
                </tr>
                <tr>
                    <td>DPH-CMD</td>
                    <td>BKSB-1027</td>
                    <td>5/27/2009</td>
                    <td>Firmware update</td>
                    <td><a class="LINK" target="_blank" href="../Bulletins/BK/BKSB-1027.pdf">Download</a>
                    </td>
                </tr>
                <tr>
                    <td>ALL BK</td>
                    <td>BKSB-1026</td>
                    <td>2/2/2009</td>
                    <td>IOGEAR GUC232A (Information Only)</td>
                    <td><a class="LINK" target="_blank" href="../Bulletins/BK/BKSB-1026.pdf">Download</a>
                    </td>
                </tr>
                <tr>
                    <td>DPHx</td>
                    <td>BKSB-1025</td>
                    <td>9/2/2008</td>
                    <td>Quantar BCL</td>
                    <td><a class="LINK" target="_blank" href="../Bulletins/BK/BKSB-1025.pdf">Download</a>
                    </td>
                </tr>
                <tr>
                    <td>GPH-CMD</td>
                    <td>BKSB-1024</td>
                    <td>6/4/2008</td>
                    <td>Keypad FCN menu issues</td>
                    <td><a class="LINK" target="_blank" href="../Bulletins/BK/BKSB-1024.pdf">Download</a>
                    </td>
                </tr>
                <tr>
                    <td>DMH</td>
                    <td>BKSB-1023</td>
                    <td>4/9/2008</td>
                    <td>User selectable Squelch &amp; Quantar BCL</td>
                    <td><a class="LINK" target="_blank" href="../Bulletins/BK/BKSB-1023.pdf">Download</a>
                    </td>
                </tr>
                <tr>
                    <td>GPH-CMD</td>
                    <td>BKSB-1022</td>
                    <td>12/12/2005</td>
                    <td>Group Scan</td>
                    <td><a class="LINK" target="_blank" href="../Bulletins/BK/BKSB-1022.pdf">Download</a>
                    </td>
                </tr>
                <tr>
                    <td>DPHx</td>
                    <td>BKSB-1021</td>
                    <td>12/17/2004</td>
                    <td>Firmware update</td>
                    <td><a class="LINK" target="_blank" href="../Bulletins/BK/BKSB-1021.pdf">Download</a>
                    </td>
                </tr>
                <tr>
                    <td>DPH</td>
                    <td>BKSB-1019</td>
                    <td>11/13/2004</td>
                    <td>Firmware update</td>
                    <td><a class="LINK" target="_blank" href="../Bulletins/BK/BKSB-1019.pdf">Download</a>
                    </td>
                </tr>
                <tr>
                    <td>LZA2027</td>
                    <td>BKSB-1018</td>
                    <td>7/23/2003</td>
                    <td>Difficulty adjusting Audio Levels</td>
                    <td><a class="LINK" target="_blank" href="../Bulletins/BK/BKSB-1018.pdf">Download</a>
                    </td>
                </tr>
                <tr>
                    <td>LAA0258</td>
                    <td>BKSB-1017</td>
                    <td>7/23/2003</td>
                    <td>No Tx audio from desk microphone&nbsp;</td>
                    <td><a class="LINK" target="_blank" href="../Bulletins/BK/BKSB-1017.pdf">Download</a>
                    </td>
                </tr>
                <tr>
                    <td>DPH</td>
                    <td>BKSB-1016</td>
                    <td>5/6/2003</td>
                    <td>Squelch Information only.</td>
                    <td><a class="LINK" target="_blank" href="../Bulletins/BK/BKSB-1016.pdf">Download</a>
                    </td>
                </tr>
                <tr>
                    <td>LZA2027</td>
                    <td>BKSB-1015</td>
                    <td>12/10/2002</td>
                    <td>Manual update</td>
                    <td><a class="LINK" target="_blank" href="../Bulletins/BK/BKSB-1015.pdf">Download</a>
                    </td>
                </tr>
                <tr>
                    <td>GBH</td>
                    <td>BKSB-1014</td>
                    <td>9/26/2002</td>
                    <td>Poor audio/lock-up with Tone Remote</td>
                    <td><a class="LINK" target="_blank" href="../Bulletins/BK/BKSB-1014.pdf">Download</a>
                    </td>
                </tr>
                <tr>
                    <td>GPH</td>
                    <td>BKSB-1013</td>
                    <td>9/10/2002</td>
                    <td>Display flashing "FAIL"</td>
                    <td><a class="LINK" target="_blank" href="../Bulletins/BK/BKSB-1013.pdf">Download</a>
                    </td>
                </tr>
                <tr>
                    <td>GPH</td>
                    <td>BKSB-1010A</td>
                    <td>1/4/2002</td>
                    <td>Non-Flash / Low Volume</td>
                    <td><a class="LINK" target="_blank" href="../Bulletins/BK/BKSB-1010A.pdf">Download</a>
                    </td>
                </tr>
                <tr>
                    <td>GBH&nbsp;</td>
                    <td>BKSB-1011</td>
                    <td>11/19/2001</td>
                    <td>PS fuse accessibility</td>
                    <td><a class="LINK" target="_blank" href="../Bulletins/BK/BKSB-1011.pdf">Download</a>
                    </td>
                </tr>
                <tr>
                    <td>GMH</td>
                    <td>BKSB-1007</td>
                    <td>9/12/2001</td>
                    <td>Superfluous display at power up</td>
                    <td><a class="LINK" target="_blank" href="../Bulletins/BK/BKSB-1007.pdf">Download</a>
                    </td>
                </tr>
                <tr>
                    <td>EPH</td>
                    <td>BKSB-1006</td>
                    <td>8/17/2001</td>
                    <td>Flex mode operation</td>
                    <td><a class="LINK" target="_blank" href="../Bulletins/BK/BKSB-1006.pdf">Download</a>
                    </td>
                </tr>
                <tr>
                    <td>GPH</td>
                    <td>BKSB-1003A</td>
                    <td>8/15/2001</td>
                    <td>Update to SB-1003</td>
                    <td><a class="LINK" target="_blank" href="../Bulletins/BK/BKSB-1003A.pdf">Download</a>
                    </td>
                </tr>
                <tr>
                    <td>GMH</td>
                    <td>BKSB-1005</td>
                    <td>8/15/2001</td>
                    <td>No display in low light</td>
                    <td><a class="LINK" target="_blank" href="../Bulletins/BK/BKSB-1005.pdf">Download</a>
                    </td>
                </tr>
                <tr>
                    <td>EPH</td>
                    <td>BKSB-1004</td>
                    <td>9/29/2000</td>
                    <td>High temp comm failure</td>
                    <td><a class="LINK" target="_blank" href="../Bulletins/BK/BKSB-1004.pdf">Download</a>
                    </td>
                </tr>
                <tr>
                    <td>GPH</td>
                    <td>BKSB-1003</td>
                    <td>9/29/2000</td>
                    <td>High temp comm failure</td>
                    <td><a class="LINK" target="_blank" href="../Bulletins/BK/BKSB-1003.pdf">Download</a>
                    </td>
                </tr>
            </table>
        </div>
    </div>
</section>

