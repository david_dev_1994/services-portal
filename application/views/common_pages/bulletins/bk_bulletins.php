<section id="main-conent">
   <div class='container'>
     <div class='row'>
     	 <div class="col-md-12">
     	 	<div class='col-md-3'></div>
     	 	<div class='col-md-6'>
     	 		<center>
     	 			<h3>BK Radio Service Bulletins</h3>
     	 			<a href="<?php echo base_url();?>/products/RELM-Bulletins">BK Technology Bulletins </a> |
     	 			<a href="<?php echo base_url();?>/products/Bulletins-search">Search Bulletins</a>
     	 			<br>
     	 			<a href="<?php echo base_url();?>/products/discontinued">Click Here for discontinued items</a>
     	 		</center>
     	 	</div>
     	 	<div class='col-md-3'></div>
     	 </div>
         <div class='col-md-12'>
         	<?php if (sizeof($bulletins)>0){ ?>
            <table class="table table-responsive table-bordered table-striped" style='width:100%'>
             <thead>
               <tr>
               	 <th>Radio Model</th>
               	 <th>Bulletin #</th>
               	 <th>Release Date</th>
               	 <th>Description</th>
               	 <th></th>
               </tr>
             </thead>
             <tbody>
            <tr>
             		<td>Radio</td>
             		<td>BKSB-1058</td>
             		<td>2020-03-19</td>
             		<td><p>Cleaning and disinfecting guidelines for BK Technologies radios.</p></td>
             		<td><a href="/service-portal//assets/images/BKSB-1058.pdf">Download</a></td>
             </tr>
             <tr>
             		<td>KNG, KNG2 Portable &amp; KNG Mobile</td>
             		<td>BKSB-1055</td>
             		<td>2019-05-15</td>
             		<td><p>Addresses operation and cloning issues caused by firmware version 5.6.0a.</p></td>
             		<td><a href="/service-portal//assets/images/BKSB-1055.pdf">Download</a></td>
             </tr>
             	<?php foreach ($bulletins as $key ) {
             		?>
             	<tr>
             		<td><?php echo $key->Radio_Model; ?></td>
             		<td><?php echo $key->Bulletin; ?></td>
             		<td><?php echo $key->Release_Date; ?></td>
             		<td><?php echo $key->Description; ?></td>
             		<td><a href='<?php echo base_url();?>/assets/images/<?php echo $key->File; ?>'>Download</a></td>

             	</tr>
             	<?php } ?>
             </tbody>
            </table>
            <?php }else{echo "<h2 style='text-align:center;'>Opps! No Data Found!</h2>"; }  ?>
         </div>
     </div>
   </div>
</section>
