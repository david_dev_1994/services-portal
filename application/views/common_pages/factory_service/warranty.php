<section id="main-conent">
<div class="container id="f-service__warranty">
<div class="row">
<div class="col-lg-12">
<h1 class="text-center">Warranty / Product Returns</h1>
<div class="padding-10"></div>
<h4 style="margin-top: 10px;text-align:center;">Standard Limited Warranty</h4>
<p><a href="https://www.bktechnologies.com/" target="_blank">BK Technologies<span style="vertical-align: super !important;font-size: 12px !important;">®</span></a> ("Warrantor") warrants to the Purchaser of new radio equipment of the Warrantor's manufacture that such equipment shall be free from material defects in material and workmanship for the period commencing upon the date of purchase and continuing for the following specified period of time after that date:</p>
<div class="row">
<div class="col-lg-2"></div>
<div class="col-lg-8">
<table style="margin-top: 20px !important;" class="table years table-striped table-responsive">
             <tbody>
<tbody>
<tr style="border-top: 1px solid #DDDDDD !important;">
<td>Basic Unit (Radio)</td>
<td>2 Years</td>
</tr>
<tr>
<td>Battery</td>
<td>1 Year</td>
</tr>
<tr>
<td>Antenna</td>
<td>1 Year</td>
</tr>
<tr>
<td>Other Accessories</td>
<td>1 Year</td>
</tr>
</tbody>
            </table>
</div>
<div class="col-lg-2"></div>
</div>

<p>Extended warranty options on the basic unit (radio) are provided below. There is NO extended warranty offered on the repeater, antenna, battery and all other accessories. Equipment and accessory items not manufactured by the Warrantor carry the standard warranty of the manufacturer thereof and are not covered by this warranty. FOR THE AVOIDANCE OF DOUBT, WARRANTOR MAKES NO REPRESENTATIONS OR WARRANTIES WITH RESPECT TO ANY THIRD PARTY PRODUCT, INCLUDING, WITHOUT LIMITATION, ANY WARRANTY OF MERCHANTABILITY, WARRANTY OF FITNESS FOR A PARTICULAR PURPOSE, WARRANTY OF TITLE, OR WARRANTY AGAINST INFRINGEMENT OF INTELLECTUAL PROPERTY RIGHTS OF A THIRD PARTY, WHETHER EXPRESS OR IMPLIED BY LAW, COURSE OF DEALING, COURSE OF PERFORMANCE, USAGE OF TRADE OR OTHERWISE.</p>

<p>This warranty does not cover equipment which has been: (a) damaged or not maintained as reasonable or necessary, (b) modified in any way, including the removal of the serial tag, (c) improperly installed, (d) repaired by someone other than the Warrantor or an Authorized Warranty Repair Station, (e) used in a manner or purpose for which the equipment was not intended, or (f) used with aftermarket (any accessory that is not issued/sold by Warrantor) accessories. Subject to compliance with the terms of this warranty, with respect to the applicable equipment during the applicable warranty period, Warrantor shall, in its sole discretion, repair or replace the applicable equipment.</p>

<p>IN NO EVENT WILL WARRANTOR BE LIABLE TO THE PURCHASER OR ANY THIRD PARTY FOR ANY LOSS OF USE, REVENUE OR PROFIT OR LOSS OF DATA OR DIMINUTION IN VALUE, OR ANY CONSEQUENTIAL, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR PUNITIVE DAMAGES WHETHER ARISING OUT OF BREACH CONTRACT, TORT (INCLUDING NEGLIGENCE) OR OTHERWISE, REGARDLESS OF WHETHER SUCH DAMAGES WERE FORESEEABLE AND WHETHER OR NOT WARRANTOR HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES, AND NOTWITHSTANDING THE FAILURE OF ANY AGREED OR OTHER REMEDY OF ITS ESSENTIAL PURPOSE.</p>

<p>To obtain warranty repair or replacement, the customer must return the equipment properly packaged and freight prepaid to the Warrantor or any Authorized Warranty Repair Station. The equipment will be returned freight prepaid from the Warrantor only.</p>

<p>EXCEPT FOR THE WARRANTY SET FORTH IN THE FIRST PARAGRAPH ABOVE, WARRANTOR MAKES NO WARRANTY WHATSOEVER WITH RESPECT TO THE EQUIPMENT, INCLUDING, WITHOUT LIMITATION, ANY WARRANTY OF MERCHANTABILITY, WARRANTY OF FITNESS FOR A PARTICULAR PURPOSE, WARRANTY OF TITLE, OR WARRANTY AGAINST INFRINGEMENT OF INTELLECTUAL PROPERTY RIGHTS OF A THIRD PARTY, WHETHER EXPRESS OR IMPLIED BY LAW, COURSE OF DEALING, COURSE OF PERFORMANCE, USAGE OF TRADE OR OTHERWISE. No person, whether in the employment of the Warrantor or not, is authorized to make oral or other modifications, extensions, or additions to this warranty, unless approved in writing by an authorized officer of the Warrantor at its home office.</p>

<p>The liability of the Warrantor is expressly limited to the repair or replacement of the equipment as described herein. SUCH REMEDY WILL BE THE PURCHASER’S SOLE AND EXCLUSIVE REMEDY AND WARRANTOR’S ENTIRE LIABILITY FOR ANY BREACH OF THIS LIMITED WARRANTY. Warrantor shall not be liable to the Purchaser and the Purchaser shall, upon his tender of the purchase price for the equipment, agree that the Warrantor shall not be liable, in any respect, for the equipment or damages caused thereby, except as prescribed herein, whether such liability is predicated upon negligence, tort, contract or other product liability theory.</p>
</div>
</div>

<div class="row padding-20">
<div class="col-lg-3"></div>
<div class="col-lg-3">
<a class="button" href="<?php echo base_url();?>assets/pdf/Serviceform2019.pdf">Factory Service Form</a>
</div>
<div class="col-lg-3">
<a class="button" href="<?php echo base_url();?>factory-service/pricing">Factory Service Pricing</a>
</div>
<div class="col-lg-3"></div>
</div>

<div class="padding-10"></div>
<div class="row padding-20">
<table style="margin-top: 20px !important;" class="table table-striped table-responsive">
            <tr>
                <h4 style="text-align:center !important;">BK Technologies<span class="trademark">®</span> EXTENDED WARRANTY* on Portables, Mobiles and Base Stations</h4>
            </tr>
             <tbody>

                <tr>
                    <th>KNG Series</th>
                    <th style="text-align: right">Cost</th>
                </tr>
                <tr style="border-top: 1px solid #DDDDDD !important;">
                    <td>2 PLUS 1 PROGRAM<br />
                        Extends standard 2 year warranty to 3 years total<br />
                        LFW0012</td>
                    <td style="text-align: right">$179.00</td>
                </tr>
                <tr>
                    <td>2 PLUS 2 PROGRAM<br />
                        Extends standard 2 year warranty to 4 years total<br />
                        LFW0024</td>
                    <td style="text-align: right">$229.00</td>
                </tr>
                <tr>
                    <td>2 PLUS 3 PROGRAM<br />
                        Extends standard 2 year warranty to 5 years total<br />
                        LFW0036</td>
                    <td style="text-align: right">$279.00</td>
                </tr>
                </tbody>
            </table>

<table style="margin-top: 20px !important;" class="table table-striped table-responsive">
     <tbody>

        <tr>
            <th>BKR Series</th>
            <th style="text-align: right">Cost</th>
        </tr>
        <tr style="border-top: 1px solid #DDDDDD !important;">
            <td>2 PLUS 1 PROGRAM<br />
                Extends standard 2 year warranty to 3 years total<br />
                BFW0012</td>
            <td style="text-align: right">$299.00</td>
        </tr>
        <tr>
            <td>2 PLUS 3 PROGRAM<br />
                Extends standard 2 year warranty to 5 years total<br />
                BFW0036</td>
            <td style="text-align: right">$499.00</td>
        </tr>
        </tbody>
</table>
            <p>*EXTENDED WARRANTY (not offered on repeaters, antennas, batteries and all other accessories; certain restrictions may apply on international warranties)</p>
            <p>All repairs must be performed by an authorized BK Technologies<span style="vertical-align: super !important;font-size: 12px !important;">®</span> service center during the life of the warranty. Except for duration and factory-only repair, extended warranties are identical to our standard 2 year warranty. Factory extended warranties cover all BK Technologies<span style="vertical-align: super !important;font-size: 12px !important;">®</span> portables, mobiles and base stations and must be purchased at point of sale.<br />
</div>

</div>
</section>

<style>

.table.years>tbody>tr>td, .table.years>tbody>tr>th, .table.years>tfoot>tr>td, .table.years>tfoot>tr>th, .table.years>thead>tr>td, .table.years>thead>tr>th {
    padding: 10px !important;
    width: 90% !important;
}

</style>