<section id="main-conent">
        <div class="container" id="f-service__service-container">
            <div class="text-center margin-tb-50">
                <h3><span class="font-bold ">Authorized Service Centers</span></h3>
                <form action="">
                    <select name="" id="">
                        <option>Select a State</option>
                        <option value="AL">Alabama</option>
                        <option value="AK">Alaska</option>
                        <option value="AZ">Arizona</option>
                        <option value="AR">Arkansas</option>
                        <option value="CA">California</option>
                        <option value="CO">Colorado</option>
                        <option value="CT">Connecticut</option>
                        <option value="DE">Delaware</option>
                        <option value="FL">Florida</option>
                        <option value="GA">Georgia</option>
                        <option value="HI">Hawaii</option>
                        <option value="IA">Idaho</option>
                        <option value="ID">Illinois</option>
                        <option value="IL">Indiana</option>
                        <option value="IN">Iowa</option>
                        <option value="KS">Kansas</option>
                        <option value="KY">Kentucky</option>
                        <option value="LA">Louisiana</option>
                        <option value="MA">Maine</option>
                        <option value="MD">Maryland</option>
                        <option value="ME">Massachusetts</option>
                        <option value="MI">Michigan</option>
                        <option value="MN">Minnesota</option>
                        <option value="MO">Mississippi</option>
                        <option value="MS">Missouri</option>
                        <option value="MT">Montana</option>
                        <option value="NE">Nebraska</option>
                        <option value="NV">Nevada</option>
                        <option value="NH">New Hampshire</option>
                        <option value="NJ">New Jersey</option>
                        <option value="NM">New Mexico</option>
                        <option value="NY">New York</option>
                        <option value="NC">North Carolina</option>
                        <option value="ND">North Dakota</option>
                        <option value="OH">Ohio</option>
                        <option value="OK">Oklahoma</option>
                        <option value="OR">Oregon</option>
                        <option value="PA">Pennsylvania</option>
                        <option value="RI">Rhode Island</option>
                        <option value="SC">South Carolina</option>
                        <option value="SD">South Dakota</option>
                        <option value="TN">Tennessee</option>
                        <option value="TX">Texas</option>
                        <option value="UT">Utah</option>
                        <option value="VT">Vermont</option>
                        <option value="VA">Virginia</option>
                        <option value="WA">Washington</option>
                        <option value="WV">West Virginia</option>
                        <option value="WI">Wisconsin</option>
                        <option value="WY">Wyoming</option>
                    </select>
                </form>
                <p class="font-bold">
                    There are no recommended Service Centers in the selected state.
                </p>
                <p class="font-bold">
                    Select another State
                    or search for you closest authorized dealer by zip code.
                </p>
                <a href="https://www.bktechnologies.com/contact-us/">View complete authorized service center list.</a>
                <a href="https://www.bktechnologies.com/dealer-search/">Find Dealers by zip code</a>
                <p class="font-note">
                    Please note: Not all Authorized BK Technology Dealers provide repair services.
                </p>
            </div>
        </div>
    </section>