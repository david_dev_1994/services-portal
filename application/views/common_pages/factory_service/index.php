<section id="main-conent">
    <div class="container" id="f-service">
        <div class="row">
            <div class="col-lg-12">
                <div class="text-center">
                    <h1>Customer Service & Technical Support Team</h1>
                    <div class="heading-space"></div>
                    <p>
                        Experienced, specialized technicians and customer service professionals with a gift for translating “engineer-ese’ into English.
                    </p>
                    <p>
                        Many issues can be resolved in a short conversation with one of our Product Specialists.
                    </p>
                    <p>
                        Contact the team at (800) 422-6281 or by <a href="<?php echo base_url();?>/contact">click here</a> to contact via email
                    </p>
                    <p class="font-bold">
                        Office Hours: 8:00am to 5:00 pm Eastern Time
                    </p>
                </div>
            </div>
        </div>
        <div style="margin-bottom:20px;" class="row">
            <div class="col-lg-12">
                <div class="col-lg-4">
                    <a class="button" href="<?php echo base_url();?>assets/pdf/Serviceform2019.pdf" target="_blank">Factory Service Form</a>
                </div>
                <!-- <div class="col-lg-3">
                    <a class="button" href="<?php echo base_url();?>/return-merchandise-authorization" target="_blank">RMA Form</a>
                </div> -->
                <div class="col-lg-4">
                    <a class="button" href="<?php echo base_url();?>factory-service/pricing">Factory Service Pricing</a>
                </div>
                <div class="col-lg-4">
                    <a class="button" href="<?php echo base_url();?>factory-service/warranty">Warranty Statement</a>
                </div>
            </div>
        </div>

        <div class="row padding-20">
            <div class="col-lg-12">
                <h3>Repair Policies</span></h3>
                <p>
                    It is the practice and policy of the Service Department to restore repaired products to original factory specifications whenever possible. Warranty items will be repaired or replaced at the discretion of the Customer Service Department.
                </p>
                <p>
                    To obtain factory repair, pack the Product carefully and include the
                    <a href="<?php echo base_url();?>assets/pdf/Serviceform2013.pdf">factory service form</a> describing the specific concern that has caused you to return it. For your protection, it is advisable to insure the parcel against loss or damage. The Product (with evidence of original installation or purchase) should be shipped or delivered to:
                </p>
                <!--<ul class="text-center margin-tb-50">
                    <li class="font-bold"><h4>BK Technologies<span style="vertical-align: super !important;">®</span></h4></li>
                    <li>Attention: Customer Service</li>
                    <li>7100 Technology Drive,</li>
                    <li>West Melbourne, Florida 32904</li>
                </ul>-->
                <p>Most <a href="https://www.bktechnologies.com/" target="_blank">BK Technologies<span style="vertical-align: super !important;font-size: 12px !important;">®</span></a> Dealers are authorized Warranty Repair facilities.</p>
                <ul class="margin-tb-15">
                    <li class="margin-tb-15"></li>
                    <!--                <li class="margin-tb-15"><a href="-->
                    <?php //echo base_url();?>
                        <!--factory-service/service-dealers">Click Here</a> for a list of recommended BK Technology Wireless repair centers</li>-->
                        <!--                <li class="margin-tb-15"><a href="https://www.bktechnologies.com/dealers">Click Here</a> to find your closest Authorized dealer</li>-->
                        <!--                <li class="margin-tb-15"><a href="-->
                        <?php //echo base_url();?>
                            <!--parts/Parts_Distributor">Click Here</a> to find Parts Distributors</li>-->
                </ul>
            </div>
        </div>
        <div class="row padding-20">
            <div class="col-lg-6">
                <h4>BK Radio<span class="trademark">®</span> Warranty Identification</h4>
                <p class="margin-tb-15">
                    The serial numbers of BK Radio<span style="vertical-align: super !important;font-size: 12px !important;">®</span> portable and mobile radios include a date code that can be used to determine warranty coverage. The first two digits indicate the year of production, the second two digits indicate the month of production with the remaining digits being the inventory control number.
                </p>
            </div>
            <div class="col-lg-6">
                <h4>BK Technologies<span class="trademark">®</span> Radio Warranty Identification</h4>
                <p class="margin-tb-15">
                    Warranty coverage for <a href="https://www.bktechnologies.com/" target="_blank">BK Technologies<span style="vertical-align: super !important;    font-size: 12px !important;">®</span></a> products are based on purchase date. Proof of purchase may be required for warranty repair.
                </p>
                <p>
                    Contact Customer Service for information on your radio's warranty status.
                </p>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <ul>
                    <li class="margin-tb-15"><span class="font-bold">Example:</span> 0921123</li>
                    <li class="margin-tb-15"><span class="font-bold">09</span> = year
                        <span class="font-bold">     21</span> = week
                        <span class="font-bold">     123</span> = control number</li>
                    <li class="margin-tb-15">This radio is under warranty until May of 2011.</li>
                </ul>
            </div>
        </div>
    </div>
</section>