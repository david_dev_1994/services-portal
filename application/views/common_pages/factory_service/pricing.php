<section id="main-conent">
    <div class="container" id="f-service__pricing">
<div class="row">
<div class="col-lg-12">
<div class="text-center">
<h1>Factory Service Pricing</h1>
<h4 style="text-align:center !important;">Effective Date: November 1st, 2018</h4>
<div class="padding-10"></div>
<p>The following repair charges will apply to radios not covered under warranty.*</p>
<p>These charges do not include the repair or replacement of accessories (such as batteries, antennas, case parts, etc.) and do not include shipping charges.</p>
<p></p>
<div class="padding-10"></div>
</div>
</div>
</div>

<div class="row padding-10">
<div class="col-lg-12">
<div class="f-serice__pricing-table">
            <div class="table-block">
                <h4 style="text-align:center !important;color:#ffffff !important;">Minimum Service Charge</h4>
                <span>$100.00</span></b>
                <p style="text-align:center !important;color: #ffffff !important;padding-top: 15px !important;">Factory Service Base Pricing</p>
            </div>
            <div>
                <table>
                    <tr>
                        <td class="font-bold">
                            Product
                        </td>
                        <td class="font-bold">
                            Base Price
                        </td>
                    </tr>
                    <tr>
                        <td>PORTABLES</td>
                        <td>$355.00</td>
                    </tr>
                    <tr>
                        <td>MOBILES</td>
                        <td>$545.00</td>
                    </tr>
                    <tr>
                        <td>KAA0660 / KAA0670</td>
                        <td>$200.00</td>
                    </tr>
                    <tr>
                        <td>BASE STATIONS</td>
                        <td>$545.00 + PARTS</td>
                    </tr>
                    <tr>
                        <td>RDPR SERIES AND TMRxxx</td>
                        <td>$420.00 + PARTS</td>
                    </tr>
                    <tr>
                        <td>REPEATERS</td>
                        <td>CALL FOR QUOTE</td>
                    </tr>
                    <tr>
                        <td>PROGRAMMING</td>
                        <td>$75.00</td>
                    </tr>
                    <tr>
                        <td>MINIMUM SERVICE CHARGE</td>
                        <td>$100.00</td>
                    </tr>
                    <tr>
                        <td>EXPEDITED 2 DAY TURN</td>
                        <td>$125.00</td>
                    </tr>
                    <tr>
                        <td>TUNE & CLEAN</td>
                        <td>$125.00</td>
                    </tr>
                    </tbody></table>
            </div>
        </div>
</div>
</div>

<div class="row padding-10">
<div class="col-lg-12">
<div class="f-service__pricing-desc margin-tb-50">
<div class="table-block">
            <p>Factory Service/Support is no longer available for the following units:</p>
</div>            
<div>
                <h5><b>Factory Service/Support is no longer available for the following units:</b></h5>
                <h5>Regency, Wilson and Octagon Portables and Mobiles
                    UC102 or UC202, WHS series, PTV series Portables</h5>

                <h5 class="margin-tb-15">HS & MS Scanners, UC2200 or UC4200 & RH256NB
                    E-Series (VHF), L-Series, M-Series and J-Series King Radio or Bendix/King Portables</h5>

                <h5 class="margin-tb-15">E-Series (UHF) Bendix/King Portables and Mobiles (exception being the EPU/EPV 4992M)</h5>

                <h5 class="margin-tb-15">E-Series (VHF), L-Series, G-Series (**non-flash version**) BK Radio<span style="vertical-align: super !important;font-size: 12px !important;">®</span>, King Radio or Bendix/King
                    Mobiles
                    ERU/DRU & EBU/DRU Desktop Stations</h5>

                <h5 class=class="margin-tb-15"> *Extensively damaged equipment may be deemed unrepairable and returned at owner's expence.
                    Prices subject to change</h5>

                <h5 class="margin-tb-15">**To determine whether or not your G-Series radio is a flash version vs. a non-flash version look at
                    the serial number of your radio serial numbers beginning with 0119 and older are non-flash. If the
                    serial number is missing you can push and hold the #1 key on the keypad if the display changes to a
                    hexadecimal number it is a flash model if the display does not change then the radio is a non-flash
                    radio. If there are any questions as to whether or not this radio is flash or non-flash you can call
                    the service department for any help.</h5>
            </div>
        </div>
</div>
</div>

</div>
</section>