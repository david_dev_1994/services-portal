<section id="main-conent">
    <div class="container text-center" id="contact">
        <div class="row contact-form">
            <form class="contact-form" action="" method="post">
                <div class="col-lg-12">
                    <div class="text-center" id="contact_email">

                        <?php if ($this->session->flashdata('error')){?>
                            <div class=" text-center errors_container alert alert-danger alert-dismissable">
                                <?= $this->session->flashdata('error');?>
                            </div>
                            <?php }?>

                                <?php if ($this->session->flashdata('success')){?>
                                    <div class="text-center errors_container alert alert-success alert-dismissable">
                                        <?= $this->session->flashdata('success');?>
                                    </div>
                                    <?php }?>

                                        <?php if(isset($message)){
                        ?>
                                            <center>
                                                <p style='margin-top: 34px;' class='alert alert-success'>
                                                    <?= $msg?>
                                                </p>
                                            </center>
                                            <?php
                    } else{?>
                                                <h1 style="color: #ffffff !important;">RMA Request</h1>
                                                <div class="padding-10"></div>
                                                <h3 style="text-align: left !important; color: #ffffff !important;">Customer Information</h3>
                                                <div class="form-content">
                                                    <div class="form-group row">
                                                        <div class="col-lg-6">
                                                            <label for="name">Company Name/Account Number</label>
                                                            <input id="name" name='name' type="text" required>
                                                        </div>
                                                        <div class="col-lg-6">
                                                            <label for="requested">Requested By</label>
                                                            <input type="text" name="requested" id="requested" required>
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <div class="col-lg-6">
                                                            <label for="email">Email Address</label>
                                                            <input type="email" name='email' id="email" required>
                                                        </div>
                                                        <div class="col-lg-6">
                                                            <label for="phone">Phone Number</label>
                                                            <input type="tel" name='phone' id="phone" required>
                                                        </div>
                                                    </div>
                                                    <div class="inner">
                                                        <p class="initial">Test</p>
                                                    </div>
                                                    <h3 style="text-align: left !important; color: #ffffff !important;">Product and Return Information</h3>
                                                    <div class="form-group row">
                                                        <div class="col-lg-12">
                                                            <label for="reason">Reason</label>
                                                            <select onchange="rsn(this)" name="r_state" id="reason" required>
                                                                <option>Select Reason</option>
                                                                <option>Order Error</option>
                                                                <option>Shipping Error</option>
                                                                <!--<option>Product Damaged</option>-->
                                                            </select>
                                                        </div>
                                                    </div>
                                                <div class="form-group row">
                                                    <div class="col-lg-12 dtl" style="display: none">
                                                        <label for="details">Details</label>
                                                        <textarea class="detail" name="err_details" rows="7" cols="70" placeholder="Please Mention the Error Details!"></textarea>
                                                    </div>
                                                </div>

                                                    <button type="button" id="btnAdd" class="btn btn-primary">Add More</button>
                                                    <!-- <button type="button" class="btn btn-danger btnRemove">Remove</button> -->
                                                    <div class="form-group group">
                                                        <div class="row">
                                                        <div class="col-lg-1">
                                                            <label for="quantity">Quantity</label>
                                                            <input type="text" name="quantity[]" id="quantity" required>
                                                        </div>
                                                        <div class="col-lg-2">
                                                            <label for="part_number">Part Number</label>
                                                            <input type="text" name="part_number[]" id="part_number" required>
                                                        </div>
                                                        <div class="col-lg-3">
                                                            <label for="invoice">Invoice/Order Number</label>
                                                            <input type="text" name="invoice[]" id="invoice" required>
                                                        </div>
                                                        <div class="col-lg-1">
                                                            <label for="price">Price</label>
                                                            <input type="text" name="price[]" id="price" required>
                                                        </div>
                                                        <div class="col-lg-3">
                                                            <label for="problem">Problem/Reason For Return</label>
                                                            <input type="text" name="problem[]" id="problem" required>
                                                        </div>
														
                                                        <div class="col-lg-2">
                                                            <button type="button" class="btn btn-danger btnRemove bt_pos">Remove</button>
                                                        </div>
                                                        </div>
														
														<div class="row">
														<div class="col-lg-6">
                                                            <label for="comments">Comments</label>
                                                            <input type="textarea" name="comments" id="comments" required>
                                                        </div>
														</div>
                                                    </div>
                                                </div>
                                                <div id="radioDiv" class="form-group row radio-buttons">
                                                    <div class="col-lg-12">
                                                        <label for="product-option">Please Select One:</label>
                                                        <label class="radio-inline ">
                                                            <input type="radio" name="optradio" onclick="reqrd(this)" value="replacement">Replacement</label>
                                                        <label class="radio-inline">
                                                            <input type="radio" name="optradio" onclick="reqrd(this)" value="credit">Credit</label>
                                                    </div>
                                                </div>
                                                <div class="row credit-card hide">
                            <div class="form-group row col-lg-5">
                                <select class="req1" name="credit_card" id="credit_card-method" >
                                    <option value="">Select Credit Method</option>
                                    <option value="*Credit back to original credit card">*Credit back to original credit card</option>
                                    <option value="*Credit to account">*Credit to account </option>
                                </select>

                            </div>
                        </div>
                                                <div class="replacement-section hide">
                            <div class="inner">
                                <p class="initial">Test</p>
                            </div>
                            <h3 style="text-align: left !important; color: #ffffff !important;">Replacement Order</h3>
                            <div class="form-content-2">
                                <button type="button" id="btnAdd2" class="btn btn-primary">Add More</button>
                                <div class="form-group group-2">
                                    <div class="row">
                                    <div class="col-lg-1">
                                        <label for="quantity">Quantity</label>
                                        <input class="req" type="text" name="quantity1[]" id="quantity" >
                                    </div>
                                    <div class="col-lg-2">
                                        <label for="part_number">Part Number</label>
                                        <input class="req" type="text" name="part_number1[]" id="part_number" >
                                    </div>
                                    <div class="col-lg-3">
                                        <label for="invoice">PO#</label>
                                        <input class="req" type="text" name="invoice1[]" id="invoice" >
                                    </div>
                                    <div class="col-lg-1">
                                        <label for="price">Price</label>
                                        <input class="req" type="text" name="price1[]" id="price" >
                                    </div>
                                    <div class="col-lg-2 ">
                                        <button type="button" class="btn btn-danger btnRemove bt_pos">Remove</button>
                                    </div>
                                    </div>
                                </div>
                            </div>
                            <div class="inner-2">
                                <p class="initial">Test</p>
                            </div>
                            <h3>Billing Address</h3>
                            <div class="form-group row">
                                <div class="col-lg-2">
                                    <label for="b_name">Name</label>
                                    <input class="req" type="text" name="b_name" id="b_name" >
                                </div>
                                <div class="col-lg-3">
                                    <label for="address">Street Address</label>
                                    <input class="req" type="text" name="address" id="address" >
                                </div>
                                <div class="col-lg-3">
                                    <label for="city">City</label>
                                    <input class="req" type="text" name='city' id="city">
                                </div>
                                <div class="col-lg-2">
                                    <label for="state">State</label>
                                    <input class="req" type="text" name='state' id="state">
                                </div>
                                <div class="col-lg-2">
                                    <label for="b_zip">Zip Code</label>
                                    <input class="req" type="text" name='b_zip' id="b_zip">
                                </div>
                            </div>
                            <h3>Shipping Address</h3>
                            <div class="form-group row">
                                <div class="col-lg-2">
                                    <label for="s_name">Name</label>
                                    <input class="req" type="text" name="s_name" id="s_name">
                                </div>
                                <div class="col-lg-3">
                                    <label for="s-address">Street Address</label>
                                    <input class="req" type="text" name="s-address" id="s-address">
                                </div>
                                <div class="col-lg-3">
                                    <label for="s-city">City</label>
                                    <input class="req" type="text" name='s-city' id="s-city" >
                                </div>
                                <div class="col-lg-2">
                                    <label for="s-state">State</label>
                                    <input class="req" type="text" name='s-state' id="s-state" >
                                </div>
                                <div class="col-lg-2">
                                    <label for="s_zip">Zip Code</label>
                                    <input class="req" type="text" name='s_zip' id="s_zip" >
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-lg-12">
                                    <label for="s-method">Shipping Method</label>
                                    <label><small class="red">*If upgraded shipping method is selected, customer may be responsible for shipping costs.</small></label>
                                    <select class="req" name="s-method" id="s-method" >
                                        <option>Select Shipping Method</option>
                                        <option>Overnight</option>
                                        <option>2 Day</option>
                                        <option>3 Day</option>
                                        <option>Ground</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                                                <p class="initial">Test</p>
                                                <h3 style="text-align: left !important; color: #ffffff !important; font-size: 20px !important;">Terms:</h3>
                                                <ul class="rma-request">
                                                <li>In order to expedite the processing of the RMA number, please fill out this form completely.</li>
                                                <li>Once the RMA request is processed, the Return Merchandise Authorization number and shipping instructions will be sent via e-mail.</li>
                                                <li>Orders for replacement products will be charged immediately.</li>
                                                <li>Credit for returned item will occur when product is received. Please allow up to two weeks for processing after the returned product is received.</li>
                                                <li>You will be receiving a submission confirmation via email. If you do not receive the confirmation within 48 hours, please reach out to <a href="mailto:sales@bktechnologies.com">sales@bktechnologies.com</a>.</li>
                                                <li>Return orders may be subject to restocking fee.</li>

                                                </ul>
                                                <div class="form-group row">
                                                    <div class="col-lg-12">
                                                        <label for="signature">I accept the terms of the RMA Request form.</label>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <div class="col-lg-3">
                                                        <label for="signature">E-Signature</label>
                                                        <input class="" type="text" name="signature" id="signature" required>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <div class="col-lg-12">
                                                        <input type="submit" class='btn btn-info'>
                                                        <!-- <button class='btn btn-warning' type="reset">Reset Form</button> -->
                                                    </div>
                                                </div>
</div>
            </div>
            </form>
            <?php }?>
                </div>
                </div>
</section>

<script>
    $('.form-content').multifield({
        section: '.group',
        btnAdd:'#btnAdd',
        btnRemove:'.btnRemove',
    });

    $('.form-content-2').multifield({
        section: '.group-2',
        btnAdd:'#btnAdd2',
        btnRemove:'.btnRemove',
    });

    $("#btnAdd").one('click', function () {
        $(".inner").append("<p>Test</p>");
        $("p.initial").addClass("hide");
    });
    $("#btnAdd2").one('click', function () {
        $(".inner-2").append("<p>Test</p>");
        $("p.initial").addClass("hide");
    });

$("input[type=radio]") // select the radio by its id
    .change(function(){ // bind a function to the change event
        if( $(this).is(":checked") ){ // check if the radio is checked
            var val = $(this).val(); // retrieve the value
            if(val == "replacement") {
$(".replacement-section").removeClass("hide");
$('.credit-card').addClass('hide');
} else {
$(".replacement-section").addClass("hide");
$('.credit-card').removeClass('hide');
}
        }
    });



</script>

<script>
    function reqrd(val) {
        if (val.value=='replacement')
        {
            $(".req").prop('required',true);
            $(".req1").prop('required',false);
        }else {
            $(".req").prop('required',false);
            $('.req1').prop('required',true);
        }
    }

    function rsn(e) {
        if (e.value=='Order Error')
        {
            $(".detail").prop('required',true);
            $(".dtl").show();
        }else {
            $(".detail").prop('required',false);
            $(".dtl").hide();
        }

    }
</script>

<style>
    section#main-conent {
        padding: 90px 0 75px 0 !important;
    }

    .row.contact-form {
        background: #294e6e !important;
        padding-bottom: 10px !important;
    }

    #contact h3 {
        margin-top: 0px !important;
        margin-left: 15px !important;
        text-align: left !important;
        color: #ffffff !important;
    }

    select#reason, select#s-method {
        width: 48.5% !important;
        float: left !important;
    }

    .row.contact-form label.radio-inline {
        display: inline-block !important;
        float: left !important;
        margin-right: 30px !important;
    }

    .radio-inline input[type=radio] {
        width: auto !important;
    }

    .space-filler {
        visibility: hidden !important;
    }

    .inner p, .inner-2 {
        visibility: hidden !important;
    }

    p.initial {
        visibility: hidden !important;
    }

    button#btnAdd, button#btnAdd2 {
        background: #00a6d5 !important;
        border-color: #00a6d5 !important;
        color: #ffffff !important;
        display: block !important;
        margin-left: 15px !important;
        border-radius: 0px !important;
        font-size: 14px !important;
        text-transform: uppercase !important;
        font-family: Lato !important;
        font-weight: 600 !important;
        margin-bottom: 10px !important;
        padding: 6px 25px !important;
    }

ul.rma-request {
    text-align: left !important;
    list-style-type: disc !important;
    color: #ffffff !important;
    margin-left: 35px !important;
    font-family: Lato !important;
    margin-bottom: 30px !important;
    margin-right: 20px !important;
}

ul.rma-request a {
    color: #ffffff !important;
}

.row.contact-form textarea {
    width: 100% !important;
    padding: 8px 10px !important;
    margin-bottom: 20px !important;
}

    .bt_pos{
        position: relative;
        top: 35%;
    }

</style>