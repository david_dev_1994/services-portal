<!DOCTYPE HTML>

<html>

<head>

<title>Admin Login Panel</title>

<!-- Meta-Tags -->

<meta name="viewport" content="width=device-width, initial-scale=1">

<meta http-equiv="Content-Type" content="text/html; charset=utf-8">

<meta name="keywords" content="Instant Sign In Form Widget Responsive, Login Form Web Template, Flat Pricing Tables, Flat Drop-Downs, Sign-Up Web Templates, Flat Web Templates, Login Sign-up Responsive Web Template, Smartphone Compatible Web Template, Free Web Designs for Nokia, Samsung, LG, Sony Ericsson, Motorola Web Design">

<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>

<!-- //Meta-Tags -->



<!-- Custom Theme files -->

<link href="<?php echo base_url();?>assets/css/popup-box.css" rel="stylesheet" type="text/css" media="all" />

<link href="<?php echo base_url();?>assets/css/style2.css" rel='stylesheet' type='text/css' />



<!-- font-awesome-icons -->

<link href="<?php echo base_url();?>assets/css/font-awesome.css" rel="stylesheet"> 

<!-- //font-awesome-icons -->



<!--fonts--> 

<link href="//fonts.googleapis.com/css?family=Open+Sans:400,600,700,800" rel="stylesheet">

<link href="//fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i&amp;subset=cyrillic,cyrillic-ext,greek,greek-ext,latin-ext,vietnamese" rel="stylesheet">

<!--//fonts--> 
<!--js-->

<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery-2.1.4.min.js"></script>
</head>

<body>