<div class="agileinfo-dot">

<!--background-->

<!-- login -->

<!--<h1>Admin Login Form</h1>-->

	<div class="login-section">

	    <div class="login-w3l">

<!--		<i class="fa fa-medkit" aria-hidden="true"></i>-->

		<h2 class="sub-head-w3-agileits">Admin Sign in</h2>

			<div class="login-form">

					<div style="color: red">
					<?php echo $this->session->flashdata('login_error');?>
					</div>
					<?php echo form_open('admin/admin/login',array('data-toggle'=>"validator",'role'=>"form"));?>

					<div class="input">
<!--						<i class="fa fa-user" aria-hidden="true"></i> -->
						<?php echo form_input('username',set_value('username'),array('placeholder'=>'User Email','class'=>'fas fa-user'));?>
					<?php echo form_error('username');?>
						
					</div>

					<div class="input">

<!--						<i class="fa fa-unlock-alt" aria-hidden="true"></i>-->

						<?php echo form_password('password','',array('placeholder'=>'Password','class'=>'lock'));?>
						<?php echo form_error('password');?>
					</div>

					<div class="signin-rit">

						<!-- <span class="checkbox1">

							<label class="checkbox">
								<input type="checkbox" name="checkbox" checked="">Remember me</label>

						</span> -->

						<a id="forgotBtn" class="forgot book popup-with-zoom-anim button-isi zoomIn animated" data-wow-delay=".5s" href=".forgot_password">Forgot Password?</a>

						<div class="clear"> </div>

					</div>

					
					<?php echo form_submit('login','Sign in');?>

				<?php echo form_close();?>	

				<!--<p>Don't have an account?<a id="signupbtn" class="book popup-with-zoom-anim button-isi zoomIn animated" data-wow-delay=".5s" href=".user_register"> Sign up</a></p>-->

			</div>

			<!-- //login -->

<!--			<div class="social_icons agileinfo">-->
<!---->
<!--				<ul class="top-links">-->
<!---->
<!--					<li><a href="#"><i class="fa fa-facebook"></i></a></li>-->
<!---->
<!--					<li><a href="#"><i class="fa fa-twitter"></i></a></li>-->
<!---->
<!--					<li><a href="#"><i class="fa fa-linkedin"></i></a></li>-->
<!---->
<!--					<li><a href="#"><i class="fa fa-google-plus"></i></a></li>-->
<!---->
<!--				</ul>-->
<!---->
<!--			</div>-->

		</div>

		<div class="profile-agileits  bg-color-agile">
<!--		<div class="profile-agileits">-->

<!--			<h3>BK Technology Admin Portal.</h3>-->

		</div>

		<div class="clear"></div>

	</div>

			<p class="footer">© <?php echo date('Y')?> All Rights Reserved | <a href="<?php echo base_url();?>">BK_Technologies</a></p>

			<!--//login-->

			<div class="pop-up"> 

	<div id="small-dialog" class="user_register mfp-hide book-form">

		<h3 class="sub-head-w3-agileits">Sign Up</h3>	

		<div class="login-form">			
			
			<p class="error"><?php echo $this->session->flashdata('signup_error');?></p>
					<?php echo form_open('admin/admin/signup',array('data-toggle'=>"validator",'role'=>"form"));?>

				<div class="input">

					<i class="fa fa-user" aria-hidden="true"></i> 
					<?php echo form_input('user_login',set_value('user_login'),array('placeholder'=>'Username','class'=>'user'));?>
					
						<?php echo form_error('user_login');?>

				</div>

				<div class="input">

					<i class="fa fa-envelope" aria-hidden="true"></i>					
					<?php echo form_input('user_email',set_value('user_email'),array('placeholder'=>'Email','class'=>'email'));?>					
						<?php echo form_error('user_email');?>

				</div>

				<div class="input">

					<i class="fa fa-unlock-alt" aria-hidden="true"></i> 
					
					<?php echo form_password('user_pass','',array('placeholder'=>'Password','class'=>'password'));?>					
						<?php echo form_error('user_pass');?>

				</div>

				<div class="input">

					<i class="fa fa-unlock-alt" aria-hidden="true"></i> 					
					<?php echo form_password('user_conf_pass',"",array('placeholder'=>'Confirm Password','class'=>'password'));?>					
					<?php echo form_error('user_conf_pass');?>

				</div>				

				
					<?php echo form_submit('signup','Sign Up');?>

				<?php echo form_close();?>

		</div>

	</div>


	<div id="small-dialog" class="forgot_password mfp-hide book-form">

		<h3 class="sub-head-w3-agileits">Reset Password</h3>	

		<div class="login-form">			
			
			<p class="error"><?php echo $this->session->flashdata('forgot_error');?></p>
					<?php echo form_open('admin/admin/reset_password',array('data-toggle'=>"validator",'role'=>"form"));?>

				<div class="input">

					<i class="fa fa-user" aria-hidden="true"></i> 
					<?php echo form_input('email',set_value('email'),array('placeholder'=>'Email','class'=>'email'));?>
					
						<?php echo form_error('email');?>

				</div>
					<?php echo form_submit('reset_password','Send');?>

				<?php echo form_close();?>

		</div>

	</div>

	<div id="small-dialog" class="password_reseted mfp-hide book-form" >

		<h3 class="sub-head-w3-agileits">Your password is reset. Please check your email.</h3>	

	</div>


</div>	

</div>


<a id="passwordreseted" class="book popup-with-zoom-anim button-isi zoomIn animated" data-wow-delay=".5s" href=".password_reseted" style="display: none;""></a>

<?php if(isset($view_signup) && $view_signup==1):?>
<script type="text/javascript">
	$(document).ready(function(){
		setTimeout(function(){
				$('#signupbtn').trigger('click');
		},700);
		
	});

</script>
<?php endif;?>
<?php if(isset($view_signup) && $view_signup==2):?>
<script type="text/javascript">
	$(document).ready(function(){
		setTimeout(function(){
				$('#forgotBtn').trigger('click');
		},700);
		
	});

</script>
<?php endif;?>

<?php 
$pass_success=$this->session->flashdata('reset_successful');
if($pass_success==1):?>
<script type="text/javascript">
	$(document).ready(function(){
		setTimeout(function(){
				$('#passwordreseted').trigger('click');
		},700);
		
	});

</script>
<?php endif;?>


