
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="box">
        <section class="content-header">
            <h1 class="text-center">RMA Form Listing</h1>
        </section>
        <br>
        <!-- Main content -->
        <section class="content">
            <?php if (!isset($list1)){ ?>
            <div class='container-fluid' style="background-image: black">
                <div class="form-page">

                    <?php if (isset($err_msg)) { ?>
                        <div class=" alert alert-danger alert-dismissable">
                            <?= $err_msg ?>
                        </div>
                    <?php } ?>
                    <?php if (isset($succ_msg)) { ?>
                        <div class="text-center alert alert-success alert-dismissable">
                            <?= $succ_msg ?>
                        </div>
                    <?php } ?>
                    <?php if ($this->session->flashdata("validation_errors")) { ?>
                        <div class="text-center alert alert-danger alert-dismissable">
                            <?= $this->session->flashdata("validation_errors") ?>
                        </div>
                    <?php } ?>

                    <!--                        form body-->
                    <div class="text-center" id="contact">
                        <div class="row contact-form" style="background: #294e6e !important;padding-bottom: 60px !important;">
                            <form class="contact-form" action="" method="post">
                                <div class="col-lg-12">
                                    <div class="text-center" id="contact_email" style="text-align: center;">

                                        <?php if ($this->session->flashdata('error')) { ?>
                                            <div class=" text-center errors_container alert alert-danger alert-dismissable">
                                                <?= $this->session->flashdata('error'); ?>
                                            </div>
                                        <?php } ?>

                                        <?php if ($this->session->flashdata('success')) { ?>
                                            <div class="text-center errors_container alert alert-success alert-dismissable">
                                                <?= $this->session->flashdata('success'); ?>
                                            </div>
                                        <?php } ?>

                                        <?php if (isset($message)){
                                            ?>
                                            <center>
                                                <p style='margin-top: 34px;' class='alert alert-success'><?= $msg ?></p>
                                            </center>
                                            <?php
                                        } else{ ?>
                                        <h1 style="color: #ffffff !important;">RMA Request</h1>
                                        <div class="padding-10"></div>
                                        <h3 style="text-align: left !important; color: #ffffff !important;">Customer
                                            Information</h3>
                                        <div class="form-content">
                                            <div class="form-group" style="margin-bottom: 15px;">
                                                <div class="col-lg-6" style="width: 50%;float:left;padding:0 15px">
                                                    <label for="name">Company Name/Account Number</label>
                                                    <input id="name" name='name' value="<?php echo $list[0]->name; ?>"
                                                           disabled type="text" required>
                                                </div>
                                                <div class="col-lg-6" style="width: 50%;float:left;padding:0 15px">
                                                    <label for="requested">Requested By</label>
                                                    <input type="text" name="requested"
                                                           value="<?php echo $list[0]->requested; ?>" disabled
                                                           id="requested" required>
                                                </div>
                                            </div>
                                            <div class="form-group" style="margin-bottom: 15px;">
                                                <div class="col-lg-6" style="width: 50%;float:left;padding:0 15px">
                                                    <label for="email">Email Address</label>
                                                    <input type="email" name='email' id="email"
                                                           value="<?php echo $list[0]->email; ?>" disabled required>
                                                </div>
                                                <div class="col-lg-6" style="width: 50%;float:left;padding:0 15px">
                                                    <label for="phone" style="">Phone Number</label>
    <style>@media print{.btn.btn-info, .btn.btn-success{display:none}label{display: block !important;
    text-align: left !important;
    color: #ffffff !important;
    font-family: Lato !important;
    font-size: 16px !important;}
    .row.contact-form input, .row.contact-form select {
    width: 100% !important;
    padding: 8px 2px !important;
    margin-bottom: 20px !important;border:1px solid #294e6e !important;
}.col-lg-1 {
    width: 8.33333333%;float:left;
}.col-lg-2 {
    width: 16.66666667%;float:left;
}.col-lg-3 {
    width: 25%;float:left;
}.col-lg-5 {
    width: 41.66666667%;float:left;
}select#reason, select#s-method {
    width: 48.5% !important;
    float: left !important;
}p.initial {
    visibility: hidden !important;
}#contact h3 {
    margin-top: 0px !important;
    margin-left: 15px !important;
    text-align: left !important;
    color: #ffffff !important;text-align: left !important;
    color: #ffffff !important;
    font-size: 20px !important;
}
.row.contact-form input, .row.contact-form select {
    width: 100% !important;
    padding: 8px 2px !important;
    margin-bottom: 20px !important;
}select#reason, select#s-method {
    width: 48.5% !important;
    float: left !important;
}.col-lg-12 {
    width: 100%;float:left;
}
    </style>
                                                    <input type="tel" name='phone' id="phone"
                                                           value="<?php echo $list[0]->phone; ?>" disabled required>
                                                </div>
                                            </div>
                                            <div class="inner"><p class="initial">Test</p></div>
                                            <h3 style="text-align: left !important; color: #ffffff !important;">Product
                                                and Return Information</h3>
                                            <div class="form-group" style="margin-bottom: 15px;">
                                                <div class="col-lg-12">
                                                    <label for="reason">Reason</label>
                                                    <select name="r_state" id="reason" required disabled>
                                                        <option>Select Reason</option>
                                                        <option selected="selected"><?php echo $list[0]->r_state; ?></option>
                                                        <option disabled="disabled">Order Error</option>
                                                        <option disabled="disabled">Shipping Error</option>
                                                        <option disabled="disabled">Product Damaged</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <?php if ($list[0]->r_state == "Order Error") { ?>
                                                <div class="form-group row" style="margin-bottom: 15px;">
                                                    <div class="col-lg-12 dtl">
                                                        <label for="details">Details</label>
                                                        <textarea class="detail" name="err_details" disabled rows="7"
                                                                  cols="70"
                                                                  placeholder="Please Mention the Error Details!"><?= $list[0]->err_details; ?></textarea>
                                                    </div>
                                                </div>
                                            <?php } ?>
                                            <?php foreach ($list[0]->quantity as $index => $value) { ?>
                                                <div class="form-group group" style="margin-bottom: 15px;">
                                                    <div class="col-lg-1">
                                                        <label for="quantity">Quantity</label>
                                                        <input type="text" name="quantity[]" disabled
                                                               value="<?= isset($list[0]->quantity[$index]) ? $list[0]->quantity[$index] : ''; ?>"
                                                               id="quantity" required>
                                                    </div>
                                                    <div class="col-lg-2">
                                                        <label for="part_number">Part Number</label>
                                                        <input type="text" name="part_number[]" disabled
                                                               value="<?= isset($list[0]->part_number[$index]) ? $list[0]->part_number[$index] : '' ?>"
                                                               id="part_number" required>
                                                    </div>
                                                    <div class="col-lg-3">
                                                        <label for="invoice">Invoice/Order Number</label>
                                                        <input disabled type="text" name="invoice[]" id="invoice"
                                                               value="<?= isset($list[0]->invoice[$index]) ? $list[0]->invoice[$index] : ''; ?>"
                                                               required>
                                                    </div>
                                                    <div class="col-lg-1">
                                                        <label for="price">Price</label>
                                                        <input disabled type="number" name="price[]" id="price"
                                                               value="<?= isset($list[0]->price[$index]) ? $list[0]->price[$index] : ''; ?>"
                                                               required>
                                                    </div>
                                                    <div class="col-lg-5">
                                                        <label for="problem">Problem/Reason For Return</label>
                                                        <input disabled type="text" name="problem[]"
                                                               value="<?= isset($list[0]->problem[$index]) ? $list[0]->problem[$index] : ''; ?>"
                                                               id="problem" required>
                                                    </div>
                                                </div>
                                            <?php } ?>
                                        </div>
                                        <div class="form-group radio-buttons" style="margin-bottom: 15px;"></div>
                                        <div class="col-lg-12">
                                            <label for="product-option">Please Select One:</label>
                                            <label class="radio-inline">
                                                <input style="margin-left: -65px;" disabled type="radio" name="optradio"
                                                       value="replacement" <?php echo $list[0]->optradio == "replacement" ? 'checked' : ""; ?> >Replacement</label>
                                            <label class="radio-inline">
                                                <input style="margin-left: -43px;" disabled type="radio" name="optradio"
                                                       value="credit" <?php echo $list[0]->optradio == "credit" ? 'checked' : ""; ?> >Credit</label>
                                        </div>
                                    </div>
                                    <?php if ($list[0]->optradio == "replacement") { ?>
                                        <div class="inner"><p class="initial">Test</p></div>
                                        <h3 style="text-align: left !important; color: #ffffff !important;">Replacement
                                            Order</h3>
                                        <div class="form-content-2">
                                            <?php foreach ($list[0]->quantity1 as $index => $value) { ?>
                                                <div class="form-group group-2">
                                                    <div class="col-lg-1">
                                                        <label for="quantityy">Quantity</label>
                                                        <input disabled type="text" name="quantity1[]"
                                                               value="<?= isset($list[0]->quantity1[$index]) ? $list[0]->quantity1[$index] : ''; ?>"
                                                               required>
                                                    </div>
                                                    <div class="col-lg-2">
                                                        <label for="part_numberr">Part Number</label>
                                                        <input disabled type="text" name="part_number1[]"
                                                               value="<?= isset($list[0]->part_number1[$index]) ? $list[0]->part_number1[$index] : ''; ?>"
                                                               required>
                                                    </div>
                                                    <div class="col-lg-3">
                                                        <label for="invoicee">PO#</label>
                                                        <input disabled type="text" name="invoice1[]"
                                                               value="<?= isset($list[0]->invoice1[$index]) ? $list[0]->invoice1[$index] : ''; ?>"
                                                               required>
                                                    </div>
                                                    <div class="col-lg-1">
                                                        <label for="pricee">Price</label>
                                                        <input disabled type="text" name="price1[]"
                                                               value="<?= isset($list[0]->price1[$index]) ? $list[0]->price1[$index] : ''; ?>"
                                                               required>
                                                    </div>
                                                    <div class="col-lg-5 space-filler">
                                                        <label for="probleme">Problem/Reason For Return</label>
                                                        <input disabled type="text" name="problem1[]"
                                                               value="<?= isset($list[0]->problem1[$index]) ? $list[0]->problem1[$index] : ''; ?>">
                                                    </div>
                                                </div>
                                            <?php } ?>
                                            <div class="form-group" id="num"></div>
                                        </div>
                                        <div class="inner-2"><p class="initial">Test</p></div>
                                        <h3>Billing Address</h3>
                                        <div class="form-group">
                                            <div class="col-lg-2">
                                                <label for="b_name">Name</label>
                                                <input type="text" disabled name="b_name" id="b_name"
                                                       value="<?php echo $list[0]->b_name; ?>" required>
                                            </div>
                                            <div class="col-lg-3">
                                                <label for="address">Street Address</label>
                                                <input type="text" disabled name="address" id="address"
                                                       value="<?php echo $list[0]->address; ?>" required>
                                            </div>
                                            <div class="col-lg-3">
                                                <label for="city">City</label>
                                                <input type="text" name='city' disabled id="city"
                                                       value="<?php echo $list[0]->city; ?>" required>
                                            </div>
                                            <div class="col-lg-2">
                                                <label for="state">State</label>
                                                <input type="text" name='state' disabled id="state"
                                                       value="<?php echo $list[0]->state; ?>" required>
                                            </div>
                                            <div class="col-lg-2">
                                                <label for="b_zip">Zip Code</label>
                                                <input type="text" name='b_zip' disabled id="b_zip"
                                                       value="<?php echo $list[0]->b_zip; ?>" required>
                                            </div>
                                        </div>
                                        <h3>Shipping Address</h3>
                                        <div class="form-group">
                                            <div class="col-lg-2">
                                                <label for="s_name">Name</label>
                                                <input type="text" name="s_name" disabled id="s_name"
                                                       value="<?php echo $list[0]->s_name; ?>" required>
                                            </div>
                                            <div class="col-lg-3">
                                                <label for="s-address">Street Address</label>
                                                <input type="text" name="s-address" disabled id="s-address"
                                                       value="<?php echo $list[0]->{'s-address'}; ?>" required>
                                            </div>
                                            <div class="col-lg-3">
                                                <label for="s-city">City</label>
                                                <input type="text" name='s-city' disabled id="s-city"
                                                       value="<?php echo $list[0]->{'s-city'}; ?>" required>
                                            </div>
                                            <div class="col-lg-2">
                                                <label for="s-state">State</label>
                                                <input type="text" name='s-state' disabled id="s-state"
                                                       value="<?php echo $list[0]->{'s-state'}; ?>" required>
                                            </div>
                                            <div class="col-lg-2">
                                                <label for="s_zip">Zip Code</label>
                                                <input type="text" name='s_zip' id="s_zip" disabled
                                                       value="<?php echo $list[0]->s_zip; ?>" required>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-lg-12">
                                                <label for="s-method">Shipping Method</label>
                                                <label>
                                                    <small class="red">*If upgraded shipping method is selected,
                                                        customer may be responsible for shipping costs.
                                                    </small>
                                                </label>
                                                <select name="s-method" id="s-method" disabled>
                                                    <option>Select Shipping Method</option>
                                                    <option selected="selected"><?php echo $list[0]->{'s-method'}; ?></option>
                                                    <option>Overnight</option>
                                                    <option>2 Day</option>
                                                    <option>3 Day</option>
                                                    <option>Ground</option>
                                                </select>
                                            </div>
                                        </div>
                                    <?php } ?>
                                    <p class="initial">Test</p>
                                    <h3 style="text-align: left !important; color: #ffffff !important; font-size: 20px !important;">
                                        Terms:</h3>
                                    <ul class="rma-request">
                                        <li>In order to expedite the processing of the RMA number, please fill out this
                                            form completely.
                                        </li>
                                        <li>Once the RMA request is processed, the Return Merchandise Authorization
                                            number and shipping instructions will be sent via e-mail.
                                        </li>
                                        <li>Orders for replacement products will be charged immediately.</li>
                                        <li>Credit for returned item will occur when product is received. Please allow
                                            up to two weeks for processing after the returned product is received.
                                        </li>
                                        <li>You will be receiving a submission confirmation via email. If you do not
                                            receive the confirmation within 48 hours, please reach out to <a
                                                    href="mailto:sales@bktechnologies.com">sales@bktechnologies.com</a>.
                                        </li>
                                        <li>Return orders may be subject to restocking fee.</li>
                                    </ul>

                                    <div class="form-group">
                                        <div class="col-lg-12">
                                            <label for="signature">I accept the terms of the RMA Request form.</label>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-lg-3">
                                            <label for="signature">E-Signature</label>
                                            <input type="text" name="signature" disabled id="signature"
                                                   value="<?php echo $list[0]->signature; ?>" required>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-lg-12">
                                            <a type="button" href="<?= base_url('admin/dashboard/rma_form') ?>"
                                               class='btn btn-info'>Close</a>
                                            <!-- <button class='btn btn-warning' type="reset">Reset Form</button> -->
                                        </div>
                                    </div>
                            </form>
                            <?php } ?>
                        </div>
                    </div>

                    <div class="form-group" style="float: right; margin: 10px;">
                        <div class="col-lg-2">
                            <a type="button" onclick="printContent('contact')" class='btn btn-success'>Print</a>
                            <!-- <button class='btn btn-warning' type="reset">Reset Form</button> -->
                        </div>
                    </div>

<!--                    <a onclick="printContent('contact')">Print</a>-->

                    <!--                        form body ends-->
                </div>
                <?php } else { ?>
                    <div class="row">
                        <h1 class="text-center">
                            No Data Found
                        </h1>
                    </div>
                <?php } ?>

        </section>
        <!-- /.content -->
    </div>
</div>
<!-- /.content-wrapper -->
<!--<script src="--><?php //echo base_url();?><!--assets/admin/js/htmltopdf.js"></script>-->

<script>
    $('.form-content').multifield({
        section: '.group',
        btnAdd: '#btnAdd',
        btnRemove: '.btnRemove',
    });

    $('.form-content-2').multifield({
        section: '.group-2',
        btnAdd: '#btnAdd2',
        btnRemove: '.btnRemove',
    });

    $("#btnAdd").one('click', function () {
        $(".inner").append("<p>Test</p>");
        $("p.initial").addClass("hide");
    });
    $("#btnAdd2").one('click', function () {
        $(".inner-2").append("<p>Test</p>");
        $("p.initial").addClass("hide");
    });

    $("input[type=radio]") // select the radio by its id
        .change(function () { // bind a function to the change event
            if ($(this).is(":checked")) { // check if the radio is checked
                var val = $(this).val(); // retrieve the value
                if (val == "replacement") {
                    $(".replacement-section").removeClass("hide");
                } else {
                    $(".replacement-section").addClass("hide");
                }
            }
        });

    function printContent(el) {
        var restorePage = document.body.innerHTML;
        var printableContent = document.getElementById(el).innerHTML;
        document.body.innerHTML = printableContent;
        window.print();
        document.body.innerHTML = restorePage;
    }


//     function print() {
//         var element = document.getElementById('contact');
//         var opt = {
//             margin: 1,
//             filename: (Math.floor(Math.random() * 10000) + 1) + '.pdf',
//         };
//
// // New Promise-based usage:
//         html2pdf().set(opt).from(element).save();
//     }

</script>

<style>
    section#main-conent {
        padding: 90px 0 75px 0 !important;
    }

    .row.contact-form {
        background: #294e6e !important;
        padding-bottom: 10px !important;
    }

    #contact h3 {
        margin-top: 0px !important;
        margin-left: 15px !important;
        text-align: left !important;
        color: #ffffff !important;
    }

    select#reason, select#s-method {
        width: 48.5% !important;
        float: left !important;
    }

    .row.contact-form label.radio-inline {
        display: inline-block !important;
        float: left !important;
        margin-right: 30px !important;
    }

    .radio-inline input[type=radio] {
        width: auto !important;
    }

    .space-filler {
        visibility: hidden !important;
    }

    .inner p, .inner-2 {
        visibility: hidden !important;
    }

    p.initial {
        visibility: hidden !important;
    }

    button#btnAdd, button#btnAdd2 {
        background: #00a6d5 !important;
        border-color: #00a6d5 !important;
        color: #ffffff !important;
        display: block !important;
        margin-left: 15px !important;
        border-radius: 0px !important;
        font-size: 14px !important;
        text-transform: uppercase !important;
        font-family: Lato !important;
        font-weight: 600 !important;
        margin-bottom: 10px !important;
        padding: 6px 25px !important;
    }

    ul.rma-request {
        text-align: left !important;
        list-style-type: disc !important;
        color: #ffffff !important;
        margin-left: 35px !important;
        font-family: Lato !important;
        margin-bottom: 30px !important;
        margin-right: 20px !important;
    }

    ul.rma-request a {
        color: #ffffff !important;
    }

    .navbar {
        margin-top: 0px;
    }

    footer {
        background: #000000;
        padding: 20px 0px !important;
    }

    .box {
        position: relative;
        border-radius: 3px;
        background: #ffffff;
        border-top: 0px solid #d2d6de;
        margin-bottom: 20px;
        width: 100%;
        box-shadow: 0 1px 1px rgba(0, 0, 0, 0.1);
    }
</style>

<style>
    .contact h4, .contact h4 a {
        font-size: 18px !important;
        font-family: Lato !important;
        line-height: 24px !important;
        color: #000000 !important;
    }

    .row.contact-form {
        background: #294e6e !important;
        padding-bottom: 60px !important;
    }

    .row.contact-form label {
        display: block !important;
        text-align: left !important;
        color: #ffffff !important;
        font-family: Lato !important;
        font-size: 16px !important;
    }

    .row.contact-form input, .row.contact-form select {
        width: 100% !important;
        padding: 8px 2px !important;
        margin-bottom: 20px !important;
    }

    .row.contact-form textarea {
        width: 100% !important;
        height: 150px !important;
        margin-bottom: 20px !important;
    }

    .row.contact-form .btn-info {
        border-radius: 0px !important;
        background: #00a6d5 !important;
        border-color: #00a6d5 !important;
        color: #ffffff !important;
        font-family: Lato !important;
        text-transform: uppercase !important;
        font-size: 17px !important;
        display: inline-block !important;
        width: 100% !important;
        font-weight: 600 !important;
        text-align: center !important;
        padding: 10px 0px !important;
        transition: padding 0.5s ease, background 0.5s ease, border-color 0.5s ease, color 0.5s ease !important;
        margin-bottom: 20px !important;
    }

    .row.contact-form .btn-info:hover {
        color: #294e6e !important;
        background: #ffffff !important;
        border-color: #ffffff !important;
        text-decoration: none !important;
    }

</style>