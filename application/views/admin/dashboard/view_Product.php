 <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        View Product
       
      </h1>
     
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title"></h3>

          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                    title="Collapse">
              <i class="fa fa-minus"></i></button>
            <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
              <i class="fa fa-times"></i></button>
          </div>
        </div>
        <div class="box-body">
          <?php if(sizeof($user_about_data)>0){?>
          <table class="table" id="datatableId">
            <thead>
              <tr><th>Portables</th>
                <th>Mobiles</th>
                <th>Base Stations</th>
                <th>Repeaters</th>
                <th>Type</th>
                                
                <th>Status</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody>

              
              <?php foreach($user_about_data as $row){?>
              <tr id="row_<?php echo $row->id;?>">
                <td><form method="post" action="<?php echo base_url();?>/admin/dashboard/view_product_variation"><input type="submit" value='<?php echo $row->Portables;?>' name="products"></form></td>
                <td><form method="post" action="<?php echo base_url();?>/admin/dashboard/view_product_variation"><input type="submit" value='<?php echo $row->Mobiles;?>' name="products"></form></td>
                <td><form method="post" action="<?php echo base_url();?>/admin/dashboard/view_product_variation"><input type="submit" value='<?php echo $row->BaseStations;?>' name="products"></form></td>
                <td><form method="post" action="<?php echo base_url();?>/admin/dashboard/view_product_variation"><input type="submit" value='<?php echo $row->Repeaters;?>' name="products"></form></td>
                <td><?php if($row->Type==1){ echo "KAA0732 > KAA0733";} if($row->Type==2){ echo "KAA0733";}if($row->Type==3){ echo "BK Radio Analog Radios";}if($row->Type==4){ echo "BK Technology Brand Analog Radios";}?></td>
                                
                <td id="status_<?php echo $row->id;?>"><?php if($row->status==1){?> <a onclick="active_row(2,<?php echo $row->id;?>,1)" class="btn btn-success btn-sm"><i class="fa  fa-thumbs-o-up"></i> Active</a><?php }else{ ?><a onclick="active_row(2,<?php echo $row->id;?>,0)" class="btn btn-danger btn-sm"><i class="fa  fa-thumbs-o-down"></i> In-active</a><?php } ?></td>
                <td>
                  <a onclick="delete_row(2,<?php echo $row->id;?>)" class="btn btn-danger btn-sm"><i class="fa   fa-trash"></i></a></td>
              </tr>
              <?php } ?>
            </tbody>
          </table>
          <?php }else{echo "<h2 style='text-align:center;'>Opps! No Data Found!</h2>";} ?>
        </div>
        <!-- /.box-body -->
        <div class="box-footer">
        
        </div>
        <!-- /.box-footer-->
      </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->



