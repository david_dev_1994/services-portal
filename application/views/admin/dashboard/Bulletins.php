 <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Add Bulletins
       
      </h1>
     
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title"></h3>

          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                    title="Collapse">
              <i class="fa fa-minus"></i></button>
            <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
              <i class="fa fa-times"></i></button>
          </div>
        </div>
        <div class="box-body">
          <div class="col-md-6 col-md-offset-3">
            <?php $notification=$this->session->flashdata('notification');
            if($notification){ ?>            
            <div id="snackbar"><?php echo $notification;?></div>
            <script type="text/javascript">$(document).ready(function(){
              notificationFun();
            });</script>
              
              <?php }?>
              <?php echo validation_errors();?>
              <form data-toggle="validator" role="form" method="post" enctype="multipart/form-data">
                  <div class="form-group">
                    <label class="control-label" for="institute">Radio Model</label>

                    <input class="form-control" data-minlength="3" data-error="Must enter minimum of 3 characters" id="institute" name="Radio_Model" value="<?php echo set_value('Radio_Model');?>" placeholder="Radio Model"  type="text" required />
                    <div class="help-block with-errors"><?php echo form_error('Radio_Model');?></div>
                  </div>
                  <div class="form-group">
                    <label class="control-label" for="course">Bulletin</label>
                    <input class="form-control"  id="Bulletin" name="Bulletin" data-minlength="3" data-error="Must enter minimum of 3 characters" value="<?php echo set_value('Bulletin');?>" placeholder="Bulletin"  type="text" required />
                    <div class="help-block with-errors"><?php echo form_error('Bulletin');?></div>
                  </div>
                  
                  <div class="form-group">
                    <label class="control-label" for="start_date">Release Date</label>
                    <input class="form-control"  id="Release_Date" name="Release_Date" value="<?php echo set_value('Release_Date');?>"   type="date" required />
                    <div class="help-block with-errors"><?php echo form_error('Release_Date');?></div>
                  </div>
                  <div class="form-group">
                    <label class="control-label" for="start_date">Type</label>
                      <select class="form-control" name="Type">
                        <option value="1">BK Technology</option>
                        <option value="2">BM</option>
                        <option value="3">Discontinued</option>
                      </select>
                  </div>
                   <div class="form-group">
                    <label class="control-label" for="start_date">PDF FILE</label>
                    <input type="file" id="user_image" name="user_image" required >
                    <div class="help-block with-errors"><?php echo form_error('Release_Date');?></div>
                  </div>
                   
                   <div class="form-group">
                    <label class="control-label" for="score">Description</label>
                    <div class="form-group">
                         <textarea id="editor1" name="Description"></textarea>
                       </div>
                    <div class="help-block with-errors"><?php echo form_error('Description');?></div>
                  </div>
                  <div class="form-group">
                         <button name="save_education" class="btn btn-primary" type="submit">
                             Save
                         </button>
                     </div>
              </form>
            </div>

        </div>
        <!-- /.box-body -->
        <div class="box-footer">
        
        </div>
        <!-- /.box-footer-->
      </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->


