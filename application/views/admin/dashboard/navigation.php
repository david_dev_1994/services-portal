  <!-- Left side column. contains the sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="<?php echo base_url();?>/assets/images/<?php echo $this->session->userdata('user_image');?>" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p><?php echo $this->session->userdata('user_name');?></p>
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>
      <!-- search form -->
    <!--   <form action="#" method="get" class="sidebar-form">
        <div class="input-group">
          <input type="text" name="q" class="form-control" placeholder="Search...">
          <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
        </div>
      </form> -->
      <!-- /.search form -->
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">MAIN NAVIGATION</li>

          <li class="<?php if(isset($active_class) && $active_class=='dashboard'){echo 'active';} ?>">
              <a href="<?php echo base_url();?>/admin/dashboard">
                <i class="fa  fa-dashboard"></i> <span>Dashboard</span>
              </a>
          </li>

          <!--          Alberta -->
          <li class="treeview <?php if(isset($active_class) && $active_class=='Alberta'){echo 'active';} ?>">
              <a href="#">
                  <i class="fa  fa-dashboard"></i> <span>Alberta</span>
                  <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
              </a>
              <ul class="treeview-menu ">
                  <li class="<?php if(isset($active_sub_class) && $active_sub_class=='alb_dashboard'){echo 'active';} ?>">
                      <a target="_blank" href="<?= base_url('Alberta'); ?>">
                          <i class="fa  fa-home"></i> <span>Alberta Dashboard</span>
                      </a>
                  </li>
                  <li class="<?php if(isset($active_sub_class) && $active_sub_class=='Alberta_User'){echo 'active';} ?>"><a href="<?php echo base_url();?>/admin/dashboard/view_user/1"><i class="fa  fa-eye"></i>View Users</a></li>
                  <!--                  <li class="--><?php //if(isset($active_sub_class) && $active_sub_class=='adduser'){echo 'active';} ?><!--"><a href="--><?php //echo base_url();?><!--/admin/dashboard/add_user"><i class="fa fa-plus"></i> Add New</a></li>-->
              </ul>
          </li>


          <!--          calfire -->
          <li class="treeview <?php if(isset($active_class) && $active_class=='CalFire'){echo 'active';} ?>">
              <a href="#">
                  <i class="fa  fa-dashboard"></i> <span>Calfire</span>
                  <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
              </a>
              <ul class="treeview-menu ">
                  <li class="<?php if(isset($active_sub_class) && $active_sub_class=='cal_dashboard'){echo 'active';} ?>">
                      <a href="<?php echo base_url();?>/admin/dashboard/showall/3">
                          <i class="fa  fa-home"></i> <span>Calfire Repair Profiles</span>
                      </a>
                  </li>
                  <li class="<?php if(isset($active_sub_class) && $active_sub_class=='CalFire_User'){echo 'active';} ?>"><a href="<?php echo base_url();?>/admin/dashboard/view_user/3"><i class="fa  fa-eye"></i>View Users</a></li>
                  <!--                  <li class="--><?php //if(isset($active_sub_class) && $active_sub_class=='adduser'){echo 'active';} ?><!--"><a href="--><?php //echo base_url();?><!--/admin/dashboard/add_user"><i class="fa fa-plus"></i> Add New</a></li>-->
              </ul>
          </li>


          <!--          Florida -->
          <li class="treeview <?php if(isset($active_class) && $active_class=='Florida'){echo 'active';} ?>">
              <a href="#">
                  <i class="fa  fa-dashboard"></i> <span>Florida</span>
                  <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
              </a>
              <ul class="treeview-menu ">
                  <li class="<?php if(isset($active_sub_class) && $active_sub_class=='flo_dashboard'){echo 'active';} ?>">
                      <a href="<?php echo base_url();?>/admin/dashboard/showall/2">
                          <i class="fa  fa-home"></i> <span>Florida Reapir Profiles</span>
                      </a>
                  </li>
                  <li class="<?php if(isset($active_sub_class) && $active_sub_class=='Florida_User'){echo 'active';} ?>"><a href="<?php echo base_url();?>/admin/dashboard/view_user/2"><i class="fa  fa-eye"></i>View Users</a></li>
                  <!--                  <li class="--><?php //if(isset($active_sub_class) && $active_sub_class=='adduser'){echo 'active';} ?><!--"><a href="--><?php //echo base_url();?><!--/admin/dashboard/add_user"><i class="fa fa-plus"></i> Add New</a></li>-->
              </ul>
          </li>

          <!--          USFS -->
          <li class="treeview <?php if(isset($active_class) && $active_class=='Usfs'){echo 'active';} ?>">
              <a href="#">
                  <i class="fa  fa-dashboard"></i> <span>USFS</span>
                  <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
              </a>
              <ul class="treeview-menu ">
                  <li class="<?php if(isset($active_sub_class) && $active_sub_class=='Usfs_dashboard'){echo 'active';} ?>">
                      <a href="<?php echo base_url();?>/admin/dashboard/showall/4">
                          <i class="fa  fa-home"></i> <span>USFS Reapir Profiles</span>
                      </a>
                  </li>
                  <li class="<?php if(isset($active_sub_class) && $active_sub_class=='Usfs_User'){echo 'active';} ?>"><a href="<?php echo base_url();?>/admin/dashboard/view_user/4"><i class="fa  fa-eye"></i>View Users</a></li>
                  <!--                  <li class="--><?php //if(isset($active_sub_class) && $active_sub_class=='adduser'){echo 'active';} ?><!--"><a href="--><?php //echo base_url();?><!--/admin/dashboard/add_user"><i class="fa fa-plus"></i> Add New</a></li>-->
              </ul>
          </li>


          <li class="<?php if(isset($active_class) && $active_class=='RMA'){echo 'active';} ?>">
              <a href="<?php echo base_url();?>admin/dashboard/rma_form">
                  <i class="fa fa-file"></i> <span>RMA-Listing</span>
              </a>
          </li>

          <li class="<?php if(isset($active_class) && $active_class=='Downloads'){echo 'active';} ?>">
              <a href="<?php echo base_url('admin/dashboard/downloads');?>">
                  <i class="fa fa-file"></i> <span>Downloads</span>
              </a>
          </li>

        <li class="<?php if (isset($active_class) && $active_class == 'Serial Registration') {
            echo 'active';
        } ?>">
            <a href="<?php echo base_url('admin/dashboard/serialReg'); ?>">
                <i class="fa fa-file"></i> <span>Registered Serials</span>
            </a>
        </li>




          <!--         <li class="treeview --><?php //if(isset($active_class) && $active_class=='Bulletins'){echo 'active';} ?><!--">-->
<!--          <a href="#">-->
<!--            <i class="fa  fa-bar-chart"></i> <span>Bulletins</span>-->
<!--            <span class="pull-right-container">-->
<!--              <i class="fa fa-angle-left pull-right"></i>-->
<!--            </span>-->
<!--          </a>-->
<!--          <ul class="treeview-menu ">-->
<!--            <li class="--><?php //if(isset($active_sub_class) && $active_sub_class=='view_experience'){echo 'active';} ?><!--"><a href="--><?php //echo base_url();?><!--/admin/dashboard/view_Bulletins"><i class="fa  fa-eye"></i> View</a></li>-->
<!--             <li class="--><?php //if(isset($active_sub_class) && $active_sub_class=='add_Bulletins'){echo 'active';} ?><!--"><a href="--><?php //echo base_url();?><!--/admin/dashboard/add_Bulletins"><i class="fa fa-plus"></i> Add New</a></li>            -->
<!--          </ul>-->
<!--        </li>-->
<!--        <li class="treeview --><?php //if(isset($active_class) && $active_class=='product'){echo 'active';} ?><!--">-->
<!--          <a href="#">-->
<!--            <i class="fa  fa-bar-chart"></i> <span>Product</span>-->
<!--            <span class="pull-right-container">-->
<!--              <i class="fa fa-angle-left pull-right"></i>-->
<!--            </span>-->
<!--          </a>-->
<!--          <ul class="treeview-menu ">-->
<!--            <li class="--><?php //if(isset($active_sub_class) && $active_sub_class=='view_product'){echo 'active';} ?><!--"><a href="--><?php //echo base_url();?><!--/admin/dashboard/view_Product"><i class="fa  fa-eye"></i> View</a></li>-->
<!--             <li class="--><?php //if(isset($active_sub_class) && $active_sub_class=='add_product'){echo 'active';} ?><!--"><a href="--><?php //echo base_url();?><!--/admin/dashboard/add_Product"><i class="fa fa-plus"></i> Add New</a></li>            -->
<!--          </ul>-->
<!--        </li>-->
<!---->
<!--<li class="treeview --><?php //if(isset($active_class) && $active_class=='part'){echo 'active';} ?><!--">-->
<!--          <a href="#">-->
<!--            <i class="fa  fa-bar-chart"></i> <span>Parts</span>-->
<!--            <span class="pull-right-container">-->
<!--              <i class="fa fa-angle-left pull-right"></i>-->
<!--            </span>-->
<!--          </a>-->
<!--          <ul class="treeview-menu ">-->
<!--            <li class="--><?php //if(isset($active_sub_class) && $active_sub_class=='awards'){echo 'active';} ?><!--"><a href="--><?php //echo base_url();?><!--/admin/dashboard/awards"><i class="fa  fa-eye"></i> View & Add Category</a></li>-->
<!--             <li class="--><?php //if(isset($active_sub_class) && $active_sub_class=='add_Part'){echo 'active';} ?><!--"><a href="--><?php //echo base_url();?><!--/admin/dashboard/add_Part"><i class="fa fa-plus"></i> Add Part</a></li>-->
<!--<li class="--><?php //if(isset($active_sub_class) && $active_sub_class=='view_part'){echo 'active';} ?><!--"><a href="--><?php //echo base_url();?><!--/admin/dashboard/view_part"><i class="fa fa-plus"></i> View Part</a></li>            -->
<!--          </ul>-->
<!--        </li>-->
<!---->
<!---->
<!--<li class="treeview --><?php //if(isset($active_class) && $active_class=='manuals'){echo 'active';} ?><!--">-->
<!--          <a href="#">-->
<!--            <i class="fa  fa-bar-chart"></i> <span>Manuals</span>-->
<!--            <span class="pull-right-container">-->
<!--              <i class="fa fa-angle-left pull-right"></i>-->
<!--            </span>-->
<!--          </a>-->
<!--          <ul class="treeview-menu ">-->
<!--            -->
<!--             <li class="--><?php //if(isset($active_sub_class) && $active_sub_class=='add_manuals'){echo 'active';} ?><!--"><a href="--><?php //echo base_url();?><!--/admin/dashboard/add_manuals"><i class="fa fa-plus"></i> Add Manuals</a></li>-->
<!--<li class="--><?php //if(isset($active_sub_class) && $active_sub_class=='view_manuals'){echo 'active';} ?><!--"><a href="--><?php //echo base_url();?><!--/admin/dashboard/view_manuals"><i class="fa fa-plus"></i> View Manuals</a></li>            -->
<!--          </ul>-->
<!--        </li>-->

<!--         <li class="--><?php //if(isset($active_class) && $active_class=='User'){echo 'active';} ?><!--">-->
<!--          <a href="--><?php //echo base_url();?><!--/admin/dashboard/view_user">-->
<!--            <i class="fa fa-users"></i> <span>Users</span>           -->
<!--          </a>-->
<!--         </li>-->

<!--        <li class="--><?php //if(isset($active_class) && $active_class=='codeplug'){echo 'active';} ?><!--">-->
<!--          <a href="--><?php //echo base_url();?><!--/admin/dashboard/view_codeplug">-->
<!--            <i class="fa fa-bar-chart"></i> <span>Codeplug</span>           -->
<!--          </a>-->
<!--        </li>-->
<!--        <li class="--><?php //if(isset($active_class) && $active_class=='Spreedsheet'){echo 'active';} ?><!--">-->
<!--          <a href="--><?php //echo base_url();?><!--/admin/dashboard/view_Esn_spread_sheet">-->
<!--            <i class="fa fa-bar-chart"></i> <span>Esn Spread Sheet</span>           -->
<!--          </a>-->
<!--        </li>-->


<!--          Clients-->

<!--          <li class="treeview --><?php //if(isset($active_class) && $active_class=='User'){echo 'active';} ?><!--">-->
<!--              <a href="#">-->
<!--                  <i class="fa  fa-bar-chart"></i> <span>Clients</span>-->
<!--                  <span class="pull-right-container">-->
<!--              <i class="fa fa-angle-left pull-right"></i>-->
<!--            </span>-->
<!--              </a>-->
<!--              <ul class="treeview-menu ">-->
<!--                  <li class="--><?php //if(isset($active_sub_class) && $active_sub_class=='UserList'){echo 'active';} ?><!--"><a href="--><?php //echo base_url();?><!--/admin/dashboard/view_user"><i class="fa  fa-eye"></i> View</a></li>-->
<!--<!--                  <li class="-->--><?php ////if(isset($active_sub_class) && $active_sub_class=='adduser'){echo 'active';} ?><!--<!--"><a href="-->--><?php ////echo base_url();?><!--<!--/admin/dashboard/add_user"><i class="fa fa-plus"></i> Add New</a></li>-->-->
<!--              </ul>-->
<!--          </li>-->






<!---->
<!--          <li class="treeview --><?php //if(isset($active_class) && $active_class=='Calfire'){echo 'active';} ?><!--">-->
<!--          <a href="#">-->
<!--            <i class="fa  fa-bar-chart"></i> <span>Calfire</span>-->
<!--            <span class="pull-right-container">-->
<!--              <i class="fa fa-angle-left pull-right"></i>-->
<!--            </span>-->
<!--          </a>-->
<!--          <ul class="treeview-menu ">-->
<!--            <li class="--><?php //if(isset($active_sub_class) && $active_sub_class=='view_Calfire'){echo 'active';} ?><!--"><a href="--><?php //echo base_url();?><!--/admin/dashboard/view_Calfire"><i class="fa  fa-eye"></i> View</a></li>-->
<!--             <li class="--><?php //if(isset($active_sub_class) && $active_sub_class=='add_Calfire'){echo 'active';} ?><!--"><a href="--><?php //echo base_url();?><!--/admin/dashboard/add_Calfire"><i class="fa fa-plus"></i> Add New</a></li>-->
<!--          </ul>-->
<!--         </li>-->

        <!--<li class="<?php if(isset($active_class) && $active_class=='about'){echo 'active';} ?>">
          <a href="<?php echo base_url();?>/admin/dashboard/about">
            <i class="fa  fa-user"></i> <span>About</span>           
          </a>
        </li>-->
        <!--<li class="<?php if(isset($active_class) && $active_class=='social'){echo 'active';} ?>">
          <a href="<?php echo base_url();?>/admin/dashboard/social">
            <i class="fa  fa-rss"></i> <span>Social Network</span>           
          </a>
        </li>-->
        <!-- <li class="treeview <?php if(isset($active_class) && $active_class=='experience'){echo 'active';} ?>">
          <a href="#">
            <i class="fa  fa-bar-chart"></i> <span>Experience</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu ">
            <li class="<?php if(isset($active_sub_class) && $active_sub_class=='view_experience'){echo 'active';} ?>"><a href="<?php echo base_url();?>/admin/dashboard/view_experience"><i class="fa  fa-eye"></i> View</a></li>
             <li class="<?php if(isset($active_sub_class) && $active_sub_class=='add_experience'){echo 'active';} ?>"><a href="<?php echo base_url();?>/admin/dashboard/add_experience"><i class="fa fa-plus"></i> Add New</a></li>            
          </ul>
        </li>-->

        <!--<li class="treeview <?php if(isset($active_class) && $active_class=='education'){echo 'active';} ?>">
          <a href="#">
            <i class="fa  fa-graduation-cap"></i> <span>Education</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu ">
            <li class="<?php if(isset($active_sub_class) && $active_sub_class=='view_education'){echo 'active';} ?>"><a href="<?php echo base_url();?>/admin/dashboard/view_education"><i class="fa  fa-eye"></i> View</a></li>
             <li class="<?php if(isset($active_sub_class) && $active_sub_class=='add_education'){echo 'active';} ?>"><a href="<?php echo base_url();?>/admin/dashboard/add_education"><i class="fa fa-plus"></i> Add New</a></li>            
          </ul>
        </li>-->
        
       <!-- <li class="<?php if(isset($active_class) && $active_class=='interests'){echo 'active';} ?>">
          <a href="<?php echo base_url();?>/admin/dashboard/interests">
            <i class="fa  fa-heart"></i> <span>Interests</span>           
          </a>
        </li>-->
        <!--<li class="<?php if(isset($active_class) && $active_class=='awards'){echo 'active';} ?>">
          <a href="<?php echo base_url();?>/admin/dashboard/awards">
            <i class="fa  fa-certificate"></i> <span>Awards</span>           
          </a>
        </li>-->
        <!--<li class="<?php if(isset($active_class) && $active_class=='theme_setting'){echo 'active';} ?>">
          <a href="<?php echo base_url();?>/admin/dashboard/theme_setting">
            <i class="fa  fa-cog"></i> <span>Theme Setting</span>           
          </a>
        </li>-->
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>

  <!-- =============================================== -->