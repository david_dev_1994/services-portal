  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 2.4.0
    </div>
    <strong>Copyright &copy; <?php echo date('Y');?> <a href="#">RP</a>.</strong> All rights
    reserved.
  </footer>

   <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->


<!-- Bootstrap 3.3.7 -->
<script src="<?php echo base_url();?>/assets/admin/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/1000hz-bootstrap-validator/0.11.5/validator.min.js">
       </script>
<!-- SlimScroll -->
<script src="<?php echo base_url();?>/assets/admin/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="<?php echo base_url();?>assets/admin/bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="<?php echo base_url();?>assets/admin/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?php echo base_url();?>assets/admin/js/demo.js"></script>
<!-- CK Editor-->
<script src="https://cdn.ckeditor.com/4.8.0/standard/ckeditor.js"></script>
<script> CKEDITOR.replace( 'editor1' ); </script>
<!-- DataTables -->
<!--<script src="--><?php //echo base_url();?><!--assets/admin/plugins/datatables.net/js/jquery.dataTables.min.js"></script>-->
<!--<script src="--><?php //echo base_url();?><!--assets/admin/plugins/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>-->

<!--  <script src="--><?php //echo base_url(); ?><!--assets/admin/js/datatable.min.js" type="text/javascript"></script>-->
  <script src="<?php echo base_url(); ?>assets/admin/plugins/datatables/datatables.min.js" type="text/javascript"></script>
  <script src="<?php echo base_url(); ?>assets/admin/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js" type="text/javascript"></script>
  <script src="<?php echo base_url(); ?>assets/admin/js/custom.js" type="text/javascript"></script>


<script>
  $(document).ready(function () {
    $('.sidebar-menu').tree();

   
  });

</script>

<script>
 function notificationFun() {
    var x = document.getElementById("snackbar")
    x.className = "show";
    setTimeout(function(){ x.className = x.className.replace("show", ""); }, 5000);
   }


</script>

</body>
</html>
