<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title><?php if(isset($page_title)){echo $page_title;}?></title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

  <!--rana-->
  <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">


  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="<?php echo base_url();?>/assets/admin/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?php echo base_url();?>/assets/admin/bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="<?php echo base_url();?>/assets/admin/bower_components/Ionicons/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url();?>/assets/admin/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="<?php echo base_url();?>/assets/admin/css/skins/_all-skins.min.css">
  <link rel="stylesheet" href="<?php echo base_url();?>/assets/admin/css/plugins.min.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
 <!-- jQuery 3 -->
<script src="<?php echo base_url();?>/assets/admin/bower_components/jquery/dist/jquery.min.js"></script>
  <script src="<?= base_url('/assets/styles/js/custom.js')?>"></script>


  <!--<style>-->
<!--  * {box-sizing: border-box}-->
<!---->
<!--  /* Full-width input fields */-->
<!--  input[type=text], input[type=password] {-->
<!--    width: 100%;-->
<!--    padding: 15px;-->
<!--    margin: 5px 0 22px 0;-->
<!--    display: inline-block;-->
<!--    border: none;-->
<!--    background: #f1f1f1;-->
<!--  }-->
<!---->
<!--  input[type=text]:focus, input[type=password]:focus {-->
<!--    background-color: #ddd;-->
<!--    outline: none;-->
<!--  }-->
<!---->
<!--  hr {-->
<!--    border: 1px solid #f1f1f1;-->
<!--    margin-bottom: 25px;-->
<!--  }-->
<!---->
<!--  /* Set a style for all buttons */-->
<!--  button {-->
<!--    background-color: #4CAF50;-->
<!--    color: white;-->
<!--    padding: 14px 20px;-->
<!--    margin: 8px 0;-->
<!--    border: none;-->
<!--    cursor: pointer;-->
<!--    width: 100%;-->
<!--    opacity: 0.9;-->
<!--  }-->
<!---->
<!--  button:hover {-->
<!--    opacity:1;-->
<!--  }-->
<!---->
<!--  /* Extra styles for the cancel button */-->
<!--  .cancelbtn {-->
<!--    padding: 14px 20px;-->
<!--    background-color: #f44336;-->
<!--  }-->
<!---->
<!--  /* Float cancel and signup buttons and add an equal width */-->
<!--  .cancelbtn, .signupbtn {-->
<!--    float: left;-->
<!--    width: 50%;-->
<!--  }-->
<!---->
<!--  /* Add padding to container elements */-->
<!--  .container {-->
<!--    padding: 16px;-->
<!--  }-->
<!---->
<!--  /* Clear floats */-->
<!--  .clearfix::after {-->
<!--    content: "";-->
<!--    clear: both;-->
<!--    display: table;-->
<!--  }-->
<!---->
<!--  /* Change styles for cancel button and signup button on extra small screens */-->
<!--  @media screen and (max-width: 300px) {-->
<!--    .cancelbtn, .signupbtn {-->
<!--      width: 100%;-->
<!--    }-->
<!--  }-->
<!--</style>-->

</head>
<body class="hold-transition skin-blue sidebar-mini">
<!-- Site wrapper -->
<div class="wrapper">

  <header class="main-header">
    <!-- Logo -->
    <a href="<?php echo base_url().'/admin';?>" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>R</b>P</span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>R</b>P</span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </a>

      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">      
          
         
          <!-- User Account: style can be found in dropdown.less -->
          <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <img src="<?php echo base_url();?>/assets/images/<?php echo $this->session->userdata('user_image');?>" class="user-image" alt="User Image">
              <span class="hidden-xs"><?php echo $this->session->userdata('user_name');?></span>
            </a>
            <ul class="dropdown-menu">
              <!-- User image -->
              <li class="user-header">
                <img src="<?php echo base_url();?>/assets/images/<?php echo $this->session->userdata('user_image');?>" class="img-circle" alt="User Image">

                <p>
                  <?php echo $this->session->userdata('user_name');?>
                  <small>You are Admin since <?php echo $this->session->userdata('user_registered');?></small>
                </p>
              </li>
              
              <!-- Menu Footer-->
              <li class="user-footer">
                <div class="pull-left">
                  <a href="<?php echo base_url().'/admin/dashboard';?>" class="btn btn-default btn-flat">Profile</a>
                </div>
                <div class="pull-right">
                  <?php echo anchor('admin/admin/logout','Sign out',array('class'=>'btn btn-default btn-flat')); ?>
                  
                </div>
              </li>
            </ul>
          </li>
         
        </ul>
      </div>
    </nav>
  </header>

  <!-- =============================================== -->

  <style>
#snackbar {
    visibility: hidden;
    min-width: 250px;
    margin-left: -125px;
    background-color: #333;
    color: #fff;
    text-align: center;
    border-radius: 2px;
    padding: 16px;
    position: fixed;
    z-index: 1;
    bottom: 30px;
    font-size: 17px;
    right: 10%;

}

#snackbar.show {
    visibility: visible;
    -webkit-animation: fadein 0.5s, fadeout 0.5s 2.5s;
    animation: fadein 0.5s, fadeout 0.5s 2.5s;
}

@-webkit-keyframes fadein {
    from {bottom: 0; opacity: 0;} 
    to {bottom: 30px; opacity: 1;}
}

@keyframes fadein {
    from {bottom: 0; opacity: 0;}
    to {bottom: 30px; opacity: 1;}
}

@-webkit-keyframes fadeout {
    from {bottom: 30px; opacity: 1;} 
    to {bottom: 0; opacity: 0;}
}

@keyframes fadeout {
    from {bottom: 30px; opacity: 1;}
    to {bottom: 0; opacity: 0;}
}


  </style>