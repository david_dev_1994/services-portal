 <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        View Product Variation
       
      </h1>
     
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title"></h3>

          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                    title="Collapse">
              <i class="fa fa-minus"></i></button>
            <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
              <i class="fa fa-times"></i></button>
          </div>
        </div>
        <div class="box-body">
        	<ul class="nav nav-tabs">
			  <li class="active"><a data-toggle="tab" href="#home">
Firmware & Patches</a></li>
			  <li><a data-toggle="tab" href="#menu1">Software</a></li>
			  <li><a data-toggle="tab" href="#menu2">Manuals Etc.</a></li>
			  <li><a data-toggle="tab" href="#menu3">Drivers Etc..</a></li>
			</ul>

			<div class="tab-content">
			  <div id="home" class="tab-pane fade in active">
			  	  <div class='col-md-12 well'>
			           <form data-toggle="validator" role="form" method="post" enctype="multipart/form-data">
			             <div class='col-md-4'>
			               <div class="form-group">
			               <label>Release Date</label>
			                <input class="form-control" data-error="Please enter date" id="reDate" name="reDate" value="<?php echo set_value('reDate');?>"   type="date" required />
			                    <div class="help-block with-errors"><?php echo form_error('reDate');?></div>
			              </div>
			             </div>
			             <div class='col-md-4'>
			               <div class="form-group">
			               <label>Revision Number</label>
			                <input class="form-control" data-minlength="2" data-error="Must enter minimum of 2 characters" id="RevisionNumber" name="RevisionNumber" value="<?php echo set_value('RevisionNumber');?>" placeholder="Revision Number"  type="text" required />
			                    <div class="help-block with-errors"><?php echo form_error('RevisionNumber');?></div>
			                   </div>
			             </div>
			             <div class='col-md-4'>
			                   <div class="form-group">
			                <label>Required Editor</label>
			                <input class="form-control" data-minlength="2" data-error="Must enter minimum of 2 characters" id="RequiredEditor" name="RequiredEditor" value="<?php echo set_value('RequiredEditor');?>" placeholder="Required Editor"  type="text" required />
			                    <div class="help-block with-errors"><?php echo form_error('RequiredEditor');?></div>
			                 </div>
			             </div>
			             <div class='col-md-4'>
			                   <div class="form-group">
					                <label>Release Notes</label>
					                <input class="form-control"  name="ReleaseNotes"   type="file" required />
			                   </div>
			             </div>
			             <div class='col-md-4'>
			                   <div class="form-group">
					                <label>File Zip</label>
					                <input class="form-control"  name="zipFile"   type="file" required />
			                   </div>
			             </div>
			              <div class='col-md-4'>
			                    <div class="form-group" style="margin-top:20px">
                                                 <input type='hidden' value='<?php echo $products; ?>' name='products'>
<input type='hidden' value='<?php echo $products; ?>' name='productCategoty'>

			                         <button name="save_productvariation" class="btn btn-primary" type="submit">
			                             Save
			                         </button>
			                     </div>
			              </div>
			           </form>
			          </div>
			      <?php if(sizeof($productlists)>0){?>
			          <table class="table" id="datatableId">
			            <thead>
			              <tr>
			              	<th>Release Date</th>
			                <th>Revision Number</th>
			                <th>Required Editor</th>
			                <th>Status</th>
			                <th>Action</th>
			              </tr>
			            </thead>
			            <tbody>

			              
			              <?php foreach($productlists as $row){?>
			              <tr id="row_<?php echo $row->id;?>">
			                  <td><?php echo $row->reDate;?></td>
                                          <td><?php echo $row->RevisionNumber;?></td>
                                          <td><?php echo $row->RequiredEditor; ?></td>                
                                          <td id="status_<?php echo $row->id;?>"><?php if($row->status==1){?> <a onclick="active_row(6,<?php echo $row->id;?>,1)" class="btn btn-success btn-sm"><i class="fa  fa-thumbs-o-up"></i> Active</a><?php }else{ ?><a onclick="active_row(6,<?php echo $row->id;?>,0)" class="btn btn-danger btn-sm"><i class="fa  fa-thumbs-o-down"></i> In-active</a><?php } ?></td>
                                          <td>
                                          <a onclick="delete_row(6,<?php echo $row->id;?>)" class="btn btn-danger btn-sm"><i class="fa   fa-trash"></i></a></td>
			              </tr>
			              <?php } ?>
			            </tbody>
			          </table>
			          <?php }else{echo "<h2 style='text-align:center;'>Opps! No Data Found!</h2>";} ?>
			  </div>
			  <div id="menu1" class="tab-pane fade">
                             <div class='col-md-12 well'>
			           <form data-toggle="validator" role="form" method="post" enctype="multipart/form-data">
			             <div class='col-md-4'>
			               <div class="form-group">
			               <label>Release Date</label>
			                <input class="form-control" data-error="Please enter date" id="reDate" name="reDate" value="<?php echo set_value('reDate');?>"   type="date" required />
			                    <div class="help-block with-errors"><?php echo form_error('reDate');?></div>
			              </div>
			             </div>
			             <div class='col-md-4'>
			               <div class="form-group">
			               <label>Revision Number</label>
			                <input class="form-control" data-minlength="2" data-error="Must enter minimum of 2 characters" id="RevisionNumber" name="RevisionNumber" value="<?php echo set_value('RevisionNumber');?>" placeholder="Revision Number"  type="text" required />
			                    <div class="help-block with-errors"><?php echo form_error('RevisionNumber');?></div>
			                   </div>
			             </div>
			             <div class='col-md-4'>
			                   <div class="form-group">
			                <label>Part Number</label>
			                <input class="form-control" data-minlength="2" data-error="Must enter minimum of 2 characters" id="PartNumber" name="PartNumber" value="<?php echo set_value('PartNumber');?>" placeholder="Part Number"  type="text" required />
			                    <div class="help-block with-errors"><?php echo form_error('PartNumber');?></div>
			                 </div>
			             </div>
			             <div class='col-md-4'>
			                   <div class="form-group">
					                <label>Release Notes</label>
					                <input class="form-control"  name="ReleaseNotes"   type="file" required />
			                   </div>
			             </div>
			             
			              <div class='col-md-4'>
			                    <div class="form-group" style="margin-top:20px">
                                                 <input type='hidden' value='<?php echo $products; ?>' name='products'>
<input type='hidden' value='<?php echo $products; ?>' name='productCategoty'>

			                         <button name="save_productsoftware" class="btn btn-primary" type="submit">
			                             Save
			                         </button>
			                     </div>
			              </div>
			           </form>
			          </div>
			    <?php if(sizeof($productsoftware)>0){?>
			          <table class="table" id="datatableId">
			            <thead>
			              <tr>
			              	<th>Release Date</th>
			                <th>Revision Number</th>
			                <th>Part Number</th>
			                <th>Release Notes</th>
			                <th>Status</th>
			                <th>Action</th>
			              </tr>
			            </thead>
			            <tbody>

			              
			              <?php foreach($productsoftware as $row){?>
			              <tr id="row_<?php echo $row->id;?>">
			                 <td><?php echo $row->reDate;?></td>
                              <td><?php echo $row->RevisionNumber;?></td>
                              <td><?php echo $row->PartNumber; ?></td><td id="status_<?php echo $row->id;?>"><?php if($row->status==1){?> <a onclick="active_row(7,<?php echo $row->id;?>,1)" class="btn btn-success btn-sm"><i class="fa  fa-thumbs-o-up"></i> Active</a><?php }else{ ?><a onclick="active_row(7,<?php echo $row->id;?>,0)" class="btn btn-danger btn-sm"><i class="fa  fa-thumbs-o-down"></i> In-active</a><?php } ?></td>
                              <td>
                                          <a onclick="delete_row(7,<?php echo $row->id;?>)" class="btn btn-danger btn-sm"><i class="fa   fa-trash"></i></a></td>
			              </tr>
			              <?php } ?>
			            </tbody>
			          </table>
			          <?php }else{echo "<h2 style='text-align:center;'>Opps! No Data Found!</h2>";} ?>
			  </div>
			  <div id="menu2" class="tab-pane fade">
			  	      <div class='col-md-12 well'>
			           <form data-toggle="validator" role="form" method="post" enctype="multipart/form-data">
			             <div class='col-md-3'>
			               <div class="form-group">
			               <label>Type Manual</label>
			                <input class="form-control" data-error="Please enter value" id="Typemanual" name="Typemanual" value="<?php echo set_value('Typemanual');?>"   type="text" required />
			                    <div class="help-block with-errors"><?php echo form_error('Typemanual');?></div>
			              </div>
			             </div>
			             <div class='col-md-3'>
			               <div class="form-group">
			               <label>Revision Number</label>
			                <input class="form-control" data-minlength="2" data-error="Must enter minimum of 2 characters" id="Description" name="Description" value="<?php echo set_value('Description');?>" placeholder="Revision Number"  type="text" required />
			                    <div class="help-block with-errors"><?php echo form_error('Description');?></div>
			                   </div>
			             </div>
			             <div class='col-md-3'>
			                   <div class="form-group">
					                <label>File</label>
					                <input class="form-control"  name="File"   type="file" required />
			                   </div>
			             </div>
			             
			              <div class='col-md-3'>
			                    <div class="form-group" style="margin-top:20px">
                                                 <input type='hidden' value='<?php echo $products; ?>' name='products'>
<input type='hidden' value='<?php echo $products; ?>' name='productCategoty'>

			                         <button name="save_productmenual" class="btn btn-primary" type="submit">
			                             Save
			                         </button>
			                     </div>
			              </div>
			           </form>
			          </div>
			    <?php if(sizeof($productmenul)>0){?>
			          <table class="table" id="datatableId">
			            <thead>
			              <tr>
			              	<th>Type</th>
			                <th>Description</th>
			                <th>Status</th>
			                <th>Action</th>
			              </tr>
			            </thead>
			            <tbody>

			              
			              <?php foreach($productmenul as $row){?>
			              <tr id="row_<?php echo $row->id;?>">
			                  <td><?php echo $row->Typemanual;?></td>
                              <td><?php echo $row->Description;?></td>
                              <td id="status_<?php echo $row->id;?>"><?php if($row->status==1){?> <a onclick="active_row(8,<?php echo $row->id;?>,1)" class="btn btn-success btn-sm"><i class="fa  fa-thumbs-o-up"></i> Active</a><?php }else{ ?><a onclick="active_row(8,<?php echo $row->id;?>,0)" class="btn btn-danger btn-sm"><i class="fa  fa-thumbs-o-down"></i> In-active</a><?php } ?></td>
                              <td>
                              	<a onclick="delete_row(8,<?php echo $row->id;?>)" class="btn btn-danger btn-sm"><i class="fa   fa-trash"></i></a></td>
			              </tr>
			              <?php } ?>
			            </tbody>
			          </table>
			          <?php }else{echo "<h2 style='text-align:center;'>Opps! No Data Found!</h2>";} ?>
			  </div>
                    <div id="menu3" class="tab-pane fade">
                    	<div class='col-md-12 well'>
			           <form data-toggle="validator" role="form" method="post" enctype="multipart/form-data">
			             <div class='col-md-3'>
			               <div class="form-group">
			               <label>Operating System</label>
			                <input class="form-control" data-error="Please enter value" id="OperatingSystem" name="OperatingSystem" value="<?php echo set_value('OperatingSystem');?>"   type="text" required />
			                    <div class="help-block with-errors"><?php echo form_error('OperatingSystem');?></div>
			              </div>
			             </div>
			             <div class='col-md-3'>
			               <div class="form-group">
			               <label>Notes</label>
			                <input class="form-control" data-minlength="2" data-error="Must enter minimum of 2 characters" id="Description" name="Notes" value="<?php echo set_value('Notes');?>" placeholder="Revision Number"  type="text" required />
			                    <div class="help-block with-errors"><?php echo form_error('Notes');?></div>
			                   </div>
			             </div>
			             <div class='col-md-3'>
			                   <div class="form-group">
					                <label>File</label>
					                <input class="form-control"  name="file"   type="file" required />
			                   </div>
			             </div>
			             
			              <div class='col-md-3'>
			                    <div class="form-group" style="margin-top:20px">
                                                 <input type='hidden' value='<?php echo $products; ?>' name='products'>
<input type='hidden' value='<?php echo $products; ?>' name='productCategoty'>

			                         <button name="save_productdriver" class="btn btn-primary" type="submit">
			                             Save
			                         </button>
			                     </div>
			              </div>
			           </form>
			          </div>
			    <?php if(sizeof($productdriver)>0){?>
			          <table class="table" id="datatableId">
			            <thead>
			              <tr>
			              	<th>Operating System</th>
			                <th>Notes</th>
			                <th>Status</th>
			                <th>Action</th>
			              </tr>
			            </thead>
			            <tbody>

			              
			              <?php foreach($productdriver as $row){?>
			              <tr id="row_<?php echo $row->id;?>">
			                 <td><?php echo $row->OperatingSystem;?></td>
                              <td><?php echo $row->Notes;?></td>
                              <td id="status_<?php echo $row->id;?>"><?php if($row->status==1){?> <a onclick="active_row(9,<?php echo $row->id;?>,1)" class="btn btn-success btn-sm"><i class="fa  fa-thumbs-o-up"></i> Active</a><?php }else{ ?><a onclick="active_row(9,<?php echo $row->id;?>,0)" class="btn btn-danger btn-sm"><i class="fa  fa-thumbs-o-down"></i> In-active</a><?php } ?></td>
                              <td>
                              	 <a onclick="delete_row(9,<?php echo $row->id;?>)" class="btn btn-danger btn-sm"><i class="fa   fa-trash"></i></a></td>
			              </tr>
			              <?php } ?>
			            </tbody>
			          </table>
			          <?php }else{echo "<h2 style='text-align:center;'>Opps! No Data Found!</h2>";} ?>
			  </div>
			</div>

         
        </div>
        <!-- /.box-body -->
        <div class="box-footer">
        
        </div>
        <!-- /.box-footer-->
      </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->



 