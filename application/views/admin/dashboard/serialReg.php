<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>Registered Serial Numbers</h1>
    </section>

    <!-- Main content -->
    <section class="content">

        <!-- Default box -->
        <div class="box">
            <?php if ($this->session->flashdata("success")){?>
                <div class="text-center alert alert-success alert-dismissable">
                    <?= $this->session->flashdata("success")?>
                </div>
            <?php }?>
            <?php if ($this->session->flashdata("error")){?>
                <div class="text-center alert alert-danger alert-dismissable">
                    <?= $this->session->flashdata("error")?>
                </div>
            <?php }?>

            <div class="as-table">
                <div class="portlet light no-border-top as-box-border">
                    <div class="portlet-body">
                        <table class="table table-striped table-bordered table-hover dt-responsive" id="download" data-name="">
                            <thead>
                            <tr>
                                <th >Part Number</th>
                                <th >Serial Number</th>
                                <th >Company</th>
                                <th >Name</th>
                                <th >Email</th>
                                <th >Date</th>
                            </tr>
                            </thead>
                            <tbody>

                            <?php if(isset($user) && $user!='') {
                                foreach($user as $row){
                                    ?>
                                    <tr id="row_<?php echo $row->ID;?>">
                                        <td><?php echo $row->PartNumber;?> </td>
                                        <td><?php echo $row->SerialNumber;?></td>
                                        <td><?php echo $row->Company;?></td>
                                        <td><?php echo $row->FirstName.' '.$row->LastName;?></td>
                                        <td><?php echo $row->Email;?></td>
                                        <td><?php echo $row->Date;?></td>
                                    </tr>
                                <?php } }else{ ?>
                                <tr><td colspan="8"><h3 class="text-center">No Data Found</h3></td></tr>
                            <?php } ?>

                            </tbody>
                            <tfoot>
                            <tr>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                            </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div><!-- /.box-body -->
            <div class="box-footer">

            </div>
            <!-- /.box-footer-->
        </div>
        <!-- /.box -->


    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->



