<?php
/**
 * Created by PhpStorm.
 * User: Rana
 * Date: 09-Oct-18
 * Time: 7:12 PM
 */?>
<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <?php if($this->type==3){$title="CalFire";}else if($this->type==4){$title="USFS";}else {$title="Florida";} ?>
        <h1><?=$title?> Repair Profiles List</h1>
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title"></h3>

          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                    title="Collapse">
              <i class="fa fa-minus"></i></button>
            <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
              <i class="fa fa-times"></i></button>
          </div>
        </div>

          <?php if ($this->session->flashdata("success")){?>
              <div class="text-center alert alert-success alert-dismissable">
                  <?= $this->session->flashdata("success")?>
              </div>
          <?php }?>
          <?php if ($this->session->flashdata("error")){?>
              <div class="text-center alert alert-danger alert-dismissable">
                  <?= $this->session->flashdata("error")?>
              </div>
          <?php }?>

          <div class="as-table">
              <table class="table table-striped" id="datatableId">
                  <thead>
                  <tr>
                      <th scope="col">Company</th>
                      <th scope="col">Serial Number</th>
                      <th scope="col">Requested</th>
                      <th scope="col">PO Status</th>
                      <th scope="col">Customer Tracking</th>
                      <th scope="col">Recieved Date</th>
                      <th scope="col">Edit</th>
                      <th>Delete</th>
                  </tr>
                  </thead>
                  <tbody>

                  <?php if (isset($list) && $list!=400) {
                      foreach ($list as $key ) {
                          ?>
                          <tr id="row_<?php echo $key->id;?>">
                              <td><?php echo $key->Company ?></td>
                              <td><?php echo $key->Serial ?></td>
                              <td><?php echo $key->requested ?></td>
                              <td><?php echo $key->PO_Status ?></td>
                              <td><?php echo $key->CustomerTrackingNumber ?></td>
                              <td><?php echo $key->date ?></td>
                              <td><a href="<?php echo base_url('/admin/dashboard/edit/').$key->id.'/'.$this->type ?>"class="btn btn-warning btn-sm"><i class="fa fa-cog"></i></a></td>
                              <td><a onclick="delete_row1(<?=$this->type?>,<?php echo $key->id;?>)" class="btn btn-danger btn-sm"><i class="fa   fa-trash"></i></a></td>
                          </tr>
                      <?php } }else{ ?>
                      <tr><td colspan="8"><h3 class="text-center">No Data Found</h3></td></tr>
                  <?php } ?>
                  </tbody>
              </table>

              <!-- Modal HTML -->
              <div id="myModal" class="modal fade">
                  <div class="modal-dialog modal-confirm">
                      <div class="modal-content">
                          <div class="modal-header">
                              <div class="icon-box">
                                  <i class="material-icons">&#xE5CD;</i>
                              </div>
                              <h4 class="modal-title">Are you sure?</h4>
                              <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                          </div>
                          <div class="modal-body">
                              <p>Do you really want to delete the record? This process cannot be undone.</p>
                          </div>
                          <div class="modal-footer">
                              <a type="button"  class="btn btn-info" data-dismiss="modal">Cancel</a>
                              <a id="form_delete_id" href="<?=base_url('f_delete/')?>" type="button" class="btn btn-danger">Delete</a>
                          </div>
                      </div>
                  </div>
              </div>


          </div><!-- /.box-body -->
<div class="box-footer">

</div>
<!-- /.box-footer-->
</div>
<!-- /.box -->


</section>
<!-- /.content -->
</div>
<!-- /.content-wrapper -->



