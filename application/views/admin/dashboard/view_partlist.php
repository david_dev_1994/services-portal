 <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        View/Add Part List
       
      </h1>
     
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title"></h3>

          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                    title="Collapse">
              <i class="fa fa-minus"></i></button>
            <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
              <i class="fa fa-times"></i></button>
          </div>
        </div>
        <div class="box-body">
          <div class='col-md-12 well'>
           <form data-toggle="validator" role="form" method="post" enctype="multipart/form-data">
             <div class='col-md-3'>
               <div class="form-group">
               <label>Ref No</label>
                <input class="form-control" data-minlength="2" data-error="Must enter minimum of 2 characters" id="RefNo" name="RefNo" value="<?php echo set_value('RefNo');?>" placeholder="Ref No"  type="text" required />
                    <div class="help-block with-errors"><?php echo form_error('RefNo');?></div>
              </div>
             </div>
             <div class='col-md-3'>
               <div class="form-group">
               <label>Part No</label>
                <input class="form-control" data-minlength="2" data-error="Must enter minimum of 2 characters" id="PartNo" name="PartNo" value="<?php echo set_value('PartNo');?>" placeholder="Part No"  type="text" required />
                    <div class="help-block with-errors"><?php echo form_error('PartNo');?></div>
                   </div>
             </div>
             <div class='col-md-3'>
                   <div class="form-group">
                <label>Descriptions</label>
                <input class="form-control" data-minlength="2" data-error="Must enter minimum of 2 characters" id="Descriptions" name="Descriptions" value="<?php echo set_value('Descriptions');?>" placeholder="Descriptions"  type="text" required />
                    <div class="help-block with-errors"><?php echo form_error('Descriptions');?></div>
                 </div>
             </div>
              <div class='col-md-3'>
                    <div class="form-group" style="margin-top:20px">
                         <button name="save_partlist" class="btn btn-primary" type="submit">
                             Save
                         </button>
                     </div>
              </div>
           </form>
          </div>
          <?php if(sizeof($partslists)>0){?>
          <table class="table" id="datatableId">
            <thead>
              <tr>
                <th>Ref No</th>
                <th>Part No.</th>
                <th>Descriptions</th>
                <th>Status</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody>
<?php //print_r($partslist);  ?>
              
              <?php foreach($partslists as $row){ ?>
              <tr id="row_<?php echo $row->id;?>">
                <td><?php echo $row->RefNo;?></td>
                <td><?php echo $row->PartNo;?></td>
                <td><?php echo $row->Descriptions; ?></td>                
                <td id="status_<?php echo $row->id;?>"><?php if($row->status==1){?> <a onclick="active_row(5,<?php echo $row->id;?>,1)" class="btn btn-success btn-sm"><i class="fa  fa-thumbs-o-up"></i> Active</a><?php }else{ ?><a onclick="active_row(5,<?php echo $row->id;?>,0)" class="btn btn-danger btn-sm"><i class="fa  fa-thumbs-o-down"></i> In-active</a><?php } ?></td>
                <td>
                  <a onclick="delete_row(5,<?php echo $row->id;?>)" class="btn btn-danger btn-sm"><i class="fa   fa-trash"></i></a></td>
              </tr>
              <?php } ?>
            </tbody>
          </table>
          <?php }else{echo "<h2 style='text-align:center;'>Opps! No Data Found!</h2>";} ?>
        </div>
        <!-- /.box-body -->
        <div class="box-footer">
        
        </div>
        <!-- /.box-footer-->
      </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->



