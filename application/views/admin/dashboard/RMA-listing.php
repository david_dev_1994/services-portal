<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1> RMA form List</h1>
    </section>

    <!-- Main content -->
    <section class="content">

        <!-- Default box -->
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title"></h3>

                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                            title="Collapse">
                        <i class="fa fa-minus"></i></button>
                    <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
                        <i class="fa fa-times"></i></button>
                </div>
            </div>

            <?php if ($this->session->flashdata("success")){?>
                <div class="text-center alert alert-success alert-dismissable">
                    <?= $this->session->flashdata("success")?>
                </div>
            <?php }?>
            <?php if ($this->session->flashdata("error")){?>
                <div class="text-center alert alert-danger alert-dismissable">
                    <?= $this->session->flashdata("error")?>
                </div>
            <?php }?>

            <div class="as-table">
                <table class="table table-striped" id="datatableId">
                    <thead>
                    <tr>
                        <th scope="col">Name</th>
                        <th scope="col">Requested</th>
                        <th scope="col">Email</th>
                        <th scope="col">Request</th>
                        <th scope="col">Created Date</th>
                        <th scope="col">View</th>
                        <th scope="col">Action</th>
                    </tr>
                    </thead>
                    <tbody>

                    <?php if(isset($user) && $user!=400) {
                        foreach($user as $row){
                            ?>
                            <tr id="row_<?php echo $row->id;?>">
                                <td><?php echo $row->name;?> </td>
                                <td><?php echo $row->requested;?></td>
                                <td><?php echo $row->email;?></td>
                                <td><?php echo $row->optradio;?></td>
                                <td><?php echo $row->date;?></td>
                                <td><a href="<?= base_url('admin/dashboard/rma_form_view/').$row->id?>" class="btn btn-warning btn-sm"><i class="fa   fa-eye"></i></a></td>
                                <td><a onclick="delete_row(19,<?php echo $row->id;?>)" class="btn btn-danger btn-sm"><i class="fa   fa-trash"></i></a></td>
                            </tr>
                        <?php } }else{ ?>
                        <tr><td colspan="8"><h3 class="text-center">No Data Found</h3></td></tr>
                    <?php } ?>

                    </tbody>
                    <tfoot>
                    <tr>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                    </tr>
                    </tfoot>
                </table>

                <!-- Modal HTML -->
                <div id="myModal" class="modal fade">
                    <div class="modal-dialog modal-confirm">
                        <div class="modal-content">
                            <div class="modal-header">
                                <div class="icon-box">
                                    <i class="material-icons">&#xE5CD;</i>
                                </div>
                                <h4 class="modal-title">Are you sure?</h4>
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            </div>
                            <div class="modal-body">
                                <p>Do you really want to delete the record? This process cannot be undone.</p>
                            </div>
                            <div class="modal-footer">
                                <a type="button"  class="btn btn-info" data-dismiss="modal">Cancel</a>
                                <a id="form_delete_id" href="<?=base_url('f_delete/')?>" type="button" class="btn btn-danger">Delete</a>
                            </div>
                        </div>
                    </div>
                </div>


            </div><!-- /.box-body -->
            <div class="box-footer">

            </div>
            <!-- /.box-footer-->
        </div>
        <!-- /.box -->


    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->



