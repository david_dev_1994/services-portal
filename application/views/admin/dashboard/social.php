
 <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Social Network
       
      </h1>
     
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title"></h3>

          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                    title="Collapse">
              <i class="fa fa-minus"></i></button>
            <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
              <i class="fa fa-times"></i></button>
          </div>
        </div>
        <div class="box-body">
          <div class="col-md-6 col-md-offset-3">
            <?php $notification=$this->session->flashdata('notification');
            if($notification){ ?>            
            <div id="snackbar"><?php echo $notification;?></div>
            <script type="text/javascript">$(document).ready(function(){
              notificationFun();
            });</script>
              
              <?php }?>
              <?php echo validation_errors();?>
              <form data-toggle="validator" role="form" method="post">
                  <div class="form-group">
                    <label class="control-label" for="fb_link"><i class="fa fa-facebook"></i> Facebook</label>

                    <input class="form-control" id="fb_link" name="fb_link" value="<?php if($user_about_data){echo $user_about_data->fb_link;}else{ echo set_value('fb_link');}?>" placeholder="Facebook"  type="url" />
                    <div class="help-block with-errors"><?php echo form_error('fb_link');?></div>
                  </div>
                  <div class="form-group">
                    <label class="control-label" for="twitter_link"><i class="fa fa-twitter"></i> Twitter</label>

                    <input class="form-control" id="twitter_link" name="twitter_link" value="<?php if($user_about_data){echo $user_about_data->twitter_link;}else{ echo set_value('twitter_link');}?>" placeholder="Twitter"  type="url" />
                    <div class="help-block with-errors"><?php echo form_error('twitter_link');?></div>
                  </div>
                  <div class="form-group">
                    <label class="control-label" for="linkedin_link"><i class="fa fa-linkedin"></i> Linkedin</label>

                    <input class="form-control" id="linkedin_link" name="linkedin_link" value="<?php if($user_about_data){echo $user_about_data->linkedin_link;}else{ echo set_value('linkedin_link');}?>" placeholder="Linkedin"  type="url" />
                    <div class="help-block with-errors"><?php echo form_error('linkedin_link');?></div>
                  </div>
                 
                  <div class="form-group">
                         <button name="save_social" class="btn btn-primary" type="submit">
                             Save
                         </button>
                     </div>
              </form>
            </div>

        </div>
        <!-- /.box-body -->
        <div class="box-footer">
        
        </div>
        <!-- /.box-footer-->
      </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->



