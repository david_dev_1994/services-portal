
 <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Add New Experience
       
      </h1>
     
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title"></h3>

          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                    title="Collapse">
              <i class="fa fa-minus"></i></button>
            <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
              <i class="fa fa-times"></i></button>
          </div>
        </div>
        <div class="box-body">
          <div class="col-md-6 col-md-offset-3">
            <?php $notification=$this->session->flashdata('notification');
            if($notification){ ?>            
            <div id="snackbar"><?php echo $notification;?></div>
            <script type="text/javascript">$(document).ready(function(){
              notificationFun();
            });</script>
              
              <?php }?>
              <?php echo validation_errors();?>
              <form data-toggle="validator" role="form" method="post">
                  <div class="form-group">
                    <label class="control-label" for="address">Job Title</label>

                    <input class="form-control" data-minlength="5" data-error="must enter minimum of 5 characters" id="title" name="title" value="<?php echo set_value('title');?>" placeholder="Job Title"  type="text" required />
                    <div class="help-block with-errors"><?php echo form_error('title');?></div>
                  </div>
                  <div class="form-group">
                    <label class="control-label" for="company">Company/Institute</label>
                    <input class="form-control"  id="company" name="company" value="<?php echo set_value('company');?>" placeholder="Company"  type="text" required />
                    <div class="help-block with-errors"><?php echo form_error('company');?></div>
                  </div>
                  <div class="form-group">
                    <label class="control-label" for="start_date">Start Date</label>
                    <input class="form-control"  id="start_date" name="start_date" value="<?php echo set_value('start_date');?>"   type="date" required />
                    <div class="help-block with-errors"><?php echo form_error('start_date');?></div>
                  </div>
                   <div class="form-group">
                    <label class="control-label" for="end_date">End Date</label>
                    <input class="form-control"  id="end_date" name="end_date" value="<?php echo set_value('end_date');?>"   type="date"  />
                    <div class="help-block with-errors"><?php echo form_error('end_date');?></div>
                  </div>
                   
                   <div class="form-group">
                       <label for="editor1" class="control-label">Description</label>
                       <div class="form-group">
                         <textarea id="editor1" name="description"><?php echo set_value('description');?></textarea>
                       </div>
                     </div>
                  <div class="form-group">
                         <button name="save_experience" class="btn btn-primary" type="submit">
                             Save
                         </button>
                     </div>
              </form>
            </div>

        </div>
        <!-- /.box-body -->
        <div class="box-footer">
        
        </div>
        <!-- /.box-footer-->
      </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->


