 <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        view Parts
       
      </h1>
     
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title"></h3>

          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                    title="Collapse">
              <i class="fa fa-minus"></i></button>
            <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
              <i class="fa fa-times"></i></button>
          </div>
        </div>
        <div class="box-body">
          <?php if(sizeof($user_about_data)>0){?>
          <table class="table" id="datatableId">
            <thead>
              <tr>
                <th>Id</th>
                <th>Title</th>
                <th>image</th>
                <th>Type</th>
                <th>Status</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody>

              
              <?php foreach($user_about_data as $row){?>
              <tr id="row_<?php echo $row->id;?>">
                <td><?php echo $row->id;?></td>
                <td><?php echo $row->title;?></td>
                <td><img src="<?php echo base_url();?>/assets/images/<?php echo $row->image;?>" class='img img-responsive' style='width:50px'></td>
                <td><?php echo $row->typetitle;?></td>  
                <td id="status_<?php echo $row->id;?>"><?php if($row->status==1){?> <a onclick="active_row(4,<?php echo $row->id;?>,1)" class="btn btn-success btn-sm"><i class="fa  fa-thumbs-o-up"></i> Active</a><?php }else{ ?><a onclick="active_row(4,<?php echo $row->id;?>,0)" class="btn btn-danger btn-sm"><i class="fa  fa-thumbs-o-down"></i> In-active</a><?php } ?></td>
                <td>
                   <a href="<?php echo base_url();?>/admin/dashboard/view_part_list/<?php echo $row->id;?>" class="btn btn-success btn-sm"><i class="fa fa-plus"></i></a>
                   
                  <a onclick="delete_row(4,<?php echo $row->id;?>)" class="btn btn-danger btn-sm"><i class="fa   fa-trash"></i></a></td>
                  
              </tr>
              <?php } ?>
            </tbody>
          </table>
          <?php }else{echo "<h2 style='text-align:center;'>Opps! No Data Found!</h2>";} ?>
        </div>
        <!-- /.box-body -->
        <div class="box-footer">
        
        </div>
        <!-- /.box-footer-->
      </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->



