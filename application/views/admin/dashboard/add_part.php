 <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Add Part
      </h1>
     
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title"></h3>

          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                    title="Collapse">
              <i class="fa fa-minus"></i></button>
            <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
              <i class="fa fa-times"></i></button>
          </div>
        </div>
        <div class="box-body">
          <div class="col-md-6 col-md-offset-3">
            <?php $notification=$this->session->flashdata('notification');
            if($notification){ ?>            
            <div id="snackbar"><?php echo $notification;?></div>
            <script type="text/javascript">$(document).ready(function(){
              notificationFun();
            });</script>
              
              <?php }?>
              <?php echo validation_errors();?>
              <form data-toggle="validator" role="form" method="post" enctype="multipart/form-data">
                  <div class="form-group">
                    <label class="control-label" for="institute">Title</label>

                    <input class="form-control" data-minlength="2" data-error="Must enter minimum of 2 characters" id="institute" name="title" value="<?php echo set_value('title');?>" placeholder="title"  type="text" required />
                    <div class="help-block with-errors"><?php echo form_error('title');?></div>
                  </div>
                  <div class="form-group">
                    <label class="control-label" for="start_date">Type</label>
                      <select class="form-control" name="Type">
                        <?php if(isset($partTypes)){
                        foreach($partTypes as $row){
                        ?>
                        <option value="<?php echo $row->id; ?>"><?php echo $row->typetitle; ?></option>
                       
                        <?php } } ?>
                      </select>
                  </div>

                   <div class="form-group">
                    <label class="control-label" for="start_date">Image</label>
                    <input type="file" id="user_image" name="user_image" accept="image/*" required >
                    <div class="help-block with-errors"><?php echo form_error('Release_Date');?></div>
                  </div>
                   
                  <div class="form-group">
                         <button name="save_part" class="btn btn-primary" type="submit">
                             Save
                         </button>
                     </div>
              </form>
            </div>

        </div>
        <!-- /.box-body -->
        <div class="box-footer">
        
        </div>
        <!-- /.box-footer-->
      </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->


