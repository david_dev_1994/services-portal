 <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Add Prodduct
       
      </h1>
     
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title"></h3>

          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                    title="Collapse">
              <i class="fa fa-minus"></i></button>
            <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
              <i class="fa fa-times"></i></button>
          </div>
        </div>
        <div class="box-body">
          <div class="col-md-6 col-md-offset-3">
            <?php $notification=$this->session->flashdata('notification');
            if($notification){ ?>            
            <div id="snackbar"><?php echo $notification;?></div>
            <script type="text/javascript">$(document).ready(function(){
              notificationFun();
            });</script>
              
              <?php }?>
              <?php echo validation_errors();?>
              <form data-toggle="validator" role="form" method="post" >
                  <div class="form-group">
                    <label class="control-label" for="institute">Portables</label>

                    <input class="form-control" data-minlength="2" data-error="Must enter minimum of 2 characters" id="Portables" name="Portables" value="<?php echo set_value('Portables');?>"   type="text" required />
                    <div class="help-block with-errors"><?php echo form_error('Portables');?></div>
                  </div>
                  <div class="form-group">
                    <label class="control-label" for="course">Mobiles</label>
                    <input class="form-control"  id="Mobiles" name="Mobiles" data-minlength="2" data-error="Must enter minimum of 2 characters" value="<?php echo set_value('Mobiles');?>" placeholder="Mobiles"  type="text" />
                    <div class="help-block with-errors"><?php echo form_error('Mobiles');?></div>
                  </div>
                  
                  <div class="form-group">
                    <label class="control-label" for="start_date">Base Stations</label>
                    <input class="form-control"  id="BaseStations" name="BaseStations" value="<?php echo set_value('BaseStations');?>"   type="text" />
                    <div class="help-block with-errors"><?php echo form_error('BaseStations');?></div>
                  </div>
                  <div class="form-group">
                    <label class="control-label" for="start_date">Repeaters</label>
                    <input class="form-control"  id="Repeaters" name="Repeaters" value="<?php echo set_value('Repeaters');?>"   type="text" />
                    <div class="help-block with-errors"><?php echo form_error('Repeaters');?></div>
                  </div>
                  <div class="form-group">
                    <label class="control-label" for="start_date">Type</label>
                      <select class="form-control" name="Type">
                        <option value="1">KAA0732 > KAA0733</option>
                        <option value="2">KAA0733</option>
                        <option value="3">BK Radio Analog Radios</option>
                        <option value="4">BK Technology Brand Analog Radios</option>
                      </select>
                  </div>
                  
                  <div class="form-group">
                         <button name="save_productDetails" class="btn btn-primary" type="submit">
                             Save
                         </button>
                     </div>
              </form>
            </div>

        </div>
        <!-- /.box-body -->
        <div class="box-footer">
        
        </div>
        <!-- /.box-footer-->
      </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->


