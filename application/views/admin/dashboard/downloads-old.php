<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>Downloads</h1>
    </section>

    <!-- Main content -->
    <section class="content">

        <!-- Default box -->
        <div class="box">
            <?php if ($this->session->flashdata("success")){?>
                <div class="text-center alert alert-success alert-dismissable">
                    <?= $this->session->flashdata("success")?>
                </div>
            <?php }?>
            <?php if ($this->session->flashdata("error")){?>
                <div class="text-center alert alert-danger alert-dismissable">
                    <?= $this->session->flashdata("error")?>
                </div>
            <?php }?>

            <div class="as-table">
                <table class="table table-striped" id="download">
                    <thead>
                    <tr>
                        <th scope="col">Name</th>
                        <th scope="col">Company</th>
                        <th scope="col">Number of Radios</th>
                        <th scope="col">Email</th>
                        <th scope="col">Phone Number</th>
                        <th scope="col">Revision Number</th>
                        <th scope="col">Required Editior</th>
                        <th scope="col">Part Number</th>
                        <th scope="col">Category</th>
                        <th scope="col">Serial Key</th>
                        <th scope="col">Date</th>
                        <!--<th scope="col">Total Downloads</th>-->
<!--                        <th scope="col">Number of Downloads</th>-->
                        <th scope="col">File</th>
                    </tr>
                    </thead>
                    <tbody>

                    <?php if(isset($user) && $user!='') {
//                        echo '<pre>';print_r($user);exit;
                        foreach($user as $row){
                            ?>
                            <tr id="row_<?php echo $row->id;?>">
                                <td><?php echo $row->name;?> </td>
                                <td><?php echo $row->company_name;?></td>
                                <td><?php echo $row->radio_number;?></td>
                                <td><?php echo $row->email;?></td>
                                <td><?php echo $row->pnum;?></td>
                                <td><?php echo $row->revision_number;?></td>
                                <td><?php echo $row->editor;?></td>
                                <td><?php echo $row->part_number;?></td>
                                <td><?php echo $row->ware;?></td>
                                <td><?php echo $row->serial_number;?></td>
                                <td><?php echo date('m-d-y',$row->created_at);?></td>
                                <!--<td><?php// echo $row->total_download;?></td>-->
                                <td><?php echo $row->file_name;?><!--</td>
                            </tr>
                        <?php } }else{ ?>
                        <tr><td colspan="8"><h3 class="text-center">No Data Found</h3></td></tr>
                    <?php } ?>

                    </tbody>
                    <tfoot>
<!--                    <tr>-->
<!--                        <th></th>-->
<!--                        <th></th>-->
<!--                        <th></th>-->
<!--                        <th></th>-->
<!--                        <th></th>-->
<!--                        <th></th>-->
<!--                        <th></th>-->
<!--                        <th></th>-->
<!--                    </tr>-->
                    </tfoot>
                </table>

                <!-- Modal HTML -->
                <div id="myModal" class="modal fade">
                    <div class="modal-dialog modal-confirm">
                        <div class="modal-content">
                            <div class="modal-header">
                                <div class="icon-box">
                                    <i class="material-icons">&#xE5CD;</i>
                                </div>
                                <h4 class="modal-title">Are you sure?</h4>
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            </div>
                            <div class="modal-body">
                                <p>Do you really want to delete the record? This process cannot be undone.</p>
                            </div>
                            <div class="modal-footer">
                                <a type="button"  class="btn btn-info" data-dismiss="modal">Cancel</a>
                                <a id="form_delete_id" href="<?=base_url('f_delete/')?>" type="button" class="btn btn-danger">Delete</a>
                            </div>
                        </div>
                    </div>
                </div>


            </div><!-- /.box-body -->
            <div class="box-footer">

            </div>
            <!-- /.box-footer-->
        </div>
        <!-- /.box -->


    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->



