<?php
/**
 * Created by PhpStorm.
 * User: Rana
 * Date: 09-Oct-18
 * Time: 8:25 PM
 */?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="box">
        <section class="content-header">
            <?php if($this->type==3){$title="CalFire";}elseif ($this->type==2){$title="Florida Forestry";}else{$title="US Forest Service";} ?>
            <h1 class="text-center"><?=$title?> Repair Profile</h1>
        </section><br>

        <!-- Main content -->
        <section class="content">
            <?php if(!isset($list1)){?>
                <div class='container-fluid' id="f-service__pricing" style="background-image: black">
                    <div class="form-page" style="padding: 25px;">

                        <?php if (isset($err_msg)){?>
                            <div class=" alert alert-danger alert-dismissable">
                                <?= $err_msg?>
                            </div>
                        <?php }?>
                        <?php if (isset($succ_msg)){?>
                            <div class="text-center alert alert-success alert-dismissable">
                                <?= $succ_msg?>
                            </div>
                        <?php }?>
                        <?php if ($this->session->flashdata("validation_errors")){?>
                            <div class="text-center alert alert-danger alert-dismissable">
                                <?= $this->session->flashdata("validation_errors")?>
                            </div>
                        <?php }?>




                        <form id="cal_fire" name="cal_fire" action="<?=base_url('admin/Dashboard/update/')?>" method="post">
                            <div class="row">
                                <div class="col-lg-4">
                                    <div class="form-group row">
                                        <div class="col-sm-12">
                                            <label for="inputEmail3MD" class="col-auto col-form-label">PO#</label>
                                            <div class="md-form mt-0">
                                                <input name="pu" type="text" required="required"  class="form-control" value="<?php echo $list[0]->pu;?>" id="inputEmail3MD">
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-4">
                                    <div class="form-group row">
                                        <div class="col-sm-12">
                                            <label for="inputEmail3MD" class="col-auto col-form-label">Requested:</label>
                                            <div class="md-form mt-0">
                                                <input name="requested" type="text" value="<?php echo $list[0]->requested; ?>" required="required" autocomplete="off" class="form-control" id="inputEmail3MD">
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-4">
                                    <div class="form-group row">
                                        <div class="col-sm-12">
                                            <label for="inputEmail3MD" class="col-auto col-form-label">Status</label>
                                            <div class="md-form mt-0">
                                                <select required="required" name="PO_Status" class="form-control">
                                                    <option value="<?php echo $list[0]->PO_Status;?> " selected="selected"><?php echo $list[0]->PO_Status;?></option>
                                                    <option value="Submitted">Submitted</option>
                                                    <option value="In_Process">In_Process</option>
                                                    <option value="Shipped">Shipped</option>
                                                    <option value="Approved">Approved</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
<!--                            new insertions-->
                            <div class="row">
                                <div class="col-lg-4">
                                    <div class="form-group row">
                                        <div class="col-sm-12">
                                            <label for="inputEmail3MD" class="col-auto col-form-label">BK Tracking #</label>
                                            <div class="md-form mt-0">
                                                <input name="BK_TN" type="text" value="<?=$list[0]->BK_TN?>" required="required"  class="form-control" id="inputEmail3MD">
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-4">
                                    <div class="form-group row">
                                        <div class="col-sm-12">
                                            <label for="inputEmail3MD" class="col-auto col-form-label">Ship Date:</label>
                                            <div class="md-form mt-0">
                                                <input name="Ship_Date" value="<?=$list[0]->Ship_Date?>" type="date" required="required" autocomplete="off" class="form-control" id="inputEmail3MD">
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-4">
                                    <div class="form-group row">
                                        <div class="col-sm-12">
                                            <label for="inputEmail3MD" class="col-auto col-form-label">Repair Status</label>
                                            <div class="md-form mt-0">
                                                <select required="required" name="Repair_Status" class="form-control">
                                                    <option value="<?php echo $list[0]->Repair_Status;?> " selected="selected"><?php echo $list[0]->Repair_Status;?></option>
                                                    <option value="Review">Review</option>
                                                    <option value="Repairable">Repairable</option>
                                                    <option value="Un-Repairable">Un-Repairable</option>
                                                    <option value="Not Supported">Not Supported</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
<!--                            new insertions ends here-->

                            <div class="row warranty-checkbox">
                                <div class="col-sm-12">
                                    <div class="custom-control custom-checkbox">
                                        <input type="hidden" name="warranyreplace" value="0">
                                        <input type="checkbox" value="1" name="warranyreplace" <?php if($list[0]->warranyreplace==1){ echo "checked";} ?> class="custom-control-input" id="checkbox_1">
                                        <label class="custom-control-label" for="checkbox_1">If this box is checked, this is warranty replacement part request.</label>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="form-group row">
                                        <div class="col-sm-12">
                                            <label for="inputEmail3MD" class="col-auto col-form-label">Company</label>
                                            <div class="md-form mt-0">
                                                <select required="required" name="Company" class="form-control">
                                                    <option value="<?php echo $list[0]->Company;?>" selected="selected"><?php echo $list[0]->Company;?></option>
                                                    <?php if($this->type==3){?>
                                                    <option value="Sacramento Command Center   /   3650 Schriever Ave Mather, CA 95655">
                                                        &nbsp;Sacramento Command Center</option>

                                                    <option value="Academy   /   CAL FIRE Academy, 4501 State Highway 104, Ione, CA 95640">
                                                        &nbsp;Academy</option>

                                                    <option value="Aviation Group   /   CAL FIRE Aviation Management, 5500 Price Ave. McClellan, CA 95652">
                                                        &nbsp;Aviation Group</option>

                                                    <option value="Mobile Equipment   /   CAL FIRE Mobile Equipment, 5950 Chiles Road Davis, CA 95618">
                                                        &nbsp;Mobile Equpment</option>

                                                    <option value="Office of the State Fire Marshal   /   1131 S Street Sacramento, CA 95814">
                                                        &nbsp;Office of the State Fire Marshal</option>

                                                    <option value="Amador-Eldorado Unit (2700)   /   2840 Mt. Danaher Road Camino, CA 95709">
                                                        &nbsp;Amador-Eldorado Unit (2700)</option>

                                                    <option value="Butte Unit (2100)   /   176 Nelson Avenue Oroville, CA 95965">
                                                        &nbsp;Butte Unit (2100)</option>

                                                    <option value="Fresno-Kings Unit (4300)   /   210 So. Academy Avenue Sanger, CA 93657">
                                                        &nbsp;Fresno-Kings (4300)</option>

                                                    <option value="Humboldt-Del Norte Unit (1200)   /   118 S. Fortuna Blvd. Fortuna, CA 95540-2796">
                                                        &nbsp;Humboldt-Del Norte Unit (1200)</option>

                                                    <option value="Lassen-Modoc Unit (2200)   /   697-345 Highway 36 Susanville, CA 96130">
                                                        &nbsp;Lassen-Modoc Unit (2200)</option>

                                                    <option value="Madera-Mariposa-Merced (4200)   /   5366 Hwy 49 North Mariposa, CA 95338">
                                                        &nbsp;Madera-Mariposa-Merced (4200)</option>

                                                    <option value="Marin County Fire (1500)   /   33 Castle Rock Ave. Woodacre, CA 94973">
                                                        &nbsp;Marin County Fire (1500)</option>

                                                    <option value="Mendocino Unit (1100)   /   17501 N. Hwy 101 Willits, CA 95490">
                                                        &nbsp;Mendocino Unit (1100)</option>

                                                    <option value="Nevada-Yuba-Placer Unit (2300)   /   13760 Lincoln Way Auburn, CA 95603 ">
                                                        &nbsp;Nevada-Yuba-Placer Unit (2300)</option>

                                                    <option value="Riverside Unit (3100)   /   210 W. San Jacinto Perris, CA 92570">
                                                        &nbsp;Riverside Unit (3100)</option>

                                                    <option value="San Benito-Monterey Unit (4600)   /   2221 Garden Road Monterey, CA 93940-5385">
                                                        &nbsp;San Benito-Monterey Unit (4600)</option>

                                                    <option value="San Bernardino Unit (3500)   /   3800 N. Sierra Way San Bernardino, CA 92405">
                                                        &nbsp;San Bernardino Unit (3500)</option>

                                                    <option value="San Diego Unit (3300)   /   2249 Jamacha Road El Cajon, CA 92019">
                                                        &nbsp;San Diego Unit (3300)</option>

                                                    <option value="San Luis Obispo Unit (3400)   /   635 N. Santa Rosa San Luis Obispo, CA 93405">
                                                        &nbsp;San Luis Obispo Unit (3400)</option>

                                                    <option value="San Mateo-Santa Cruz Unit (1700)   /   6059 Highway 9 Felton, CA 95018">
                                                        &nbsp;San Mateo-Santa Cruz Unit (1700)</option>

                                                    <option value="Santa Clara Unit (1600)   /   15670 S. Monterey St. Morgan Hill, CA 95037">
                                                        &nbsp;Santa Clara Unit (1600)</option>

                                                    <option value="Shasta-Trinity Unit (2400)   /   875 Cypress Ave Redding, CA 96001">
                                                        &nbsp;Shasta-Trinity Unit (2400)</option>

                                                    <option value="Siskiyou Unit (2600)   /   1809 Fairlane Rd. Yreka, CA 96097">
                                                        &nbsp;Siskiyou Unit (2600)</option>

                                                    <option value="Sonoma-Lake-Napa Unit (1400)   /   1199 Big Tree Lane St. Helena, CA 94574">
                                                        &nbsp;Sonoma-Lake-Napa Unit (1400)</option>

                                                    <option value="Tehama-Glenn Unit (2500)   /   604 Antelope Blvd. Red Bluff, CA 96080">
                                                        &nbsp;Tehama-Glenn Unit (2500)</option>

                                                    <option value="Telecom Unit /   1450 Expo Parkway, Suite C Sacramento, CA 95815">
                                                        &nbsp;Telecom Unit)</option>

                                                    <option value="Tulare Unit (4100)   /   1968 S. Lovers Lane Visalia, CA 93292">
                                                        &nbsp;Tulare Unit (4100)</option>

                                                    <option value="Tuolumne-Calaveras Unit (4400)   /   785 Mountain Ranch Rd. SanAndreas, CA 95249">
                                                        &nbsp;Tuolumne-Calaveras Unit (4400)</option>
                                                    <?php }?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                             <div class="row">
                                <div class="col-lg-4">
                                    <div class="form-group row">
                                        <div class="col-sm-12">
                                            <label for="inputEmail3MD" class="col-auto col-form-label">Address</label>
                                            <div class="md-form mt-0">
                                                <input name="address" type="text" required="required" value="<?php echo $list[0]->address;?>" autocomplete="off" class="form-control" id="inputEmail3MD">
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-4">
                                    <div class="form-group row">
                                        <div class="col-sm-12">
                                            <label for="inputEmail3MD" class="col-auto col-form-label">City</label>
                                            <div class="md-form mt-0">
                                                <input name="city" type="text" value="<?php echo $list[0]->city;?>" required="required" autocomplete="off" class="form-control" id="inputEmail3MD">
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-4">
                                    <div class="form-group row">
                                        <div class="col-sm-12">
                                            <label for="inputEmail3MD" class="col-auto col-form-label">Zip</label>
                                            <div class="md-form mt-0">
                                                <input name="zip" type="text" value="<?php echo $list[0]->zip;?>" required="required" autocomplete="off" class="form-control" id="inputEmail3MD">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-lg-4">
                                    <div class="form-group row">
                                        <div class="col-sm-12">
                                            <label for="inputEmail3MD" class="col-auto col-form-label">Phone Number</label>
                                            <div class="md-form mt-0">
                                                <input name="pnum" type="text" value="<?php echo $list[0]->pnum;?>" required="required" autocomplete="off" class="form-control" id="inputEmail3MD">
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-4">
                                    <div class="form-group row">
                                        <div class="col-sm-12">
                                            <label for="inputEmail3MD" class="col-auto col-form-label">Email</label>
                                            <div class="md-form mt-0">
                                                <input name="email" type="email" value="<?php echo $list[0]->email;?>" required="required" autocomplete="off" class="form-control" id="inputEmail3MD">
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-4">
                                    <div class="form-group row">
                                        <div class="col-sm-12">
                                            <label for="inputEmail3MD" class="col-auto col-form-label">Contact Person Name</label>
                                            <div class="md-form mt-0">
                                                <input name="person_name" type="text" value="<?php echo $list[0]->person_name;?>" required="required" autocomplete="off" class="form-control" id="inputEmail3MD">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row warranty-checkbox">
                                <div class="col-sm-12">
                                    <div class="custom-control custom-checkbox">
                                        <input type="hidden" name="credit_card_info" value="0">
                                        <input type="checkbox" value="1" <?php if($list[0]->credit_card_info==1){ echo "checked";} ?> name="credit_card_info" selected="selected" class="custom-control-input" id="checkbox_2">
                                        <label class="custom-control-label" for="checkbox_2">Call for credit card information</label>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="form-group row">
                                        <label for="inputEmail3MD" class="col-sm-12 col-form-label">Customer Tracking Number:</label>

                                        <div class="col-sm-12">
                                            <div class="md-form mt-0">
                                                <textarea name="CustomerTrackingNumber" class="form-control rounded-0" rows="5"><?php echo $list[0]->CustomerTrackingNumber;?></textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="form-group row">
                                        <div class="col-sm-12">
                                            <label for="inputEmail3MD" class="col-auto col-form-label">Model</label>
                                            <div class="md-form mt-0">
                                                <input required="required" name="Model" value="<?php echo $list[0]->Model;?>" type="text" class="form-control" id="inputEmail3MD">
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-6">
                                    <div class="form-group row">
                                        <div class="col-sm-12">
                                            <label for="inputEmail3MD" class="col-auto col-form-label">	Warranty Status</label>
                                            <div class="md-form mt-0">
                                                <select required="required" name="WarrantyStatus" class="form-control">
                                                    <option value="<?php echo $list[0]->WarrantyStatus;?>" selected="selected"><?php echo $list[0]->WarrantyStatus;?></option>
                                                    <option value="Expired">&nbsp;Expired</option>
                                                    <option value="Warranty">&nbsp;Warranty</option>
                                                    <option value="Warranty_Recall">&nbsp;Warranty_Recall</option>
                                                    <option value="Non_Warranty">&nbsp;Non_Warranty</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="form-group row">
                                        <div class="col-sm-12">
                                            <label for="inputEmail3MD" class="col-auto col-form-label">Qty</label>
                                            <div class="md-form mt-0">
                                                <input required="required" name="Qty" type="text" size="3" value="<?php echo $list[0]->Qty;?>" class="form-control" id="inputEmail3MD">
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-6">
                                    <div class="form-group row">
                                        <div class="col-sm-12">
                                            <label for="inputEmail3MD" class="col-auto col-form-label">Serial #</label>
                                            <div class="md-form mt-0">
                                                <input required="required" name="Serial" type="text" size="25" value="<?php echo $list[0]->Serial;?>" class="form-control" id="inputEmail3MD">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-lg-4">
                                    <h3 class="checkbox-heading">Audio</h3>
                                    <div class="custom-control custom-checkbox">
                                        <input type="hidden" name="NoAudio" value="0">
                                        <input type="checkbox" name="NoAudio" value="1" <?php if($list[0]->NoAudio==1){ echo "checked"; } ?> selected="selected"  class="custom-control-input" id="audio_1">
                                        <label class="custom-control-label" for="audio_1">No Audio</label>
                                    </div>
                                    <div class="custom-control custom-checkbox">
                                        <input type="hidden" name="LowAudio" value="0">
                                        <input type="checkbox" name="LowAudio" value="1" <?php if($list[0]->LowAudio==1){ echo "checked"; } ?> selected="selected" class="custom-control-input" id="audio_2">
                                        <label class="custom-control-label" for="audio_2">Low Audio</label>
                                    </div>
                                    <div class="custom-control custom-checkbox">
                                        <input type="hidden" name="IntermittentAudio" value="0">
                                        <input type="checkbox" name="IntermittentAudio" value="1" <?php if($list[0]->IntermittentAudio==1){ echo "checked"; } ?> selected="selected" class="custom-control-input" id="audio_3">
                                        <label class="custom-control-label" for="audio_3">Intermittent Audio</label>
                                    </div>
                                    <div class="custom-control custom-checkbox">
                                        <input type="hidden" name="NoTXAudio" value="0">
                                        <input type="checkbox" name="NoTXAudio" value="1" <?php if($list[0]->NoTXAudio==1){ echo "checked"; } ?> selected="selected" class="custom-control-input" id="audio_4">
                                        <label class="custom-control-label" for="audio_4">No TX Audio</label>
                                    </div>
                                    <div class="custom-control custom-checkbox">
                                        <input type="hidden" name="NoPLDPL" value="0">
                                        <input type="checkbox" name="NoPLDPL" value="1" <?php if($list[0]->NoPLDPL==1){ echo "checked"; } ?> selected="selected" class="custom-control-input" id="audio_5">
                                        <label class="custom-control-label" for="audio_5">No PL/DPL</label>
                                    </div>
                                    <div class="custom-control custom-checkbox">
                                        <input type="hidden" name="ConstantAudion" value="0">
                                        <input type="checkbox" name="ConstantAudion" value="1" <?php if($list[0]->ConstantAudion==1){ echo "checked"; } ?> selected="selected" class="custom-control-input" id="audio_6">
                                        <label class="custom-control-label" for="audio_6">Constant Audion</label>
                                    </div>
                                    <div class="custom-control custom-checkbox">
                                        <input type="hidden" name="WillnotSquelch" value="0">
                                        <input type="checkbox" name="WillnotSquelch" <?php if($list[0]->WillnotSquelch==1){ echo "checked"; } ?> value="1" selected="selected" class="custom-control-input" id="audio_7">
                                        <label class="custom-control-label" for="audio_7">Will not Squelch</label>
                                    </div>
                                </div>

                                <div class="col-lg-4">
                                    <h3 class="checkbox-heading">Display/Keypad</h3>
                                    <div class="custom-control custom-checkbox">
                                        <input type="hidden" name="LCD" value="0">
                                        <input type="checkbox" name="LCD" value="1" <?php if($list[0]->LCD==1){ echo "checked"; } ?> selected="selected" class="custom-control-input" id="display_1">
                                        <label class="custom-control-label" for="display_1">LCD</label>
                                    </div>
                                    <div class="custom-control custom-checkbox">
                                        <input type="hidden" name="Keypad" value="0">
                                        <input type="checkbox" name="Keypad" value="1" <?php if($list[0]->Keypad==1){ echo "checked"; } ?> selected="selected" class="custom-control-input" id="display_2">
                                        <label class="custom-control-label" for="display_2">Keypad</label>
                                    </div>
                                    <div class="custom-control custom-checkbox">
                                        <input type="hidden" name="NoDisplay" value="0">
                                        <input type="checkbox" name="NoDisplay" value="1" <?php if($list[0]->NoDisplay==1){ echo "checked"; } ?> selected="selected" class="custom-control-input" id="display_3">
                                        <label class="custom-control-label" for="display_3">No Display</label>
                                    </div>
                                    <div class="custom-control custom-checkbox">
                                        <input type="hidden" name="LEDFaulty" value="0">
                                        <input type="checkbox" name="LEDFaulty" value="1" <?php if($list[0]->LEDFaulty==1){ echo "checked"; } ?> selected="selected" class="custom-control-input" id="display_4">
                                        <label class="custom-control-label" for="display_4">LED Faulty</label>
                                    </div>
                                </div>

                                <div class="col-lg-4">
                                    <h3 class="checkbox-heading">Damage</h3>
                                    <div class="custom-control custom-checkbox">
                                        <input type="hidden" name="Housing" value="0">
                                        <input type="checkbox" name="Housing" value="1" <?php if($list[0]->Housing==1){ echo "checked"; } ?> selected="selected" class="custom-control-input" id="damage_1">
                                        <label class="custom-control-label" for="damage_1">Housing</label>
                                    </div>
                                    <div class="custom-control custom-checkbox">
                                        <input type="hidden" name="KnobsButtonsSwitches" value="0">
                                        <input type="checkbox" name="KnobsButtonsSwitches"  <?php if($list[0]->KnobsButtonsSwitches==1){ echo "checked"; } ?> value="1" selected="selected" class="custom-control-input" id="damage_2">
                                        <label class="custom-control-label" for="damage_2">Knobs/Buttons/Switches</label>
                                    </div>
                                    <div class="custom-control custom-checkbox">
                                        <input type="hidden" name="Antenna" value="0">
                                        <input type="checkbox" name="Antenna" value="1" <?php if($list[0]->Antenna==1){ echo "checked"; } ?> selected="selected" class="custom-control-input" id="damage_3">
                                        <label class="custom-control-label" for="damage_3">Antenna</label>
                                    </div>
                                    <div class="custom-control custom-checkbox">
                                        <input type="hidden" name="Label" value="0">
                                        <input type="checkbox" name="Label" value="1" <?php if($list[0]->Label==1){ echo "checked"; } ?> selected="selected" class="custom-control-input" id="damage_4">
                                        <label class="custom-control-label" for="damage_4">Label</label>
                                    </div>
                                    <div class="custom-control custom-checkbox">
                                        <input type="hidden" name="BatteryContacts" value="0">
                                        <input type="checkbox" name="BatteryContacts" value="1" <?php if($list[0]->BatteryContacts==1){ echo "checked"; } ?> selected="selected" class="custom-control-input" id="damage_5">
                                        <label class="custom-control-label" for="damage_5">Battery Contacts</label>
                                    </div>
                                    <div class="custom-control custom-checkbox">
                                        <input type="hidden" name="PhysicalDamage" value="0">
                                        <input type="checkbox" name="PhysicalDamage" value="1" <?php if($list[0]->PhysicalDamage==1){ echo "checked"; } ?> selected="selected" class="custom-control-input" id="damage_6">
                                        <label class="custom-control-label" for="damage_6">Physical Damage</label>
                                    </div>
                                    <div class="custom-control custom-checkbox">
                                        <input type="hidden" name="ChemicalDamage" value="0">
                                        <input type="checkbox" name="ChemicalDamage" value="1" <?php if($list[0]->ChemicalDamage==1){ echo "checked"; } ?> selected="selected" class="custom-control-input" id="damage_7">
                                        <label class="custom-control-label" for="damage_7">Chemical Damage</label>
                                    </div>
                                    <div class="custom-control custom-checkbox">
                                        <input type="hidden" name="LiquidDamage" value="0">
                                        <input type="checkbox" name="LiquidDamage" value="1" <?php if($list[0]->LiquidDamage==1){ echo "checked"; } ?> selected="selected" class="custom-control-input" id="damage_8">
                                        <label class="custom-control-label" for="damage_8">Liquid Damage</label>
                                    </div>
                                    <div class="custom-control custom-checkbox">
                                        <input type="hidden" name="Dead" value="0">
                                        <input type="checkbox" name="Dead" value="1"  <?php if($list[0]->Dead==1){ echo "checked"; } ?> selected="selected" class="custom-control-input" id="damage_9">
                                        <label class="custom-control-label" for="damage_9">Dead</label>
                                    </div>
                                    <div class="custom-control custom-checkbox">
                                        <input type="hidden" name="Fuse" value="0">
                                        <input type="checkbox" name="Fuse" value="1" <?php if($list[0]->Fuse==1){ echo "checked"; } ?> selected="selected" class="custom-control-input" id="damage_10">
                                        <label class="custom-control-label" for="damage_10">Fuse</label>
                                    </div>
                                </div>
                            </div>


                            <div class="bottom-checkboxes">
                                <h3 class="checkbox-heading">Radio Functions</h3>
                                <div class="row">
                                    <div class="col-lg-4">
                                        <div class="custom-control custom-checkbox">
                                            <input type="hidden" name="PoorRXSens" value="0">
                                            <input type="checkbox" name="PoorRXSens" value="1" <?php if($list[0]->PoorRXSens==1){ echo "checked"; } ?> selected="selected" class="custom-control-input" id="radio_1">
                                            <label class="custom-control-label" for="radio_1">Poor RX Sens</label>
                                        </div>
                                    </div>

                                    <div class="col-lg-4">
                                        <div class="custom-control custom-checkbox">
                                            <input type="hidden" name="UnabletoReadProgram" value="0">
                                            <input type="checkbox" name="UnabletoReadProgram" <?php if($list[0]->UnabletoReadProgram==1){ echo "checked"; } ?> value="1" selected="selected" class="custom-control-input" id="radio_2">
                                            <label class="custom-control-label" for="radio_2">Unable to Read/Program</label>
                                        </div>
                                    </div>

                                    <div class="col-lg-4">
                                        <div class="custom-control custom-checkbox">
                                            <input type="hidden" name="IntermittentRX" value="0">
                                            <input type="checkbox" name="IntermittentRX" <?php if($list[0]->IntermittentRX==1){ echo "checked"; } ?> value="1" selected="selected" class="custom-control-input" id="radio_3">
                                            <label class="custom-control-label" for="radio_3">Intermittent RX</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-4">
                                        <div class="custom-control custom-checkbox">
                                            <input type="hidden" name="NoScan" value="0">
                                            <input type="checkbox" name="NoScan" value="1"  <?php if($list[0]->NoScan==1){ echo "checked"; } ?> selected="selected" class="custom-control-input" id="radio_4">
                                            <label class="custom-control-label" for="radio_4">No Scan</label>
                                        </div>
                                    </div>

                                    <div class="col-lg-4">
                                        <div class="custom-control custom-checkbox">
                                            <input type="hidden" name="NoTXr" value="0">
                                            <input type="checkbox" name="NoTXr" value="1" <?php if($list[0]->NoTXr==1){ echo "checked"; } ?> selected="selected" class="custom-control-input" id="radio_5">
                                            <label class="custom-control-label" for="radio_5">No TXr</label>
                                        </div>
                                    </div>

                                    <div class="col-lg-4">
                                        <div class="custom-control custom-checkbox">
                                            <input type="hidden" name="IntermittentPower" value="0">
                                            <input type="checkbox" name="IntermittentPower" value="1" <?php if($list[0]->IntermittentPower==1){ echo "checked"; } ?> selected="selected" class="custom-control-input" id="radio_6">
                                            <label class="custom-control-label" for="radio_6">Intermittent Power</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-4">
                                        <div class="custom-control custom-checkbox">
                                            <input type="hidden" name="NoTrunking" value="0">
                                            <input type="checkbox" name="NoTrunking" <?php if($list[0]->NoTrunking==1){ echo "checked"; } ?> value="1" selected="selected" class="custom-control-input" id="radio_7">
                                            <label class="custom-control-label" for="radio_7">No Trunking</label>
                                        </div>
                                    </div>

                                    <div class="col-lg-4">
                                        <div class="custom-control custom-checkbox">
                                            <input type="hidden" name="NoPower" value="0">
                                            <input type="checkbox" name="NoPower" <?php if($list[0]->NoPower==1){ echo "checked"; } ?> value="1" selected="selected" class="custom-control-input" id="radio_8">
                                            <label class="custom-control-label" for="radio_8">No Power</label>
                                        </div>
                                    </div>

                                    <div class="col-lg-4">
                                        <div class="custom-control custom-checkbox">
                                            <input type="hidden" name="FailCode" value="0">
                                            <input type="checkbox" name="FailCode" <?php if($list[0]->FailCode==1){ echo "checked"; } ?> value="1" selected="selected" class="custom-control-input" id="radio_9">
                                            <label class="custom-control-label" for="radio_9">Fail Code (list code below)</label>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-lg-4">
                                        <div class="custom-control custom-checkbox">
                                            <input type="hidden" name="NoRX" value="0">
                                            <input type="checkbox" name="NoRX" value="1" <?php if($list[0]->NoRX==1) echo 'checked="checked"'; ?> selected="selected" class="custom-control-input" id="radio_10">
                                            <label class="custom-control-label" for="radio_10">No RX</label>
                                        </div>
                                    </div>

                                    <div class="col-lg-4">
                                        <div class="custom-control custom-checkbox">
                                            <input type="hidden" name="LowPower" value="0">
                                            <input type="checkbox" name="LowPower" <?php if($list[0]->LowPower==1){ echo "checked"; } ?> value="1" selected="selected" class="custom-control-input" id="radio_11">
                                            <label class="custom-control-label" for="radio_11">Low Power</label>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="complaint-box">
                                <div class="custom-control custom-checkbox">
                                    <input type="hidden" name="otherlistcomplaintbelow" value="0">
                                    <input type="checkbox" name="otherlistcomplaintbelow" <?php if($list[0]->otherlistcomplaintbelow==1){ echo "checked"; }?> value="1" selected="selected" class="custom-control-input" id="complaint_1">
                                    <label class="custom-control-label" for="complaint_1">Other (list complaint below)</label>
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-12">
                                        <div class="md-form mt-0">
                                            <textarea class="form-control rounded-0" name="otherlistcomplaintbelowtext" rows="5"><?php echo $list[0]->otherlistcomplaintbelowtext; ?></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
<!--new-->
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="form-group row">
                                        <label for="inputEmail3MD" class="col-sm-12 col-form-label">Comments:</label>

                                        <div class="col-sm-12">
                                            <div class="md-form mt-0">
                                                <textarea name="Complaints" class="form-control rounded-0" rows="5"><?=$list[0]->Complaints?></textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
<!--new ends-->

                            <div class="form-submit-wrap">
                                <input type="hidden" name="table_id" value="<?=$this->type?>">
                                <input type="hidden" name="id" value="<?=$list[0]->id?>">
                                <button type="button" onclick="window.print();" class="btn btn-primary"><i class="fa fa-print mr-1"></i> Print</button>
                                <?php if($this->session->is_admin or $this->session->access_level==3){?>
                                    <button name="update" type="submit" id="Submit" form="cal_fire" value="Update" class="btn btn-success"><i class="fa fa-check mr-1"></i> Update</button>
                                <?php } ?>
                            </div>

                    </div>
                </div>
            <?php }else{?>
                <div class="row">
                    <h1 class="text-center">
                        No Data Found
                    </h1>
                </div>
            <?php }?>

        </section>
        <!-- /.content -->
    </div>
</div>
<!-- /.content-wrapper -->




