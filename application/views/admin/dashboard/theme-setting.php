  <!-- Bootstrap Color Picker -->
  <link rel="stylesheet" href="<?php echo base_url();?>/assets/admin/bower_components/bootstrap-colorpicker/dist/css/bootstrap-colorpicker.min.css">
  
<!-- bootstrap color picker -->
<script src="<?php echo base_url();?>/assets/admin/bower_components/bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js"></script>

 <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Theme Setting
       
      </h1>
     
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title"></h3>

          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                    title="Collapse">
              <i class="fa fa-minus"></i></button>
            <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
              <i class="fa fa-times"></i></button>
          </div>
        </div>
        <div class="box-body">
          <div class="col-md-6 col-md-offset-3">
            <?php $notification=$this->session->flashdata('notification');
            if($notification){ ?>            
            <div id="snackbar"><?php echo $notification;?></div>
            <script type="text/javascript">$(document).ready(function(){
              notificationFun();
            });</script>
              
              <?php }?>
              
              <?php if(isset($user_theme_data)){ 
                      $options=array(); 
                      foreach ($user_theme_data as $key => $val) {       
                        $options[$val->key]=$val->value;      
                     }
                   }
              ?>

              <form data-toggle="validator" role="form" method="post">
                  <div class="form-group">
                      <label>Theme Color:</label>

                      <div class="input-group them_color">
                        <input type="text" name="theme_color" class="form-control" 
                        value="<?php if(isset($options['theme_color'])){echo $options['theme_color'];}else{ echo set_value('theme_color');}?>">

                        <div class="input-group-addon">
                          <i></i>
                        </div>
                      </div>
                      <!-- /.input group -->
                  </div>
                 <div class="form-group">
                      <label>Navigation Link Color:</label>

                      <div class="input-group them_color">
                        <input type="text" name="nav_link_color" class="form-control"
                        value="<?php if(isset($options['nav_link_color'])){echo $options['nav_link_color'];}else{ echo set_value('nav_link_color');}?>">

                        <div class="input-group-addon">
                          <i></i>
                        </div>
                      </div>
                      <!-- /.input group -->
                  </div>
                  <div class="form-group">
                      <label>Navigation Link Hover Color:</label>

                      <div class="input-group them_color">
                        <input type="text" name="nav_link_hover_color" class="form-control"
                        value="<?php if(isset($options['nav_link_hover_color'])){
                          echo $options['nav_link_hover_color'];}else{ 
                            echo set_value('nav_link_hover_color');}?>">

                        <div class="input-group-addon">
                          <i></i>
                        </div>
                      </div>
                      <!-- /.input group -->
                  </div>
                   <div class="form-group">
                      <label>Skills Icon Hover Color:</label>

                      <div class="input-group them_color">
                        <input type="text" name="skill_icon_hover_color" class="form-control"
                        value="<?php if(isset($options['skill_icon_hover_color'])){
                          echo $options['skill_icon_hover_color'];}else{ 
                            echo set_value('skill_icon_hover_color');}?>">

                        <div class="input-group-addon">
                          <i></i>
                        </div>
                      </div>
                      <!-- /.input group -->
                  </div>
                  <div class="form-group">
                         <button name="save_options" class="btn btn-primary" type="submit">
                             Save
                         </button>
                     </div>
              </form>
            </div>

        </div>
        <!-- /.box-body -->
        <div class="box-footer">
        
        </div>
        <!-- /.box-footer-->
      </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->


<script type="text/javascript">
   $('.them_color').colorpicker();
</script>