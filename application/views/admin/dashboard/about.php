
 <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        About
       
      </h1>
     
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title"></h3>

          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                    title="Collapse">
              <i class="fa fa-minus"></i></button>
            <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
              <i class="fa fa-times"></i></button>
          </div>
        </div>
        <div class="box-body">
          <div class="col-md-6 col-md-offset-3">
            <?php $notification=$this->session->flashdata('notification');
            if($notification){ ?>            
            <div id="snackbar"><?php echo $notification;?></div>
            <script type="text/javascript">$(document).ready(function(){
              notificationFun();
            });</script>
              
              <?php }?>
              <?php echo validation_errors();?>
              <form data-toggle="validator" role="form" method="post">
                  <div class="form-group">
                    <label class="control-label" for="address">Address</label>

                    <input class="form-control" data-minlength="5" data-error="must enter minimum of 5 characters" id="address" name="address" value="<?php if($user_about_data){echo $user_about_data->address;}else{ echo set_value('address');}?>" placeholder="Address"  type="text" required />
                    <div class="help-block with-errors"><?php echo form_error('address');?></div>
                  </div>
                  <div class="form-group">
                    <label class="control-label" for="city">City</label>
                    <input class="form-control"  id="city" name="city" value="<?php if($user_about_data){echo $user_about_data->city;}else{ echo set_value('city');}?>" placeholder="City"  type="text" required />
                    <div class="help-block with-errors"><?php echo form_error('city');?></div>
                  </div>
                  <div class="form-group">
                    <label class="control-label" for="country">Country</label>
                    <input class="form-control"  id="country" name="country" value="<?php if($user_about_data){echo $user_about_data->country;}else{ echo set_value('country');}?>" placeholder="Country"  type="text" required />
                    <div class="help-block with-errors"><?php echo form_error('country');?></div>
                  </div>
                  <div class="form-group">
                    <label class="control-label" for="zip">Zip Code</label>
                    <input class="form-control"  id="zip" name="zipcode" value="<?php if($user_about_data){echo $user_about_data->zipcode;}else{ echo set_value('zipcode');}?>" placeholder="Zip Code"  type="text" required />
                    <div class="help-block with-errors"><?php echo form_error('zipcode');?></div>
                  </div>
                  
                  <div class="form-group">
                    <label class="control-label" for="phone">Phone (xxx-xxx-xxxx)</label>                   
                  
                     <input id="phone" type="tel" name="phone" class="form-control" value="<?php if($user_about_data){echo $user_about_data->phone;}else{ echo set_value('phone');}?>" pattern="^\d{3}-\d{3}-\d{4}$" >
                    <div class="help-block with-errors"><?php echo form_error('phone');?></div>
                  </div>

                  <div class="form-group">
                    <label class="control-label" for="mobile">Mobile</label>                   
                  
                     <input id="mobile" type="tel" name="mobile" value="<?php if($user_about_data){echo $user_about_data->mobile;}else{ echo set_value('mobile');}?>" class="form-control" pattern="^\d{10}$" required >
                    <div class="help-block with-errors"><?php echo form_error('mobile');?></div>
                  </div>

                  <div class="form-group">
                       <label for="bob" class="control-label">DOB</label>
                       <input type="date" class="form-control" value="<?php if($user_about_data){echo $user_about_data->dob;}else{ echo set_value('dob');}?>" name="dob" id="dob" format="dd/MM/yyyy"required>
                       <div class="help-block with-errors"><?php echo form_error('dob');?></div>
                    </div>
                   <div class="form-group">
                       <label for="editor1" class="control-label">Short Description</label>
                       <div class="form-group">
                         <textarea id="editor1" name="detail"><?php if($user_about_data){echo $user_about_data->detail;}else{ echo set_value('detail');}?></textarea>
                       </div>
                     </div>
                  <div class="form-group">
                         <button name="save_about" class="btn btn-primary" type="submit">
                             Save
                         </button>
                     </div>
              </form>
            </div>

        </div>
        <!-- /.box-body -->
        <div class="box-footer">
        
        </div>
        <!-- /.box-footer-->
      </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->


