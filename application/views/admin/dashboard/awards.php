<link rel="stylesheet" href="https://cdn.rawgit.com/konpa/devicon/df6431e323547add1b4cf45992913f15286456d3/devicon.min.css">
 <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Part Category
       
      </h1>
     
    </section>

    <!-- Main content -->
    <section class="content">

   

       <!-- Default box -->
    
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">
            Part category
          </h3>


          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                    title="Collapse">
              <i class="fa fa-minus"></i></button>
            <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
              <i class="fa fa-times"></i></button>
          </div>
        </div>
        <div class="box-body">
           <form method="post" id="awards_edit_form" data-toggle="validator" role="form" style="display: none;">
          <table class="table " >
             <thead>
              <tr>
                <th>Skill</th>
                <th>Icon</th>                             
                <th>Status</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody>
            <tr id="edit_award" >
                <td>
                  <div class="form-group">
                <input class="form-control" data-minlength="3" data-error="Must enter minimum of 3 characters" id="edit_description" name="typetitle" value="<?php echo set_value('typetitle');?>" placeholder="title"  type="text" required />
                    <p class="help-block with-errors"><?php echo form_error('typetitle');?></p>
                  </div>
              </td>
            
             <td>
                <input type="hidden"  name="id" id="award_edit_id" value="">
                <button name="edit_awards" class="btn btn-primary btn-xs" type="submit">
                             Update
                         </button>
                         <button id="cancle_edit" class="btn btn-info btn-xs" type="button">
                             Cancle
                         </button>
                       </td>
              </tr>
              </tbody>
          </table></form>

         <form method="post" id="awards_form" data-toggle="validator" role="form">
          <table class="table table-bordered">
            <thead>
              <tr>
                <th>Category Title</th>                                           
                <th>Status</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody>
              <tr id='add_award'>
                <td>
                  <div class="form-group">
                <input class="form-control" data-minlength="3" data-error="Must enter minimum of 3 characters" id="title" name="typetitle" value="<?php echo set_value('typetitle');?>" placeholder=""  type="text" required />
                    <p class="help-block with-errors"><?php echo form_error('typetitle');?></p>
                  </div>
              </td>
           
              <td>                
                <button name="save_partCategory" class="btn btn-primary" type="submit">
                            Save
                         </button>
                       </td>
              </tr>
               <?php if(sizeof($user_education_data)>0){?>
              <?php foreach($user_education_data as $row){?>
              
              <tr id="row_<?php echo $row->id;?>">
                <td id="row_title_<?php echo $row->id;?>"><?php echo $row->typetitle;?></td>
                                         
                <td id="status_<?php echo $row->id;?>"><?php if($row->status==1){?> <a onclick="active_row(3,<?php echo $row->id;?>,1)" class="btn btn-success btn-xs"><i class="fa  fa-thumbs-o-up"></i> Active</a><?php }else{ ?><a onclick="active_row(3,<?php echo $row->id;?>,0)" class="btn btn-danger btn-sm"><i class="fa  fa-thumbs-o-down"></i> In-active</a><?php } ?></td>

                <td><a onclick="editopenfun('<?php echo $row->id;?>')" class="btn btn-info btn-xs"><i class="fa fa-pencil"></i></a>
                  <a onclick="delete_row(3,<?php echo $row->id;?>)" class="btn btn-danger btn-xs"><i class="fa   fa-trash"></i></a></td>
              </tr>
             
              <?php } ?>
              <?php }else{} ?>
            </tbody>
          </table>
          </form>
          
        </div>
        <!-- /.box-body -->
        <div class="box-footer">
        
        </div>
        <!-- /.box-footer-->
      </div>
   
      <!-- /.box -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->



<?php $notification=$this->session->flashdata('notification');
            if($notification){ ?>            
            <div id="snackbar"><?php echo $notification;?></div>
            <script type="text/javascript">$(document).ready(function(){
              notificationFun();
            });</script>
              
              <?php }?>

<script type="text/javascript">
 

  function editopenfun(id){
    $("#awards_edit_form").show();
    $("#add_award").hide();

    var description=$("#row_title_"+id).html();
   
    $("tr#edit_award #edit_description").val(description);   
     $("tr#edit_award #award_edit_id").val(id);
      $("tr#edit_award #edit_description").focus();

  }

 
$("tr#edit_award #cancle_edit").click(function(){
   $("#awards_edit_form").hide();
    $("#add_award").show();
    $("tr#edit_award #edit_description").val('');  
     $("tr#edit_award #award_edit_id").val('');
});
</script>