<link rel="stylesheet" href="https://cdn.rawgit.com/konpa/devicon/df6431e323547add1b4cf45992913f15286456d3/devicon.min.css">
 <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Skills And Workflow
       
      </h1>
     
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="col-md-6">
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">
            Skill
          </h3>


          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                    title="Collapse">
              <i class="fa fa-minus"></i></button>
            <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
              <i class="fa fa-times"></i></button>
          </div>
        </div>
        <div class="box-body">
          <form method="post" id="skill_with_icon" data-toggle="validator" role="form">
          <table class="table from-second" style="display: none;">
             <thead>
              <tr>
                <th>Skill</th>
                <th>Icon</th>                             
                <th>Status</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody>
            <tr id="edit" >
                <td>
                  <div class="form-group">
                <input class="form-control" data-minlength="3" data-error="Must enter minimum of 3 characters" id="edit_title" name="title" value="<?php echo set_value('institute');?>" placeholder="Skill"  type="text" required />
                    <p class="help-block with-errors"><?php echo form_error('title');?></p>
                  </div>
              </td>
            <td>
              <div class="form-group">
                <input class="form-control" data-minlength="3" data-error="Must enter minimum of 3 characters" id="edit_icon" name="icon" value="<?php echo set_value('icon');?>" placeholder="devicon-php-plain"  type="text" required />
                    <p class="help-block with-errors"><?php echo form_error('icon');?></p>
                  </div>
              </td>
              <td>
                <a href="http://konpa.github.io/devicon/"
              target="popup" 
  onclick="window.open('http://konpa.github.io/devicon/','popup','width=1000,height=600,scrollbars=no,resizable=no'); return false;">Icon</a>
              </td><td>
                <input type="hidden"  name="id" id="skill_edit_id" value="">
                <button name="edit_skills" class="btn btn-primary btn-xs" type="submit">
                             Update
                         </button>
                         <button id="cancle_edit" class="btn btn-info btn-xs" type="button">
                             Cancle
                         </button>
                       </td>
              </tr>
              </tbody>
          </table></form>

         <form method="post" id="skill_with_icon" data-toggle="validator" role="form">
          <table class="table table-bordered">
            <thead>
              <tr>
                <th>Skill</th>
                <th>Icon</th>                             
                <th>Status</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody>
              <tr id="add">
                <td >
                  <div class="form-group">
                <input class="form-control" data-minlength="3" data-error="Must enter minimum of 3 characters" id="title" name="title" value="<?php echo set_value('institute');?>" placeholder="Skill"  type="text" required />
                    <p class="help-block with-errors"><?php echo form_error('title');?></p>
                  </div>
              </td>
            <td>
              <div class="form-group">
                <input class="form-control" data-minlength="3" data-error="Must enter minimum of 3 characters" id="icon" name="icon" value="<?php echo set_value('icon');?>" placeholder="devicon-php-plain"  type="text" required />
                    <p class="help-block with-errors"><?php echo form_error('icon');?></p>
                  </div>
              </td>
              <td>
                <a href="http://konpa.github.io/devicon/"
              target="popup" 
  onclick="window.open('http://konpa.github.io/devicon/','popup','width=1000,height=600,scrollbars=no,resizable=no'); return false;">Icon</a>
              </td><td>
                <input type="hidden"  name="type" value="1">
                <button name="save_skills" class="btn btn-primary" type="submit">
                             Save
                         </button>
                       </td>
              </tr>

              
               <?php if(sizeof($user_education_data)>0){?>
              <?php foreach($user_education_data as $row){?>
              <?php if($row->type==1){?>
              <tr id="row_<?php echo $row->id;?>">
                <td id="row_title_<?php echo $row->id;?>"><?php echo $row->title;?></td>
                <td class="text-center"><span style="font-size:50px"><i id="row_icon_<?php echo $row->id;?>" class="<?php echo $row->icon;?>"></i></span></td>                            
                <td id="status_<?php echo $row->id;?>"><?php if($row->status==1){?> <a onclick="active_row(3,<?php echo $row->id;?>,1)" class="btn btn-success btn-xs"><i class="fa  fa-thumbs-o-up"></i> Active</a><?php }else{ ?><a onclick="active_row(3,<?php echo $row->id;?>,0)" class="btn btn-danger btn-sm"><i class="fa  fa-thumbs-o-down"></i> In-active</a><?php } ?></td>

                <td><a  onclick="editopenfun('<?php echo $row->id;?>')" class="btn btn-info btn-xs"><i class="fa fa-pencil"></i></a>
                  <a onclick="delete_row(3,<?php echo $row->id;?>)" class="btn btn-danger btn-xs"><i class="fa   fa-trash"></i></a></td>
              </tr>
              <?php }?>
              <?php } ?>
              <?php }else{} ?>
            </tbody>
          </table>
          </form>
          
        </div>
        <!-- /.box-body -->
        <div class="box-footer">
        
        </div>
        <!-- /.box-footer-->
      </div>
    </div>
      <!-- /.box -->

       <!-- Default box -->
      <div class="col-md-6">
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">
            Workflow
          </h3>


          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                    title="Collapse">
              <i class="fa fa-minus"></i></button>
            <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
              <i class="fa fa-times"></i></button>
          </div>
        </div>
        <div class="box-body">
           <form method="post" id="skill_with_icon" data-toggle="validator" role="form">
          <table class="table from-forth " style="display: none;">
             <thead>
              <tr>
                <th>Skill</th>
                <th>Icon</th>                             
                <th>Status</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody>
            <tr id="edit_forth" >
                <td>
                  <div class="form-group">
                <input class="form-control" data-minlength="3" data-error="Must enter minimum of 3 characters" id="edit_title" name="title" value="<?php echo set_value('institute');?>" placeholder="Skill"  type="text" required />
                    <p class="help-block with-errors"><?php echo form_error('title');?></p>
                  </div>
              </td>
            
             <td>
                <input type="hidden"  name="id" id="skill_edit_id" value="">
                <button name="edit_skills" class="btn btn-primary btn-xs" type="submit">
                             Update
                         </button>
                         <button id="cancle_edit" class="btn btn-info btn-xs" type="button">
                             Cancle
                         </button>
                       </td>
              </tr>
              </tbody>
          </table></form>
         <form method="post" id="skill_with_icon" data-toggle="validator" role="form">
          <table class="table table-bordered">
            <thead>
              <tr>
                <th>Skill</th>                                           
                <th>Status</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody>
              <tr id='add_second'>
                <td>
                  <div class="form-group">
                <input class="form-control" data-minlength="3" data-error="Must enter minimum of 3 characters" id="title" name="title" value="<?php echo set_value('institute');?>" placeholder="Skill"  type="text" required />
                    <p class="help-block with-errors"><?php echo form_error('title');?></p>
                  </div>
              </td>
           
              <td>
                <input type="hidden"  name="type" value="2">
                <button name="save_skills" class="btn btn-primary" type="submit">
                            Save
                         </button>
                       </td></tr>
               <?php if(sizeof($user_education_data)>0){?>
              <?php foreach($user_education_data as $row){?>
              <?php if($row->type==2){?>
              <tr id="row_<?php echo $row->id;?>">
                <td id="row_title_<?php echo $row->id;?>"><?php echo $row->title;?></td>
                                         
                <td id="status_<?php echo $row->id;?>"><?php if($row->status==1){?> <a onclick="active_row(3,<?php echo $row->id;?>,1)" class="btn btn-success btn-xs"><i class="fa  fa-thumbs-o-up"></i> Active</a><?php }else{ ?><a onclick="active_row(3,<?php echo $row->id;?>,0)" class="btn btn-danger btn-sm"><i class="fa  fa-thumbs-o-down"></i> In-active</a><?php } ?></td>

                <td><a onclick="editopenfun2('<?php echo $row->id;?>')" class="btn btn-info btn-xs"><i class="fa fa-pencil"></i></a>
                  <a onclick="delete_row(3,<?php echo $row->id;?>)" class="btn btn-danger btn-xs"><i class="fa   fa-trash"></i></a></td>
              </tr>
              <?php }?>
              <?php } ?>
              <?php }else{} ?>
            </tbody>
          </table>
          </form>
          
        </div>
        <!-- /.box-body -->
        <div class="box-footer">
        
        </div>
        <!-- /.box-footer-->
      </div>
    </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->



<?php $notification=$this->session->flashdata('notification');
            if($notification){ ?>            
            <div id="snackbar"><?php echo $notification;?></div>
            <script type="text/javascript">$(document).ready(function(){
              notificationFun();
            });</script>
              
              <?php }?>

<script type="text/javascript">
  function editopenfun(id){
    $(".from-second").show();
    $("tr#add").hide();

    var title=$("#row_title_"+id).html();
    var icon=$("#row_icon_"+id).attr('class');
    $("tr#edit #edit_title").val(title);
    $("tr#edit #edit_icon").val(icon);
     $("tr#edit #skill_edit_id").val(id);
     $("tr#edit #edit_title").focus();
  }

  function editopenfun2(id){
    $(".from-forth").show();
    $("tr#add_second").hide();

    var title=$("#row_title_"+id).html();
   
    $("tr#edit_forth #edit_title").val(title);   
     $("tr#edit_forth #skill_edit_id").val(id);
      $("tr#edit_forth #edit_title").focus();

  }

 

$("tr#edit #cancle_edit").click(function(){
    $(".from-second").hide();
    $("tr#add").show();  
    $("tr#edit #edit_title").val('');
    $("tr#edit #edit_icon").val('');
     $("tr#edit #skill_edit_id").val('');
});

$("tr#edit_forth #cancle_edit").click(function(){
   $(".from-forth").hide();
    $("tr#add_second").show(); 
    $("tr#edit #edit_title").val('');  
     $("tr#edit #skill_edit_id").val('');
});
</script>