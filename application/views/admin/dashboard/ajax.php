<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script type="text/javascript">
		function delete_row(table,id){

			swal({
				  title: "Are you sure?",
				  text: "Once deleted, you will not be able to recover this imaginary file!",
				  icon: "warning",
				  buttons: true,
				  dangerMode: true,
				})
				.then((willDelete) => {
				   if(willDelete) {

				  		$.ajax({
				  			url:"<?php echo base_url().'/admin/dashboard/delete';?>",
				  			type:'post',
				  			data:{table:table,id:id},
				  			success:function(res){

				  				if(res==1){
				  					var row='row_'+id;
				  					$("#"+row).hide();
				  				swal("Your date has been deleted Successfully!", {
				     				icon: "success",
				    				});
				  				}else{
				  					swal("Delition Failed!", {
				     				icon: "error",
				    				});
				  				}
				  			}
				  		});

				    
				  } else {
				    swal("Your data is safe!");
				  }
				});
		}

		function delete_row1(table,id){

			swal({
				title: "Are you sure?",
				text: "Once deleted, you will not be able to recover this imaginary file!",
				icon: "warning",
				buttons: true,
				dangerMode: true,
			})
				.then((willDelete) => {
				if(willDelete) {

					$.ajax({
						url:"<?php echo base_url().'/admin/dashboard/deletee';?>",
						type:'post',
						data:{table:table,id:id},
						success:function(res){

							if(res==1){
								var row='row_'+id;
								$("#"+row).hide();
								swal("Your date has been deleted Successfully!", {
									icon: "success",
								});
							}else{
								swal("Delition Failed!", {
									icon: "error",
								});
							}
						}
					});


				} else {
					swal("Your data is safe!");
		}
		});
		}


		function active_row(table,id,status){ //only use for changing the user status ( because it also sends email to users on their approval on server side i change_status function)

			swal({
				  title: "Are you sure?",
				  text: "You want to change the status?",
				  icon: "warning",
				  buttons: true,
				  dangerMode: true,
				})
				.then((willDelete) => {
				  if (willDelete) {

				  		$.ajax({
				  			url:"<?php echo base_url().'/admin/dashboard/change_status';?>",
				  			type:'post',
				  			data:{table:table,id:id,status:status},
				  			success:function(res){

				  				if(res==1){
				  					var col='status_'+id;
				  					if(status==1){
										var btn='<a onclick="active_row(1,'+id+',0)" class="btn btn-danger btn-xs"><i class="fa  fa-thumbs-o-down"></i> In-active</a>';
										window.location.reload(false);
				  					}else{
				  						var btn='<a onclick="active_row(1,'+id+',1)" class="btn btn-success btn-xs"><i class="fa  fa-thumbs-o-up"></i> Active</a>';
										window.location.reload(false);
				  					}
				  					$("#"+col).html(btn);				  					
				  				swal("Status changed Successfully!", {
				     				icon: "success",
				    				});
								}else{
				  					swal("Status change Failed!", {
				     				icon: "error",
								});
				  				}
				  			}
				  		});

				    
				  } else {
				    swal("Your data is safe!");
				  }
				});
		}


		function validate(form){

			var data=form.serializearray();
			$.ajax({
				url:"<?php echo base_url().'/admin/dashboard'?>",
				type:'post',
				data:data,
				success:function(res){
					$(".new_password_form").show();
					$(".old_password_form").hide();
				},
				error:function(){
					$(".new_password_form").hide();
					$(".old_password_form").show();
				}
			});


		}


		function chng(table,id,acc_type){ //only use for changing the user status ( because it also sends email to users on their approval on server side i change_status function)

			swal({
				title: "Are you sure?",
				text: "You want to update the Account type?",
				icon: "warning",
				buttons: true,
				dangerMode: true,
			})
				.then((willDelete) => {
				if (willDelete) {

					$.ajax({
						url:"<?php echo base_url().'/admin/dashboard/change_acc_type';?>",
						type:'post',
						data:{table:table,id:id,acc_type:acc_type.value},
						success:function(res){

							if(res==1){
								window.location.reload(true);
								swal("Account Type updated Successfully!", {
									icon: "success",
								});
							}else{
								swal("Account type updation Failed!", {
									icon: "error",
								});
							}
						}
					});
				} else {
					swal("Your data is safe!");
		}
		});
		}


</script>

