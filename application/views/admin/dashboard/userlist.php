<?php
/**
 * Created by PhpStorm.
 * User: Rana
 * Date: 09-Oct-18
 * Time: 7:12 PM
 */?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1> User Profiles List</h1>
  </section>

  <!-- Main content -->
  <section class="content">

    <!-- Default box -->
    <div class="box">
      <div class="box-header with-border">
        <h3 class="box-title"></h3>

        <div class="box-tools pull-right">
          <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                  title="Collapse">
            <i class="fa fa-minus"></i></button>
          <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
            <i class="fa fa-times"></i></button>
        </div>
      </div>

      <?php if ($this->session->flashdata("success")){?>
        <div class="text-center alert alert-success alert-dismissable">
          <?= $this->session->flashdata("success")?>
        </div>
      <?php }?>
      <?php if ($this->session->flashdata("error")){?>
        <div class="text-center alert alert-danger alert-dismissable">
          <?= $this->session->flashdata("error")?>
        </div>
      <?php }?>

      <div class="as-table">
        <table class="table table-striped" id="datatableId">
          <thead>
          <tr>
            <th scope="col">Name</th>
            <th scope="col">Email</th>
            <th scope="col">Phone</th>
            <th scope="col">Account Type</th>
            <th scope="col">Current Status</th>
            <th scope="col">Action</th>
          </tr>
          </thead>
          <tbody>

          <?php if(isset($user) && $user!=400) {
            foreach($user as $row){
              ?>
              <tr id="row_<?php echo $row->id;?>">
                <td><?php echo $row->name;?> </td>
                <td><?php echo $row->email;?></td>
                <td><?php echo $row->number;?></td>
                <!--                <td>--><?php //echo $row->status; ?><!--</td>-->
                <td id="status_<?php echo $row->id;?>">

                  <select onchange="chng(16,<?php echo $row->id;?>,this);" required name="acc_type" id="acc_type" class='form-control'>
                    <option disabled selected >Change Account Type</option>
                    <option <?=$row->account_type==1?"selected='Selected'":"" ?> value="1">Dealer</option>
                    <option <?=$row->account_type==2?"selected='Selected'":"" ?> value="2">Staff</option>
                  </select>

                </td>

                <td id="status_<?php echo $row->id;?>"><?php if($row->status==1){?> <a onclick="active_row(16,<?php echo $row->id;?>,1)" class="btn btn-success btn-sm"><i class="fa  fa-thumbs-o-up"></i> Active</a><?php }else{ ?><a onclick="active_row(16,<?php echo $row->id;?>,0)" class="btn btn-danger btn-sm"><i class="fa  fa-thumbs-o-down"></i> In-active</a><?php } ?></td>
                <td>
                  <a onclick="delete_row(16,<?php echo $row->id;?>)" class="btn btn-danger btn-sm"><i class="fa   fa-trash"></i></a></td>
              </tr>
            <?php } }else{ ?>
            <tr><td colspan="8"><h3 class="text-center">No Data Found</h3></td></tr>
          <?php } ?>

          </tbody>
        </table>

        <!-- Modal HTML -->
        <div id="myModal" class="modal fade">
          <div class="modal-dialog modal-confirm">
            <div class="modal-content">
              <div class="modal-header">
                <div class="icon-box">
                  <i class="material-icons">&#xE5CD;</i>
                </div>
                <h4 class="modal-title">Are you sure?</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
              </div>
              <div class="modal-body">
                <p>Do you really want to delete the record? This process cannot be undone.</p>
              </div>
              <div class="modal-footer">
                <a type="button"  class="btn btn-info" data-dismiss="modal">Cancel</a>
                <a id="form_delete_id" href="<?=base_url('f_delete/')?>" type="button" class="btn btn-danger">Delete</a>
              </div>
            </div>
          </div>
        </div>


      </div><!-- /.box-body -->
      <div class="box-footer">

      </div>
      <!-- /.box-footer-->
    </div>
    <!-- /.box -->


  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->



