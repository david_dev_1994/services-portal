
 <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Add New Education
       
      </h1>
     
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title"></h3>

          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                    title="Collapse">
              <i class="fa fa-minus"></i></button>
            <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
              <i class="fa fa-times"></i></button>
          </div>
        </div>
        <div class="box-body">
          <div class="col-md-6 col-md-offset-3">
            <?php $notification=$this->session->flashdata('notification');
            if($notification){ ?>            
            <div id="snackbar"><?php echo $notification;?></div>
            <script type="text/javascript">$(document).ready(function(){
              notificationFun();
            });</script>
              
              <?php }?>
              <?php echo validation_errors();?>
              <form data-toggle="validator" role="form" method="post">
                  <div class="form-group">
                    <label class="control-label" for="institute">Institute/Board/University</label>

                    <input class="form-control" data-minlength="3" data-error="Must enter minimum of 3 characters" id="institute" name="institute" value="<?php echo set_value('institute');?>" placeholder="Institute/Board/University"  type="text" required />
                    <div class="help-block with-errors"><?php echo form_error('institute');?></div>
                  </div>
                  <div class="form-group">
                    <label class="control-label" for="course">Course</label>
                    <input class="form-control"  id="course" name="course" data-minlength="3" data-error="Must enter minimum of 3 characters" value="<?php echo set_value('course');?>" placeholder="Course"  type="text" required />
                    <div class="help-block with-errors"><?php echo form_error('course');?></div>
                  </div>
                  <div class="form-group">
                    <label class="control-label" for="subject">Subject</label>
                    <input class="form-control"  id="subject" name="subject" data-minlength="3" data-error="Must enter minimum of 3 characters"  value="<?php echo set_value('subject');?>" placeholder="Subject"  type="text" required />
                    <div class="help-block with-errors"><?php echo form_error('subject');?></div>
                  </div>
                  <div class="form-group">
                    <label class="control-label" for="start_date">Start Date</label>
                    <input class="form-control"  id="start_date" name="start_date" value="<?php echo set_value('start_date');?>"   type="date" required />
                    <div class="help-block with-errors"><?php echo form_error('start_date');?></div>
                  </div>
                   <div class="form-group">
                    <label class="control-label" for="end_date">End Date</label>
                    <input class="form-control"  id="end_date" name="end_date" value="<?php echo set_value('end_date');?>"   type="date"  />
                    <div class="help-block with-errors"><?php echo form_error('end_date');?></div>
                  </div>
                   
                   <div class="form-group">
                    <label class="control-label" for="score">Score(%)</label>
                    <input class="form-control"  id="score" name="score" value="<?php echo set_value('score');?>" placeholder="Score(%)"  type="number" min="0" max="100" step="0.01" required />
                    <div class="help-block with-errors"><?php echo form_error('score');?></div>
                  </div>
                  <div class="form-group">
                         <button name="save_education" class="btn btn-primary" type="submit">
                             Save
                         </button>
                     </div>
              </form>
            </div>

        </div>
        <!-- /.box-body -->
        <div class="box-footer">
        
        </div>
        <!-- /.box-footer-->
      </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->


