 <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        User Profile
      </h1>     
    </section>

    <!-- Main content -->
    <section class="content">

        <?php if ($this->session->flashdata('error')){?>
            <div class="text-center alert alert-danger alert-dismissable">
                <?= $this->session->flashdata("error")?>
            </div>
        <?php }?>

      <div class="row">
        <div class="col-md-3">

          <!-- Profile Image -->
          <div class="box box-primary">
            <div class="box-body box-profile">
              <img class="profile-user-img img-responsive img-circle" src="<?php echo base_url();?>/assets/images/<?php echo $this->session->userdata('user_image');?>" alt="User profile picture">

              <h3 class="profile-username text-center"><?php echo $this->session->userdata('user_name');?></h3>

              <p class="text-muted text-center"><?php echo $this->session->userdata('user_tag_name');?></p>

              

              <!-- <a href="#" class="btn btn-primary btn-block"><b>Follow</b></a> -->
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->

         
        </div>
        <!-- /.col -->
        <div class="col-md-9">
          <div class="nav-tabs-custom">
            <ul class="nav nav-tabs rptabs">
              <!-- <li class=""><a href="#resume" data-toggle="tab">Resume</a></li>-->
              <li class='active'><a href="#settings" data-toggle="tab">Settings</a></li>
              <li ><a href="#profile_image" data-toggle="tab">Profile Image</a></li>
              <li><a href="#password" data-toggle="tab">Password</a></li>
             
              
            </ul>
            <div class="tab-content">
              <div class=" tab-pane " id="resume">
                <p class="help-block">Visit your resume at below link.</p>
                <a target="_blank" href="<?php echo base_url().'/user/'.$this->session->userdata('user_login');?>"><?php echo base_url().'/user/'.$this->session->userdata('user_login');?></a>

              </div>
              <div class=" tab-pane " id="profile_image">
                <form method="post"  enctype="multipart/form-data" data-toggle="validator" role="form">
               <div class="form-group">
                  <label for="user_image">Profile Image</label>
                  <input type="file" id="user_image" name="user_image" onchange="readURL(this)" accept="image/*" required >
                  <p class="help-block with-errors"></p>
                  
                  <p class="help-block"></p>
                   <button type="submit" class="btn btn-success" name="save_user_image">Submit</button>
                  </div>
                  <div class="form-group ">
                    <div class="col-sm-offset-2 col-sm-10">
                      
                    </div>
                  </div>
                </form>
                <div class="form-group"><img id="img_preview" src=""></div>
              </div>
              <!-- /.tab-pane -->
             

                <div class="tab-pane" id="password">

                <form class="form-horizontal" id="old_password_form" data-toggle="validator" role="form" method="post">
                  <div class="form-group">
                    <label for="user_old_pass" class="col-sm-2 control-label">Old Password</label>

                    <div class="col-sm-10">
                      <input type="password" class="form-control" id="user_old_pass" name="user_old_pass" placeholder="Enter Old Password" value="" required>
                    </div>
                    <p class="help-block with-errors"><?php echo form_error('user_old_pass');?></p>
                  </div>
                 
                  <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                      <button type="submit" class="btn btn-warning" name="check_old_password">Submit</button>
                    </div>
                  </div>
                </form>
                 <form class="form-horizontal" id="new_password_form" data-toggle="validator" role="form" method="post" style="display: none;">
                  <div class="form-group">
                    <label for="new_user_pass" class="col-sm-2 control-label">New Password</label>

                    <div class="col-sm-10">
                      <input type="password" class="form-control" id="new_user_pass" name="user_pass" placeholder="New Password" value="" required>
                    </div>
                    <p class="help-block with-errors"><?php echo form_error('user_pass');?></p>
                  </div>
                  <div class="form-group">
                    <label for="user_conf_pass" class="col-sm-2 control-label">Confirm Password</label>

                    <div class="col-sm-10">
                      <input type="password" class="form-control" id="user_conf_pass" name="user_conf_pass" placeholder="Confirm Password" value="" required>
                    </div>
                    <p class="help-block with-errors"><?php echo form_error('user_conf_pass');?></p>
                  </div>
                 
                  <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                      <button type="submit" class="btn btn-success" name="new_password">Submit</button>
                    </div>
                  </div>
                </form>

              </div>
              <!-- /.tab-pane -->



              <div class="tab-pane active" id="settings">
                <form class="form-horizontal profile_form" data-toggle="validator" role="form" method="post">
                  <div class="form-group">
                    <label for="user_firstname" class="col-sm-2 control-label">First Name</label>

                    <div class="col-sm-10">
                      <input type="text" class="form-control" id="user_firstname" name="user_firstname" placeholder="First Name" value="<?php if($user_about_data){echo $user_about_data->user_firstname;}else{ echo set_value('user_firstname');}?>" data-minlength="3" data-error="Must enter minimum of 3 characters" required>
                    </div>
                    <div class="help-block with-errors"></div>
                    <?php echo form_error('user_firstname');?>
                  </div>
                   <div class="form-group">
                    <label for="user_lastname" class="col-sm-2 control-label">Last Name</label>

                    <div class="col-sm-10">
                      <input type="text" class="form-control" id="user_lastname" name="user_lastname" placeholder="Last Name" value="<?php if($user_about_data){echo $user_about_data->user_lastname;}else{ echo set_value('user_lastname');}?>" required>
                    </div>
                    <p class="help-block with-errors"></p>
                    <?php echo form_error('user_lastname');?>
                  </div>
                  <div class="form-group">
                    <label for="user_email" class="col-sm-2 control-label">Email</label>

                    <div class="col-sm-10">
                      <input type="email" class="form-control" id="user_email" name="user_email" placeholder="Email" value="<?php if($user_about_data){echo $user_about_data->user_email;}else{ echo set_value('user_email');}?>" required>
                    </div>
                    <p class="help-block with-errors"></p>
                    <?php echo form_error('email');?>
                  </div> 
                  <div class="form-group">
                    <label for="user_tag_name" class="col-sm-2 control-label">Tag Name</label>

                    <div class="col-sm-10">
                      <input type="text" class="form-control" id="user_tag_name" name="user_tag_name" placeholder="Tag Name" value="<?php if($user_about_data){echo $user_about_data->user_tag_name;}else{ echo set_value('user_tag_name');}?>" required>
                    </div>
                    <p class="help-block with-errors"></p>
                    <?php echo form_error('email');?>
                  </div>              
                  <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                      <button type="submit" class="btn btn-success" name="save_profile">Submit</button>
                    </div>
                  </div>
                </form>
              </div>
              <!-- /.tab-pane -->

              
            </div>
            <!-- /.tab-content -->
          </div>
          <!-- /.nav-tabs-custom -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->


<?php 
$old_pass_confirmed=$this->session->flashdata('old_pass_confirmed');
if($old_pass_confirmed){
?>
<script type="text/javascript">
  $(document).ready(function(){
        $("#old_password_form").hide();
        $("#new_password_form").show();
    });
</script>
<?php
}else{
?>
<script type="text/javascript">
  $(document).ready(function(){
        $("#old_password_form").show();
        $("#new_password_form").hide();
    });
</script>
<?php  
}

$active_pan=$this->session->flashdata('active_pan');
if($active_pan){
?>
<script type="text/javascript">
  $(document).ready(function(){
          
        $('.nav-tabs a[href="#<?php echo $active_pan;?>"]').tab('show');      
    });
</script>
<?php  
}
$notification=$this->session->flashdata('notification');
            if($notification){ ?>            
            <div id="snackbar"><?php echo $notification;?></div>
            <script type="text/javascript">$(document).ready(function(){
              notificationFun();
            });</script>
              
<?php }?>
<?php
$old_pass_confirmed_error=$this->session->flashdata('old_pass_confirmed_error');
            if($old_pass_confirmed_error){ ?>            
            <div id="snackbar"><?php echo $old_pass_confirmed_error;?></div>
            <script type="text/javascript">$(document).ready(function(){
              notificationFun();
            });</script>
              
<?php }?>


<script type="text/javascript">
      function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#img_preview')
                        .attr('src', e.target.result);
                };

                reader.readAsDataURL(input.files[0]);
            }
        }
</script>

<style type="text/css">
  .form-group.has-error .help-block{
    text-align: center;
  }
</style>