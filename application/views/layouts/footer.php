<script>
    var base_url="<?= base_url(); ?>";
</script>
<style>
body{
font-family: 'Open Sans', sans-serif;
}
.row {
    margin-right: 0px;
    margin-left: 0px;
}
/**Nav bar**/

#header{
height:75px;
}

.navbar-toggler{
background-color:#fff;
}

.navbar {
    margin-top: 0;
}
.header_menu{
height:0;
}

.navbar-brand>img {    
    max-width: 100px;
}
#header .header_menu a {
    color: #fff;
    transition: all .2s;
}
#header .header_menu a:hover {
    color: #00a6d5;
}


/***Carousel**/
.carousel-caption{
bottom:30%;
}
.carousel-caption a {
    color: #fff;
    padding: 10px 20px;
    border-radius: 5px;
    border: 1px solid #fff;
    transition: all .3s;
text-decoration:none;
}
.carousel-caption a:hover {
    background: #337ab7;
    color: #fff;
    border-color: #337ab7;
}
.carousel-caption h3 {
    margin-bottom: 30px;
font-size:50px;
text-transform:uppercase;
}
.carousel-control.left, .carousel-control.right{
    background-image:none;
}

.carousel-inner .item{
max-height:400px;
}
.carousel-inner .item img{
min-height:400px;
}



.main-conent__container>div {
    float: none;
}

.main-conent__hp-description.top {
    background: #337ab7;
color:#fff;
}
.main-conent__hp-description.top .homeheading{
margin:0 auto;
}
.main-conent__hp-description.top a{
color: #fff;
    border: 1px solid #fff;
    padding: 10px 20px;
    border-radius: 5px;
    margin: 20px;
text-decoration:none;
    transition: all 0.5s;
}
.main-conent__hp-description.top a:hover{
background:#fff;
color:#337ab7
}
.main-conent__hp-description.top p{
color:#fff;
}


.services h1{
    margin-bottom: 30px;
font-weight: 600;
}

.icon-wrap {
    width: 82px;
    height: 82px;
    text-align: center;
    -webkit-border-radius: 50%;
    -moz-border-radius: 50%;
    border-radius: 50%;
    -webkit-transition: all 0.5s;
    -moz-transition: all 0.5s;
    -ms-transition: all 0.5s;
    -o-transition: all 0.5s;
    transition: all 0.5s;
}
.icon-wrap {
    border: solid 2px #337ab7;
}
.icon-wrap i {
    font-size: 38px;
    line-height: 78px;
    -webkit-transition: all 0.5s;
    -moz-transition: all 0.5s;
    -ms-transition: all 0.5s;
    -o-transition: all 0.5s;
    transition: all 0.5s;
    padding-left: 2px;
}
.servks{
margin:20px auto;
}
.servks a{
text-decoration:none;

}
.servks:hover .icon-wrap{
background:#337ab7;
}
.servks:hover .icon-wrap i{
color:#fff;
}

.servks h2 {
    color: #444;
    font-size: 25px;
    text-transform: capitalize;
}

.before-footer{
background-color: #222;
}

.homeheading {
    background: transparent;
    padding: 1%;
    box-shadow: none;
    color: #ddd;
}
.main-conent__hp-description.bottom a{
color: #fff;
    border: 1px solid #fff;
    padding: 10px 20px;
    border-radius: 5px;
    margin: 20px;
text-decoration:none;
    transition: all 0.5s;
}
.main-conent__hp-description.bottom a:hover{
background:#fff;
color:#337ab7
}

 .footercolor{     
       
    text-align: left;
 }
 .footercolor p,.footercolor h3,.footercolor h5{
      color:#fff
   }
.footercolor p {
    font-size: 15px;
}

.footercolor h3 {
    margin-left: 0;
    color: #fff;
    font-size: 16px;
    font-weight: 600;
    margin-bottom: 15px;
    padding-left: 0;
    text-transform: uppercase;
}

.container-fluid.footerks {
    background: #141414;
    clear: both;
text-align: center;
   padding: 20px 10%;

}

ul.footerul {
    padding-left: 10px;
}
li.footerli a{
text-decoration:none;
color:#fff;
transition: all .2s;
}
li.footerli a:hover{
color: #00a6d5;
}

li.footerli a:before {
    content: "\f0da";
    display: inline-block;
    color: #1aa9d4;
    font-family: FontAwesome;
    position: absolute;
    left: 15px;
}

.copy-right {
    text-align: right;
}
.copy-right p {
    font-size:16px;
color:#ddd;
}

img.iso-logo {
    width: 40% !important;
    margin-right: 25px !important;
}

img.jas-anz-logo {
    width: 20% !important;
}

@media only screen and (min-width: 768px) {
  #header .header_menu a {    
    font-weight: 600;
}
}



</style>
<footer>
    <div class="block no-padding">
        <div class="container">
            <div class="row">
                <div class="col-lg-4">
                    <h3 class="widget-title"><span>Navigation Links</span></h3>
                    <ul id="menu-footer-navigation-1" class="menu">
                        <li class="menu-item"><a href="https://www.bktechnologies.com/">Home</a></li>
                        <li class="menu-item"><a href="https://www.bktechnologies.com/about-us/">About Us</a></li>
                        <li class="menu-item"><a href="https://www.bktechnologies.com/agencies/">Solutions</a></li>
                        <li class="menu-item"><a href="https://www.bktechnologies.com/bk-sales/">Where To Buy</a></li>
                        <li class="menu-item"><a href="https://bktechnologies.com/service-portal/" class="gtrackexternal">Support</a></li>
                        <li class="menu-item"><a href="https://www.bktechnologies.com/press/">Press</a></li>
                        <li class="menu-item"><a href="https://www.bktechnologies.com/investor-relations/">Investor Relations</a></li>
                    </ul>
                </div>
                <div class="col-lg-4">
                    <h3 class="widget-title"><span>Connect With Us</span></h3>
                    <ul class="ut-sociallinks">
                        <li><a href="https://www.facebook.com/BK-Technologies-106082389889629/" target="_blank" class="gtrackexternal"><i class="fa fa-facebook"></i></a></li>
                        <li><a href="https://www.instagram.com/bktechnologies/" target="_blank" class="gtrackexternal"><i class="fa fa-instagram"></i></a></li>
                        <li><a href="https://www.linkedin.com/company/bk-technologies" target="_blank" class="gtrackexternal"><i class="fa fa-linkedin"></i></a></li>
                        <li><a href="https://twitter.com/BKTechUSA" target="_blank" class="gtrackexternal"><i class="fa fa-twitter"></i></a></li>
                        <li><a href="https://www.youtube.com/channel/UCMKSiuxlKiXn5YV04Vh4xgw" target="_blank" class="gtrackexternal"><i class="fa fa-youtube"></i></a></li>
                    </ul>
                    
                    <h3 class="widget-title"><span>Certifications</span></h3>
                    <div class="textwidget">
                        <img class="iso-logo" src="https://www.bktechnologies.com/wp-content/uploads/2019/04/ISO-9001.jpg">
                        <img class="jas-anz-logo" src="https://www.bktechnologies.com/wp-content/uploads/2019/04/JAS-ANZ-Logo.jpg"></p>
                    </div>
                    
                </div>
                <div class="col-lg-4">
                    <h3 class="widget-title"><span>Company Resources</span></h3>
                    <div class="menu-footer-navigation-2-container">
                        <ul id="menu-footer-navigation-2" class="menu">
                            <li class="menu-item"><a href="https://www.bktechnologies.com/ourevents/">Community Events</a></li>
                            <li class="menu-item"><a href="https://www.bktechnologies.com/career/">Careers</a></li>
                            <li class="menu-item"><a href="https://www.bktechnologies.com/contact-us/">Contact Us</a></li>
                            <li class="menu-item"><a href="https://www.bktechnologies.com/privacy-policy/">Privacy Policy</a></li>
                            <li class="menu-item"><a href="https://www.bktechnologies.com/terms-of-service/">Terms Of Service</a></li>
                            <li class="menu-item"><a href="https://www.bktechnologies.com/html-sitemap/">Sitemap</a></li>
                            <li class="menu-item"><a href="https://www.bktechnologies.com/508-accessibility-template/">508 Compliance</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer><!-- FOOTER -->

<div class="bottom-footer">
    <div class="container">
        <div class="row">
            <div class="col-lg-6">
                <p>© 2019 BK Technologies<span style="vertical-align: text-bottom;font-size: 12px !important;">®</span></p>
            </div>
            <div class="col-lg-6">
                <p style="float:right;">Web Design By <a href="http://theadleaf.com/" target="_blank">The AD Leaf</a></p>
            </div>
        </div>
    </div>
</div><!-- Bottom Footer -->    



<script src='<?=base_url("node_modules/jquery/dist/jquery.js")?>'></script>
<script src='<?=base_url("node_modules/bootstrap/dist/js/bootstrap.js")?>'></script>
<script src='<?=base_url("assets/js/bootstrap-4-navbar.js")?>'></script>
<script src="<?php echo base_url('assets/plugins/jQuery-Mask-Plugin-master/src/jquery.mask.js'); ?>" type="text/javascript"></script>
<script src="<?php echo base_url('assets/plugins/jQuery-Mask-Plugin-master/dist/jquery.mask.js'); ?>" type="text/javascript"></script>
<script src='<?=base_url("assets/js/script.js")?>'></script>
<script src='<?=base_url("assets/js/countries.js")?>'></script>
<script language="javascript">
    populateCountries("country", "state"); // first parameter is id of country drop-down and second parameter is id of state drop-down
</script>


    <script>

     $(".ksdropdown").click(function(){
        var url=$(this).attr("href");

window.open(url,'_blank');

});
   </script>
<script src='https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js'></script>


<style>
html,body,p,a,li,h1,h2,h3,h4,h5,h6,span,i{
font-family: 'Open Sans', sans-serif;
}
</style>
<script>
    $(document).ready( function () {
        if($('#bkmanualstable').length > 0){
            $('#bkmanualstable').DataTable();
        }

    } );

</script>


</body>
</html>