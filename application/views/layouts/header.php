<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" type="image/png" href="https://www.bktechnologies.com/wp-content/uploads/2018/02/bk-favicon.png"/>
    <title>BK Technologies - Service Portal</title>
    <link rel="stylesheet" href='<?=base_url("node_modules/bootstrap/dist/css/bootstrap.css")?>'>
    <link rel="stylesheet" href='<?=base_url("node_modules/font-awesome/css/font-awesome.css")?>'>
    <link rel="stylesheet" href='<?=base_url("assets/css/dropdown.css")?>'>
    <link rel="stylesheet" href='<?=base_url("assets/css/style.css")?>'>
      <link rel="stylesheet" href='<?=base_url("assets/css/custom.css")?>'>
    <link rel="stylesheet" href='https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css'>
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <script src='<?=base_url("assets/js/jquery.multifield.min.js")?>'></script>
<link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,700" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Poppins" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Lato:300,300i,400,400i,700,700i" rel="stylesheet">


<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-4759378-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-4759378-1');
</script>


</head>
<body>
<header id="header">
    <nav id="menu-wrap" class="navbar navbar-expand-md navbar-light btco-hover-menu">
        <div class="container">
            <a class="navbar-brand" href="<?=base_url()?>"><img class="pad" src="https://bktechnologies.com/service-portal/assets/bk-logo-big-png.png" alt="BK Technology"></a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNavDropdown">
 <?php
 $accountType='';
 $type=0;

                            if($this->session->isLoggedIn){
                              if ($this->session->loggedUserAccountType=="Government Federal") {
                                $accountType="AHS";
                                $type=1;
                                ?>
                                <style type="text/css">
                                  #mainmanuks{
                                    display: none;
                                  }
                                </style>
                                <?php
                              }
                              if ($this->session->loggedUserAccountType=="Relm/BK Radio Dealer") {
                                $accountType="Dealer";
                                $type=1;
                              }
                              if ($this->session->loggedUserAccountType=="Government State") {
                                $accountType="Client";
                                $type=1;
                                ?>
                                <style type="text/css">
                                  #mainmanuks{
                                    display: none;
                                  }
                                </style>
                                <?php

                              }
                              if ($this->session->loggedUserAccountType=="Government Local") {
                                $accountType="Calfire";
                                $type=1;
                              }
                              
                        ?>
<center><h4 class='text text-warning' onclick="showMainMenu(1)">You are logged in as Demo <?php echo $accountType; ?></h4>

</center>

<?php } ?>
                <ul class="navbar-nav header_menu" id='mainmanuks'>

                    <!--change begins-->
                    <li class="nav-item">
                        <a class="nav-link" href="<?=base_url("/tech_view")?>">Support/Technical Documentation</a>
                    </li>

                    <li class="nav-item">
                        <a class="nav-link" href="<?=base_url("/factory-service")?>">Repair</a>
                    </li>

<!--                    <li class="nav-item">-->
<!--                        <a class="nav-link" href="--><?php //echo base_url();?><!--factory-service/warranty">Warranty</a>-->
<!--                    </li>-->

                    <li class="nav-item">
                        <a class="nav-link" href="<?=base_url("/products")?>">Firmware/Software/Drivers</a>
                    </li>

                    <li class="nav-item">
                        <a class="nav-link" href="<?=base_url("contact")?>">Contact</a>
<ul class="dropdown-menu">
<li><a class="dropdown-item main-link" href="<?=base_url("contact")?>">Tech Support</a></li>
<li><a class="dropdown-item main-link" href="<?=base_url("agency_portals")?>">Agency Portals</a></li>
<!--<li><a class="dropdown-item sub-link" target="_blank" href="https://bktechnologies.com/service-portal/Alberta">Alberta</a>
<li><a class="dropdown-item sub-link" target="_blank" href="https://bktechnologies.com/service-portal/CalFire">CalFire</a>
<li><a class="dropdown-item sub-link" target="_blank" href="https://bktechnologies.com/service-portal/flfs">Florida Forestry</a>
<li><a class="dropdown-item sub-link" target="_blank" href="https://bktechnologies.com/service-portal/usfs">USFS</a>-->
</ul>
                    </li>

                    <!--change ends-->
                    <?php
                        if($_SERVER["REQUEST_URI"] === "/my-page"):
                    ?>
                        <li class="nav-item">
                            <a class="nav-link" href="/logout">Logout</a>
                        </li>
                    <?php
                        endif;
                    ?>
                </ul>

                <!-- after Login menu -->
                <?php
                   if($this->session->isLoggedIn){
                    if ($this->session->loggedUserAccountType=="Government Federal") {
                        ?>
                <ul class="navbar-nav header_menu">
                    <li class="nav-item active">
                        <a class="nav-link dropdown-toggle" href="https://bootstrapthemes.co" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Products<span class="sr-only">(current)</span>
                        </a>
                        
                        
                                <ul class="dropdown-menu">
                                  <li><a class="dropdown-item" target="_blank" href="https://www.bktechnologies.com/product/kng-mobile-raadio/">KNG-M800 Mobile</a></li>
                                    <li><a class="dropdown-item" target="_blank" href="https://www.bktechnologies.com/product/kng-base-station/">KNG-B800 Base</a></li>
                                </ul>
                              
                    </li>
                    <li class="nav-item active">
                        <a class="nav-link dropdown-toggle" href="https://bootstrapthemes.co" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Support<span class="sr-only">(current)</span>
                        </a>
                        <ul class="dropdown-menu">
                                  <li><a class="dropdown-item" target="_blank" href="<?=base_url("download-codeplug")?>">Download Codeplug</a></li>
                                    <li><a class="dropdown-item" target="_blank" href="<?=base_url("upload-codeplug")?>">Upload Codeplug</a></li>
                                </ul>
                    </li>

                    <li class="nav-item active">
                        <a class="nav-link dropdown-toggle" href="https://bootstrapthemes.co" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Documentation<span class="sr-only">(current)</span>
                        </a>
                        <ul class="dropdown-menu">
                                  <li><a class="dropdown-item" target="_blank" href="<?=base_url("download-manuals")?>">Download Manuals</a></li>
                                    <li><a class="dropdown-item" target="_blank" href="<?=base_url()?>assets/Relm_amb_radio_install_guide.pdf">Service Installation</a></li>
                                    <li><a class="dropdown-item" target="_blank" href="<?=base_url()?>assets/Trunked_Radio_Programming_Guide_Rev-7.pdf">Trunking Guide</a></li>
                                    <li><a class="dropdown-item" target="_blank" href="<?=base_url()?>assets/KAA0101 Battery Field Capacity Testing.pdf">CADEX-Battery Field Capacity Testing</a></li>
                                    <li><a class="dropdown-item" target="_blank" href="<?=base_url("Esn-spread-sheet")?>">ESN Spread Sheet</a></li>
                                </ul>
                    </li>
                    
                   
                        <li class="nav-item">
                            <a class="nav-link" href="/logout">Logout</a>
                        </li>
                   
                </ul>
                <?php
              }
              if ($this->session->loggedUserAccountType=="Government State") {
                ?>
                <ul class="navbar-nav header_menu" id=''>
                    <li class="nav-item active">
                        <a class="nav-link dropdown-toggle" href="https://bootstrapthemes.co" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Products<span class="sr-only">(current)</span>
                        </a>
                       
                        <ul class="dropdown-menu"  aria-labelledby="navbarDropdownMenuLink">
                            <li><a class="dropdown-item dropdown-toggle ksdropdown" href="https://www.bktechnologies.com/product-category/digital/" >Digital Radios</a>
                                <ul class="dropdown-menu">
                                    <li><a class="dropdown-item dropdown-toggle ksdropdown" href="https://www.bktechnologies.com/product-category/portables/">Portables</a>
                                        <ul class="dropdown-menu">
                                            <li><a class="dropdown-item" href="https://www.bktechnologies.com/product-category/portables/" target="_blank">KNG</a></li>
                                            <li><a class="dropdown-item" href="https://www.bktechnologies.com/product-category/portables/" target="_blank">Standart DPH</a></li>
                                            <li><a class="dropdown-item" href="https://www.bktechnologies.com/product-category/portables/" target="_blank">Command DPH</a></li>
                                        </ul>
                                    </li>
                                    <li><a class="dropdown-item dropdown-toggle ksdropdown" href="https://www.bktechnologies.com/product-category/mobiles/" >Mobiles</a>
                                        <ul class="dropdown-menu">
                                            <li><a class="dropdown-item" href="https://www.bktechnologies.com/product-category/mobiles/" target="_blank">KNG</a></li>
                                            <li><a class="dropdown-item" href="https://www.bktechnologies.com/product-category/mobiles/" target="_blank">DMHX</a></li>
                                        </ul>
                                    </li>
                                    <li><a class="dropdown-item" target="_blank" href="https://www.bktechnologies.com/product-category/base-stations/">Base Stations</a></li>
                                    <li><a target="_blank" class="dropdown-item" href="https://www.bktechnologies.com/product-category/repeaters/">Repeaters</a></li>

                                </ul>
                            </li>
                            <li><a class="dropdown-item dropdown-toggle ksdropdown" href="https://www.bktechnologies.com/product-category/analog/">Analog Radios</a>
                                <ul class="dropdown-menu">
                                    <li><a class="dropdown-item" target="_blank" href="https://www.bktechnologies.com/product-category/portables/">Portables</a></li>
                                    <li><a class="dropdown-item" target="_blank" href="https://www.bktechnologies.com/product-category/mobiles/">Mobiles</a></li>
                                    <li><a class="dropdown-item" target="_blank" href="https://www.bktechnologies.com/product-category/base-stations/">Base Stations</a></li>
                                    <li><a class="dropdown-item" target="_blank" href="https://www.bktechnologies.com/product-category/repeaters/">Repeaters</a></li>
                                </ul>
                            </li>
                            <li><a class="dropdown-item dropdown-toggle ksdropdown" href="https://www.bktechnologies.com/product-category/business-industrial/">Business Radios</a>
                                <ul class="dropdown-menu">
                                    <li><a class="dropdown-item" target="_blank" href="https://www.bktechnologies.com/product-category/portables/">Portables</a></li>
                                    <li><a class="dropdown-item" target="_blank" href="https://www.bktechnologies.com/product-category/mobiles/">Mobiles</a></li>
                                </ul>
                            </li>
                            </ul>
                           
                        
                    </li>
                     
                    <li class="nav-item">
                        <a class="nav-link dropdown-toggle" href="https://bootstrapthemes.co" id="navbarDropdownMenuLink" data-toggle="dropdown" target="_blank" aria-haspopup="true" aria-expanded="false">
                            Factory Service
                        </a>
                        <ul class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                            <li><a class="dropdown-item " target="_blank" href="<?=base_url("factory-service/service-dealers")?>">Service Home</a></li>
                            <li><a class="dropdown-item dropdown-toggle ksdropdown" href="<?=base_url("factory-service")?>">Repair</a>
                                <ul class="dropdown-menu">
                                    <li><a class="dropdown-item dropdown-toggle" href="<?=base_url("factory-service/service-dealers")?>">Factory Service</a>
                                        <ul class="dropdown-menu">
                                            <li><a class="dropdown-item" target="_blank" href="<?=base_url("/factory-service")?>">Policies</a></li>
                                            <li><a class="dropdown-item" target="_blank" href="<?=base_url("/factory-service/pricing")?>">Pricing</a></li>
                                            <li><a class="dropdown-item" target="_blank" href="<?=base_url("")?>assets/pdf/Serviceform2013.pdf">Service Form</a></li>
                                        </ul>
                                    </li>
                                    <li><a class="dropdown-item" target="_blank" href="<?=base_url("/factory-service/service-dealers")?>">Serice Centers</a></li>
                                    <li><a class="dropdown-item" target="_blank" href="https://www.bktechnologies.com/dealer-search/">Dealers</a></li>
                                    <li><a class="dropdown-item" target="_blank" href="<?=base_url("/factory-service/warranty")?>">Warranty Info</a></li>
                                    <li><a class="dropdown-item" target="_blank" href="<?=base_url("/parts")?>">Replacement Parts</a></li>
                                </ul>
                            </li>
                            <li><a class="dropdown-item dropdown-toggle ksdropdown" href="<?=base_url("/products")?>">Radio Editors</a>
                                <ul class="dropdown-menu">
                                    <li><a class="dropdown-item" target="_blank" href="<?=base_url("/products")?>">Current Releases</a></li>
                                    <li><a class="dropdown-item" target="_blank" href="<?=base_url("/products")?>">Updates</a></li>
                                </ul>
                            </li>
                            <li>
                                <a class="dropdown-item dropdown-toggle ksdropdown" href="<?=base_url("/products")?>">Firmware</a>
                                <ul class="dropdown-menu">
                                    <li><a class="dropdown-item" target="_blank" href="<?=base_url("/products")?>">Current Releases</a></li>
                                </ul>
                            </li>
                            <li>
                                <a class="dropdown-item dropdown-toggle ksdropdown" href="<?=base_url("Manuals/RELM-Manuals")?>">Publications</a>
                                <ul class="dropdown-menu">
                                    <li><a class="dropdown-item" target="_blank" href="<?=base_url("Manuals/RELM-Manuals")?>">Manuals</a></li>
                                    <li><a class="dropdown-item" target="_blank" href="<?=base_url("/bulletins/bk-bulletins")?>">Service Bulletin</a></li>
                                </ul>
                            </li>
                            <li>
                                <a class="dropdown-item dropdown-toggle ksdropdown" href="<?=base_url("contact/email-contact")?>">Contact Service</a>
                                <ul class="dropdown-menu">
                                    <li><a class="dropdown-item" target="_blank" href="<?=base_url("/contact/email-contact")?>">Comment/Question</a></li>
                                    <li><a class="dropdown-item" target="_blank" href="<?=base_url("/forums")?>">Tech Forums</a></li>
                                </ul>
                            </li>
                        </ul>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link dropdown-toggle" href="https://bootstrapthemes.co" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Resources
                        </a>
                        <ul class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                            <li><a class="dropdown-item" href="https://www.bktechnologies.com/ourevents/" target="_blank">Events</a></li>
                            <li><a class="dropdown-item" target="_blank" href="https://www.bktechnologies.com/contact-us/">Contracts</a></li>
                            <li><a class="dropdown-item" target="_blank" href="https://www.bktechnologies.com/category/press-room/">Press Room</a></li>
                        </ul>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link dropdown-toggle" href="https://bootstrapthemes.co" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Where to Buy
                        </a>
                        <ul class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                            <li><a class="dropdown-item" target="_blank" href="https://www.bktechnologies.com/dealer-search/">Local Dealers</a></li>
                            <!--<li><a class="dropdown-item" href="#">Regional Reps</a></li>-->
                        </ul>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link dropdown-toggle" href="https://bootstrapthemes.co" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Contact
                        </a>
                        <ul class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                            <li><a class="dropdown-item" target="_blank" href="<?=base_url("contact")?>">Corporate Info</a></li>
                        </ul>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" target="_blank" href="https://www.bktechnologies.com/">BK Homepage</a>
                    </li>
                   
                    <li class="nav-item">
                        <?php
                            if(!$this->session->isLoggedIn):
                        ?>
                            <a class="nav-link" href="<?=base_url("sign-in")?>">Sign In</a>
                        <?php
                            else :
                        ?>
                            <a class="nav-link" href="<?=base_url("my-page")?>">My page</a>
                        <?php
                            endif;
                        ?>
                    </li>
                    
                        <li class="nav-item">
                            <a class="nav-link" href="/logout">Logout</a>
                        </li>
                    
                </ul>
                <?php
              }



   } ?>


            </div>
        </div>
    </nav>
</header>
