<?php
/**
 * Created by PhpStorm.
 * User: Maddy
 * Date: 04-Oct-18
 * Time: 3:49 PM
 */
?>
<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700" rel="stylesheet">
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="<?= base_url('assets/styles/css/bootstrap.min.css') ?>">
    <link rel="stylesheet" href="<?= base_url('assets/styles/css/fontawesome-all.min.css')?>">
    <link rel="stylesheet" href="<?= base_url('assets/styles/css/mdb.min.css')?>">
    <link rel="stylesheet" href="<?= base_url('assets/styles/css/style.css')?>">

    <title>BK Technologies</title>
</head>
<body>

{yield}

<footer class="main-footer">
    <div class="container">
        <p>Copyright &copy; 2018 BK Technologies,  All Rights Reserved.</p>
    </div>
</footer>




<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="<?= base_url('/assets/styles/js/jquery-3.3.1.min.js')?>"></script>
<script src="<?= base_url('/assets/styles/js/popper.min.js')?>"></script>
<script src="<?= base_url('/assets/styles/js/bootstrap.min.js')?>"></script>
<script src="<?= base_url('/assets/styles/js/mdb.min.js')?>"></script>
<script src="<?= base_url('/assets/styles/js/custom.js')?>"></script>
<script>
    if ( window.history.replaceState ) {
        window.history.replaceState( null, null, window.location.href );
    }
</script>
</body>
</html>
