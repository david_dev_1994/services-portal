<?php
/**
 * Created by PhpStorm.
 * User: Maddy
 * Date: 15-Oct-18
 * Time: 2:59 PM
 */
?>
<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700" rel="stylesheet">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="<?=base_url('assets/alberta_styling/css/bootstrap.min.css')?>">
    <link rel="stylesheet" href="<?=base_url('assets/alberta_styling/css/fontawesome-all.min.css')?>">
    <link rel="stylesheet" href="<?=base_url('assets/alberta_styling/css/mdb.min.css')?>">
    <link rel="stylesheet" href="<?=base_url('assets/alberta_styling/css/style.css')?>">
    <title>Alberta Health Services - BK Technologies</title>
</head>
<body>
<div class="header">
    <div class="header-top">
        <div class="container-fluid">
            <div class="row align-items-center justify-content-between">
                <div class="col-md-auto">
                    <div class="logo-wrap">
                        <a href="<?=base_url('Alberta')?>">
                            <img width="auto" height="85px" src="<?=base_url('assets/styles/images/alberta-logo.jpg')?>" alt="BK Logo">
                        </a>
                    </div>
                </div>
                <div class="col-md-auto">
                    <div class="header-top-content">
                        <h3 class="phone">
                            <i class="fas fa-phone"></i>
                            <a href="tel: 1-800-422-6281">1-800-422-6281</a>
                        </h3>
                        <h3 class="email">
                            <i class="fas fa-envelope"></i>
                            <a href="mailto:service@bktechnologies.com">service@bktechnologies.com</a>
                        </h3>
                    </div>
                </div>
                <div class="col-md-auto">
                    <div class="logo-wrap bk-logo-portal">
                        <a target="_blank" href="https://www.bktechnologies.com/">
                            <img src=<?=base_url("assets/styles/images/bk-logo-portal.png")?> alt="BK Logo">
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <nav class="mb-1 navbar navbar-expand-lg navbar-dark info-color">
<!--        <a class="navbar-brand" href="--><?//=base_url();?><!--">BK Technologies</a>-->
        <button class="navbar-toggler collapsed" type="button" data-toggle="collapse" data-target="#navbarSupportedContent-4" aria-controls="navbarSupportedContent-4" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="navbar-collapse collapse" id="navbarSupportedContent-4" style="">
            <ul class="navbar-nav mr-auto">

                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#Products" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Products
                    </a>
                    <ul class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                        <li><a target="_blank" class="dropdown-item" href="https://www.bktechnologies.com/product-category/portables/">Portable</a></li>
                        <li><a target="_blank" class="dropdown-item" href="https://www.bktechnologies.com/product-category/mobiles/">Mobiles</a></li>
                        <li><a target="_blank" class="dropdown-item" href="https://www.bktechnologies.com/product-category/base-stations/">Base Stations</a></li>

                    </ul>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#Support" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Support
                    </a>
                    <ul class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                        <li><a target="_blank" class="dropdown-item" href="<?=base_url('products/BK_Digital')?>">Download Firmware/Software</a></li>
                        <li><a class="dropdown-item" href="<?=base_url('download_codeplug')?>">Download Codeplug</a></li>
                        <li><a class="dropdown-item" href="<?=base_url('codeplug')?>"><?= $this->session->userdata('alb_loggedUserId') == 48 || $this->session->userdata('alb_logged_acc_type') == 2?'Upload Codeplug':'Codeplug List'; ?></a></li>
                    </ul>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#Documentation" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Documentation
                    </a>
                    <ul class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                        <li><a target="_blank" class="dropdown-item" href="<?=base_url('products/BK_Digital')?>">Download Manuals</a></li>
                        <li><a target="_blank" class="dropdown-item" href="<?php echo base_url();?>assets/documentation/Relm_amb_radio_install_guide.pdf">Service Installations</a></li>
                        <li><a target="_blank" class="dropdown-item" href="<?php echo base_url();?>assets/documentation/Trunked_Radio_Programming_Guide_Rev-7.pdf">Trunking Guide</a></li>
                        <li><a target="_blank" class="dropdown-item" href="<?php echo base_url();?>assets/documentation/KAA0101%20Battery%20Field%20Capacity%20Testing.pdf">CADEX-Battery Field Capacity Testing</a></li>
                        <li><a class="dropdown-item" href="<?=base_url('Esn-spread-sheet')?>">ESN Spread Sheet</a></li>
                    </ul>
                </li>
            </ul>
            <ul class="navbar-nav ml-auto">
                <li class="nav-item">
                    <a class="nav-link waves-effect waves-light" href="<?=base_url('Alberta')?>">
                        <i class="fas fa-home"></i> Home
                    </a>
                </li>

                <li class="nav-item">
                    <a class="nav-link waves-effect waves-light" href="<?=base_url('alb_logout')?>">
                        <i class="fas fa-power-off"></i> Logout
                    </a>
                </li>
            </ul>
        </div>
    </nav>
</div>

{yield}

<footer class="main-footer">
    <div class="container">
        <p>Copyright &copy; 2018 BK Technologies, All Right Reserved.</p>
    </div>
</footer>

<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="<?=base_url('assets/alberta_styling/js/jquery-3.3.1.min.js')?>"></script>
<script src="<?=base_url('assets/alberta_styling/js/popper.min.js')?>"></script>
<script src="<?=base_url('assets/alberta_styling/js/bootstrap.min.js')?>"></script>
<script src="<?=base_url('assets/alberta_styling/js/mdb.min.js')?>"></script>
<script src="<?=base_url('assets/alberta_styling/js/custom.js')?>"></script>
<script src="<?=base_url('assets/alberta_styling/js/myjs.js')?>"></script>
</body>

</html>
