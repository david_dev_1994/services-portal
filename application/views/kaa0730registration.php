<!DOCTYPE html>
<html lang="en">
<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" type="image/png" href="https://www.bktechnologies.com/wp-content/uploads/2018/02/bk-favicon.png"/>
    <title>BK Technologies - Service Portal</title>
    <link rel="stylesheet" href='<?= base_url("node_modules/bootstrap/dist/css/bootstrap.css") ?>'>
    <link rel="stylesheet" href='<?= base_url("node_modules/font-awesome/css/font-awesome.css") ?>'>
    <link rel="stylesheet" href='<?= base_url("assets/css/dropdown.css") ?>'>
    <link rel="stylesheet" href='<?= base_url("assets/css/style.css") ?>'>
    <link rel="stylesheet" href='<?= base_url("assets/css/custom.css") ?>'>
    <link rel="stylesheet" href='https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css'>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script src='<?= base_url("assets/js/jquery.multifield.min.js") ?>'></script>
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Poppins" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Lato:300,300i,400,400i,700,700i" rel="stylesheet">


    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-4759378-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];

        function gtag() {
            dataLayer.push(arguments);
        }

        gtag('js', new Date());

        gtag('config', 'UA-4759378-1');
    </script>


</head>
<body>

<section id="main-conent">
    <div class="container text-center" id="software_update">
        <!--<h3 class="su__header">-->
        <!--    BK Technologies<span style="vertical-align: super !important;font-size: 12px !important;">速</span> Serial Number Registration-->
        <!--</h3>-->
                    
        <div class="page-main">
            <div class="container">
<?php if (isset($msg)){?>
                        <div class="errors_container alert alert-danger alert-dismissable">
                            <?= $msg?>
                        </div>
                    <?php }?>
                    <?php if ($this->session->flashdata('success')){?>
                        <div class="alert alert-success alert-dismissable">
                            <?= $this->session->flashdata('success')?>
                        </div>
                    <?php }?>
                    <?php if ($this->session->flashdata("validation_errors")){?>
                        <div class="text-center alert alert-danger alert-dismissable">
                            <?= $this->session->flashdata("validation_errors")?>
                        </div>
                    <?php }?>
                <div class="card login-card">
                    <h5 class="card-header info-color white-text text-center py-4" style="margin-top: unset !important;">
                        <strong>Register Serial Number</strong>
                    </h5>
                    <!--Card content-->
                    <div class="card-body px-lg-5 pt-0" style="margin: 10px">
                        <!-- Form -->
                        <form class="as-login-form" action="" method="post" id="SerialNumber">
                            <div class="col-md-6 form-group">
                                <label for="username">Serial Number</label>
                                <input class='hidden' type="text" name="serialNumber" id="serial" value="" >
                                <input required  class='form-control' type="text" name="serial_number" id="serial_number" value="" >
                            </div>
                            <div class="col-md-6 form-group">
                                <label for="username">Company or Organization</label>
                                <input required class='form-control' type="text" name="company" value="<?php echo set_value('company'); ?>" >
                            </div>
                            <div class="col-md-6 form-group">
                                <label for="username">First Name</label>
                                <input required  class='form-control' type="text" name="first_name" value="<?php echo set_value('first_name'); ?>" >
                            </div>
                            <div class="col-md-6 form-group">
                                <label for="username">Last Name</label>
                                <input required class='form-control' type="text" name="last_name" value="<?php echo set_value('last_name'); ?>" >
                            </div>
                            <div class="col-md-6 form-group">
                                <label for="email">Email</label>
                                <input name="email" required type="email" id="email" class='form-control' value="<?php echo set_value('email'); ?>" >
                            </div>

                            <!-- Sign in button -->
                            <button class="btn btn-info btn-block my-4 waves-effect z-depth-0 col-md-12 form-group" id="formButton" type="submit">Submit</button>
                            <!-- Register -->
                        </form>
                        <!-- Form -->
                    </div>
                </div>

            </div>
        </div>
    </div>
</section>


<style>

    button.continue {
        border-radius: 0px !important;
        background: #00a6d5 !important;
        border-color: #00a6d5 !important;
        color: #ffffff !important;
        font-family: Lato !important;
        text-transform: uppercase !important;
        font-size: 14px !important;
        display: inline-block !important;
        font-weight: 600 !important;
        text-align: center !important;
        padding: 5px 18px !important;
        transition: padding 0.5s ease, background 0.5s ease, border-color 0.5s ease, color 0.5s ease !important;
        border: none !important;
    }

    button.continue:hover {
        background: #b90000 !important;
    }

    input {
        padding: 3.5px 5px !important;
    }

</style>

<script src='<?=base_url("node_modules/jquery/dist/jquery.js")?>'></script>
<script src='<?=base_url("node_modules/bootstrap/dist/js/bootstrap.js")?>'></script>
<script src='<?=base_url("assets/js/bootstrap-4-navbar.js")?>'></script>
<script src="<?php echo base_url('assets/plugins/jQuery-Mask-Plugin-master/src/jquery.mask.js'); ?>" type="text/javascript"></script>
<script src="<?php echo base_url('assets/plugins/jQuery-Mask-Plugin-master/dist/jquery.mask.js'); ?>" type="text/javascript"></script>
<script src='<?=base_url("assets/js/script.js")?>'></script>
<script src='<?=base_url("assets/js/countries.js")?>'></script>
<script language="javascript">
    populateCountries("country", "state"); // first parameter is id of country drop-down and second parameter is id of state drop-down
</script>


<script>

    $('#serial_number').focusout(function () {
        var str = $('#serial_number').val();
        var newSerial = str.replace(/-/g, '');
        $('#serial').attr('value',newSerial);
    });


    $(".ksdropdown").click(function(){
        var url=$(this).attr("href");

        window.open(url,'_blank');

    });
</script>
<script src='https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js'></script>


<style>
    html,body,p,a,li,h1,h2,h3,h4,h5,h6,span,i{
        font-family: 'Open Sans', sans-serif;
    }
</style>


</body>
</html>