<?php
/**
 * Created by PhpStorm.
 * User: Maddy
 * Date: 02-Oct-18
 * Time: 8:41 PM
 */
?>
<div class="header">
    <div class="header-top">
        <div class="container-fluid">
            <div class="row align-items-center justify-content-between">
                <div class="col-md-auto">
                    <div class="logo-wrap">
                        <a href="<?=base_url()?>">
                            <img src=<?=base_url("assets/styles/images/bk-logo.jpg")?> alt="BK Logo">
                        </a>
                    </div>
                </div>
                <div class="col-md-auto">
                    <div class="header-top-content">
                        <h3 class="phone"><i class="fas fa-phone"></i> 1-800-422-6281</h3>
                        <h3 class="email">
                            <i class="fas fa-envelope"></i>
                            <a href="mailto:rleeney@bktechnologies.com">rleeney@bktechnologies.com </a>
                        </h3>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <nav class="mb-1 navbar navbar-expand-lg navbar-dark info-color">
        <a class="navbar-brand" href="<?=base_url()?>">BK Technologies</a>
        <button class="navbar-toggler collapsed" type="button" data-toggle="collapse" data-target="#navbarSupportedContent-4" aria-controls="navbarSupportedContent-4" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="navbar-collapse collapse" id="navbarSupportedContent-4" style="">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item active">
                    <a class="nav-link waves-effect waves-light" href="#">
                        <?php if (!$this->session->is_logged_in){?>
                            <a class="nav-link waves-effect waves-light" href="<?=base_url('login')?>">
                                <i class="fas fa-power-off"></i> Login
                            </a>
                        <?php }else{?>
                            <a class="nav-link waves-effect waves-light" href="<?=base_url('logout')?>">
                                <i class="fas fa-power-off"></i> Logout
                            </a>
                        <?php }?>

                    </a>
                </li>
            </ul>
        </div>
    </nav>
</div>

<div class="page-main">
    <div class="container">

        <div class="card login-card">

            <?php if ($this->session->flashdata('error')){?>
                <div class="errors_container alert alert-danger alert-dismissable">
                    <?= $this->session->flashdata('error');?>
                </div>
            <?php }?>
            <?php if (isset($msg)){?>
                <div class="errors_container alert alert-danger alert-dismissable">
                    <?= $msg?>
                </div>
            <?php }?>

            <h5 class="card-header info-color white-text text-center py-4">
                <strong>Sign in</strong>
            </h5>
            <!--Card content-->
            <div class="card-body px-lg-5 pt-0">
                <!-- Form -->
                <form class="as-login-form" action="<?=base_url('/login')?>" method="post">
                    <!-- Email -->
                    <div class="md-form">
                        <input name="email" type="email" id="materialLoginFormEmail" class="form-control">
                        <label for="materialLoginFormEmail">E-mail</label>
                    </div>
                    <!-- Password -->
                    <div class="md-form">
                        <input name="password" type="password" id="materialLoginFormPassword" class="form-control">
                        <label for="materialLoginFormPassword">Password</label>
                    </div>
                    <div class="d-flex justify-content-around">
                        <div>
                            <!-- Remember me -->
                            <div class="form-check">
                                <input type="checkbox" class="form-check-input" id="materialLoginFormRemember">
                                <label class="form-check-label" for="materialLoginFormRemember">Remember me</label>
                            </div>
                        </div>
                        <div>
                            <!-- Forgot password -->
<!--                            <a href="">Forgot password?</a>-->
                        </div>
                    </div>
                    <!-- Sign in button -->
                    <button class="btn btn-info btn-block my-4 waves-effect z-depth-0" type="submit">Sign in</button>
                    <!-- Register -->
                    <p>Request Access to
                        <a href="<?=base_url('sign-up')?>" target="_blank">bktechnologies.com</a>
                    </p>
                </form>
                <!-- Form -->
            </div>
        </div>

    </div>
</div>
