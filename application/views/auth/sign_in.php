<section id="login-content">
    <div class="container-fluid " >
<div class='row' style="background-image:url(<?=base_url()?>assets/img/login-banner.jpg);height:550px">
  <div class='col-md-4 col-md-offset-4 ' style='margin-top:30px;background: #ffffff5c;'>

      <?php if ($this->session->flashdata('error')){?>
         <div class="errors_container alert alert-danger alert-dismissable">
            <?= $this->session->flashdata('error');?>
        </div>
      <?php }?>
      <?php if (isset($msg)){?>
          <div class="errors_container alert alert-danger alert-dismissable">
              <?= $msg?>
          </div>
      <?php }?>

      <div class='alert alert-info'>
<h4 style='color: blue;'>BK Technolgies Customer Login</h4></div>
<style>
label {
    color: blue;
}
</style>
      <form action="<?=base_url('/login')?>" method="post">
            <div>
                <label for="email">Email</label>
                <input id="email" type="email" name="email" class="form-control">
            </div>
            <div>
                <label for="password">Password</label>
                <input id="password" type="password" name="password" class="form-control">
            </div>
<br>
            <button type="submit" class=' btn btn-info pull-right'>Login</button>
        </form>
        <p class="lc__registration_link">
            <a href="<?=base_url('/sign-up')?>" style='color: blue;'>Sign Up</a>
        </p>
    </div>
</div>
</div>
        
</section>
