<?php
$this->load->library('form_validation');
?>

<section id="register-content">
    <div class="container-fluid " >
<div class='row' style="background-image:url(<?=base_url()?>assets/img/login-banner.jpg);min-height:600px">
  <div class='col-md-4 col-md-offset-4 ' style='margin-top:30px;background: #ffffff5c;'>
      <?php if (isset($msg)){?>
        <div class="errors_container alert alert-danger alert-dismissable">
            <?= $msg?>
        </div>
      <?php }?>
      <?php if ($this->session->flashdata('message')){?>
          <div class="alert alert-success alert-dismissable">
              <?= $this->session->flashdata('message')?>
          </div>
      <?php }?>

      <div class='alert alert-info'>
<h4 style='color: blue;'>BK Technolgies Customer Registration</h4></div>
<style>
label {
    color: blue;
}
</style>
        <form action='<?=base_url("/register")?>' method="post">
            <div>
                <label for="username">Username</label>
                <input required id="username" class='form-control' type="text" name="username" value="<?php echo set_value('username'); ?>" >
            </div>
            <div>
                <label for="email">Email</label>
                <input name="email" required type="email" id="email" class='form-control' value="<?php echo set_value('email'); ?>" >
            </div>
            <div>
                <label for="password">Password</label>
                <input  name="password" required id="password" type="password" class='form-control' value="<?php echo set_value('password'); ?>">
            </div>
            <div>
                <label for="password_rpt">Password Repeat</label>
                <input name="password_rpt" required id="password_rpt" type="password" class='form-control' value="<?php echo set_value('password_rpt'); ?>">
            </div>
<!--            <div>-->
<!--                <label for="first_name">First Name</label>-->
<!--                <input name="first_name" type="text" id="first_name" class='form-control'>-->
<!--            </div>-->
<!--            <div>-->
<!--                <label for="last_name">Last Name</label>-->
<!--                <input name="last_name" type="text" id="last_name" class='form-control'>-->
<!--            </div>-->
            <div>
                <label for="number">Phone</label>
                <input name="number" type="number" id="number" class='form-control' value="<?php echo set_value('number'); ?>" >
            </div>
            <div>
                <label for="account_type">Account Type</label>
                <select name="access" id="acess" class='form-control'>
                    <option disabled selected >Select</option>
                    <option value="1">Alberta</option>
                    <option value="2">Florida</option>
                    <option value="3">CalFire</option>
                </select>
            </div>
<br>
            <button class="submit_reg_form btn btn-info pull-right" type="submit">Register</button>
        </form>
        <p class="rc__registration_link">
            <a href="<?=base_url('/sign-in')?>" style='color: blue;'>Sign In</a>
        </p>
    </div>
</div>
</div>
</section>