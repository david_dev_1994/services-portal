<?php
/**
 * Created by PhpStorm.
 * User: Maddy
 * Date: 08-Oct-18
 * Time: 4:54 PM
 */?>

<div class="header">
    <div class="header-top">
        <div class="container-fluid">
            <div class="row align-items-center justify-content-between">
                <div class="col-md-auto">
                    <div class="logo-wrap">
                        <a href="#">
                            <img src=<?=base_url("assets/styles/images/bk-logo.jpg")?> alt="BK Logo">
                        </a>
                    </div>
                </div>
                <div class="col-md-auto">
                    <div class="header-top-content">
                        <h3 class="phone"><i class="fas fa-phone"></i> 1-800-422-6281</h3>
                        <h3 class="email">
                            <i class="fas fa-envelope"></i>
                            <a href="mailto:rleeney@bktechnologies.com">rleeney@bktechnologies.com </a>
                        </h3>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <nav class="mb-1 navbar navbar-expand-lg navbar-dark info-color">
        <a class="navbar-brand" href="#">BK Technologies</a>
        <button class="navbar-toggler collapsed" type="button" data-toggle="collapse" data-target="#navbarSupportedContent-4" aria-controls="navbarSupportedContent-4" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="navbar-collapse collapse" id="navbarSupportedContent-4" style="">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item active">
                    <a class="nav-link waves-effect waves-light" href="#">
                        <?php if (!$this->session->is_logged_in){?>
                            <a class="nav-link waves-effect waves-light" href="<?=base_url('login')?>">
                                <i class="fas fa-power-off"></i> Login
                            </a>
                        <?php }else{?>
                            <a class="nav-link waves-effect waves-light" href="<?=base_url('logout')?>">
                                <i class="fas fa-power-off"></i> Logout
                            </a>
                        <?php }?>

                    </a>
                </li>
            </ul>
        </div>
    </nav>
</div>

<div class="page-main">
    <div class="container">

        <div class="card login-card">

            <?php if (isset($msg)){?>
                <div class="errors_container alert alert-danger alert-dismissable">
                    <?= $msg?>
                </div>
            <?php }?>
            <?php if ($this->session->flashdata('message')){?>
                <div class="alert alert-success alert-dismissable">
                    <?= $this->session->flashdata('message')?>
                </div>
            <?php }?>

            <h5 class="card-header info-color white-text text-center py-4">
                <strong>Request an Account</strong>
            </h5>
            <!--Card content-->
            <div class="card-body px-lg-5 pt-0">
                <!-- Form -->
                <form class="as-login-form" action="<?=base_url('/register')?>" method="post">
                    <!-- Email -->
                    <div class="md-form">
                        <label for="username">Username</label>
                        <input required id="username" class='form-control' type="text" name="username" value="<?php echo set_value('username'); ?>" >
                    </div>
                    <!-- Password -->
                    <div class="md-form">
                        <label for="email">Email</label>
                        <input name="email" required type="email" id="email" class='form-control' value="<?php echo set_value('email'); ?>" >
                    </div>

                    <div class="md-form">
                        <label for="password">Password</label>
                        <input  name="password" required id="password" type="password" class='form-control' value="<?php echo set_value('password'); ?>">
                    </div>

                    <div class="md-form">
                        <label for="password_rpt">Password Repeat</label>
                        <input name="password_rpt" required id="password_rpt" type="password" class='form-control' value="<?php echo set_value('password_rpt'); ?>">
                    </div>

                    <div class="md-form">
                        <label for="number">Phone</label>
                        <input name="number" type="number" id="number" class='form-control' value="<?php echo set_value('number'); ?>" >
                    </div>

                    <div class="md-form">
                        <select required name="access" id="acess" class='form-control'>
                            <option disabled selected >Select Account Type</option>
                            <option value="1">Alberta</option>
                            <option value="2">Florida</option>
                            <option value="3">CalFire</option>
                        </select>
<!--                        <label for="account_type">Account Type</label>-->
                    </div>

                    <!-- Sign in button -->
                    <button class="btn btn-info btn-block my-4 waves-effect z-depth-0" type="submit">Submit</button>
                    <!-- Register -->
                    <p>Already have a registered account?
                        <a href="<?=base_url('login')?>">Log-in here</a>
                    </p>
                </form>
                <!-- Form -->
            </div>
        </div>

    </div>
</div>
