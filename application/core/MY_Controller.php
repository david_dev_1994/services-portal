<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class MY_Controller extends CI_Controller {

    public function __construct() {
        parent::__construct();
    }

    /**
     * @param $view
     * @param bool $data
     */
    public function loadView($view, $data=null, $pageTitle = "")
    {
        $this->load->view('layouts/header', array("title" => $pageTitle));
        $this->load->view($view, $data);
        $this->load->view('layouts/footer');
    }

}