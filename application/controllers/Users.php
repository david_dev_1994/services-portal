<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Users extends MY_Controller
{

    function __construct()
    {
        parent::__construct();
//        echo "<pre>";
//        print_r($_SESSION);
//        exit;


        $params = $this->router->fetch_method();
        $required_auth = array('index', 'default_ahs', 'Esn_spread_sheet', 'upload_esn', 'download_codeplug', 'codeplug', 'upload_codeplug'); //(Alberta access level =1)

        if (in_array($params, $required_auth)) {
            if ($this->session->alb_access_level != 1) {
//                $this->session->set_flashdata('error',"You must logged in first to access the portal");
                redirect(base_url('alb_signIn'));
            }
            $this->layout = 'alberta/default';
        }
    }

    public function index()
    {
//        $this->loadView('users/my_page');
        $this->load->view('common_pages/alberta/main');
    }

    public function default_ahs()
    {

        //   $this->layout='admin/default';
        if ($this->session->isLoggedIn) {
            redirect(base_url("/sign-in"));
            exit();
        }
//        if($this->session->loggedUserAccountType=="Government State"){
//         $this->load->view('users/governmentstate');
//         $this->load->view('alberta/main');
//        }else{
//        $this->load->view('users/dashboard');
        $this->load->view('common_pages/alberta/main');
//        }
    }

    public function download_codeplug()
    {
        $file = array();
//        $file['filelist'] = $this->crud->getAllcp();
        $file['mobiles'] = $this->crud->getAllBy('codeplug',array('device'=>'mobiles'),null);
        $file['portables'] = $this->crud->getAllBy('codeplug',array('device'=>'portables'),null);
        $file['base_stations'] = $this->crud->getAllBy('codeplug',array('device'=>'base_stations'),null);

        if (!$file['mobiles'] && !$file['portables'] && !$file['base_stations']) {
            $file['list'] = 400;
        }
        $this->load->view('common_pages/alberta/download_codeplug', $file);
    }

    public function codeplug()
    {

        $file = array();
//            $con=array('status'=>'0','userid'=>$this->session->alb_loggedUserId);
        $con = array('status' => '0');
//            $file['filelist']=$this->crud->getAllBy('codeplug',$con,array('date'=>'desc'));
        $file['filelist'] = $this->crud->getAllcp();

        if (!$file['filelist']) {
            $file['list'] = 400;
        }
//            print_r($file);
//            exit;

        $file['filelist']['username'] = $this->session->alb_loggedUserUsername;
        $this->load->view('common_pages/alberta/upload_codeplug', $file);


    }

    public function upload_codeplug()
    {

        if ($this->input->post('FileUpload')) {
            $config['upload_path'] = "./assets/codeplug/";
//                $config['allowed_types']        = 'zip';
            $config['allowed_types'] = '*';
//                $config['max_size']     = '4999'; #KB
            $config['overwrite'] = false; //when set to false then if duplicate files exists then new name will be given to latest file
//                echo $this->input->post('device');exit;
            $this->load->library('upload', $config);

            if (!$this->upload->do_upload('file1')) {
                $this->session->set_flashdata('error', $this->upload->display_errors());
                redirect('codeplug');
            } else {
                $date = date("d-m-Y");
                $this->load->helper('file');
                $data = $this->upload->data();
                $filname = $data['file_name'];
                $array = array(
                    'file' => "$filname",
                    'userid' => $this->session->alb_loggedUserId,
                    'date' => $date,
                    'device' => $this->input->post('device')
                );

                if ($this->crud->insert('codeplug', $array)) {
                    $this->session->set_flashdata('success', "File Successfully Uploaded");
                    redirect('codeplug');
                }

            }
        } else {
            $this->session->set_flashdata('error', "Invalid user Request");
            redirect('codeplug'); //codeplug upload page route
        }

    }


    public function Esn_spread_sheet()
    {

        $file = array();
        $con = array('status' => '0');
        $file['filelist'] = $this->crud->getAllBy('Spreedsheet', $con, array('date' => 'desc'));
        if (!$file['filelist']) {
            $file['list'] = 400;
        }
        // $file['filelist']['username'] = $this->session->alb_loggedUserUsername;
        $this->load->view('users/ESN_Spreadsheets', $file);

    } //list of already uploaded spread sheets

    public function upload_esn()
    {
        if ($this->input->post('FileUpload')) {

            $this->load->library("form_validation");
            $this->form_validation->set_rules('notes', 'Notes', 'required');

            if (!$this->form_validation->run()) {
                $this->session->set_flashdata('error', validation_errors());
                redirect('Esn-spread-sheet');
            }

            $config['upload_path'] = "./assets/pdf/";
            $config['allowed_types'] = 'xlsx|csv|xls';
            $config['overwrite'] = false; //when set to false then if duplicate files exists then new name will be given to latest file

            $this->load->library('upload', $config);
            if (!$this->upload->do_upload('file1')) {
                $this->session->set_flashdata('error', $this->upload->display_errors());
                redirect('Esn-spread-sheet');
            } else {
                $date = date("m-d-Y");
                $this->load->helper('file');
                $data = $this->upload->data();
                $filname = $data['file_name'];
                $notes = $this->input->post('notes');
                $array = array('file' => "$filname", 'username' => $this->session->alb_loggedUserUsername, 'notes' => $notes, 'date' => $date);
                if ($this->crud->insert('Spreedsheet', $array)) {
                    $this->session->set_flashdata('success', "File Successfully Uploaded");
                    redirect('Esn-spread-sheet');
                }
            }
        } else {
            $this->session->set_flashdata('error', "Invalid user Request");
            redirect('Esn-spread-sheet');
        }

    } //upload esn spreadsheet

//        public function `Fire(){
//             if(!$this->session->isLoggedIn)
//                {
//                    redirect(base_url("/sign-in"));
//                    exit();
//                }
//            $file = array();
//            $con=array('status'=>'0');
//            $file['filelists']=$this->crud->getAllBy('calFireRestrickt',$con)?$this->crud->getAllBy('calFireRestrickt',$con):array();
//             $this->loadView('users/calFireRestrickt',$file);
//        }
}
