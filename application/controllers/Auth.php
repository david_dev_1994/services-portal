<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends MY_Controller {
    function __construct()
    {
        parent::__construct();
        $this->layout='admin/default';
        $this->load->helper('functions_helper');
        $this->load->library("form_validation");
    }

    //florida
    public function flo_signIn()
    {
        //$this->layout='admin/default';
        if($this->session->access_level){
            $this->session->set_flashdata('error',"You are already Logged in");
            redirect(base_url('flfs'));
            exit();
        }
//        $this->loadView('auth/sign_in', null, "Sign In");//old login
        $this->load->view('common_pages/florida/login');
    }
    public function flo_login()
    {
        if($this->session->access_level){
            $this->session->set_flashdata('error',"You are already Logged in");
            redirect(base_url('flfs'));
            exit();
        }
//        if( count( $this->input->post()) && base_url("/sign-in") === $_SERVER["HTTP_REFERER"]) {
        if( count( $this->input->post())) {

            $this->load->library("form_validation");

            $this->form_validation->set_rules('password', 'Password', 'required|trim');
            $this->form_validation->set_rules('email', 'Email', 'required|trim|valid_email');

            if ($this->form_validation->run() == false) {
                $msg['msg'] = validation_errors();
                $this->load->view('common_pages/florida/login',$msg);
            } else {
				$userData = [
                    "email" => $this->input->post('email'),
                    "password" => md5($this->input->post('password')),
                    "access_level" => 2 //email for florida portal
//                    "status" => 1 //verify that user is an active user
                ];
                $this->load->model("users");
                $user = $this->users->findclient($userData);

				if ($user) {
                    //                 $userPassword = $user->password;
                    //                 if(password_verify($userData["password"], $userPassword)){

                    if ($user->status==1){

                        $sess_data=array(
                            'loggedUserId'=>$user->id,
                            'loggedUserUsername'=>$user->name,
                            'access_level'=>(int)$user->access_level,
							'account_type'=>(int)$user->account_type
                            //                        'is_admin'=>false
                        );
                        switch ($user->access_level) {
                            case 2: {
                                $this->session->set_userdata($sess_data); //session start
                                redirect(base_url('flfs')); //florida home
                                break;
                            }
                            default:
//                               $this->session->set_flashdata('error','Incorrect Credentials');
                                $this->session->set_flashdata('error','You are using wrong portal for login');
                                redirect(base_url("/flo_signIn"));
                            //redirect @ home apply checks at method direct link access
                        }
                    }else{
                        $this->session->set_flashdata('error','Account is not approved yet');
                        redirect(base_url("/flo_signIn"));
                    }
                } else {
                    $this->session->set_flashdata('error','Invalid Email/Password provided');
                    redirect(base_url("/flo_signIn"));
                }
            }
        } else {
            var_dump(2);
        die();
            redirect(base_url("/flo_signIn"));
        }
//        redirect(base_url());
    }
    public function flo_signUp()
    {
        if($this->session->access_level){
            $this->session->set_flashdata('error',"Kindly logout first to perform this operation");
            redirect(base_url('flfs'));
            exit();
        }
//        $this->loadView('auth/sign_up', null, "Sign Up"); //old design
        $this->load->view('common_pages/florida/flo_signup');
    }
    public function flo_register()
    {
        if($this->session->access_level){
            $this->session->set_flashdata('error',"Kindly logout first to perform this operation");
            redirect(base_url('flfs'));
            exit();
        }
        if( count( $this->input->post())){
            $this->load->library("form_validation");

            $this->form_validation->set_rules('username', 'Username', 'required|trim|max_length[16]|min_length[3]');
            $this->form_validation->set_rules('password', 'Password', 'required|trim|max_length[16]|min_length[3]');
            $this->form_validation->set_rules('password_rpt', 'Password Repeat', 'required|trim|matches[password]');
//            $this->form_validation->set_rules('email', 'Email', 'required|trim|valid_email|is_unique[client.email]');
            $this->form_validation->set_rules('email', 'Email', 'required|trim|valid_email|callback_verifyEmail['. 2 .']'); //2 for florida
            $this->form_validation->set_rules('number', 'Phone', 'trim');
            $this->form_validation->set_rules('access', 'Access Level', 'trim|required');
            $this->form_validation->set_rules('acc_type', 'Account Type', 'trim|required');
            $this->form_validation->set_message('is_unique', 'This %s has already been registered with us. Use different email for each portal');



            if(!$this->form_validation->run()){
                $msg['msg']=validation_errors();
                $this->load->view('common_pages/florida/flo_signup',$msg);
            } else {
                $accountType = trim($this->input->post('access'));
                switch ($accountType){
                    case 2:{$accountType = 2;break;}
                    default: {
                        $accountType = false;
                    }
                }
                if($accountType === false ) {
                    $msg['msg']= "Please select a valid access level";
                    $this->load->view('common_pages/florida/flo_signup',$msg);
                } else {
                    $this->load->model('users');
                    $registerData = [
                        "name" => $this->input->post('username'),
                        "password" => md5($this->input->post('password')),
                        "email" => $this->input->post('email'),
                        "number" => $this->input->post('number'),
                        "access_level" => $accountType,
                        "account_type" => $this->input->post('acc_type'),
                        "status" => 0,
                    ];
                    $result = $this->users->addclient($registerData);
                    if($result === true){
                        $this->sendmail($this->input->post('username'),$this->input->post('email'),'Florida Forestry');
                        $this->sendmail_admin($this->input->post('username'),$this->input->post('email'),'Florida Forestry');
                        $this->session->set_flashdata('message', "Your account has been created successfully, once admin approves it then you will be notified by email.");
                        redirect(base_url('/flo_sign-up'));
                    } else {
                        $msg['msg']= "Failed to insert record";
                        $this->load->view('common_pages/florida/flo_signup',$msg);
                    }
                }
            }
        } else {
            redirect(base_url("/flo_sign-up"));
        }
    } //register florida user
    public function logout(){

        if(!$this->session->access_level){
            $this->session->set_flashdata('error',"You are already logged out");
            redirect(base_url('flo_signIn'));
            exit();
        }
        //        $this->session->sess_destroy(); //Rana
        $this->session->unset_userdata(["access_level", "loggedUserId", "loggedUserUsername",]); //florida
        redirect(base_url('flo_signIn'));
    }//florida logout
    function flfs_forgot_password(){

        if ($this->input->post('email')){
            $this->load->library("form_validation");
            $this->load->library('encryption');
            $this->form_validation->set_rules('email',"Email",'trim|required|valid_email');

            if($this->form_validation->run()==TRUE){
                $user_data=$this->input->post();
                unset($user_data['reset_password']);
                if($res=$this->crud->getthis('client'," access_level=2 and email = '".$user_data['email']."'")){

                    $hash = $this->encryption->encrypt($res->id);
//                $hash= str_replace("/",",",$hash);
                    $message='Click on the link to reset your password: '. base_url('flo_resetpass/').$hash;
                    $this->email_to($user_data['email'],$message);
                    $this->session->set_flashdata('success',"Reset password link has been sent to your mail");
                    redirect(base_url('flo_signIn'));
                }else{
                    $this->session->set_flashdata('error',"This email is not registered with us!");
                    redirect(base_url('flo_signIn'));
                }
            }else{
                $this->session->set_flashdata('error',validation_errors());
                redirect(base_url('flo_signIn'));
            }
        }else{
            $this->session->set_flashdata('error',"Invalid User Request");
            redirect(base_url('flo_signIn'));
        }
    }
    function flo_resetpass()
    {
        $this->load->library("form_validation");
        $this->load->library('encryption');
        if ($this->input->post('reset'))
        {
            $this->form_validation->set_rules('password', 'Password', 'required|trim|max_length[16]|min_length[3]');
            $this->form_validation->set_rules('c_password', 'Password Repeat', 'required|trim|matches[password]');

            if(!$this->form_validation->run()){
                $msg['msg']=validation_errors();
                $msg['res']=$this->input->post('id');
                $this->load->view('common_pages/florida/flo_resetform',$msg);
            } else{
                $data['password']= md5($this->input->post('password'));
                $where= array('id'=>$this->input->post('id'),'access_level'=>2);

                if ($this->crud->update('client',$data,$where))
                {
                    $this->session->set_flashdata('success',"Password has been reset successfully");
                    redirect(base_url('flo_signIn'));
                }else {
                    $this->session->set_flashdata('error',"Failed to update password");
                    redirect(base_url('flo_resetpass/'.$this->input->post('id')));
                }
            }
        }else
        {
            if ($this->uri->segment(2))
            {
                $trail = func_get_args();
                $id=join("/",$trail);

//                $id = str_replace(",","/",$this->encryption->decrypt($this->uri->segment(2)));
                $id = $this->encryption->decrypt($id);
                if ($res['res']=$this->crud->getthis('client'," access_level=2 and id = ".$id)) {
                    $res['res']=$res['res']->id;
                    $this->load->view('common_pages/florida/flo_resetform', $res);
                }else{
                    $this->session->set_flashdata('error',"No Record Found");
                    redirect(base_url('flo_signIn'));
                }
            }else{
                $this->session->set_flashdata('error',"Invalid user request");
                redirect(base_url('flo_signIn'));
            }
        }
    }

    //calfire
    public function cal_signIn()
    {
        //$this->layout='admin/default';
        if($this->session->cal_access_level){
            $this->session->set_flashdata('error',"You are already Logged in");
            redirect(base_url('CalFire'));
            exit();
        }
//        $this->loadView('auth/sign_in', null, "Sign In");//old login
        $this->load->view('common_pages/calfire/login');
    }
    public function cal_login()
    {
        if($this->session->cal_access_level){
            $this->session->set_flashdata('error',"You are already Logged in");
            redirect(base_url('CalFire'));
            exit();
        }
//        if( count( $this->input->post()) && base_url("/sign-in") === $_SERVER["HTTP_REFERER"]) {
        if( count( $this->input->post())) {

            $this->load->library("form_validation");

            $this->form_validation->set_rules('password', 'Password', 'required|trim');
            $this->form_validation->set_rules('email', 'Email', 'required|trim|valid_email');

            if ($this->form_validation->run() == false) {
                $msg['msg'] = validation_errors();
                $this->load->view('common_pages/calfire/login',$msg);
            } else {

                $userData = [
                    "email" => $this->input->post('email'),
                    "password" => md5($this->input->post('password')),
                    "access_level" => 3 //email for calfire portal
//                    "status" => 1 //verify that user is an active user
                ];
                $this->load->model("users");
                $user = $this->users->findclient($userData);

                if ($user) {
                    //                 $userPassword = $user->password;
                    //                 if(password_verify($userData["password"], $userPassword)){
                    if ($user->status==1){
                        
                        $sess_data=array(
                            'cal_loggedUserId'=>$user->id,
                            'cal_loggedUserUsername'=>$user->name,
                            'cal_loggedUserUseremail'=>$user->email,
                            'cal_access_level'=>(int)$user->access_level
//                        'is_admin'=>false
                        );
                        
                        switch ($user->access_level) {
                            case 3: {
                                $this->session->set_userdata($sess_data); //session start
                                redirect(base_url('CalFire')); //Calfire home
                                break;
                            }
                            default:
                                $this->session->set_flashdata('error','You are using wrong portal for login');
                                redirect(base_url("/cal_signIn"));
                        }

                    }else{
                        $this->session->set_flashdata('error','Account is not approved yet');
                        redirect(base_url("/cal_signIn"));
                    }

                } else {
                    $this->session->set_flashdata('error','Invalid Email/Password Provided');
                    redirect(base_url("/cal_signIn"));
                }
            }
        } else {
            redirect(base_url("/cal_signIn"));
        }
//        redirect(base_url());
    }
    public function cal_signUp()
    {
        if($this->session->cal_access_level){
            $this->session->set_flashdata('error',"Kindly logout first to perform this operation");
            redirect(base_url('CalFire'));
            exit();
        }
//        $this->loadView('auth/sign_up', null, "Sign Up"); //old design
        $this->load->view('common_pages/calfire/cal_signup');
    }
    public function cal_register()
    {
        if($this->session->cal_access_level){
            $this->session->set_flashdata('error',"Kindly Logout First To Perform This Operation");
            redirect(base_url('CalFire'));
            exit();
        }
        if( count( $this->input->post())){
            $this->load->library("form_validation");

            $this->form_validation->set_rules('username', 'Username', 'required|trim|max_length[16]|min_length[3]');
            $this->form_validation->set_rules('password', 'Password', 'required|trim|max_length[16]|min_length[3]');
            $this->form_validation->set_rules('password_rpt', 'Password Repeat', 'required|trim|matches[password]');
//            $this->form_validation->set_rules('email', 'Email', 'required|trim|valid_email|is_unique[client.email]');
            $this->form_validation->set_rules('email', 'Email', 'required|trim|valid_email|callback_verifyEmail['. 3 .']'); //3 for calfire
            $this->form_validation->set_rules('number', 'Phone', 'trim');
            $this->form_validation->set_rules('access', 'Access Level', 'trim|required');
            $this->form_validation->set_rules('acc_type', 'Account Type', 'trim|required');
            $this->form_validation->set_message('is_unique', 'This %s has already been registered with us. Use different email for each portal');

            if(!$this->form_validation->run()){
                $msg['msg']=validation_errors();
                $this->load->view('common_pages/calfire/cal_signup',$msg);
            } else {
                $accountType = trim($this->input->post('access'));
                switch ($accountType){
                    case 3:{$accountType = 3;break;}
                    default: {
                        $accountType = false;
                    }
                }
                if($accountType === false ) {
                    $msg['msg']= "Please select a valid access level";
                    $this->load->view('common_pages/calfire/cal_signup',$msg);
                } else {
                    $this->load->model('users');
                    $registerData = [
                        "name" => $this->input->post('username'),
                        "password" => md5($this->input->post('password')),
                        "email" => $this->input->post('email'),
                        "number" => $this->input->post('number'),
                        "access_level" => $accountType,
                        "account_type" => $this->input->post('acc_type'),
                        "status" => 0,
                    ];
                    $result = $this->users->addclient($registerData);
                    if($result === true){
                        $this->sendmail($this->input->post('username'),$this->input->post('email'),'CalFire');
                        $this->sendmail_admin($this->input->post('username'),$this->input->post('email'),'CalFire');
                        $this->session->set_flashdata('message', "Your account has been created successfully, once admin approves it then you will be notified by email.");
                        redirect(base_url('/cal_sign-up'));
                    } else {
                        $msg['msg']= "Failed to insert record";
                        $this->load->view('common_pages/calfire/cal_signup',$msg);
                    }
                }
            }
        } else {
            redirect(base_url("/cal_sign-up"));
        }
    } //register florida user
    public function cal_logout(){

        if(!$this->session->cal_access_level){
            $this->session->set_flashdata('error',"You are already Logged out");
            redirect(base_url('cal_signIn'));
            exit();
        }
        //        $this->session->sess_destroy(); //Rana
        $this->session->unset_userdata(["cal_access_level", "cal_loggedUserId", "cal_loggedUserUsername",]);
        redirect(base_url('CalFire'));
    }//calfire logout
    function cal_forgot_password(){

        if ($this->input->post('email')){

            $this->load->library("form_validation");
            $this->load->library('encryption');
            $this->form_validation->set_rules('email',"Email",'trim|required|valid_email');

            if($this->form_validation->run()==TRUE){
                $user_data=$this->input->post();
                unset($user_data['reset_password']);
                if($res=$this->crud->getthis('client'," access_level=3 and email = '".$user_data['email']."'")){

                    $hash = $this->encryption->encrypt($res->id);
//                $hash= str_replace("/",",",$hash);
                    $message='Click on the link to reset your password: '. base_url('cal_resetpass/').$hash;
                    $this->email_to($user_data['email'],$message);
                    $this->session->set_flashdata('success',"Reset password link has been sent to your mail");
                    redirect(base_url('cal_signIn'));
                }else{
                    $this->session->set_flashdata('error',"This email is not registered with us!");
                    redirect(base_url('cal_signIn'));
                }
            }else{
                $this->session->set_flashdata('error',validation_errors());
                redirect(base_url('cal_signIn'));
            }
        }else{
            $this->session->set_flashdata('error',"Invalid User Request");
            redirect(base_url('cal_signIn'));
        }

    }
    function cal_resetpass()
    {
        $this->load->library("form_validation");
        $this->load->library('encryption');
        if ($this->input->post('reset'))
        {
            $this->form_validation->set_rules('password', 'Password', 'required|trim|max_length[16]|min_length[3]');
            $this->form_validation->set_rules('c_password', 'Password Repeat', 'required|trim|matches[password]');

            if(!$this->form_validation->run()){
                $msg['msg']=validation_errors();
                $msg['res']=$this->input->post('id');
                $this->load->view('common_pages/calfire/cal_resetform',$msg);
            } else{
                $data['password']= md5($this->input->post('password'));
                $where= array('id'=>$this->input->post('id'),'access_level'=>3);

                if ($this->crud->update('client',$data,$where))
                {
                    $this->session->set_flashdata('success',"Password has been reset successfully");
                    redirect(base_url('cal_signIn'));
                }else {
                    $this->session->set_flashdata('error',"Failed to update password");
                    redirect(base_url('cal_resetpass/'.$this->input->post('id')));
                }
            }
        }else
        {
            if ($this->uri->segment(2))
            {
                $trail = func_get_args();
                $id=join("/",$trail);

//                $id = str_replace(",","/",$this->encryption->decrypt($this->uri->segment(2)));
                $id = $this->encryption->decrypt($id);
                if ($res['res']=$this->crud->getthis('client'," access_level=3 and id = ".$id)) {
                    $res['res']=$res['res']->id;
                    $this->load->view('common_pages/calfire/cal_resetform', $res);
                }else{
                    $this->session->set_flashdata('error',"No Record Found");
                    redirect(base_url('cal_signIn'));
                }
            }else{
                $this->session->set_flashdata('error',"Invalid user request");
                redirect(base_url('cal_signIn'));
            }
        }
    }


    //USFS
    public function usfs_signIn()
    {
        //$this->layout='admin/default';
        if($this->session->usfs_access_level){
            $this->session->set_flashdata('error',"You are already Logged in");
            redirect(base_url('usfs'));
            exit();
        }
//        $this->loadView('auth/sign_in', null, "Sign In");//old login
        $this->load->view('common_pages/usfs/login');
    }
    public function usfs_login()
    {
        if($this->session->usfs_access_level){
            $this->session->set_flashdata('error',"You are already Logged in");
            redirect(base_url('usfs'));
            exit();
        }
//        if( count( $this->input->post()) && base_url("/sign-in") === $_SERVER["HTTP_REFERER"]) {
        if( count( $this->input->post())) {

            $this->load->library("form_validation");

            $this->form_validation->set_rules('password', 'Password', 'required|trim');
            $this->form_validation->set_rules('email', 'Email', 'required|trim|valid_email');

            if ($this->form_validation->run() == false) {
                $msg['msg'] = validation_errors();
                $this->load->view('common_pages/usfs/login',$msg);
            } else {

                $userData = [
                    "email" => $this->input->post('email'),
                    "password" => md5($this->input->post('password')),
                    "access_level" => 4 //usfs portal
//                    "status" => 1 //verify that user is an active user
                ];
                $this->load->model("users");
                $user = $this->users->findclient($userData);
                if ($user) {
                    //                 $userPassword = $user->password;
                    //                 if(password_verify($userData["password"], $userPassword)){

                    if ($user->status==1){
						$sess_data=array(
                            'usfs_loggedUserId'=>$user->id,
                            'usfs_loggedUserUsername'=>$user->name,
                            'usfs_loggedUserUseremail'=>$user->email,
                            'usfs_access_level'=>(int)$user->access_level,
							'usfs_account_type'=>(int)$user->account_type
                            //                        'is_admin'=>false
                        );
                        
                        switch ($user->access_level) {
                            case 4: {
                                $this->session->set_userdata($sess_data); //session start
                                redirect(base_url('usfs')); //florida home
                                break;
                            }
                            default:
//                               $this->session->set_flashdata('error','Incorrect Credentials');
                                $this->session->set_flashdata('error','You are using wrong portal for login');
                                redirect(base_url("/usfs_signIn"));
                            //redirect @ home apply checks at method direct link access
                        }
                    }else{
                        $this->session->set_flashdata('error','Account is not approved yet');
                        redirect(base_url("/usfs_signIn"));
                    }
                } else {
                    $this->session->set_flashdata('error','Invalid Email/Password provided');
                    redirect(base_url("/usfs_signIn"));
                }
            }
        } else {
            redirect(base_url("/usfs_signIn"));
        }
//        redirect(base_url());
    }
    public function usfs_signUp()
    {
        if($this->session->usfs_access_level){
            $this->session->set_flashdata('error',"Kindly logout first to perform this operation");
            redirect(base_url('usfs'));
            exit();
        }
//        $this->loadView('auth/sign_up', null, "Sign Up"); //old design
        $this->load->view('common_pages/usfs/usfs_signup');
    }
    public function usfs_register()
    {
        if($this->session->usfs_access_level){
            $this->session->set_flashdata('error',"Kindly logout first to perform this operation");
            redirect(base_url('usfs'));
            exit();
        }
        if( count( $this->input->post())){
            $this->load->library("form_validation");

            $this->form_validation->set_rules('username', 'Username', 'required|trim|max_length[16]|min_length[3]');
            $this->form_validation->set_rules('password', 'Password', 'required|trim|max_length[16]|min_length[3]');
            $this->form_validation->set_rules('password_rpt', 'Password Repeat', 'required|trim|matches[password]');
            $this->form_validation->set_rules('email', 'Email', 'required|trim|valid_email|callback_verifyEmail['. 4 .']'); //4 for usfs
            $this->form_validation->set_rules('number', 'Phone', 'trim');
            $this->form_validation->set_rules('access', 'Access Level', 'trim|required');
            $this->form_validation->set_rules('acc_type', 'Account Type', 'trim|required');
            $this->form_validation->set_message('is_unique', 'This %s has already been registered with us. Use different email for each portal');



            if(!$this->form_validation->run()){
                $msg['msg']=validation_errors();
                $this->load->view('common_pages/usfs/usfs_signup',$msg);
            } else {
                $accountType = trim($this->input->post('access'));
                switch ($accountType){
                    case 4:{$accountType = 4;break;}
                    default: {
                        $accountType = false;
                    }
                }
                if($accountType === false ) {
                    $msg['msg']= "Please select a valid access level";
                    $this->load->view('common_pages/usfs/usfs_signup',$msg);
                } else {
                    $this->load->model('users');
                    $registerData = [
                        "name" => $this->input->post('username'),
                        "password" => md5($this->input->post('password')),
                        "email" => $this->input->post('email'),
                        "number" => $this->input->post('number'),
                        "access_level" => $accountType,
                        "account_type" => $this->input->post('acc_type'),
                        "status" => 0,
                    ];
                    $result = $this->users->addclient($registerData);
                    if($result === true){
                        $this->sendmail($this->input->post('username'),$this->input->post('email'),'United State Forest Service');
                        $this->sendmail_admin($this->input->post('username'),$this->input->post('email'),'United State Forest Service');
                        $this->session->set_flashdata('message', "Your account has been created successfully, once admin approves it then you will be notified by email.");
                        redirect(base_url('/usfs_sign-up'));
                    } else {
                        $msg['msg']= "Failed to insert record";
                        $this->load->view('common_pages/usfs/usfs_signup',$msg);
                    }
                }
            }
        } else {
            redirect(base_url("/usfs_sign-up"));
        }
    } //register florida user
    public function usfs_logout(){

        if(!$this->session->usfs_access_level){
            $this->session->set_flashdata('error',"You are already logged out");
            redirect(base_url('usfs_signIn'));
            exit();
        }
        //        $this->session->sess_destroy(); //Rana
        $this->session->unset_userdata(["usfs_access_level", "usfs_loggedUserId", "usfs_loggedUserUsername",]); //florida
        redirect(base_url('usfs_signIn'));
    }//florida logout
    function usfs_forgot_password(){

        if ($this->input->post('email')){
            $this->load->library("form_validation");
            $this->load->library('encryption');
            $this->form_validation->set_rules('email',"Email",'trim|required|valid_email');

            if($this->form_validation->run()==TRUE){
                $user_data=$this->input->post();
                unset($user_data['reset_password']);
                if($res=$this->crud->getthis('client'," access_level=4 and email = '".$user_data['email']."'")){

                    $hash = $this->encryption->encrypt($res->id);
//                $hash= str_replace("/",",",$hash);
                    $message='Click on the link to reset your password: '. base_url('usfs_resetpass/').$hash;
                    $this->email_to($user_data['email'],$message);
                    $this->session->set_flashdata('success',"Reset password link has been sent to your mail");
                    redirect(base_url('usfs_signIn'));
                }else{
                    $this->session->set_flashdata('error',"This email is not registered with us!");
                    redirect(base_url('usfs_signIn'));
                }
            }else{
                $this->session->set_flashdata('error',validation_errors());
                redirect(base_url('usfs_signIn'));
            }
        }else{
            $this->session->set_flashdata('error',"Invalid User Request");
            redirect(base_url('usfs_signIn'));
        }
    }
    function usfs_resetpass()
    {
        $this->load->library("form_validation");
        $this->load->library('encryption');
        if ($this->input->post('reset'))
        {
            $this->form_validation->set_rules('password', 'Password', 'required|trim|max_length[16]|min_length[3]');
            $this->form_validation->set_rules('c_password', 'Password Repeat', 'required|trim|matches[password]');

            if(!$this->form_validation->run()){
                $msg['msg']=validation_errors();
                $msg['res']=$this->input->post('id');
                $this->load->view('common_pages/usfs/usfs_resetform',$msg);
            } else{
                $data['password']= md5($this->input->post('password'));
                $where= array('id'=>$this->input->post('id'),'access_level'=>4);

                if ($this->crud->update('client',$data,$where))
                {
                    $this->session->set_flashdata('success',"Password has been reset successfully");
                    redirect(base_url('usfs_signIn'));
                }else {
                    $this->session->set_flashdata('error',"Failed to update password");
                    redirect(base_url('usfs_resetpass/'.$this->input->post('id')));
                }
            }
        }else
        {
            if ($this->uri->segment(2))
            {
                $trail = func_get_args();
                $id=join("/",$trail);

//                $id = str_replace(",","/",$this->encryption->decrypt($this->uri->segment(2)));
                $id = $this->encryption->decrypt($id);
                if ($res['res']=$this->crud->getthis('client'," access_level=4 and id = ".$id)) {
                    $res['res']=$res['res']->id;
                    $this->load->view('common_pages/usfs/usfs_resetform', $res);
                }else{
                    $this->session->set_flashdata('error',"No Record Found");
                    redirect(base_url('usfs_signIn'));
                }
            }else{
                $this->session->set_flashdata('error',"Invalid user request");
                redirect(base_url('usfs_signIn'));
            }
        }
    }


    //Alberta
    public function alb_signIn()
    {
        //$this->layout='admin/default';
        if($this->session->alb_access_level){
            $this->session->set_flashdata('error',"You are already Logged in");
            redirect(base_url('Alberta'));
            exit();
        }
//        $this->loadView('auth/sign_in', null, "Sign In");//old login
        $this->load->view('common_pages/alberta/login');
    }
    public function alb_login()
    {
        if($this->session->alb_access_level){
            $this->session->set_flashdata('error',"You are already Logged in");
            redirect(base_url('Alberta'));
            exit();
        }
//        if( count( $this->input->post()) && base_url("/sign-in") === $_SERVER["HTTP_REFERER"]) {
        if( count( $this->input->post())) {

            $this->load->library("form_validation");

            $this->form_validation->set_rules('password', 'Password', 'required|trim');
            $this->form_validation->set_rules('email', 'Email', 'required|trim|valid_email');

            if ($this->form_validation->run() == false) {
                $msg['msg'] = validation_errors();
                $this->load->view('common_pages/alberta/login',$msg);
            } else {

                $userData = [
                    "email" => $this->input->post('email'),
                    "password" => md5($this->input->post('password')),
                    "access_level" => 1 //email for alberta portal
//                    "status" => 1 //verify that user is an active user
                ];
                $this->load->model("users");
                $user = $this->users->findclient($userData);

                if ($user) {
                    if ($user->status==1){

                        $sess_data=array(
                            'alb_loggedUserId'=>$user->id,
                            'alb_loggedUserUsername'=>$user->name,
                            'alb_access_level'=>(int)$user->access_level,
                            'alb_logged_email'=>$user->email,
                            'alb_logged_acc_type'=>$user->account_type
//                        'is_admin'=>false
                        );
                        switch ($user->access_level) {
                            case 1: {
                                $this->session->set_userdata($sess_data); //session start
                                redirect(base_url('Alberta')); //florida home
                                break;
                            }
                            default:
                                $this->session->set_flashdata('error','You are using wrong portal for login');
                                redirect(base_url("/alb_signIn"));
                        }

                    }else{
                        $this->session->set_flashdata('error','Account is not approved yet');
                        redirect(base_url("/alb_signIn"));
                    }

                } else {
                    $this->session->set_flashdata('error','Invalid Email/Password Provided');
                    redirect(base_url("/alb_signIn"));
                }
            }
        } else {
            redirect(base_url("/alb_signIn"));
        }
//        redirect(base_url());
    }
    public function alb_signUp()
    {
        if($this->session->alb_access_level){
            $this->session->set_flashdata('error',"Kindly logout first to perform this operation");
            redirect(base_url('Alberta'));
            exit();
        }
//        $this->loadView('auth/sign_up', null, "Sign Up"); //old design
        $this->load->view('common_pages/alberta/alb_signup');
    }
    public function alb_register()
    {
        if($this->session->alb_access_level){
            $this->session->set_flashdata('error',"Kindly Logout First To Perform This Operation");
            redirect(base_url('Alberta'));
            exit();
        }
        if( count( $this->input->post())){
            $this->load->library("form_validation");

            $this->form_validation->set_rules('username', 'Username', 'required|trim|max_length[16]|min_length[3]');
            $this->form_validation->set_rules('password', 'Password', 'required|trim|max_length[16]|min_length[3]');
            $this->form_validation->set_rules('password_rpt', 'Password Repeat', 'required|trim|matches[password]');
//            $this->form_validation->set_rules('email', 'Email', 'required|trim|valid_email|is_unique[client.email]');
            $this->form_validation->set_rules('email', 'Email', 'required|trim|valid_email|callback_verifyEmail['. 1 .']'); //1 for alberta
            $this->form_validation->set_rules('number', 'Phone', 'trim');
            $this->form_validation->set_rules('access', 'Access Level', 'trim|required');
            $this->form_validation->set_rules('acc_type', 'Account Type', 'trim|required');
            $this->form_validation->set_message('is_unique', 'This %s has already been registered with us. Use different email for each portal');

            if(!$this->form_validation->run()){
                $msg['msg']=validation_errors();
                $this->load->view('common_pages/alberta/alb_signup',$msg);
            } else {
                $accountType = trim($this->input->post('access'));
                switch ($accountType){
                    case 1:{$accountType = 1;break;}
                    default: {
                        $accountType = false;
                    }
                }
                if($accountType === false ) {
                    $msg['msg']= "Please select a valid access level";
                    $this->load->view('common_pages/alberta/alb_signup',$msg);
                } else {
                    $this->load->model('users');
                    $registerData = [
                        "name" => $this->input->post('username'),
                        "password" => md5($this->input->post('password')),
                        "email" => $this->input->post('email'),
                        "number" => $this->input->post('number'),
                        "access_level" => $accountType,
                        "account_type" => $this->input->post('acc_type'),
                        "status" => 0,
                    ];
                    $result = $this->users->addclient($registerData);
                    if($result === true){
                        $this->sendmail($this->input->post('username'),$this->input->post('email'),'Alberta');
                        $this->sendmail_admin($this->input->post('username'),$this->input->post('email'),'Alberta');
                        $this->session->set_flashdata('message', "Your account has been created successfully, once admin approves it then you will be notified by email.");
                        redirect(base_url('alb_sign-up'));
                    } else {
                        $msg['msg']= "Failed to insert record";
                        $this->load->view('common_pages/alberta/alb_signup',$msg);
                    }
                }
            }
        } else {
            redirect(base_url("/alb_sign-up"));
        }
    } //register alberta user
    public function alb_logout(){

        if(!$this->session->alb_access_level){
            $this->session->set_flashdata('error',"You are already Logged out");
            redirect(base_url('alb_signIn'));
            exit();
        }
        //        $this->session->sess_destroy(); //Rana
        $this->session->unset_userdata(["alb_access_level", "alb_loggedUserId", "alb_loggedUserUsername",]);
        redirect(base_url('alb_signIn'));
    }//alberta logout
    function alb_forgot_password(){

        if ($this->input->post('email')){
            $this->load->library("form_validation");
            $this->load->library('encryption');
            $this->form_validation->set_rules('email',"Email",'trim|required|valid_email');

            if($this->form_validation->run()==TRUE){
                $user_data=$this->input->post();
                unset($user_data['reset_password']);
                if($res=$this->crud->getthis('client'," access_level=1 and email = '".$user_data['email']."'")){

                    $hash = $this->encryption->encrypt($res->id);
//                $hash= str_replace("/",",",$hash);
                    $message='Click on the link to reset your password: '. base_url('alb_resetpass/').$hash;
                    $this->email_to($user_data['email'],$message);
                    $this->session->set_flashdata('success',"Reset password link has been sent to your mail");
                    redirect(base_url('alb_signIn'));
                }else{
                    $this->session->set_flashdata('error',"This email is not registered with us!");
                    redirect(base_url('alb_signIn'));
                }
            }else{
                $this->session->set_flashdata('error',validation_errors());
                redirect(base_url('alb_signIn'));
            }
        }else{
            $this->session->set_flashdata('error',"Invalid User Request");
            redirect(base_url('alb_signIn'));
        }
    }
    function alb_resetpass()
    {
        $this->load->library("form_validation");
        $this->load->library('encryption');
        if ($this->input->post('reset'))
        {
            $this->form_validation->set_rules('password', 'Password', 'required|trim|max_length[16]|min_length[3]');
            $this->form_validation->set_rules('c_password', 'Password Repeat', 'required|trim|matches[password]');

            if(!$this->form_validation->run()){
                $msg['msg']=validation_errors();
                $msg['res']=$this->input->post('id');
                $this->load->view('common_pages/alberta/alb_resetform',$msg);
            } else{
                $data['password']= md5($this->input->post('password'));
                $where= array('id'=>$this->input->post('id'),'access_level'=>1);

                if ($this->crud->update('client',$data,$where))
                {
                    $this->session->set_flashdata('success',"Password has been reset successfully");
                    redirect(base_url('alb_signIn'));
                }else {
                    $this->session->set_flashdata('error',"Failed to update password");
                    redirect(base_url('alb_resetpass/'.$this->input->post('id')));
                }
            }
        }else
        {
            if ($this->uri->segment(2))
            {
                $trail = func_get_args();
                $id=join("/",$trail);

//                $id = str_replace(",","/",$this->encryption->decrypt($this->uri->segment(2)));
                $id = $this->encryption->decrypt($id);
                if ($res['res']=$this->crud->getthis('client'," access_level=1 and id = ".$id)) {
                    $res['res']=$res['res']->id;
                    $this->load->view('common_pages/alberta/alb_resetform', $res);
                }else{
                    $this->session->set_flashdata('error',"No Record Found");
                    redirect(base_url('alb_signIn'));
                }
            }else{
                $this->session->set_flashdata('error',"Invalid user request");
                redirect(base_url('alb_signIn'));
            }
        }
    }




    //common function
    private function sendmail($name,$email,$portalname)
    {
        $htmlContent = "Welcome to $portalname<br>Thanks for registrating on $portalname.<br>Your account approval request has been forwarded to admin. You will be notified once admin will approve your account.<br>Thanks.<br>";
        sendemail($email,'service@bktechnologies.com',$htmlContent,'Account Creation Success Message');

//        $this->load->library('email');
//        $this->email->from('service@bktechnologies.com',"BK Technologies");
//        $this->email->to($email);
//        $this->email->subject('Account Creation Success Message');
//        $this->email->message($htmlContent);
//        $this->email->send();
    } //for account creation (fixed message template)


    //send mail admin
    private function sendmail_admin($name,$email,$portalname)
    {
        if($user=$this->crud->login('users',array('id'=>8))) //admin record id is 8 in database
        {
            $htmlContent = "Hi,<br>$name has requested for access to $portalname<br>,Having email $email <br>Please have a look at it to approve/deny<br>Thanks.<br>";
            sendemail($user[0]->user_email,'service@bktechnologies.com',$htmlContent,'NEW Account Approval Requested');

//            $this->load->library('email');
//            $this->email->from('service@bktechnologies.com',"BK Technologies");
//            $this->email->to($user[0]->user_email);
//            $this->email->subject('NEW Account Approval Requested');
//            $this->email->message($htmlContent);
//            $this->email->send();
        }
    } //for account creation (fixed message template)


//reset password
    function email_to($email,$message){
        sendemail($email,'service@bktechnologies.com',$message,'Reset Password');

//        $this->load->library('email');
//    	$this->email->from('service@bktechnologies.com',"BK Technologies");
//        $this->email->to($email);
//        $this->email->subject('Reset Password');
//        $this->email->message($message);
//        $this->email->send();
    }

//checks if email already exists in the relevent portal (for register user)
    function verifyEmail($email,$ac_type)
    {
        $isValid = false;

        $userData = [
            "email" => $email,
            "access_level" => (int)$ac_type
//                    "status" => 1 //verify that user is an active user
        ];
        $this->load->model("users");
        $user = $this->users->findclient($userData);


        if(empty($user) && !isset($user->id)){
            $isValid= true;
        }else{
            $this->form_validation->set_message('verifyEmail', 'This email has already been registered with us. Use different email for each portal');
        }
        return $isValid;
    }






//    public function signIn()
//    {
//        //$this->layout='admin/default';
//        if($this->session->access_level){
//            redirect(base_url());
//            exit();
//        }
////        $this->loadView('auth/sign_in', null, "Sign In");//old login
//        $this->load->view('auth/login'); //new design
//    }
//    public function signUp()
//    {
//        if($this->session->access_level){
//            redirect(base_url());
//            exit();
//        }
////        $this->loadView('auth/sign_up', null, "Sign Up"); //old design
//        $this->load->view('auth/signup'); //new design
//    }

//    public function register()
//    {
//        if($this->session->access_level){
//            redirect(base_url());
//            exit();
//        }
//        if( count( $this->input->post()) && base_url("/sign-up") === $_SERVER["HTTP_REFERER"]){
//            $this->load->library("form_validation");
//
//            $this->form_validation->set_rules('username', 'Username', 'required|trim|max_length[16]|min_length[3]');
//            $this->form_validation->set_rules('password', 'Password', 'required|trim|max_length[16]|min_length[3]');
//            $this->form_validation->set_rules('password_rpt', 'Password Repeat', 'required|trim|matches[password]');
//            $this->form_validation->set_rules('email', 'Email', 'required|trim|valid_email|is_unique[client.email]');
//            $this->form_validation->set_rules('number', 'Phone', 'trim');
//            $this->form_validation->set_rules('access', 'Access Level', 'trim|required');
//            $this->form_validation->set_rules('acc_type', 'Account Type', 'trim|required');
//
//            if(!$this->form_validation->run()){
////                $this->session->set_flashdata("error", validation_errors());
//                $msg['msg']=validation_errors();
////                $this->loadView('auth/sign_up',$msg,'Sign-up'); //old design
//                $this->load->view('auth/signup',$msg); //new design
////                redirect(base_url("/sign-up"));
//            } else {
//                $accountType = trim($this->input->post('access'));
//                switch ($accountType){
//                    case 1:{$accountType = 1; break;}
//                    case 2:{$accountType = 2;break;}
//                    case 3:{$accountType = 3;break;}
//                    default: {
//                        $accountType = false;
//                    }
//                }
//                if($accountType === false ) {
//                    $msg['msg']= "Please select a valid access level";
//                    $this->load->view('auth/signup',$msg); //new design
////                    $this->loadView('auth/sign_up',$msg,'Sign-up'); //old design
//                } else {
//                    $this->load->model('users');
//                    $user = $this->users->findUserByUsername($this->input->post('username'));
//                    $registerData = [
//                        "name" => $this->input->post('username'),
//                        "password" => md5($this->input->post('password')),
//                        "email" => $this->input->post('email'),
//                        "number" => $this->input->post('number'),
//                        "access_level" => $accountType,
//                        "account_type" => $this->input->post(),
//                        "status" => 0,
//                    ];
//                    $result = $this->users->addclient($registerData);
//                    if($result === true){
//
////                        $msg['success']="Account created successfully (Admin verification needed)";
////                        $this->loadView('auth/sign_up',$msg,'Sign-up');
//                        $this->session->set_flashdata('message', "Account created successfully (Admin verification needed");
//                        redirect(base_url('/sign-up'));
////                        redirect(base_url("/sign-in"));
//                    } else {
//                        $msg['msg']= "Failed to insert record";
////                        $this->loadView('auth/sign_up',$msg,'Sign-up');//old design
//                        $this->load->view('auth/signup',$msg); //new design
//                    }
//                }
//            }
//        } else {
//            redirect(base_url("/sign-up"));
//        }
//    } //create clients

//    public function login()
//    {
//        if($this->session->access_level){
//            redirect(base_url());
//            exit();
//        }
////        if( count( $this->input->post()) && base_url("/sign-in") === $_SERVER["HTTP_REFERER"]) {
//        if( count( $this->input->post())) {
//
//            $this->load->library("form_validation");
//
//            $this->form_validation->set_rules('password', 'Password', 'required|trim');
//            $this->form_validation->set_rules('email', 'Email', 'required|trim|valid_email');
//
//            if ($this->form_validation->run() == false) {
//                $msg['msg'] = validation_errors();
//                $this->load->view('auth/login',$msg); //new design
//            } else {
//
//                $userData = [
//                    "email" => $this->input->post('email'),
//                    "password" => md5($this->input->post('password')),
//                    "status" => 1 //verify that user is an active user
//                ];
//                $this->load->model("users");
//                $user = $this->users->findclient($userData);
//
//                if ($user) {
//    //                 $userPassword = $user->password;
//    //                 if(password_verify($userData["password"], $userPassword)){
//                    $sess_data=array(
//                        'loggedUserId'=>$user->id,
//                        'loggedUserUsername'=>$user->name,
//                        'access_level'=>(int)$user->access_level
////                        'is_admin'=>false
//                    );
//                    $this->session->set_userdata($sess_data);
//                    switch ($user->access_level) {
//                        case 1: {
//                            redirect(base_url('default-ahs'));//alberta home
//                            break;
//                        }
//                        case 2: {
//                            redirect(base_url('flfs')); //florida home
//                            break;
//                        }
//                        case 3: {
//                            redirect(base_url('CalFire'));//CalFire Home
//                            break;
//                        }
//                        default:
//                            echo "hammad: ".$user->access_level;
//                            redirect(base_url());
//                        //redirect @ home apply checks at method direct link access
//                    }
//    //                     redirect(base_url("/my-page"));
//    //                 } else {
//    //                     $this->session->set_flashdata('error', '<p class="error_message">Invalid Username or Password</p>');
//    //                     redirect(base_url("/sign-in"));
//    //                 }
////                    print_r($this->session->all_userdata());
////                    exit;
//                } else {
//                    $this->session->set_flashdata('error','Invalid Username or Password or unverified account');
//                    redirect(base_url("/sign-in"));
//                }
//            }
//        } else {
//            redirect(base_url("/sign-in"));
//        }
////        redirect(base_url());
//    }

//    public function logout(){
//
//        echo $_SERVER['HTTP_REFERER'];
//        exit;
//
//        echo "<pre>";
//        print_r($_SESSION);
//        exit;
//
////        if(!$this->session->access_level){
////            redirect(base_url());
////            exit();
////        }
//        $access_level=$this->session->access_level;
//
//        if ($this->session->access_level)//florida
//        {
//            $access_level=$this->session->access_level;
//        }elseif ($this->session->cal_access_level)
//        {
//            $access_level=$this->session->cal_access_level;
//        }elseif ($this->session->alb_access_level)
//        {
//            $access_level=$this->session->alb_access_level;
//        }
//
////        $this->session->sess_destroy(); //Rana
////        $this->session->unset_userdata(["access_level", "loggedUserId", "loggedUserUsername",]);
//
//        switch ($access_level){
//            case 1:
//            {
//                $this->session->unset_userdata(["alb_access_level", "alb_loggedUserId", "alb_loggedUserUsername",]); //florida
//                redirect(base_url('default-ahs'));
//                break;
//            }
//            case 2:
//            {
//                $this->session->unset_userdata(["access_level", "loggedUserId", "loggedUserUsername",]); //florida
//                redirect(base_url('flfs'));
//                break;
//            }
//            case 3:
//            {
//                $this->session->unset_userdata(["cal_access_level", "cal_loggedUserId", "cal_loggedUserUsername",]);
//                redirect(base_url('CalFire'));
//                break;
//            }
//            default:
//                redirect(base_url());
//        }
//
//    }

}
