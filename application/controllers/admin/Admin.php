<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->library('form_validation');
		//$this->load->model('crud');
		$this->form_validation->set_error_delimiters('<p class="error" style="color:red;margin:0;text-align:left;">', '</p>');

    }
	
	public function index(){
		if($this->session->userdata('is_logged_in')){
			redirect('admin/dashboard');
		}
		$this->load->view('admin/loginheader');
		$this->load->view('admin/login');
		$this->load->view('admin/loginfooter');
	}

	function login(){

		$this->form_validation->set_rules('username',"Username",'trim|required');
		$this->form_validation->set_rules('password','Password','trim|required');

		if($this->form_validation->run()==TRUE){

			$user_data=$this->input->post();

			$user_login_data=array(
				'user_email'=>$user_data['username'],
				'user_pass' =>md5($user_data['password'])
			);
//			$user_login_data=array(
//				'email'=>$user_data['username'],
//				'password' =>md5($user_data['password'])
//			);

//			if($user=$this->crud->login('admin',$user_login_data)){
			if($user=$this->crud->login('users',$user_login_data)){
				//echo "<pre>";print_r($user);echo "</pre>";die;
				$session_data=array(
					'user_id'=>$user[0]->id,
					'user_login'=>$user[0]->user_login,
					'user_image'=>$user[0]->user_image!="" ? $user[0]->user_image : 'default_user_image.png',
					'user_name'=>$user[0]->user_image!=""? $user[0]->user_firstname.' '.$user[0]->user_lastname :'No Name',
					'user_tag_name'=>$user[0]->user_tag_name!=""?$user[0]->user_tag_name:"",
					'user_registered'=>$user[0]->user_registered,
					'is_logged_in'=>true,
                    'is_admin'=>true
				);
				$this->session->set_userdata($session_data);
				redirect('admin/dashboard');
			}else{
				$this->session->set_flashdata('login_error',"Either username or password wrong!");
				redirect('admin');
			}

		}

		$this->load->view('admin/loginheader');
		$this->load->view('admin/login');
		$this->load->view('admin/loginfooter');

	}

//	function signup(){
//		$this->form_validation->set_rules('user_login',"Username",'trim|required|is_unique[users.user_login]');
//		$this->form_validation->set_rules('user_email',"Email",'trim|required|valid_email|is_unique[users.user_email]');
//		$this->form_validation->set_rules('user_pass','Password','trim|required');
//		$this->form_validation->set_rules('user_conf_pass','Confirm Password','trim|required|matches[user_pass]');
//
//		if($this->form_validation->run()==TRUE){
//			$user_data=$this->input->post();
//			unset($user_data['signup']);
//			unset($user_data['user_conf_pass']);
//			$user_data['user_pass']=md5($user_data['user_pass']);
//			if($this->crud->insert('users',$user_data)){
//				$this->session->set_flashdata('signup_error','Registration Successful!');
//			}else{
//				$this->session->set_flashdata('signup_error','Registration Failed!');
//			}
//		}
//		$data=array('view_signup'=>1);
//		$this->load->view('admin/loginheader');
//		$this->load->view('admin/login',$data);
//		$this->load->view('admin/loginfooter');
//
//	}


	function logout(){
		if($this->session->userdata('is_logged_in')==TRUE){
            $this->session->unset_userdata(['user_login','user_image','user_name','user_tag_name','user_registered','is_logged_in','is_admin']);
//			$this->session->sess_destroy();
			redirect('admin');
		}
        redirect(base_url());
	}


	function reset_password(){
		$this->form_validation->set_rules('email',"Email",'trim|required|valid_email');
		
		if($this->form_validation->run()==TRUE){
			$user_data=$this->input->post();
			unset($user_data['reset_password']);
			$email=array('user_email'=>$user_data['email']);
			if($this->crud->validate('users',$email)){
				$new_pass=$this->generateRandomString();
				if($this->crud->update('users',array('user_pass'=>md5($new_pass)),$email)){
					$message='New Password for your account is : '.$new_pass;
						$this->email_to($user_data['email'],$message);
						$this->session->set_flashdata('reset_successful',1);
						redirect('admin');
				}
			}else{
					$this->session->set_flashdata('forgot_error',"This email is not registered with us!");
			}
		}

		$data=array('view_signup'=>2);
		$this->load->view('admin/loginheader');
		$this->load->view('admin/login',$data);
		$this->load->view('admin/loginfooter');
	}

	function generateRandomString($length = 10) {
		    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
		    $charactersLength = strlen($characters);
		    $randomString = '';
		    for ($i = 0; $i < $length; $i++) {
		        $randomString .= $characters[rand(0, $charactersLength - 1)];
		    }
		    return $randomString;
	}

	function email_to($emailId,$message){
				$this->load->library('email');

				$this->email->from('rp.linuxbn@gmail.com', 'Your Name');
//				$this->email->from('', 'BK Technologies Mail Service');
				$this->email->to($emailId);
				$this->email->subject('New Password');
				$this->email->message($message);
				$this->email->send();
	}


}