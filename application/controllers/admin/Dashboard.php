<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
* 
*/
class Dashboard extends CI_Controller
{
  
  function __construct()
  {
    parent::__construct();
    $this->load->library('form_validation');
    $this->load->model('crud');
    if(!$this->session->userdata('is_logged_in')){
      redirect('admin');
    }
  }

  function set_session_data(){
    if($this->session->userdata('is_logged_in')){
      $check_data=array('id' => $this->session->userdata('user_id'));
      if($user=$this->crud->validate('users',$check_data)){

        $session_data=array(
          'user_id'=>$user[0]->id,
          'user_login'=>$user[0]->user_login,
          'user_image'=>$user[0]->user_image!="" ? $user[0]->user_image : 'default_user_image.png',
          'user_name'=>$user[0]->user_image!=""? $user[0]->user_firstname.' '.$user[0]->user_lastname :'No Name',
          'user_tag_name'=>$user[0]->user_tag_name!=""?$user[0]->user_tag_name:"",
          'user_registered'=>$user[0]->user_registered,
          'is_logged_in'=>true,
            'is_admin'=>'true'
        );
        $this->session->set_userdata($session_data);

      }
    
    }
  }

  function index(){
    $data_to_insert=$this->input->post();
    if(isset($data_to_insert['save_profile'])){

      $this->form_validation->set_rules('user_firstname','First Name','trim|required');
      $this->form_validation->set_rules('user_lastname','Last Name','trim|required');     
      $this->form_validation->set_rules('user_email','Email','trim|required|valid_email');
      if($this->form_validation->run()==true){
        $check_data=array('id'=>$this->session->userdata('user_id'));
        unset($data_to_insert['save_profile']);
        
        if($this->crud->validate('users',$check_data)){
          if($this->crud->update('users',$data_to_insert,$check_data)){
            $this->set_session_data();
            $this->session->set_flashdata('notification','Updated Successfully!');
          }
        }
      }

    }

    if(isset($data_to_insert['check_old_password'])){

      $this->form_validation->set_rules('user_old_pass','Password','trim|required');
      
      if($this->form_validation->run()==true){
        
        $check_data=array('id'=>$this->session->userdata('user_id'),'user_pass'=>md5($data_to_insert['user_old_pass']));
        unset($data_to_insert['check_old_password']);
        
        if($this->crud->validate('users',$check_data)){
          
          $this->session->set_flashdata('old_pass_confirmed',true);
          $this->session->set_flashdata('active_pan','password');
          $this->session->set_flashdata('old_pass_confirmed_error','Old Password is Correct!');
          redirect('admin/dashboard');        
        }else{
          $this->session->set_flashdata('old_pass_confirmed',false);
          $this->session->set_flashdata('active_pan','password');
          $this->session->set_flashdata('old_pass_confirmed_error','Old Password is Wrong!');
          redirect('admin/dashboard');
        }

      }

    }
    if(isset($data_to_insert['new_password'])){

      $this->form_validation->set_rules('user_pass','New Password','trim|required');
      $this->form_validation->set_rules('user_conf_pass','Confirm Password','trim|required|matches[user_pass]');
      
      if($this->form_validation->run()==true){
        $check_data=array('id'=>$this->session->userdata('user_id'));
        unset($data_to_insert['new_password']);
        unset($data_to_insert['user_conf_pass']);
        $data_to_insert['user_pass']=md5($data_to_insert['user_pass']);
        if($this->crud->validate('users',$check_data)){
          if($this->crud->update('users',$data_to_insert,$check_data)){
            $this->session->set_flashdata('notification','Updated Successfully!');
            $this->session->set_flashdata('active_pan','password');
            redirect('admin/dashboard');
          }
        }
      }

    }

    if(isset($data_to_insert['save_user_image'])){
    

    

                $config['upload_path']          = "./assets/images/";
                $config['allowed_types']        = 'gif|jpg|png';
                $config['max_size']             = 100;
                $config['max_width']            = 1024;
                $config['max_height']           = 768;

                $this->load->library('upload', $config);

                if ( ! $this->upload->do_upload('user_image'))
                {
                        $error = array('error' => $this->upload->display_errors());                        
                        $this->session->set_flashdata('notification',$error);
                        
                        redirect('admin/dashboard');
                }
                else
                {
                    $this->load->helper('file');
                    $check_data=array('id'=>$this->session->userdata('user_id'));
                    $result=$this->crud->getAllBy('users',$check_data)[0];
                    if($result->user_image!=""){
                      $file_nam_old='/assets/images/'.$result->user_image;                    
                      unlink(FCPATH . $file_nam_old);                   
                      
                    }
                        $data = array('upload_data' => $this->upload->data());                   
                        $image_to_insert['user_image']=$data['upload_data']['file_name'];
                        
                        if($this->crud->update('users',$image_to_insert,$check_data)){
                          $this->set_session_data();
                      $this->session->set_flashdata('notification','Updated Successfully!');   
                    
                      redirect('admin/dashboard');
            }
                        
                }

    }
    

    $user=array('id'=>$this->session->userdata('user_id'));
    $data=$this->crud->getAllBy('users',$user)[0];
    $fetched_data['user_about_data']=$data?$data:array();
   // $fetched_data['skills_count']=$this->crud->countAll('skills');
    //$fetched_data['awards_count']=$this->crud->countAll('awards');

    $heade_data=array('page_title'=>"Dashboard");
    $active_pages=array('active_class'=>'dashboard','active_sub_class'=>'dashboard');
    
    $this->load->view('admin/dashboard/header',$heade_data);
    $this->load->view('admin/dashboard/navigation',$active_pages);
    $this->load->view('admin/dashboard/dashboard',$fetched_data);
    $this->load->view('admin/dashboard/footer');
  }
  function validate(){
    $data_to_insert=$this->input->post();
    $table=$data_to_insert['table'];    
        
    $check_data=array('id'=>$this->session->userdata('user_id'),'user_pass'=>md5($data_to_insert['user_old_pass']));
    unset($data_to_insert['table']);
        
        if($this->crud->validate('users',$check_data)){
          
          $this->session->set_flashdata('old_pass_confirmed',true);
          $this->session->set_flashdata('active_pan','password');       
        }else{
          $this->session->set_flashdata('old_pass_confirmed',false);
          $this->session->set_flashdata('active_pan','password');
        }

      
  }
  function about(){
    
    $data_to_insert=$this->input->post();

    if(isset($data_to_insert['save_about'])){
      /*echo "<pre>";print_r($data_to_insert);echo "</pre>";die;*/
      $this->form_validation->set_rules('address',"Address",'trim|required');
      $this->form_validation->set_rules('city','City','trim|required');
      $this->form_validation->set_rules('country','Country','trim|required');
      $this->form_validation->set_rules('zipcode','Zip Code','trim|required');
      $this->form_validation->set_rules('mobile','Mobile Number','trim|required');
      $this->form_validation->set_rules('dob','Date Of Birth','trim|required');
      if($this->form_validation->run()==true){

        $check_data=array('user_id'=>$this->session->userdata('user_id'));
        unset($data_to_insert['save_about']);
        
        if($this->crud->validate('about',$check_data)){
          if($this->crud->update('about',$data_to_insert,$check_data)){
            $this->session->set_flashdata('notification','Updated Successfully!');
          }
          
        }else{
          
          $data_to_insert['user_id']=$this->session->userdata('user_id');
          if($this->crud->insert('about',$data_to_insert)){
            $this->session->set_flashdata('notification','Inseted Successfully!');
          }

        }
      }
    
    }
    $fetched_data=array();
    $user=array('user_id'=>$this->session->userdata('user_id'));
    $fetched_data['user_about_data']=$this->crud->getAllBy('about',$user)[0];
    $heade_data=array('page_title'=>"About");
    $active_pages=array('active_class'=>'about','active_sub_class'=>'about');
    $this->load->view('admin/dashboard/header',$heade_data);
    $this->load->view('admin/dashboard/navigation',$active_pages);
    $this->load->view('admin/dashboard/about',$fetched_data);
    $this->load->view('admin/dashboard/footer');
  }


    //    function add_Bulletins(){
//
//    $data_to_insert=$this->input->post();
//
//    if(isset($data_to_insert['save_education'])){
//      /*echo "<pre>";print_r($data_to_insert);echo "</pre>";die;
//      $this->form_validation->set_rules('title',"Job Title",'trim|required');
//      $this->form_validation->set_rules('company','Company/Institute Name','trim|required');
//      $this->form_validation->set_rules('start_date','Start Date','trim|required');*/
//
//      //if($this->form_validation->run()==true){
//
//
//$config['upload_path']          = "./assets/images/";
//                $config['allowed_types']        = 'application/pdf|pdf|application/octet-stream|csv';
//               /* $config['max_size']             = 100;
//                $config['max_width']            = 1024;
//                $config['max_height']           = 768;*/
//
//                $this->load->library('upload', $config);
//
//                if ( ! $this->upload->do_upload('user_image'))
//                {
//                        $error = array('error' => $this->upload->display_errors());
//                        $this->session->set_flashdata('notification',$error);
//                        redirect('admin/dashboard/add_Bulletins');
//                }
//                else
//                {
//                    $this->load->helper('file');
//                    $check_data=array('id'=>$this->session->userdata('user_id'));
//                    $result=$this->crud->getAllBy('users',$check_data)[0];
//
//                        $data = array('upload_data' => $this->upload->data());
//                        $data_to_insert['File']=$data['upload_data']['file_name'];
//
//        unset($data_to_insert['save_education']);
//          //$data_to_insert['user_id']=$this->session->userdata('user_id');
//          if($this->crud->insert('bulletins',$data_to_insert)){
//            $this->session->set_flashdata('notification','Inseted Successfully!');
//            redirect('admin/dashboard/add_Bulletins');
//          }
//      //}
//
//    }
//}
//    $fetched_data=array();
//    $heade_data=array('page_title'=>"Add Bulletins");
//    $active_pages=array('active_class'=>'Bulletins','active_sub_class'=>'add_Bulletins');
//    $this->load->view('admin/dashboard/header',$heade_data);
//    $this->load->view('admin/dashboard/navigation',$active_pages);
//    $this->load->view('admin/dashboard/Bulletins.php',$fetched_data);
//    $this->load->view('admin/dashboard/footer');
//  }
//    public function view_Bulletins(){
//               // $user=array('user_id'=>$this->session->userdata('user_id'));
//    $fetched_data['user_about_data']=$this->crud->getAll('bulletins')?$this->crud->getAll('bulletins'):array();
//    $heade_data=array('page_title'=>"Bulletins");
//    $active_pages=array('active_class'=>'Bulletins','active_sub_class'=>'view_Bulletins');
//    $this->load->view('admin/dashboard/header',$heade_data);
//    $this->load->view('admin/dashboard/navigation',$active_pages);
//    $this->load->view('admin/dashboard/view_Bulletins',$fetched_data);
//    $this->load->view('admin/dashboard/footer');
//    $this->load->view('admin/dashboard/ajax');
//     }
//    function add_Product(){
//
//    $data_to_insert=$this->input->post();
//
//    if(isset($data_to_insert['save_productDetails'])){
//      unset($data_to_insert['save_productDetails']);
//          //$data_to_insert['user_id']=$this->session->userdata('user_id');
//          if($this->crud->insert('Kaa0732-33',$data_to_insert)){
//            $this->session->set_flashdata('notification','Inseted Successfully!');
//            redirect('admin/dashboard/add_Product');
//          }
//
//
//    }
//
//    $fetched_data=array();
//    $heade_data=array('page_title'=>"Add Bulletins");
//    $active_pages=array('active_class'=>'product','active_sub_class'=>'add_product');
//    $this->load->view('admin/dashboard/header',$heade_data);
//    $this->load->view('admin/dashboard/navigation',$active_pages);
//    $this->load->view('admin/dashboard/add_product.php',$fetched_data);
//    $this->load->view('admin/dashboard/footer');
//  }
//    public function view_Product(){
//               // $user=array('user_id'=>$this->session->userdata('user_id'));
//    $fetched_data['user_about_data']=$this->crud->getAll('Kaa0732-33')?$this->crud->getAll('Kaa0732-33'):array();
//    $heade_data=array('page_title'=>"Bulletins");
//    $active_pages=array('active_class'=>'product','active_sub_class'=>'view_Product');
//    $this->load->view('admin/dashboard/header',$heade_data);
//    $this->load->view('admin/dashboard/navigation',$active_pages);
//    $this->load->view('admin/dashboard/view_Product',$fetched_data);
//    $this->load->view('admin/dashboard/footer');
//    $this->load->view('admin/dashboard/ajax');
//     }
//    public function view_Product_types(){
//    $fetched_data['user_about_data']=$this->crud->getAll('Kaa0732-33')?$this->crud->getAll('Kaa0732-33'):array();
//    $heade_data=array('page_title'=>"Bulletins");
//    $active_pages=array('active_class'=>'product','active_sub_class'=>'view_Product');
//    $this->load->view('admin/dashboard/header',$heade_data);
//    $this->load->view('admin/dashboard/navigation',$active_pages);
//    $this->load->view('admin/dashboard/view_Product',$fetched_data);
//    $this->load->view('admin/dashboard/footer');
//    $this->load->view('admin/dashboard/ajax');
//     }

    // parts category list
//    function awards(){
//
//        $data_to_insert=$this->input->post();
//
//        if(isset($data_to_insert['save_partCategory'])){
//            unset($data_to_insert['save_partCategory']);
//
//              //$data_to_insert['user_id']=$this->session->userdata('user_id');
//              if($this->crud->insert('partsType',$data_to_insert)){
//                $this->session->set_flashdata('notification','Inserted Successfully!');
//                redirect('admin/dashboard/awards');
//              }else{
//                $this->session->set_flashdata('notification','Insertion Failed!');
//              }
//        }
//        if(isset($data_to_insert['edit_awards'])){
//            unset($data_to_insert['edit_awards']);
//            $id=$data_to_insert['id'];
//            unset($data_to_insert['id']);
//
//              if($this->crud->update('partsType',$data_to_insert,array('id'=>$id))){
//                $this->session->set_flashdata('notification','Updated Successfully!');
//                redirect('admin/dashboard/awards');
//              }else{
//                $this->session->set_flashdata('notification','Updation Failed!');
//              }
//        }
//
//
//        //$user=array('user_id'=>$this->session->userdata('user_id'));
//        $data=$this->crud->getAll('partsType');
//        $fetched_data['user_education_data']=$data?$data:array();
//        $heade_data=array('page_title'=>"PART CATEGORY");
//        $active_pages=array('active_class'=>'part','active_sub_class'=>'awards');
//        $this->load->view('admin/dashboard/header',$heade_data);
//        $this->load->view('admin/dashboard/navigation',$active_pages);
//        $this->load->view('admin/dashboard/awards',$fetched_data);
//        $this->load->view('admin/dashboard/footer');
//        $this->load->view('admin/dashboard/ajax');
//      }
//
//    function add_Part(){
//     $data_to_insert=$this->input->post();
//
//    if(isset($data_to_insert['save_part'])){
//
//                $config['upload_path']          = "./assets/images/";
//                $config['allowed_types']        = 'gif|jpg|png';
//                /*$config['max_size']             = 100;
//                $config['max_width']            = 1024;
//                $config['max_height']           = 768;*/
//
//                $this->load->library('upload', $config);
//
//                if ( ! $this->upload->do_upload('user_image'))
//                {
//                        $error = array('error' => $this->upload->display_errors());
//                        $this->session->set_flashdata('notification',$error);
//                        redirect('admin/dashboard/add_Part');
//                }
//                else
//                {
//                    $this->load->helper('file');
//                    $check_data=array('id'=>$this->session->userdata('user_id'));
//                    $result=$this->crud->getAllBy('users',$check_data)[0];
//
//                        $data = array('upload_data' => $this->upload->data());
//                        $data_to_insert['image']=$data['upload_data']['file_name'];
//
//        unset($data_to_insert['save_part']);
//          //$data_to_insert['user_id']=$this->session->userdata('user_id');
//          if($this->crud->insert('parts',$data_to_insert)){
//            $this->session->set_flashdata('notification','Inseted Successfully!');
//            redirect('admin/dashboard/add_Part');
//          }
//      //}
//
//    }
//}
//
//    $fetched_data=array();
//    $data=$this->crud->getAll('partsType');
//    $fetched_data['partTypes']=$data?$data:array();
//    $heade_data=array('page_title'=>"Add Bulletins");
//    $active_pages=array('active_class'=>'part','active_sub_class'=>'add_Part');
//    $this->load->view('admin/dashboard/header',$heade_data);
//    $this->load->view('admin/dashboard/navigation',$active_pages);
//    $this->load->view('admin/dashboard/add_part',$fetched_data);
//    $this->load->view('admin/dashboard/footer');
//  }
//
//
//    public function view_part(){
//   $fetched_data['user_about_data']=$this->crud->selectusingjoin('parts','partsType','partsType.id=parts.type','parts.id,parts.image,parts.status,parts.title,partsType.typetitle')?$this->crud->selectusingjoin('parts','partsType','parts.type = partsType.id','parts.id,parts.image,parts.status,parts.title,partsType.typetitle'):array();    $heade_data=array('page_title'=>"Part");
//    $active_pages=array('active_class'=>'part','active_sub_class'=>'view_part');
//    $this->load->view('admin/dashboard/header',$heade_data);
//    $this->load->view('admin/dashboard/navigation',$active_pages);
//    $this->load->view('admin/dashboard/view_part',$fetched_data);
//    $this->load->view('admin/dashboard/footer');
//    $this->load->view('admin/dashboard/ajax');
//     }
//
//    function view_part_list(){
//    $data_id = $this->uri->segment(4);
//
//    $data_to_insert=$this->input->post();
//           if(isset($data_to_insert['save_partlist'])){
//                unset($data_to_insert['save_partlist']);
//          // $cond=array('parts_id'=>$data_id);
//           $data_to_insert['parts_id']=$data_id;
//           if($this->crud->insert('partsList',$data_to_insert)){
//            $this->session->set_flashdata('notification','Inserted Successfully!');
//            //redirect('admin/dashboard/view_part_list');
//          }else{
//            $this->session->set_flashdata('notification','Insertion Failed!');
//          }
//    }
//    $fetched_data=array();
//    $cond=array('parts_id'=>$data_id);
//
//    $fetched_data['partslists']=$this->crud->getAllBy('partsList',$cond)?$this->crud->getAllBy('partsList',$cond):array();
//    $heade_data=array('page_title'=>"Edit Experience");
//    $active_pages=array('active_class'=>'part','active_sub_class'=>'view_part');
//    $this->load->view('admin/dashboard/header',$heade_data);
//    $this->load->view('admin/dashboard/navigation',$active_pages);
//    $this->load->view('admin/dashboard/view_partlist',$fetched_data);
//    $this->load->view('admin/dashboard/footer');
//    $this->load->view('admin/dashboard/ajax');
//  }
//    public function view_product_variation(){
//    $productname= $_POST['products'];
//     $data_to_insert=$this->input->post();
//     $fetched_data=array();
//    $cond=array('productCategoty'=>$productname);
//         $data_to_insert=$this->input->post();
//
//              if(isset($data_to_insert['save_productvariation'])){
//                    $config['upload_path']= "./assets/images/";
//                   // $config['allowed_types']='application/pdf|pdf|application/octet-stream|csv|application/zip|application/x-zip|application/x-zip-compressed|application/octet-stream|application/x-compress|application/x-compressed|multipart/x-zip';
//
//                    $config['allowed_types']= '*';
//                    $config['max_size'] = '25024155';
//                    $this->load->library('upload', $config);
//                    $this->upload->initialize($config);
//                    if ( ! $this->upload->do_upload('ReleaseNotes'))
//                          {
//                                  $error = array('error' => $this->upload->display_errors());
//                                  $this->session->set_flashdata('notification',$error);
//                                 // redirect('admin/dashboard/add_Bulletins');
//
//                          }
//                          else
//                          {
//
//                            $this->load->helper('file');
//                            $check_data=array('id'=>$this->session->userdata('user_id'));
//                            $result=$this->crud->getAllBy('users',$check_data)[0];
//                            $data = array('upload_data' => $this->upload->data());
//                            $data_to_insert['ReleaseNotes']=$data['upload_data']['file_name'];
//
//                             // code for upload zip file
//
//
//                           // $this->load->library('upload', $config);
//                            if ( ! $this->upload->do_upload('zipFile'))
//                          {
//                                  $error = array('error' => $this->upload->display_errors());
//                                  $this->session->set_flashdata('notification',$error);
//                                 // redirect('admin/dashboard/add_Bulletins');
//                                 print_r($error);
//                                  die;
//                          }
//                          else
//                          {
//                            $this->load->helper('file');
//                            $check_data=array('id'=>$this->session->userdata('user_id'));
//                            $result=$this->crud->getAllBy('users',$check_data)[0];
//                            $data = array('upload_data' => $this->upload->data());
//                            $data_to_insert['File']=$data['upload_data']['file_name'];
//                           // unset($data_to_insert['File']);
//                            unset($data_to_insert['products']);
//                            unset($data_to_insert['save_productvariation']);
//                               if($this->crud->insert('productvariation',$data_to_insert)){
//                                $this->session->set_flashdata('notification','Inseted Successfully!');
//                               // redirect('admin/dashboard/add_Bulletins');
//                              }
//                              }
//                //}
//
//              }
//          }
//
//              if(isset($data_to_insert['save_productsoftware'])){
//                    $config['upload_path']= "./assets/images/";
//                    $config['allowed_types']='application/pdf|pdf|application/octet-stream|csv';
//                    $this->load->library('upload', $config);
//                    if ( ! $this->upload->do_upload('ReleaseNotes'))
//                          {
//                                  $error = array('error' => $this->upload->display_errors());
//                                  $this->session->set_flashdata('notification',$error);
//                                 // redirect('admin/dashboard/add_Bulletins');
//                          }
//                          else
//                          {
//                              $this->load->helper('file');
//                              $check_data=array('id'=>$this->session->userdata('user_id'));
//                              $result=$this->crud->getAllBy('users',$check_data)[0];
//
//                                  $data = array('upload_data' => $this->upload->data());
//                                  $data_to_insert['ReleaseNotes']=$data['upload_data']['file_name'];
//                            unset($data_to_insert['products']);
//                            unset($data_to_insert['save_productsoftware']);
//                               if($this->crud->insert('productSW',$data_to_insert)){
//                                $this->session->set_flashdata('notification','Inseted Successfully!');
//                               // redirect('admin/dashboard/add_Bulletins');
//                              }
//                //}
//
//              }
//          }
//          if(isset($data_to_insert['save_productmenual'])){
//                    $config['upload_path']= "./assets/images/";
//                    $config['allowed_types']='application/pdf|pdf|application/octet-stream|csv';
//                    $this->load->library('upload', $config);
//                    if ( ! $this->upload->do_upload('File'))
//                          {
//                                  $error = array('error' => $this->upload->display_errors());
//                                  $this->session->set_flashdata('notification',$error);
//                                 // redirect('admin/dashboard/add_Bulletins');
//                          }
//                          else
//                          {
//                              $this->load->helper('file');
//                              $check_data=array('id'=>$this->session->userdata('user_id'));
//                              $result=$this->crud->getAllBy('users',$check_data)[0];
//
//                                  $data = array('upload_data' => $this->upload->data());
//                                  $data_to_insert['File']=$data['upload_data']['file_name'];
//                            unset($data_to_insert['products']);
//                            unset($data_to_insert['save_productmenual']);
//                               if($this->crud->insert('productManual',$data_to_insert)){
//                                $this->session->set_flashdata('notification','Inseted Successfully!');
//                               // redirect('admin/dashboard/add_Bulletins');
//                              }
//                //}
//
//              }
//          }
//          if(isset($data_to_insert['save_productdriver'])){
//                    $config['upload_path']= "./assets/images/";
//                    $config['allowed_types']='application/pdf|pdf|application/octet-stream|csv';
//                    $this->load->library('upload', $config);
//                    if ( ! $this->upload->do_upload('file'))
//                          {
//                                  $error = array('error' => $this->upload->display_errors());
//                                  $this->session->set_flashdata('notification',$error);
//                                 // redirect('admin/dashboard/add_Bulletins');
//                          }
//                          else
//                          {
//                              $this->load->helper('file');
//                              $check_data=array('id'=>$this->session->userdata('user_id'));
//                              $result=$this->crud->getAllBy('users',$check_data)[0];
//
//                                  $data = array('upload_data' => $this->upload->data());
//                                  $data_to_insert['file']=$data['upload_data']['file_name'];
//                            unset($data_to_insert['products']);
//                            unset($data_to_insert['save_productdriver']);
//                               if($this->crud->insert('ProductDriver',$data_to_insert)){
//                                $this->session->set_flashdata('notification','Inseted Successfully!');
//                               // redirect('admin/dashboard/add_Bulletins');
//                              }
//                //}
//
//              }
//          }
//      $fetched_data['products']= $productname;
//      $fetched_data['productlists']=$this->crud->getAllBy('productvariation',$cond)?$this->crud->getAllBy('productvariation',$cond):array();
//      $fetched_data['productsoftware']=$this->crud->getAllBy('productSW',$cond)?$this->crud->getAllBy('productSW',$cond):array();
//      $fetched_data['productmenul']=$this->crud->getAllBy('productManual',$cond)?$this->crud->getAllBy('productManual',$cond):array();
//      $fetched_data['productdriver']=$this->crud->getAllBy('ProductDriver',$cond)?$this->crud->getAllBy('ProductDriver',$cond):array();
//       $heade_data=array('page_title'=>"View|Add Product");
//       $active_pages=array('active_class'=>'product','active_sub_class'=>'view_product');
//      $this->load->view('admin/dashboard/header',$heade_data);
//      $this->load->view('admin/dashboard/navigation',$active_pages);
//      $this->load->view('admin/dashboard/view_product_variation',$fetched_data);
//      $this->load->view('admin/dashboard/footer');
//      $this->load->view('admin/dashboard/ajax');
// }
//
//    function add_manuals(){
//
//    $data_to_insert=$this->input->post();
//
//    if(isset($data_to_insert['save_manuals'])){
//
//
//$config['upload_path']          = "./assets/images/";
//                $config['allowed_types']        = 'application/pdf|pdf|application/octet-stream|csv';
//               /* $config['max_size']             = 100;
//                $config['max_width']            = 1024;
//                $config['max_height']           = 768;*/
//
//                $this->load->library('upload', $config);
//
//                if ( ! $this->upload->do_upload('file'))
//                {
//                        $error = array('error' => $this->upload->display_errors());
//                        $this->session->set_flashdata('notification',$error);
//                        redirect('admin/dashboard/add_manuals');
//                }
//                else
//                {
//                    $this->load->helper('file');
//                    $check_data=array('id'=>$this->session->userdata('user_id'));
//                    $result=$this->crud->getAllBy('users',$check_data)[0];
//
//                        $data = array('upload_data' => $this->upload->data());
//                        $data_to_insert['File']=$data['upload_data']['file_name'];
//
//        unset($data_to_insert['save_manuals']);
//          //$data_to_insert['user_id']=$this->session->userdata('user_id');
//          if($this->crud->insert('manuals',$data_to_insert)){
//            $this->session->set_flashdata('notification','Inseted Successfully!');
//            redirect('admin/dashboard/add_manuals');
//          }
//      //}
//
//    }
//}
//    $fetched_data=array();
//    $heade_data=array('page_title'=>"Add Manuals");
//    $active_pages=array('active_class'=>'manuals','active_sub_class'=>'add_manuals');
//    $this->load->view('admin/dashboard/header',$heade_data);
//    $this->load->view('admin/dashboard/navigation',$active_pages);
//    $this->load->view('admin/dashboard/add_manuals',$fetched_data);
//    $this->load->view('admin/dashboard/footer');
//  }
//    public function view_manuals(){
//               // $user=array('user_id'=>$this->session->userdata('user_id'));
//    $fetched_data['manuals']=$this->crud->getAll('manuals')?$this->crud->getAll('manuals'):array();
//    $heade_data=array('page_title'=>"Bulletins");
//    $active_pages=array('active_class'=>'manuals','active_sub_class'=>'view_manuals');
//    $this->load->view('admin/dashboard/header',$heade_data);
//    $this->load->view('admin/dashboard/navigation',$active_pages);
//    $this->load->view('admin/dashboard/view_manuals',$fetched_data);
//    $this->load->view('admin/dashboard/footer');
//    $this->load->view('admin/dashboard/ajax');
//     }

    public function view_user(){
        $user_type =  $this->uri->segment(4);
        if (isset($user_type) && is_numeric($user_type)) {
            $fetched_data['user']=$this->crud->getuser('client',$user_type);
            if (!$fetched_data['user'])
            {
                $fetched_data['user']=400;
            }

            if ($user_type==1)
            {
                $heade_data=array('page_title'=>"Alberta");
                $active_pages=array('active_class'=>'Alberta','active_sub_class'=>'Alberta_User');
            }elseif ($user_type==2)
            {
                $heade_data=array('page_title'=>"Florida");
                $active_pages=array('active_class'=>'Florida','active_sub_class'=>'Florida_User');
            }elseif ($user_type==3)
            {
                $heade_data=array('page_title'=>"CalFire");
                $active_pages=array('active_class'=>'CalFire','active_sub_class'=>'CalFire_User');
            }elseif ($user_type==4)
            {
                $heade_data=array('page_title'=>"Usfs");
                $active_pages=array('active_class'=>'Usfs','active_sub_class'=>'Usfs_User');
            }else{
                $heade_data=array('page_title'=>"Dashboard");
                $this->session->set_flashdata('error','Invalid id provided');
                redirect(base_url('admin/dashboard'));
//                $active_pages=array('active_class'=>'dashboard');
            }

            $this->load->view('admin/dashboard/header',$heade_data);
            $this->load->view('admin/dashboard/navigation',$active_pages);
            $this->load->view('admin/dashboard/userlist',$fetched_data);
            $this->load->view('admin/dashboard/footer');
            $this->load->view('admin/dashboard/ajax');

        }else
            {
                $this->session->set_flashdata('error','Invalid Argument provided');
                redirect(base_url('admin/dashboard'));
            }
     }


function add_Calfire(){
    
    $data_to_insert=$this->input->post();

    if(isset($data_to_insert['save_manuals'])){
      

$config['upload_path'] = "./assets/images/";
                $config['allowed_types']        = '*';
               
                $this->load->library('upload', $config);

                if ( ! $this->upload->do_upload('file'))
                {
                        $error = array('error' => $this->upload->display_errors());                        
                        $this->session->set_flashdata('notification',$error);
                        redirect('admin/dashboard/add_manuals');
                }
                else
                {
                    $this->load->helper('file');
                    $check_data=array('id'=>$this->session->userdata('user_id'));
                    $result=$this->crud->getAllBy('users',$check_data)[0];
                    
                        $data = array('upload_data' => $this->upload->data());                   
                        $data_to_insert['file']=$data['upload_data']['file_name'];

        unset($data_to_insert['save_manuals']);
          //$data_to_insert['user_id']=$this->session->userdata('user_id');
          if($this->crud->insert('calFireRestrickt',$data_to_insert)){
            $this->session->set_flashdata('notification','Inseted Successfully!');
            redirect('admin/dashboard/add_Calfire');
          }     
      //}
    
    }
}
    $fetched_data=array();    
    $heade_data=array('page_title'=>"Add Calfire");
    $active_pages=array('active_class'=>'Calfire','active_sub_class'=>'add_Calfire');
    $this->load->view('admin/dashboard/header',$heade_data);
    $this->load->view('admin/dashboard/navigation',$active_pages);
    $this->load->view('admin/dashboard/add_Calfire',$fetched_data);
    $this->load->view('admin/dashboard/footer');
  }
//
public function view_Calfire(){
               // $user=array('user_id'=>$this->session->userdata('user_id'));
    $fetched_data['manuals']=$this->crud->getAll('calFireRestrickt')?$this->crud->getAll('calFireRestrickt'):array();
    $heade_data=array('page_title'=>"Calfire");
    $active_pages=array('active_class'=>'Calfire','active_sub_class'=>'view_Calfire');
    $this->load->view('admin/dashboard/header',$heade_data);
    $this->load->view('admin/dashboard/navigation',$active_pages);
    $this->load->view('admin/dashboard/view_Calfire',$fetched_data);
    $this->load->view('admin/dashboard/footer');
    $this->load->view('admin/dashboard/ajax');
 }


  function delete(){
    $date_to_delete=$this->input->post();
    if($this->crud->delete($date_to_delete)){
      echo 1;
    }else{ 
      echo 0;
    }
  }

  public function view_codeplug(){
    $fetched_data['user']=$this->crud->getAll('codeplug')?$this->crud->getAll('codeplug'):array();
    $heade_data=array('page_title'=>"codeplug");
    $active_pages=array('active_class'=>'codeplug','active_sub_class'=>'');
    $this->load->view('admin/dashboard/header',$heade_data);
    $this->load->view('admin/dashboard/navigation',$active_pages);
    $this->load->view('admin/dashboard/view_codeplug',$fetched_data);
    $this->load->view('admin/dashboard/footer');
    $this->load->view('admin/dashboard/ajax');
     }

  public function view_Esn_spread_sheet(){
    $fetched_data['user']=$this->crud->getAll('Spreedsheet')?$this->crud->getAll('Spreedsheet'):array();
    $heade_data=array('page_title'=>"Spreedsheet");
    $active_pages=array('active_class'=>'Spreedsheet','active_sub_class'=>'');
    $this->load->view('admin/dashboard/header',$heade_data);
    $this->load->view('admin/dashboard/navigation',$active_pages);
    $this->load->view('admin/dashboard/view_Esn_spread_sheet',$fetched_data);
    $this->load->view('admin/dashboard/footer');
    $this->load->view('admin/dashboard/ajax');
     }

  function change_status(){

      $date_to_delete=$this->input->post();

      if (isset($date_to_delete['id'])){

         $res = $this->crud->getthis('client',"id = ".$this->input->post('id')); //returns objects

          if($this->crud->change_status($date_to_delete)){

              $check=1;
              if($date_to_delete['status']==0)
              {
                  if ($res->access_level==1)
                  {
                      $portal= "Alberta";
                  }elseif($res->access_level==2)
                  {
                      $portal= "Florida Forestry";
                  }elseif($res->access_level==4)
                  {
                      $portal= "United States Forest Service";
                  }else{
                      $portal= "CalFire";
                  }
                  $RES = $this->sendmail($res->name,$res->email,$portal);

              }

          }else{
              $check=0;
          }
          echo $check;
      }else{
          debug('404');
      }

  }
  function change_acc_type(){

        $post_data=$this->input->post();

        if (isset($post_data['id'])){

            if($this->crud->change_acc_type($post_data)){

                $check=1;
            }else{
                $check=0;
            }
            echo $check;
        }else{
            debug('404');
        }

    }

    private function sendmail($name,$email,$portal)
    {
        $this->load->helper('functions_helper');
        $htmlContent = "Hi $name<br>Your account for $portal has been approved by admin.<br>Feel Free to Ask any question.<br>Thanks for using $portal<br>Thanks.<br>";
        sendemail($email,'service@bktechnologies.com',$htmlContent,'Account Creation Success Message');

//        $this->load->library('email');
//        $this->email->from('service@bktechnologies.com',"BK Technologies");
//        $this->email->to($email);
//        $this->email->subject('Account Creation Success Message');
//        $this->email->message($htmlContent);
//        $this->email->send();
    }


    function theme_setting(){
    $data_to_insert=$this->input->post();


      if(isset($data_to_insert['save_options'])){
        $flag_insert=1;
        $flag_update=1;   
        unset($data_to_insert['save_options']);
        
        foreach ($data_to_insert as $key => $value) {   
          $check_data=array('user_id'=>$this->session->userdata('user_id'),
              'key'=>$key
            );
          
              if($this->crud->validate('options',$check_data)){
                $option_to_save=array('value'=>$value);
                if($this->crud->update('options',$option_to_save,$check_data)){   
                    
                }else{
                  $flag_update=0;
                }
                
              }else{
                $option_to_save=array('key'=>$key,'value'=>$value);
                $option_to_save['user_id']=$this->session->userdata('user_id');
                if($this->crud->insert('options',$option_to_save)){   
                  
                }else{
                  $flag_insert=0;
                }

              }
            
        }

        if($flag_update==1){
          $this->session->set_flashdata('notification','Updated Successfully!');
        }else{
          $this->session->set_flashdata('notification','Updation Failed!');
        }
        if($flag_insert==1){
          $this->session->set_flashdata('notification','Inseted Successfully!');
        }else{
          $this->session->set_flashdata('notification','Insertion Failed!');
        }

        
      
    
    }

    $user=array('user_id'=>$this->session->userdata('user_id'));
    $data=$this->crud->getAllBy('options',$user);
    $fetched_data['user_theme_data']=$data?$data:array();
    $heade_data=array('page_title'=>"Theme Settings");
    $active_pages=array('active_class'=>'theme_setting','active_sub_class'=>'theme_setting');
    $this->load->view('admin/dashboard/header',$heade_data);
    $this->load->view('admin/dashboard/navigation',$active_pages);
    $this->load->view('admin/dashboard/theme-setting',$fetched_data);
    $this->load->view('admin/dashboard/footer');
  }

  function register_user()
  {
//      $heade_data=array('page_title'=>"Add new User");
//      $this->load->view('admin/dashboard/header',$heade_data);
      $this->load->view('layouts/header', array("title" => "Sign Up"));
      $this->load->view('users/signup');
      $this->load->view('layouts/footer');
//      $this->load->view('admin/dashboard/footer');
//      $this->load->view('admin/dashboard/ajax');
  }


  //rana changes
    //all repair profiles for admin uses uri param to differentiate b/w calfire and florida
    public function showall(){

        $type=$this->uri->segment(4);
        if (isset($type) && is_numeric($type)) {

            if ($type==2) //florida
            {
                $this->type=2;
                $heade_data=array('page_title'=>"Florida Repair Profiles");
                $table='florida';
                $active_pages=array('active_class'=>'Florida','active_sub_class'=>'flo_dashboard');
            }elseif ($type==4) {//calfire
                $this->type=4;
                $heade_data=array('page_title'=>"United States Forest Service Repair Profiles");
                $table='usfs';
                $active_pages=array('active_class'=>'Usfs','active_sub_class'=>'Usfs_dashboard');
            }elseif ($type==3) {//calfire
                $this->type=3;
                $heade_data=array('page_title'=>"CalFire Repair Profiles");
                $table='calfire';
                $active_pages=array('active_class'=>'Calfire','active_sub_class'=>'cal_dashboard');
            }else{
                    $heade_data=array('page_title'=>"Dashboard");
                    $this->session->set_flashdata('error','Invalid id provided');
                    redirect(base_url('admin/dashboard'));
            }

            $con=array('status'=>'0');
            $fetched_data=array();
            $fetched_data['list']=$this->crud->getAllBy($table,$con);
            if (!$fetched_data['list'])
            {
                $fetched_data['list']=400;
            }
            $this->load->view('admin/dashboard/header',$heade_data);//sets page title
            $this->load->view('admin/dashboard/navigation',$active_pages);//sets the side nav active page
//            $this->load->view('admin/dashboard/userlist',$fetched_data); //loads data content file
            $this->load->view('admin/dashboard/repair_profiles_data',$fetched_data); //loads data content file
            $this->load->view('admin/dashboard/footer'); //footer
            $this->load->view('admin/dashboard/ajax');

//            $this->load->view("common_pages/florida/main",$fetched_data);
        }else{
            $this->session->set_flashdata('error','Argument is non numeric');
            redirect(base_url('admin/dashboard'));
        }
    }


    public function edit(){ //edit a profile record
        $type=$this->uri->segment(5);
        $id=$this->uri->segment(4);
        if (isset($type) && is_numeric($type) &&(isset($id) && is_numeric($id))){

            if ($type==2) //florida
            {
                $this->type=2;
                $heade_data=array('page_title'=>"Florida Repair Profiles");
                $table='florida';
                $active_pages=array('active_class'=>'Florida','active_sub_class'=>'flo_dashboard');
            }elseif ($type==3) {//calfire
                $this->type=3;
                $heade_data=array('page_title'=>"CalFire Repair Profiles");
                $table='calfire';
                $active_pages=array('active_class'=>'Calfire','active_sub_class'=>'cal_dashboard');
            }elseif ($type==4) {//calfire
                $this->type=4;
                $heade_data=array('page_title'=>"United States Forest Service Repair Profiles");
                $table='usfs';
                $active_pages=array('active_class'=>'Usfs','active_sub_class'=>'Usfs_dashboard');
            }else{
                $heade_data=array('page_title'=>"Dashboard");
                $this->session->set_flashdata('error','Invalid id provided');
                redirect(base_url('admin/dashboard'));
            }

            $fetched_data=array();
                $con=array('status'=>'0','id'=>$id);
                $fetched_data['list']=$this->crud->getAllBy($table,$con);
                if (empty($fetched_data['list']))
                {
                    $fetched_data['list1'] = 400; //no record found
                }
                $this->load->view('admin/dashboard/header',$heade_data);//sets page title
                $this->load->view('admin/dashboard/navigation',$active_pages);//sets the side nav active page
                $this->load->view('admin/dashboard/edit_repair_profile_data',$fetched_data); //loads data content file
                $this->load->view('admin/dashboard/footer'); //footer
                $this->load->view('admin/dashboard/ajax');
        }else
        {
            $this->session->set_flashdata('error','Invalid id provided');
            redirect(base_url('admin/dashboard'));
        }
    }

    public function update(){ //update request

        $POST=$this->input->post();
        $type=$POST['table_id'];

        if (isset($type) && is_numeric($type)) {

            if ($type==2) //florida
            {
                $heade_data=array('page_title'=>"Florida Repair Profiles");
                $table='florida';
                $active_pages=array('active_class'=>'Florida','active_sub_class'=>'flo_dashboard');
            }elseif ($type==3) {//calfire
                $heade_data=array('page_title'=>"CalFire Repair Profiles");
                $table='calfire';
                $active_pages=array('active_class'=>'Calfire','active_sub_class'=>'cal_dashboard');
            }elseif ($type==4) {//calfire
                $heade_data=array('page_title'=>"United States Forest Service Repair Profiles");
                $table='usfs';
                $active_pages=array('active_class'=>'Usfs','active_sub_class'=>'Usfs_dashboard');
            }else{
                $heade_data=array('page_title'=>"Dashboard");
                $this->session->set_flashdata('error','Invalid id provided');
                redirect(base_url('admin/dashboard'));
            }

            if (isset($POST['update'])) {

                $this->load->library("form_validation");

                $this->form_validation->set_rules('pu', 'Po#', 'required|trim');
                $this->form_validation->set_rules('requested', 'Requested', 'required|trim');
                $this->form_validation->set_rules('Company', 'Company', 'required|trim');
                $this->form_validation->set_rules('CustomerTrackingNumber', 'CustomerTrackingNumber', 'required|trim');
                $this->form_validation->set_rules('Model', 'Model', 'required|trim');
                $this->form_validation->set_rules('WarrantyStatus', 'Warrenty', 'required|trim');
                $this->form_validation->set_rules('Qty', 'Quantity', 'required|trim');
                $this->form_validation->set_rules('Serial', 'Serial Number', 'required|trim');

                $this->form_validation->set_rules('address', 'Address', 'required|trim');
                $this->form_validation->set_rules('city', 'City', 'required|trim');
                $this->form_validation->set_rules('zip', 'Zip Code', 'required|trim');
                $this->form_validation->set_rules('pnum', 'Phone Number', 'required|trim');
                $this->form_validation->set_rules('email', 'email', 'required|trim');
                $this->form_validation->set_rules('person_name', 'Contact Person Name', 'required|trim');

                if ($this->form_validation->run() == false) {
                    $this->session->set_flashdata("validation_errors", validation_errors());
                    redirect(base_url('admin/dashboard/edit/').$POST['id'].'/'.$type);
                } else {

                    $where= array('id'=>$POST['id']);
                    unset($POST['update']);
                    unset($POST['table_id']);
                    unset($POST['id']);
                    if ($this->crud->update($table,$POST,$where))
                    {
                        $this->session->set_flashdata("success","Record Successfully Updated");
                        redirect(base_url('admin/dashboard/showall/').$type);
                    }else
                    {
                        $this->session->set_flashdata("error","Record Updation Failed");
                        redirect(base_url('admin/dashboard/showall/').$type);
                    }
                }
            }else{
                $this->session->set_flashdata('error','Invalid Request Received');
                redirect(base_url('admin/dashboard'));
            }
        }else
            {
                $this->session->set_flashdata('error','Argument is non numeric');
                redirect(base_url('admin/dashboard'));
            }
    }

    public function deletee(){

        $post=$this->input->post();
        if (isset($post['id']) && isset($post['table'])){

            $con=array('id'=>$post['id']);
            if ($post['table']==3)
            {
                $table='calfire';
            }elseif($post['table']==2){
                $table='florida';
            }elseif($post['table']==4){
                $table='usfs';
            }
            if($this->crud->delete1($table,$con)){
                echo 1;
            }else{
                echo 0;
            }

        }else {
            echo 0;
        }
    }
    
        public function downloads()
    {
        // $fetched_data['user'] = $this->crud->get_downloads();
        $fetched_data['user'] = $this->crud->getRma('user_downloads', '*', 'id', 'desc');
        $heade_data = array('page_title' => "Downloads");
        $active_pages = array('active_class' => 'Downloads', 'active_sub_class' => '');

        // echo '<pre>';print_r($fetched_data);exit;
        $this->load->view('admin/dashboard/header', $heade_data);
        $this->load->view('admin/dashboard/navigation', $active_pages);
        $this->load->view('admin/dashboard/downloads', $fetched_data);
        $this->load->view('admin/dashboard/footer');
        $this->load->view('admin/dashboard/ajax');
    }

    public function serialReg()
    {
        $fetched_data['user'] = $this->crud->getRma('kaa0733registration', '*', 'id', 'desc');
        $heade_data = array('page_title' => "Serial Registration");
        $active_pages = array('active_class' => 'Serial Registration', 'active_sub_class' => '');

        $this->load->view('admin/dashboard/header', $heade_data);
        $this->load->view('admin/dashboard/navigation', $active_pages);
        $this->load->view('admin/dashboard/serialReg', $fetched_data);
        $this->load->view('admin/dashboard/footer');
        $this->load->view('admin/dashboard/ajax');
    }

    function rma_form()
    {

        $con=array();
        $fetched_data=array();
        // $fetched_data['user']=$this->crud->getAllBy('rtn_mcd_auth_data',$con);
        $fetched_data['user']=$this->crud->getRma('rtn_mcd_auth_data','*','id','desc');

        $heade_data=array('page_title'=>"RMA Listing");
        $active_pages=array('active_class'=>'RMA','active_sub_class'=>'');

        $this->load->view('admin/dashboard/header',$heade_data);
        $this->load->view('admin/dashboard/navigation',$active_pages);
        $this->load->view('admin/dashboard/RMA-listing',$fetched_data);
        $this->load->view('admin/dashboard/footer');
        $this->load->view('admin/dashboard/ajax');

    }
    
    
    function rma_form_view($id='')
    {

        //verify here whether record exists or not using id
        $table = 'rtn_mcd_auth_data';
        if ($id!='')
        {
            $con=array('id'=>$id);
            $fetched_data['list']=$this->crud->getAllBy($table,$con);
            if (!empty($fetched_data['list']))
            {

                $fetched_data['list'][0]->quantity = json_decode($fetched_data['list'][0]->quantity);
                $fetched_data['list'][0]->part_number = json_decode($fetched_data['list'][0]->part_number);
                $fetched_data['list'][0]->invoice = json_decode($fetched_data['list'][0]->invoice);
                $fetched_data['list'][0]->price = json_decode($fetched_data['list'][0]->price);
                $fetched_data['list'][0]->problem = json_decode($fetched_data['list'][0]->problem);
                $fetched_data['list'][0]->quantity1 = json_decode($fetched_data['list'][0]->quantity1);
                $fetched_data['list'][0]->part_number1 = json_decode($fetched_data['list'][0]->part_number1);
                $fetched_data['list'][0]->invoice1 = json_decode($fetched_data['list'][0]->invoice1);
                $fetched_data['list'][0]->price1 = json_decode($fetched_data['list'][0]->price1);

                $heade_data=array('page_title'=>"RMA Listing");
                $active_pages=array('active_class'=>'RMA','active_sub_class'=>'');
                $this->load->view('admin/dashboard/header',$heade_data);
                $this->load->view('admin/dashboard/navigation',$active_pages);
                $this->load->view('admin/dashboard/RMA-form',$fetched_data);
                $this->load->view('admin/dashboard/footer');
                $this->load->view('admin/dashboard/ajax');
            }else
            {
                $this->session->set_flashdata('error','No Record Found');
                redirect(base_url('admin/dashboard/rma_form'));
            }
        }else
        {
            $this->session->set_flashdata('error','Invalid request');
            redirect(base_url('admin/dashboard'));
        }
    }

}
