<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 *
 */
class Crud extends CI_Model
{


    function __construct()
    {
        parent::__construct();


    }

    public function get_table_name($id)
    {
        $table = array(
            '1' => 'bulletins',
            '2' => 'Kaa0732-33',
            '3' => 'partsType',
            '4' => 'parts',
            '5' => 'partsList',
            '6' => 'productvariation',
            '7' => 'productSW',
            '8' => 'productManual',
            '9' => 'ProductDriver',
            '10' => 'manuals',
            '11' => 'usersold',
            '12' => 'codeplug',
            '13' => 'Spreedsheet',
            '15' => 'calFireRestrickt',
            '16' => 'client',
            '17' => 'download_prods',
            '18' => 'dp_item_list',
            '19' => 'rtn_mcd_auth_data'

        );
        return $table[$id];
    }

    function login($table, $data)
    {
//		echo "<pre>";print_r($data);echo "</pre>";die;
        $res = $this->db->get_where($table, $data);
        if ($res->num_rows() > 0)
            return $res->result();
        else return false;
    }

    function insert($table, $data)
    {

        $this->db->insert($table, $data);
        if ($this->db->affected_rows() > 0) {
            return true;
        }
        return false;
    }

    public function get_downloads()
    {
        $res = $this->db->query("SELECT  *,(SELECT count(*) as tot from user_downloads ud1 where ud.email = ud1.email and ud.ware = ud1.ware  GROUP by email,ware) total_download from user_downloads ud GROUP by email,ware");
        return $res->result();
    }


    function update($table, $data, $where)
    {
        $this->db->where($where);
        if ($this->db->update($table, $data)) {
            return true;
        }
        return false;
    }

    function validate($table, $data)
    {
        $res = $this->db->get_where($table, $data);
        if ($res->num_rows() > 0) {
            return $res->result();
        }
        return false;
    }
    
        public function save($table, $data)
    {
        $this->db->insert($table, $data);
        return $this->db->insert_id();
    }

    function getAllBy($table, $data, $order = null)
    {
        if ($order != null) {
            foreach ($order as $key => $value) {
                $this->db->order_by($key, $value);
            }

        }
        $res = $this->db->get_where($table, $data);
        if ($res->num_rows() > 0) {
            return $res->result();
        }
        return false;
    }


    public function getRma($table, $select = '', $order_by = '', $order = '')
    {
        if ($select != '') {
            $this->db->select($select);
        }
        if ($order_by != '') {
            $this->db->order_by($order_by, $order);
        }
        return $this->db->get($table)->result();
    }


    function getarchived($table, $data, $order = null)
    { //rana
        if ($order != null) {
            foreach ($order as $key => $value) {
                $this->db->order_by($key, $value);
            }

        }
        $res = $this->db->query("SELECT * FROM `bulletins` WHERE Type=2 and status =0 and Release_Date not BETWEEN '2016-01-01' AND '2018-12-31'");
        if ($res->num_rows() > 0) {
            return $res->result();
        }
        return false;
    }

    function getbetween($table, $data, $order = null)
    { //rana
        if ($order != null) {
            foreach ($order as $key => $value) {
                $this->db->order_by($key, $value);
            }

        }
        $res = $this->db->query("SELECT * FROM `bulletins` WHERE Type=2 and status =0 and Release_Date BETWEEN '2016-01-01' AND '2019-12-31' ORDER BY Release_Date DESC");
        if ($res->num_rows() > 0) {
            return $res->result();
        }
        return false;
    }


    function getAllLike($table, $data, $order = null)
    {
        if ($order != null) {
            foreach ($order as $key => $value) {
                $this->db->order_by($key, $value);
            }

        }
        $this->db->like($data);
        $res = $this->db->get($table);
        if ($res->num_rows() > 0) {
            return $res->result();
        }
        return array();
    }

    function getAll($table)
    {
        $res = $this->db->get_where($table);
        if ($res->num_rows() > 0) {
            return $res->result();
        }
        return false;
    }

    function getthis($table, $cond)
    { //cond receives string conditions i.e id = 1 and name = 'hammad' etc
        $query = "select * from $table where $cond";
        $res = $this->db->query($query);
        if ($res->num_rows() > 0) {
            return $res->row();
        }
        return false;
    }


    function getuser($table, $type)
    { //rana

        $query = "select * from $table where access_level=$type";
        $res = $this->db->query($query);
        if ($res->num_rows() > 0) {
            return $res->result();
        }
        return false;
    }

    function getAllStaff($table, $type)
    { //rana

        $query = "select email from $table where access_level=$type and account_type=2";
        $res = $this->db->query($query);
        if ($res->num_rows() > 0) {
            return $res->result_array();
        }
        return false;
    }


    public function selectusingjoin($table1, $table2, $condition, $selected)
    {

        $this->db->select($selected);
        $this->db->from($table1);
        $this->db->join($table2, $condition);
        $query = $this->db->get();
        $data = $query->result();
        return $data;
    }


    function delete($data)
    {

        $table = $data['table'];
        $id = $data['id'];
        $table = $this->get_table_name($table);

        if ($this->db->delete($table, array('id' => $id))) {
            return true;
        } else {
            return false;
        }
    }

    function delete1($data, $where)
    { //(rana) delete1 is variation receives table name and deletes based on condition (returns true or false)

        if ($this->db->delete($data, $where)) {
            return true;
        } else {
            return false;
        }
    }


    function change_status($data)
    {

        $table = $data['table'];
        $id = $data['id'];
        $status = $data['status'];
        $table = $this->get_table_name($table);
        if ($status == 1) {
            $status = 0;
        } else {
            $status = 1;
        }
        $data = array('status' => $status);
        if ($this->update($table, $data, array('id' => $id))) {
            return true;
        } else {
            return false;
        }

    }

    function change_acc_type($data)
    {

        $table = $data['table'];
        $id = $data['id'];
        $acc_type = $data['acc_type'];
        $table = $this->get_table_name($table);
        $data = array('account_type' => $acc_type);
        if ($this->update($table, $data, array('id' => $id))) {
            return true;
        } else {
            return false;
        }

    }

    function countAll($table)
    {
        return $this->db->count_all($table);
    }

    public function get_current_page_records($limit, $start, $table, $con)
    {

        $this->db->limit($limit, $start);
        //$this->db->where($con);
        $query = $this->db->get_where($table, $con);

        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }

            return $data;
        }

        return false;
    }

    function getAllcp()
    {
        $res = $this->db->query('select cp.*,c.name from codeplug cp left join client c on cp.userid = c.id order by cp.date desc');
        if ($res->num_rows() > 0) {
            return $res->result();
        }
        return false;
    }


}