<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Users extends CI_Model
{
    protected $_table = 'usersold';

    public function addUser($data)
    {
        $result = $this->db->insert($this->_table, $data);
        return $result;
    }

    public function findUserByUsername($username){
        $this->db->where('username', $username);
        $result = $this->db->get($this->_table)->row();
        return $result;
    }

    public function addclient($data)
    {
        $result = $this->db->insert('client', $data);
        return $result;
    }

    public function findclient($where){
        $result = $this->db->get_where('client',$where);
        return $result->row(); //return objects
    }


}